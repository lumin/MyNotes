<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="{M,R,D}L" FOLDED="false" ID="ID_806386173" CREATED="1641242834029" MODIFIED="1642394353464" STYLE="oval">
<font NAME="Gentium" SIZE="18"/>
<hook NAME="MapStyle" zoom="1.558">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000">
<font NAME="Gentium" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Gentium" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Gentium" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Gentium" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="111" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Meta" LOCALIZED_STYLE_REF="styles.topic" POSITION="right" ID="ID_897570916" CREATED="1641520773367" MODIFIED="1648337505775">
<icon BUILTIN="info"/>
<edge COLOR="#00ffff"/>
<node TEXT="what&apos;s this" ID="ID_34209772" CREATED="1641520840738" MODIFIED="1641520995587">
<font NAME="Gentium"/>
<node TEXT="Personal notes for regular paper reading." ID="ID_1963538377" CREATED="1641520844766" MODIFIED="1641520995588">
<font NAME="Gentium"/>
</node>
<node TEXT="starting from scratch again." ID="ID_531358300" CREATED="1641520859492" MODIFIED="1641520995589">
<font NAME="Gentium"/>
</node>
<node TEXT="Copyright (C) 2021-2022 Mo Zhou" ID="ID_1535888258" CREATED="1641673010735" MODIFIED="1641673016816"/>
<node TEXT="CC-BY-SA-4.0 License" ID="ID_726858210" CREATED="1641673017130" MODIFIED="1641673022764"/>
</node>
<node TEXT="Policy for this document" ID="ID_288232758" CREATED="1641520783594" MODIFIED="1641520995590">
<font NAME="Gentium"/>
<node TEXT="1. may include preprints, but only include them when I think they are relevant or helpful." ID="ID_608512408" CREATED="1641520800532" MODIFIED="1641520995590">
<font NAME="Gentium"/>
</node>
<node TEXT="2. gradually migrate the notes from the private mm&apos;s." ID="ID_1159004725" CREATED="1641520874706" MODIFIED="1641520995596">
<font NAME="Gentium"/>
</node>
</node>
<node TEXT="symbol definitions" ID="ID_107155097" CREATED="1641521217140" MODIFIED="1641521224034">
<node TEXT=" I have read this paper (published)" ID="ID_1116629927" CREATED="1641521225706" MODIFIED="1641524141636">
<icon BUILTIN="checked"/>
</node>
<node TEXT=" I have skimmed this paper (preprint)" ID="ID_504737720" CREATED="1641523236144" MODIFIED="1641679124582">
<icon BUILTIN="pencil"/>
</node>
<node TEXT=" this paper is a TODO" ID="ID_1381101491" CREATED="1641521235679" MODIFIED="1641521246931">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT=" in doubt in details" ID="ID_420094020" CREATED="1641521248004" MODIFIED="1641521262966">
<icon BUILTIN="help"/>
</node>
<node TEXT=" insight" ID="ID_188358894" CREATED="1642449409015" MODIFIED="1642449417941">
<icon BUILTIN="yes"/>
</node>
<node TEXT=" Very good+Important paper" ID="ID_78304803" CREATED="1641521263664" MODIFIED="1641679080458">
<icon BUILTIN="bookmark"/>
</node>
<node TEXT=" Consider for removal from this map" ID="ID_1712139759" CREATED="1641522205670" MODIFIED="1641679143123">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="There is problem / Problematic" ID="ID_337495690" CREATED="1642431277318" MODIFIED="1642431293651">
<icon BUILTIN="closed"/>
</node>
<node TEXT=" Paper Tag" ID="ID_961819152" CREATED="1641524318246" MODIFIED="1641524325129">
<icon BUILTIN="password"/>
</node>
<node TEXT="My Thoughts" ID="ID_1201449199" CREATED="1641743757646" MODIFIED="1642368052082">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="browse history" ID="ID_1769952352" CREATED="1641521397167" MODIFIED="1641521457619">
<node TEXT="arXiv" ID="ID_555001851" CREATED="1641521435482" MODIFIED="1642954134722">
<node TEXT="2022" OBJECT="java.lang.Long|2022" ID="ID_1490488610" CREATED="1641521399876" MODIFIED="1641521403819">
<node TEXT="Weekly batch" ID="ID_1917473385" CREATED="1642949144502" MODIFIED="1642949169400">
<node TEXT="apr 23 ok" ID="ID_800380579" CREATED="1650825804133" MODIFIED="1650825807379"/>
</node>
</node>
</node>
<node TEXT="CVPR" ID="ID_1240053621" CREATED="1641521439738" MODIFIED="1642954136648">
<node TEXT="2022" OBJECT="java.lang.Long|2022" ID="ID_1290012760" CREATED="1646924932979" MODIFIED="1646924934382">
<node TEXT="TODO" ID="ID_726647901" CREATED="1646924935622" MODIFIED="1646924937856"/>
</node>
<node TEXT="2021" OBJECT="java.lang.Long|2021" ID="ID_1246146016" CREATED="1642954104588" MODIFIED="1642954106947">
<node TEXT="TODO" ID="ID_609674727" CREATED="1642954107744" MODIFIED="1642954109015"/>
</node>
</node>
<node TEXT="ICCV" ID="ID_436661845" CREATED="1641521441682" MODIFIED="1642954138589">
<node TEXT="2021" OBJECT="java.lang.Long|2021" ID="ID_95518471" CREATED="1641929159364" MODIFIED="1641929161060">
<node TEXT="scanned." ID="ID_1641147403" CREATED="1641929162150" MODIFIED="1642954158598">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="list stored in chrome, WIP" ID="ID_23203732" CREATED="1641929165306" MODIFIED="1642954165230"/>
</node>
<node TEXT="2019" OBJECT="java.lang.Long|2019" ID="ID_886460557" CREATED="1646924942307" MODIFIED="1646924943938">
<node TEXT="TODO" ID="ID_443194080" CREATED="1646924948431" MODIFIED="1646924949472"/>
</node>
</node>
<node TEXT="ECCV" ID="ID_536802877" CREATED="1641521443344" MODIFIED="1642954140158">
<node TEXT="2020" OBJECT="java.lang.Long|2020" ID="ID_314203344" CREATED="1642954114529" MODIFIED="1642954115862">
<node TEXT="TODO" ID="ID_1908132768" CREATED="1642954116649" MODIFIED="1642954117576"/>
</node>
<node TEXT="2018" OBJECT="java.lang.Long|2018" ID="ID_1513907030" CREATED="1646924952108" MODIFIED="1646924953701">
<node TEXT="TODO" ID="ID_388311225" CREATED="1646924954493" MODIFIED="1646924958076"/>
</node>
</node>
<node TEXT="NeurIPS" ID="ID_301924831" CREATED="1641521444597" MODIFIED="1642954144776"/>
<node TEXT="ICLR" ID="ID_1738325130" CREATED="1641521447460" MODIFIED="1642954147510">
<node TEXT="2022" OBJECT="java.lang.Long|2022" ID="ID_814285036" CREATED="1650825816095" MODIFIED="1650825819933">
<node TEXT="TODO" ID="ID_944733876" CREATED="1650825820715" MODIFIED="1650825821778"/>
</node>
</node>
<node TEXT="AAAI" ID="ID_583291215" CREATED="1641521448752" MODIFIED="1642954149324"/>
<node TEXT="ICML" ID="ID_8273996" CREATED="1642954123777" MODIFIED="1642954125427"/>
<node TEXT="ACL" ID="ID_126154590" CREATED="1642954126358" MODIFIED="1642954130606"/>
<node TEXT="researchers of interest" ID="ID_855888271" CREATED="1641929219444" MODIFIED="1641929229750">
<node TEXT="Alexandar Madry" ID="ID_303512197" CREATED="1641929236490" MODIFIED="1648335746889"/>
<node TEXT="Alan Yuille" ID="ID_362725284" CREATED="1641929240578" MODIFIED="1648335761283"/>
<node TEXT="Jun Zhu" ID="ID_586060874" CREATED="1643133067259" MODIFIED="1648335764547"/>
<node TEXT="Nicolas Carlini" ID="ID_323304377" CREATED="1643133070053" MODIFIED="1648335753367"/>
<node TEXT="Ian Goodfellow" ID="ID_1353458379" CREATED="1648335753716" MODIFIED="1648335756288"/>
</node>
</node>
</node>
<node TEXT="Misc: Surveys" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_1805920002" CREATED="1608010101067" MODIFIED="1648337561524">
<icon BUILTIN="mindmap"/>
<edge COLOR="#ff0000"/>
<node TEXT="Deep Learning Book" ID="ID_1914998592" CREATED="1624456582801" MODIFIED="1624456586327">
<node TEXT="watermelon book" ID="ID_947925241" CREATED="1624456592513" MODIFIED="1624456596605"/>
<node TEXT="flower book" ID="ID_1860750475" CREATED="1624456587361" MODIFIED="1624456591909"/>
<node TEXT="2106.11342: dive into deep learning (mxnet)" ID="ID_313730253" CREATED="1624456597805" MODIFIED="1624456609850"/>
</node>
<node TEXT="Object Detection" ID="ID_1186488032" CREATED="1619334735508" MODIFIED="1619334741761">
<node TEXT="PAMI: Weakly Supervised Object Localization and Detection: A Survey" ID="ID_550381811" CREATED="1619334770549" MODIFIED="1619334777124"/>
</node>
<node TEXT="Scene" ID="ID_282839696" CREATED="1612166497534" MODIFIED="1612166501051">
<node TEXT="2101.10531&#xa; Deep Learning for Scene Classification: A Survey" ID="ID_1588464638" CREATED="1612166502020" MODIFIED="1612166508755"/>
</node>
<node TEXT="Visual Transformer" ID="ID_84446787" CREATED="1608871011262" MODIFIED="1608871014226">
<node TEXT="2012.12556&#xa; A Survey on Visual Transformer" ID="ID_843960911" CREATED="1608871015499" MODIFIED="1608871032690"/>
<node TEXT="2101: Transformers in Vision: A Survey" ID="ID_1959303133" CREATED="1610425958283" MODIFIED="1610425966435"/>
</node>
<node TEXT="Vision+Language" ID="ID_1299391859" CREATED="1608879468011" MODIFIED="1612168631733">
<node TEXT="Pretrain" ID="ID_974958355" CREATED="1612168632729" MODIFIED="1612168636919">
<node TEXT="2012.08673&#xa;A Closer Look at the Robustness of Vision-and-Language Pre-trained Models" ID="ID_1936309513" CREATED="1608879475108" MODIFIED="1608879486030"/>
</node>
<node TEXT="Synthesis" ID="ID_740746900" CREATED="1612168639832" MODIFIED="1612168644090">
<node TEXT="2101: Adversarial Text-to-Image Synthesis: A Review" ID="ID_1737346805" CREATED="1612168645539" MODIFIED="1612168656457">
<node TEXT="2101.09983" OBJECT="java.lang.Double|2101.09983" ID="ID_1020253434" CREATED="1615357806562" MODIFIED="1615357813054"/>
</node>
</node>
</node>
<node TEXT="CBIR" ID="ID_1563320081" CREATED="1608010115823" MODIFIED="1608010118602">
<node TEXT="2012.00641: a decade survey of content based image retrieval using deep learning" ID="ID_1139748621" CREATED="1608010120162" MODIFIED="1608010139085">
<node TEXT="largely about Hashing for image retrieval" ID="ID_1180040190" CREATED="1608010444517" MODIFIED="1608010466937"/>
</node>
<node TEXT="Deep Image Retrieval: A Survey" ID="ID_1660691658" CREATED="1615357376521" MODIFIED="1615357382065">
<node TEXT="arXiv:2101.11282" ID="ID_899732298" CREATED="1615357388159" MODIFIED="1615357389285"/>
</node>
</node>
<node TEXT="Action Recognition" ID="ID_921645625" CREATED="1608010505808" MODIFIED="1608010509558">
<node TEXT="1901.09403: spatio-temporal action recognition: a survey" ID="ID_1374293575" CREATED="1608010511105" MODIFIED="1608010526886">
<node TEXT="action localization" ID="ID_1731862850" CREATED="1608010591950" MODIFIED="1608010595047"/>
</node>
</node>
<node TEXT="3D Segmentation" ID="ID_1110326685" CREATED="1615349107777" MODIFIED="1619334746208">
<node TEXT="Deep Learning based 3D Segmentation: A Survey" ID="ID_463010697" CREATED="1615349115021" MODIFIED="1615349120270">
<node TEXT="arXiv:2103.05423" ID="ID_1980243362" CREATED="1615349120519" MODIFIED="1615349126853"/>
</node>
</node>
<node TEXT="Anomaly Detection" ID="ID_170137859" CREATED="1618846931941" MODIFIED="1618846936621">
<node TEXT="2004.05993: a survey of single-scene video anomaly deteciton" ID="ID_462688942" CREATED="1618846926868" MODIFIED="1618846929066"/>
</node>
<node TEXT="Self-supervised learning" ID="ID_324088622" CREATED="1649102747207" MODIFIED="1649102756220">
<node TEXT="Self-supervised learning has two major forms, GANs and contrastive learning" ID="ID_1689550023" CREATED="1647264534821" MODIFIED="1647264549612">
<icon BUILTIN="info"/>
</node>
</node>
</node>
<node TEXT="Adversarial Attack: Classification" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_234014270" CREATED="1641521502446" MODIFIED="1647263386358">
<edge COLOR="#ff0000"/>
<node TEXT="Survey" ID="ID_1901553164" CREATED="1650817865364" MODIFIED="1650817870013">
<icon BUILTIN="info"/>
<node TEXT="python: foolbox" ID="ID_1553278631" CREATED="1650817871101" MODIFIED="1650817877372"/>
<node TEXT="python: torchattacks" ID="ID_1476724210" CREATED="1650817877844" MODIFIED="1650817881932"/>
</node>
<node TEXT="White-Box Attack" ID="ID_449007607" CREATED="1641522449462" MODIFIED="1651778178797">
<node TEXT="General" ID="ID_383717686" CREATED="1651778224542" MODIFIED="1651778226797">
<node TEXT="L-BFGS" ID="ID_1011268115" CREATED="1642438696428" MODIFIED="1642438699395"/>
<node TEXT="FGSM" ID="ID_1740870459" CREATED="1642438691608" MODIFIED="1642438694832">
<node TEXT="Explaining and harnessing adversarial examples." ID="ID_522955177" CREATED="1642455794565" MODIFIED="1642455799174">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="BIM" ID="ID_1136553892" CREATED="1642438700519" MODIFIED="1642438706658"/>
<node TEXT="C&amp;W Family" ID="ID_172679490" CREATED="1648342400405" MODIFIED="1651778386567">
<node TEXT="Original C&amp;W" ID="ID_477466679" CREATED="1651778389504" MODIFIED="1651778392437">
<node TEXT="L-2,inf" ID="ID_1315308471" CREATED="1651778194998" MODIFIED="1651778201912"/>
</node>
<node TEXT="DDN" ID="ID_336399363" CREATED="1651778505402" MODIFIED="1651778506774">
<node TEXT="Decoupling Direction and Norm for Efficient Gradient-Based L2&#xa;Adversarial Attacks and Defenses" ID="ID_270874273" CREATED="1651778394440" MODIFIED="1651778514447">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR19" ID="ID_575310466" CREATED="1651778541230" MODIFIED="1651778542707"/>
<node TEXT="L-2 only" ID="ID_1686974516" CREATED="1651778528982" MODIFIED="1651778549028"/>
</node>
</node>
</node>
<node TEXT="MIM" ID="ID_378587323" CREATED="1642438707408" MODIFIED="1642438708204"/>
<node TEXT="PGD" ID="ID_375244598" CREATED="1642438708674" MODIFIED="1649126329101">
<font BOLD="true"/>
<node TEXT="Madry (link defense)" ID="ID_605228331" CREATED="1642438862673" MODIFIED="1642438872953"/>
</node>
<node TEXT="FMN" ID="ID_1586773857" CREATED="1651778129546" MODIFIED="1651778132912">
<node TEXT="Fast Minimum-norm Adversarial Attacks through&#xa;Adaptive Norm Constraints" ID="ID_1037702643" CREATED="1651778133611" MODIFIED="1651778347593">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS21" ID="ID_688276789" CREATED="1651778164946" MODIFIED="1651778166491"/>
<node TEXT="L-0,1,2,inf" ID="ID_38984048" CREATED="1651778167330" MODIFIED="1651778172229"/>
</node>
</node>
</node>
<node TEXT="L-0 specific (sparse)" ID="ID_306012557" CREATED="1648343689366" MODIFIED="1651778304317">
<node TEXT="AutoAdversary: A Pixel Pruning Method for Sparse Adversarial Attack" ID="ID_1047746457" CREATED="1648343806032" MODIFIED="1648343808769">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Adversarial Scratches: Deployable Attacks to CNN Classifiers" ID="ID_338134227" CREATED="1650817912155" MODIFIED="1650817914568">
<icon BUILTIN="pencil"/>
<node TEXT="PR" ID="ID_368443487" CREATED="1650817915636" MODIFIED="1650817918576"/>
</node>
</node>
<node TEXT="L-2 specific" ID="ID_33509691" CREATED="1652405020774" MODIFIED="1652405024389">
<node TEXT="Decoupling Direction and Norm for Efficient Gradient-Based L2 Adversarial Attacks and Defenses" ID="ID_1633904940" CREATED="1652405025147" MODIFIED="1652405030281"/>
</node>
<node TEXT="Robust Evaluation" ID="ID_309429844" CREATED="1651778250124" MODIFIED="1651778255764">
<node TEXT="Adaptive Attack" ID="ID_574326959" CREATED="1651778584932" MODIFIED="1651778598117">
<node TEXT="On Adaptive Attacks to Adversarial Example Defenses" ID="ID_1353585071" CREATED="1651778588150" MODIFIED="1651778609942">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="Madry" ID="ID_1668867595" CREATED="1651778604440" MODIFIED="1651778606066"/>
</node>
</node>
<node TEXT="ADI" ID="ID_583399730" CREATED="1648412574268" MODIFIED="1648412663362">
<font BOLD="true"/>
<node TEXT="Practical Evaluation of Adversarial Robustness via Adaptive Auto Attack" ID="ID_634188978" CREATED="1648412612950" MODIFIED="1648413714227">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="CVPR22" ID="ID_820470690" CREATED="1648412624240" MODIFIED="1648412626126">
<node TEXT="good summary of defense methods" ID="ID_1700525729" CREATED="1648413694208" MODIFIED="1648413733242">
<icon BUILTIN="info"/>
<font BOLD="false"/>
</node>
<node TEXT="refers AA" ID="ID_1108613680" CREATED="1648412707708" MODIFIED="1648412710406"/>
<node TEXT="refers Lafeat" ID="ID_482513425" CREATED="1648412710624" MODIFIED="1648412714332"/>
</node>
<node TEXT="against many existing defense methods" ID="ID_298410983" CREATED="1648412626421" MODIFIED="1648413645064"/>
<node TEXT="only cifar10 and cifar100" ID="ID_1704582608" CREATED="1648413776618" MODIFIED="1648413786192">
<node TEXT="the state-of-the-art adversarial training cannot be done for ILSVRC2012 within acceptable cost." ID="ID_1985501665" CREATED="1648413787064" MODIFIED="1648413820906"/>
</node>
</node>
</node>
<node TEXT="Lafeat" ID="ID_1152998263" CREATED="1649126247993" MODIFIED="1649126326221">
<font BOLD="true"/>
<node TEXT="LAFEAT: Piercing Through Adversarial Defenses with Latent Features" ID="ID_1299656369" CREATED="1649126250768" MODIFIED="1649126257431">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR21" ID="ID_714896295" CREATED="1649126258478" MODIFIED="1649126260081"/>
<node TEXT="l-inf white-box attack that pierce defense" ID="ID_1474527462" CREATED="1649126290031" MODIFIED="1649126304790"/>
<node TEXT="evaluate defense" ID="ID_459840676" CREATED="1649126319746" MODIFIED="1649126322467"/>
</node>
</node>
</node>
</node>
<node TEXT="Transferrability" ID="ID_142248765" CREATED="1641678368814" MODIFIED="1641678372373">
<node TEXT="TI-FGSM" ID="ID_1220544753" CREATED="1651189229068" MODIFIED="1651189234087">
<node TEXT="Evading Defenses to Transferable Adversarial Examples by&#xa;Translation-Invariant Attacks" ID="ID_1319672231" CREATED="1651189237732" MODIFIED="1651189244732">
<node TEXT="CVPR, Jun Zhu" ID="ID_707284341" CREATED="1651189245815" MODIFIED="1651189253385"/>
</node>
</node>
<node TEXT="DI-FGSM" ID="ID_1373016052" CREATED="1651189234341" MODIFIED="1651189236405"/>
<node TEXT="An Intermediate-level Attack Framework on The Basis of Linear Regression" ID="ID_1499876765" CREATED="1648342305622" MODIFIED="1648342307756">
<icon BUILTIN="pencil"/>
<node TEXT="ECCV extension" ID="ID_1206816090" CREATED="1648342309374" MODIFIED="1648342312215"/>
</node>
<node TEXT="A Little Robustness Goes a Long Way: Leveraging&#xa;Robust Features for Targeted Transfer Attacks" ID="ID_1979578221" CREATED="1648648406841" MODIFIED="1648648407761">
<node TEXT="NIPS21" ID="ID_79033966" CREATED="1648648423018" MODIFIED="1648648435131"/>
<node TEXT="training the source classifiers to be &apos;slightly robust&apos; -- that is, robust to small magnitude adversarial examples -- substantially improves the transferability of class-targeted and representation-targeted adversarial attacks, even between architectures as different as convolutional neural networks and transformers" ID="ID_1089875092" CREATED="1648648464236" MODIFIED="1648648612665"/>
</node>
<node TEXT="Improving Adversarial Transferability with Spatial Momentum" ID="ID_1876738977" CREATED="1649124088574" MODIFIED="1649124101366">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Improving the Transferability of Targeted Adversarial Examples through Object-Based Diverse Input" ID="ID_1431448231" CREATED="1649127313699" MODIFIED="1649127316657">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1200375451" CREATED="1649127317584" MODIFIED="1649127319119"/>
<node TEXT="3D render" ID="ID_1435099598" CREATED="1649127319398" MODIFIED="1649127321736"/>
</node>
<node TEXT="Investigating Top-k White-Box and Transferable Black-box Attack" ID="ID_1163922041" CREATED="1649623286596" MODIFIED="1649623290436">
<icon BUILTIN="clanbomber"/>
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_968904429" CREATED="1649623306891" MODIFIED="1649623309747"/>
<node TEXT="extend top-1 attack to top-k attack for better transferrabilityh" ID="ID_586491602" CREATED="1649623325184" MODIFIED="1649623339168"/>
</node>
<node TEXT="DST: Dynamic Substitute Training for Data-free Black-box Attack" ID="ID_179730775" CREATED="1649629766682" MODIFIED="1649629770207">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_420518291" CREATED="1649629771272" MODIFIED="1649629774362"/>
</node>
<node TEXT="Transfer Attacks Revisited: A Large-Scale Empirical Study in Real Computer Vision Settings" ID="ID_1399088132" CREATED="1650831700735" MODIFIED="1650831706979">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<node TEXT="SP22" ID="ID_610392713" CREATED="1650831707957" MODIFIED="1650831712625"/>
<node TEXT="Conclusion" ID="ID_956796677" CREATED="1650831780208" MODIFIED="1650831782210">
<node TEXT="(1) simple surrogates do not necessarily improve real transfer attacks" ID="ID_1804759456" CREATED="1650831722896" MODIFIED="1650831735286"/>
<node TEXT="(2) no dominant surrogate architecture is found in real transfer attacks" ID="ID_247305534" CREATED="1650831735586" MODIFIED="1650831746160"/>
<node TEXT="(3) it is the gap between posterior rather than the gap between logit that increases transferability." ID="ID_1921463457" CREATED="1650831748628" MODIFIED="1650831776660"/>
</node>
<node TEXT="Problem" ID="ID_646666901" CREATED="1650831786179" MODIFIED="1650831788586">
<node TEXT="(1) model similarity is not a well-defined concept" ID="ID_442639715" CREATED="1650831789291" MODIFIED="1650831812917"/>
<node TEXT="(2) l2 norm of perturbation can geenrate high transferrability without use of gradient and is a more powerful source than l-infty norm" ID="ID_434192622" CREATED="1650831813567" MODIFIED="1650831836316"/>
</node>
</node>
<node TEXT="A Little Robustness Goes a Long Way: Leveraging&#xa;Robust Features for Targeted Transfer Attacks" ID="ID_1498468142" CREATED="1651096866911" MODIFIED="1651096879515">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS21" ID="ID_1710470826" CREATED="1651096897602" MODIFIED="1651096901122"/>
<node TEXT="(1) targeted adversarial examples -- optimized to be classified as a chosen target class tend to be less transferable between architecutres" ID="ID_743139998" CREATED="1651096901839" MODIFIED="1651096923673"/>
<node TEXT="(2) training the source classifier to be slightly robust substantially improves the transferability of class-targeted and representation-targeted adversarial attacks, even between architectures as different as convolutional and transformers." ID="ID_708495525" CREATED="1651096923943" MODIFIED="1651096970776"/>
</node>
<node TEXT="Boosting Adversarial Transferability of MLP-Mixer" ID="ID_1647127268" CREATED="1652401331014" MODIFIED="1652401333518">
<icon BUILTIN="pencil"/>
<node TEXT="2022" OBJECT="java.lang.Long|2022" ID="ID_1517117653" CREATED="1652401337780" MODIFIED="1652401340254"/>
</node>
<node TEXT="Improving the Transferability of Adversarial Examples with Restructure Embedded Patches" ID="ID_257547251" CREATED="1652403432218" MODIFIED="1652403434112">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Enhancing the Transferability via Feature-Momentum Adversarial Attack" ID="ID_1388592764" CREATED="1652404351334" MODIFIED="1652404353003">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Universal Perturbation" ID="ID_883577858" CREATED="1641522438698" MODIFIED="1641522443789">
<node TEXT="on distinctive properties of universal perturbations (madry)" ID="ID_224581737" CREATED="1641522458794" MODIFIED="1641743879665">
<icon BUILTIN="pencil"/>
<node TEXT="targeted UAP via PGD, compared to standard targeted attack" ID="ID_358135140" CREATED="1641743454798" MODIFIED="1641743500936">
<node TEXT="semantic locality" ID="ID_895004329" CREATED="1641743464407" MODIFIED="1641743681600">
<node TEXT="signal concerntrated in local regions that are most salient to humans" ID="ID_119777514" CREATED="1641743682744" MODIFIED="1641743700574"/>
</node>
<node TEXT="spatial invariance" ID="ID_596707615" CREATED="1641743672916" MODIFIED="1641743676882">
<node TEXT="still effective after translations" ID="ID_290717161" CREATED="1641743703086" MODIFIED="1641743714870">
<node TEXT="relevant to equivariant transformation property of cnns." ID="ID_1322848785" CREATED="1641743769223" MODIFIED="1641743790756">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node TEXT="less signal for generalization" ID="ID_832224960" CREATED="1641743481199" MODIFIED="1641743491198">
<node TEXT="may have reached some boundary during optimization, hence the change of behavior" ID="ID_1379890976" CREATED="1641743909288" MODIFIED="1641743938814">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Black-Box Attack" ID="ID_1336427131" CREATED="1641522444757" MODIFIED="1641522449005">
<node TEXT="ZOO" ID="ID_1856291326" CREATED="1651189988630" MODIFIED="1651189990758"/>
<node TEXT="NES" ID="ID_28596450" CREATED="1651189991106" MODIFIED="1651189992698"/>
<node TEXT="SPSA" ID="ID_1401928642" CREATED="1651189994860" MODIFIED="1651189995721"/>
<node TEXT="N-Attack" ID="ID_1112980295" CREATED="1651189995935" MODIFIED="1651189997983"/>
<node TEXT="Bandits" ID="ID_348423292" CREATED="1651189998223" MODIFIED="1651190002970">
<node TEXT="Prior Convictions: Black-box Adversarial Attacks with Bandits and Priors" ID="ID_547846157" CREATED="1651190003908" MODIFIED="1651190009368">
<node TEXT="ICLR19, Madry" ID="ID_1621186740" CREATED="1651190010823" MODIFIED="1651190015960"/>
</node>
</node>
<node TEXT="SparseEvo" ID="ID_1859620243" CREATED="1644181137459" MODIFIED="1644181140152">
<node TEXT="Query Efficient Decision Based Sparse Attacks Against Black-Box Deep Learning Models" ID="ID_985376596" CREATED="1644181086023" MODIFIED="1644181150855">
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR22" ID="ID_1001676990" CREATED="1644181090717" MODIFIED="1644181096398"/>
</node>
</node>
<node TEXT="w/ Transfer Prior" ID="ID_1448999389" CREATED="1649101767532" MODIFIED="1649101771379">
<node TEXT="Boosting Black-Box Adversarial Attacks with Meta Learning" ID="ID_460302765" CREATED="1649101772257" MODIFIED="1649101787420">
<icon BUILTIN="pencil"/>
<node TEXT="Jian Sun" ID="ID_475582790" CREATED="1649101788945" MODIFIED="1649101790751"/>
</node>
<node TEXT="Query-Efficient Black-box Adversarial Attacks Guided by a Transfer-based Prior" ID="ID_1219696767" CREATED="1649130199200" MODIFIED="1649130203653">
<icon BUILTIN="unchecked"/>
<node TEXT="TPAMI" ID="ID_1416975086" CREATED="1649130204615" MODIFIED="1649130207674"/>
</node>
<node TEXT="Attacking deep networks with surrogate-based adversarial black-box methods is easy" ID="ID_530977735" CREATED="1649171258937" MODIFIED="1649171263946">
<node TEXT="ICLR22" ID="ID_67070656" CREATED="1649171265379" MODIFIED="1649171267042"/>
<node TEXT="simple sota" ID="ID_1912418370" CREATED="1649171267306" MODIFIED="1649171272734"/>
</node>
</node>
</node>
<node TEXT="No-Box Attack" ID="ID_571284137" CREATED="1648396305541" MODIFIED="1648396309532">
<node TEXT="Practical No-box Adversarial Attacks with Training-free Hybrid Image Transformation" ID="ID_1643901069" CREATED="1648396360998" MODIFIED="1648396366599">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_768388055" CREATED="1648396367815" MODIFIED="1648396370562"/>
</node>
</node>
<node TEXT="Patch/Visible Attack" ID="ID_1333081920" CREATED="1641673351616" MODIFIED="1641673361482">
<node TEXT="Adversarial Patch" ID="ID_1249341615" CREATED="1651779533115" MODIFIED="1651779538039">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS17" ID="ID_1901210113" CREATED="1651779538988" MODIFIED="1651779541349"/>
<node TEXT="gradient descent + expectation over transformation (EoT)" ID="ID_1872685698" CREATED="1651779653288" MODIFIED="1651779665556"/>
</node>
<node TEXT="Give Me Your Attention: Dot-Product Attention Considered Harmful for Adversarial Patch Robustness" ID="ID_423751219" CREATED="1649124175641" MODIFIED="1649124183001">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_293805346" CREATED="1649124183794" MODIFIED="1649124185031"/>
<node TEXT="attention mechanism and patch" ID="ID_121363818" CREATED="1649124255059" MODIFIED="1649124259848"/>
<node TEXT="(1) dot-product attention can be the source of a major vulnerability when confronted with adversarial patch attacks" ID="ID_1806672118" CREATED="1649124285021" MODIFIED="1649124304827"/>
<node TEXT="(2) provide a theoretical understanding of this vulnerability" ID="ID_1107915502" CREATED="1649124305511" MODIFIED="1649124323794"/>
<node TEXT="(3) novel objective which targets this vulnerability explicily" ID="ID_419084162" CREATED="1649124324606" MODIFIED="1649124352752"/>
<node TEXT="(4) effective on imagenet classification (ViT) and object detection (DETR)" ID="ID_317968833" CREATED="1649124356646" MODIFIED="1649124381822"/>
</node>
</node>
<node TEXT="Other Perceptual Bound" ID="ID_1927729677" CREATED="1648396670489" MODIFIED="1648396693602">
<node TEXT="Frequency-driven Imperceptible Adversarial Attack on Semantic Similarity" ID="ID_914839874" CREATED="1648396694653" MODIFIED="1648396695806">
<node TEXT="CVPR22" ID="ID_703502731" CREATED="1648396703382" MODIFIED="1648396705165"/>
<node TEXT="attack image representation" ID="ID_679359515" CREATED="1648396714359" MODIFIED="1648396718170"/>
<node TEXT="the formulations are very similar to DML attack..." ID="ID_1021723891" CREATED="1648396793827" MODIFIED="1648396809947">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node TEXT="Unlearnable Example" ID="ID_1890366930" CREATED="1649095313088" MODIFIED="1649095318895">
<font BOLD="true"/>
<node TEXT="Robust unlearnable examples: protecting data against adversarial learning" ID="ID_392399956" CREATED="1649095319935" MODIFIED="1651190729445">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="ICLR22" ID="ID_1589305797" CREATED="1649095339570" MODIFIED="1649095341010"/>
<node TEXT="error-minimizing adversarial noise" ID="ID_1448281536" CREATED="1649095393720" MODIFIED="1649095401533"/>
<node TEXT="review" ID="ID_73581907" CREATED="1649100911953" MODIFIED="1649100912822">
<node TEXT="(1) empirical risk minimization" ID="ID_594500293" CREATED="1649100900456" MODIFIED="1649100959262">
<node TEXT="\latex $$\min_\theta l(f(x),y)$$" ID="ID_20275117" CREATED="1649100915748" MODIFIED="1649100934109"/>
</node>
<node TEXT="(2) adversarial training" ID="ID_1991971720" CREATED="1649100939238" MODIFIED="1649100963483">
<node TEXT="\latex $$\min_\theta \max_\delta l(f(x+\delta),y)$$" ID="ID_1965646229" CREATED="1649100964243" MODIFIED="1649101001179"/>
</node>
<node TEXT="(3) unlearnable examples" ID="ID_1142926687" CREATED="1649101007631" MODIFIED="1649101011820">
<node TEXT="\latex $$\min_\theta \min_\delta l(f(x+\delta), y)$$" ID="ID_528317138" CREATED="1649101013646" MODIFIED="1649101038012"/>
</node>
</node>
<node TEXT="min-max v.s. max-min adversarial training" ID="ID_1459888681" CREATED="1651190704912" MODIFIED="1651190717431">
<icon BUILTIN="info"/>
<node TEXT="+ AWP" ID="ID_1160833726" CREATED="1651190721779" MODIFIED="1651190723825"/>
</node>
</node>
</node>
<node TEXT="ViT Attack" ID="ID_91091662" CREATED="1649171362076" MODIFIED="1649171368180">
<node TEXT="Patch-Fool: Are Vision Transformers Always Robust Against Adversarial Perturbations?" ID="ID_1104321596" CREATED="1649171368894" MODIFIED="1649171374908">
<node TEXT="ICLR22" ID="ID_382618337" CREATED="1649171381502" MODIFIED="1649171383704"/>
</node>
</node>
<node TEXT="Similar Topics" ID="ID_34273182" CREATED="1642456460265" MODIFIED="1642456463434">
<node TEXT="deep dream" ID="ID_272877404" CREATED="1642456464214" MODIFIED="1642456469283">
<node TEXT="max all activation" ID="ID_174456830" CREATED="1642456470094" MODIFIED="1642456479075"/>
</node>
<node TEXT="gan inversion" ID="ID_873322690" CREATED="1642456480231" MODIFIED="1642456490208">
<node TEXT="looking for the desired latent" ID="ID_1283661620" CREATED="1642456491378" MODIFIED="1642456502325"/>
</node>
</node>
<node TEXT="STAGE" ID="ID_1760709543" CREATED="1642438720562" MODIFIED="1642438722150">
<node TEXT="Boosting adversarial attacks with momentum" ID="ID_75324346" CREATED="1642438722774" MODIFIED="1642438725961">
<icon BUILTIN="unchecked"/>
</node>
</node>
</node>
<node TEXT="Adversarial Attack: Intersection" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_1335351832" CREATED="1647263319144" MODIFIED="1647263375425">
<edge COLOR="#7c007c"/>
<node TEXT="3D PCL" ID="ID_1616919715" CREATED="1648388494887" MODIFIED="1648388497545">
<node TEXT="Shape-invariant 3D Adversarial Point Clouds" ID="ID_1106915267" CREATED="1648388498393" MODIFIED="1648388503330">
<node TEXT="CVPR22" ID="ID_1406333374" CREATED="1648388505250" MODIFIED="1648388506538"/>
</node>
</node>
<node TEXT="Hashing" ID="ID_1507222099" CREATED="1651105161638" MODIFIED="1652400526012">
<node TEXT="Cross-Modal Learning with Adversarial Samples" ID="ID_1336939841" CREATED="1651105185270" MODIFIED="1651105204976">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS19, Xidian" ID="ID_134983511" CREATED="1651105192313" MODIFIED="1651105196409"/>
<node TEXT="similar to advrank" ID="ID_792191213" CREATED="1651105196662" MODIFIED="1651105202706">
<icon BUILTIN="info"/>
</node>
</node>
<node TEXT="You See What I Want You to See: Exploring Targeted Black-Box Transferability&#xa;Attack for Hash-based Image Retrieval Systems" ID="ID_610972112" CREATED="1652400527266" MODIFIED="1652400535340">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR21" ID="ID_1602396229" CREATED="1652400536589" MODIFIED="1652400538627"/>
</node>
</node>
<node TEXT="Face Recog" ID="ID_1806006397" CREATED="1642368429380" MODIFIED="1642965654306">
<node TEXT="Similarity-based Gray-box Adversarial Attack Against Deep Face Recognition" ID="ID_272220220" CREATED="1642368773041" MODIFIED="1642368776146">
<icon BUILTIN="checked"/>
<node TEXT="Conf: FG" ID="ID_231749207" CREATED="1642428474587" MODIFIED="1642428477815"/>
<node TEXT="gray-box attack method" ID="ID_1072955941" CREATED="1642368915986" MODIFIED="1642368921963"/>
<node TEXT="minimize l2 dist" ID="ID_128968953" CREATED="1642369095672" MODIFIED="1642369098770"/>
<node TEXT="not necessary for robustnesss evaluatio for face recogn" ID="ID_907195191" CREATED="1642368922155" MODIFIED="1642368946500">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Simulated adversarial testing of face recognition models" ID="ID_1458951372" CREATED="1642429229718" MODIFIED="1651093847174">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="checked"/>
<node TEXT="CVPR22, cihang, Alan Yuille" ID="ID_1913955946" CREATED="1642429240276" MODIFIED="1651092512917"/>
<node TEXT="difference" ID="ID_465366439" CREATED="1642430507464" MODIFIED="1642430509506">
<node TEXT="find the region of adversarial latent vector, instead of find merely one isolated adversarial latent vector" ID="ID_512231452" CREATED="1642430511694" MODIFIED="1642430566964"/>
</node>
<node TEXT="I. explore semantic image manifold. test algorithms using simulators in adversarial manner. test data is synthesized" ID="ID_1598940219" CREATED="1642429255498" MODIFIED="1642430016205">
<node TEXT="simulator generated data are easier to control compared to sampling from inaccessible distribution p(x,y|psi)" ID="ID_399773417" CREATED="1642429597192" MODIFIED="1642429625957"/>
<node TEXT="adversarial parameters are found through REINFORCE rule" ID="ID_1243692238" CREATED="1642429908977" MODIFIED="1642429930358"/>
</node>
<node TEXT="II. find adversarial regions as opposed to the typical adversarial points in regular adversarial examples" ID="ID_262923196" CREATED="1642429337071" MODIFIED="1642430021419"/>
<node TEXT="experiments" ID="ID_693341535" CREATED="1642430344587" MODIFIED="1642430346783">
<node TEXT="CASIA train LFW test" ID="ID_450618483" CREATED="1642430347795" MODIFIED="1642430354141"/>
<node TEXT="Resnet50 with CosFace" ID="ID_1824197744" CREATED="1642430360674" MODIFIED="1642430371554"/>
</node>
<node TEXT="can this be used in adv def for face?" ID="ID_1655776817" CREATED="1642430586222" MODIFIED="1642430601307">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="Towards Assessing and Characterizing the Semantic&#xa;Robustness of Face Recognition" ID="ID_883936286" CREATED="1645109210847" MODIFIED="1645109221519">
<icon BUILTIN="pencil"/>
<node TEXT="insufficient evaluation but should read" ID="ID_1755819921" CREATED="1645109222870" MODIFIED="1645109231608"/>
<node TEXT="find identity-preserving variants of the face such that an FRM fails to recognize the images beloning to the same identity" ID="ID_1953905907" CREATED="1645110345005" MODIFIED="1645110368636">
<node TEXT="direction and magnitude constrained perturbations in the latent space of stylegan" ID="ID_1745093670" CREATED="1645110399067" MODIFIED="1645110411795"/>
</node>
<node TEXT="characterize semantic robustness of an FTM" ID="ID_860864962" CREATED="1645110413123" MODIFIED="1645110426406"/>
<node TEXT="combine with certification technique" ID="ID_1167140711" CREATED="1645110426782" MODIFIED="1645110663756"/>
<node TEXT="does not provide a clear defense method" ID="ID_796969338" CREATED="1645110665095" MODIFIED="1645110670209"/>
</node>
<node TEXT="Controllable Evaluation and Generation of Physical Adversarial Patch on Face Recognition" ID="ID_32522158" CREATED="1648396439382" MODIFIED="1648396442110">
<icon BUILTIN="pencil"/>
<node TEXT="Jun Zhu" ID="ID_343713912" CREATED="1648396443230" MODIFIED="1648396463845"/>
</node>
</node>
<node TEXT="Action Recognition" ID="ID_1024637428" CREATED="1652385248018" MODIFIED="1652385250882">
<node TEXT="BASAR:Black-box Attack on Skeletal Action Recognition" ID="ID_1522399852" CREATED="1652385251738" MODIFIED="1652385257416">
<node TEXT="CVPR21" ID="ID_1922762713" CREATED="1652385260947" MODIFIED="1652385263160"/>
</node>
<node TEXT="Finding Achilles&apos; Heel: Adversarial Attack on Multi-modal Action Recognition" ID="ID_1543219948" CREATED="1652390538680" MODIFIED="1652390539616"/>
</node>
<node TEXT="Deep Fake" ID="ID_123845244" CREATED="1649106133977" MODIFIED="1652403031256">
<node TEXT="Exploring Frequency Adversarial Attacks for Face Forgery Detection" ID="ID_1469136743" CREATED="1649106141942" MODIFIED="1649106147499">
<node TEXT="CVPR22" ID="ID_381395410" CREATED="1649106206179" MODIFIED="1649106207558"/>
</node>
<node TEXT="2004.00622 Evading Deepfake-Image Detectors with White- and Black-Box Attacks" ID="ID_609816375" CREATED="1590804661430" MODIFIED="1590804670862"/>
<node TEXT="Restricted Black-box Adversarial Attack Against DeepFake Face Swapping" ID="ID_514211440" CREATED="1652403059016" MODIFIED="1652403067240">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Derain" ID="ID_690819450" CREATED="1649106593249" MODIFIED="1649106595511">
<node TEXT="Towards Robust Rain Removal Against Adversarial Attacks: A Comprehensive Benchmark Analysis and Beyond" ID="ID_1582771617" CREATED="1649106596203" MODIFIED="1649106604716">
<node TEXT="CVPR22" ID="ID_1289469191" CREATED="1649106606171" MODIFIED="1649106607363"/>
</node>
</node>
<node TEXT="Multi-label Prediction" ID="ID_1211679974" CREATED="1647568084959" MODIFIED="1648388483965">
<icon BUILTIN="button_ok"/>
<node TEXT="tkml-ap: adversarial attacks to top-k multi-label learning" ID="ID_768468346" CREATED="1647568102113" MODIFIED="1647568115332">
<icon BUILTIN="unchecked"/>
<node TEXT="iccv21" ID="ID_1410742901" CREATED="1647568116971" MODIFIED="1647568120084"/>
<node TEXT="actually this looks like relevant with adversarial ranking" ID="ID_418002134" CREATED="1647568897864" MODIFIED="1647568911643"/>
<node TEXT="it defines targeted and untargeted attack in the multi-class scenario" ID="ID_296403052" CREATED="1647568917772" MODIFIED="1647568929416"/>
<node TEXT="the nature that the method recognizes top-k predictions is the same as deep ranking" ID="ID_738164770" CREATED="1647568934059" MODIFIED="1647568963511">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node TEXT="Object Detection" ID="ID_601426743" CREATED="1642440732047" MODIFIED="1647267114123">
<node ID="ID_1033919012" CREATED="1642440745175" MODIFIED="1642440749146">
<icon BUILTIN="unchecked"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <h1 class="title mathjax">
      ADC: Adversarial attacks against object Detection that evade Context consistency checks
    </h1>
  </body>
</html>
</richcontent>
<node TEXT="WACV22" ID="ID_953336901" CREATED="1642440751686" MODIFIED="1642440753515"/>
</node>
<node TEXT="Adversarial Attack and Defense of YOLO Detectors&#xa;in Autonomous Driving Scenarios" ID="ID_1277005384" CREATED="1645107484732" MODIFIED="1645107492888">
<icon BUILTIN="pencil"/>
<node TEXT="most of existing works use classification or localization loss, ignoring the objectiveness part" ID="ID_589021392" CREATED="1645107514735" MODIFIED="1645107541606"/>
<node TEXT="objectness vulnerability" ID="ID_1207230656" CREATED="1645107584853" MODIFIED="1645107591319"/>
<node TEXT="objectness-aware defense method" ID="ID_241119548" CREATED="1645107591525" MODIFIED="1645107598028"/>
<node TEXT="this paper is shallow. we should be able to do more" ID="ID_1122816208" CREATED="1645107543802" MODIFIED="1645107608352"/>
</node>
<node TEXT="Zero-Query Transfer Attacks on Context-Aware Object Detectors" ID="ID_1964614404" CREATED="1649103537343" MODIFIED="1649103547351">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1857001452" CREATED="1649103548773" MODIFIED="1649103551178"/>
<node TEXT="attack against context consistency check" ID="ID_188156799" CREATED="1649103566818" MODIFIED="1649103573102"/>
<node TEXT="can we use context check in self supervised learning?" ID="ID_283009755" CREATED="1649103599961" MODIFIED="1649103619338">
<icon BUILTIN="help"/>
</node>
</node>
</node>
<node TEXT="Optical Flow" ID="ID_1641858386" CREATED="1648337319711" MODIFIED="1648337322554">
<node TEXT="A Perturbation Constrained Adversarial Attack for Evaluating the Robustness of Optical Flow" ID="ID_1358848955" CREATED="1648337323536" MODIFIED="1648337328267">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Physical Camouflage" ID="ID_1126075389" CREATED="1648386898418" MODIFIED="1648386902958">
<node TEXT="DTA: Physical Camouflage Attacks using Differentiable Transformation Network" ID="ID_302511187" CREATED="1648386903940" MODIFIED="1648386911287">
<node TEXT="CVPR22" ID="ID_408964136" CREATED="1648386913260" MODIFIED="1648386914842"/>
</node>
</node>
<node TEXT="Physical Optical Attack" ID="ID_853398766" CREATED="1648388159738" MODIFIED="1648388167426">
<node TEXT="Shadows can be Dangerous: Stealthy and Effective Physical-world Adversarial Attack by Natural Phenomenon" ID="ID_1311407087" CREATED="1648388169624" MODIFIED="1648388171162">
<node TEXT="CVPR22" ID="ID_823769725" CREATED="1648388172796" MODIFIED="1648388174205"/>
</node>
<node TEXT="Adversarial Neon Beam: Robust Physical-World Adversarial Attack to DNNs" ID="ID_445644352" CREATED="1649628227588" MODIFIED="1649628234096">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Remote Sensing" ID="ID_1273414088" CREATED="1647267114318" MODIFIED="1647267117431">
<node TEXT="Universal adversarial perturbation for remote sensing images" ID="ID_16084759" CREATED="1647267118151" MODIFIED="1647267125456">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Universal Adversarial Examples in Remote Sensing: Methodology and Benchmark" ID="ID_1566703454" CREATED="1646945260207" MODIFIED="1646945261393"/>
</node>
<node TEXT="Sponge Attack" ID="ID_579695162" CREATED="1649126139602" MODIFIED="1649126143868">
<node TEXT="Energy-Latency Attacks via Sponge Poisoning" ID="ID_1762498833" CREATED="1649126144627" MODIFIED="1649126153017">
<icon BUILTIN="pencil"/>
<node TEXT="increase energy consumption" ID="ID_1483642661" CREATED="1649126154047" MODIFIED="1649126161988"/>
</node>
</node>
<node TEXT="Tracking" ID="ID_404459343" CREATED="1644181489904" MODIFIED="1644181494136"/>
<node TEXT="Trajectory Prediction" ID="ID_1501279328" CREATED="1642953386986" MODIFIED="1642965658792">
<node ID="ID_1380187352" CREATED="1642953399320" MODIFIED="1642953401824">
<icon BUILTIN="pencil"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    On Adversarial Robustness of Trajectory Prediction for Autonomous Vehicles
  </body>
</html>
</richcontent>
<node TEXT="misguiding title" ID="ID_974407250" CREATED="1642953418674" MODIFIED="1642953424787"/>
<node TEXT="only presents attack to perturb trajectory" ID="ID_1226515884" CREATED="1642953425024" MODIFIED="1642953431087"/>
</node>
</node>
<node TEXT="Text classification" ID="ID_1810668731" CREATED="1652390887363" MODIFIED="1652390891735">
<node TEXT="Deep Text Classification Can be Fooled" ID="ID_1793547879" CREATED="1652390892614" MODIFIED="1652390893505">
<node TEXT="IJCAI18" ID="ID_1571644259" CREATED="1652390894219" MODIFIED="1652390898949"/>
</node>
</node>
<node TEXT="Unsupervised" ID="ID_1306713728" CREATED="1647567664211" MODIFIED="1648386875575">
<node TEXT="shaoyuan has one" ID="ID_1775536369" CREATED="1647567668516" MODIFIED="1647567688192">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="on the robustness of domain adaption to adversarial attacks" ID="ID_1358706166" CREATED="1647567673533" MODIFIED="1647567685949">
<icon BUILTIN="pencil"/>
</node>
</node>
</node>
<node TEXT="Attack Detection &amp; Vul Analysis" LOCALIZED_STYLE_REF="styles.topic" POSITION="right" ID="ID_1244682927" CREATED="1646761472294" MODIFIED="1647048997914">
<edge COLOR="#7c0000"/>
<node TEXT="Attack Signature" ID="ID_822623830" CREATED="1651096591068" MODIFIED="1651096596793">
<font BOLD="true"/>
<node TEXT="Identification of Attack-Specific Signatures in Adversarial Examples" ID="ID_1517932797" CREATED="1651096604936" MODIFIED="1651096617795">
<icon BUILTIN="pencil"/>
<node TEXT="Rama" ID="ID_83793265" CREATED="1651096619569" MODIFIED="1651096620790"/>
</node>
</node>
<node TEXT="Vulnerability Analysis" ID="ID_1332546973" CREATED="1641743569630" MODIFIED="1647048986674">
<node TEXT="rethinking feature uncertainty in stochastic neural networks for adversarial robustness" ID="ID_829421687" CREATED="1641678579615" MODIFIED="1641678600199">
<icon BUILTIN="pencil"/>
<node TEXT="good performance" ID="ID_1343393281" CREATED="1641678601770" MODIFIED="1641678604282"/>
</node>
<node TEXT="adversarial feature selection against evasion attacks" ID="ID_977262344" CREATED="1642370251827" MODIFIED="1642370266767">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="adversarial examples are features, not bugs" ID="ID_1358699566" CREATED="1644179365321" MODIFIED="1644179630861">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="madry" ID="ID_790867985" CREATED="1644179621610" MODIFIED="1644179623393"/>
</node>
<node TEXT="FINDING BIOLOGICAL PLAUSIBILITY FOR ADVERSARIALLY ROBUST FEATURES VIA METAMERIC TASKS" ID="ID_876129354" CREATED="1644179632853" MODIFIED="1644179995411">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR22" ID="ID_1345745634" CREATED="1644179644121" MODIFIED="1644179645620"/>
<node TEXT="adversarially robust representations capture peripheral computation better than non-robust representations" ID="ID_1873265753" CREATED="1644179653393" MODIFIED="1644179681284"/>
<node TEXT="robust representations capture peripheral computation similar to current state-of-the-art texture peripheral vision models" ID="ID_180695247" CREATED="1644179681505" MODIFIED="1644179704075"/>
<node TEXT="support the idea that localized texture summary statistic representations may drive human invariance to adversarial perturbations and that the incorporation of such representations in DNNs could give rise to useful properties like adversarial robustness" ID="ID_1448992825" CREATED="1644179707291" MODIFIED="1644179745688"/>
</node>
<node TEXT="Overparametrization improves robustness against adversarial attacks: A replication study" ID="ID_1930796846" CREATED="1647265255669" MODIFIED="1647265266564">
<icon BUILTIN="pencil"/>
<node TEXT="overparameterization is not enough" ID="ID_908075250" CREATED="1647265269687" MODIFIED="1647265276771"/>
<node TEXT="smarter architecture seems inevitable" ID="ID_1927769443" CREATED="1647265276973" MODIFIED="1647265283779"/>
<node TEXT="but the abstract does not talk about loss and generalization" ID="ID_638811091" CREATED="1647265284038" MODIFIED="1647265313609">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="On the Properties of Adversarially-Trained CNNs" ID="ID_682274955" CREATED="1649127468447" MODIFIED="1649127475029">
<icon BUILTIN="pencil"/>
<node TEXT="(1) feature maps in AT models are more dense and activate more frequently compared to natural models" ID="ID_701621821" CREATED="1649127480694" MODIFIED="1649127499308"/>
<node TEXT="(2) feature maps in AT models are more redundant than in natural models, thus reducing the effective number of channels in hidden layers" ID="ID_780256294" CREATED="1649127500149" MODIFIED="1649127525663"/>
<node TEXT="(3) latent space of AT model offers representations with different degrees of robustness" ID="ID_647914246" CREATED="1649127525859" MODIFIED="1649127550766">
<node TEXT="This confirms the fact that AT preserves extra information about the input that is ignored by the robust classifier" ID="ID_381951281" CREATED="1649127551562" MODIFIED="1649127570752"/>
</node>
<node TEXT="(4) color bias of AT models and point out subtle failure modes that may undermine the deployment of robust models in practice" ID="ID_1720778162" CREATED="1649127657702" MODIFIED="1649127679045"/>
</node>
<node TEXT="Adversarial Robustness through the Lens of Convolutional Filters" ID="ID_1169357550" CREATED="1649633031581" MODIFIED="1649633034132">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22w" ID="ID_1187196788" CREATED="1649633034956" MODIFIED="1649633037261"/>
</node>
<node TEXT="+Bias" ID="ID_273337616" CREATED="1651955397842" MODIFIED="1651955400204">
<node TEXT="Holistic Approach to Measure Sample-level Adversarial Vulnerability&#xa;and its Utility in Building Trustworthy Systems" ID="ID_1380608765" CREATED="1651955401029" MODIFIED="1651955407542">
<node TEXT="CVPR22w" ID="ID_808643348" CREATED="1651955408814" MODIFIED="1651955410645"/>
</node>
</node>
</node>
<node TEXT="Patch Detection" ID="ID_1544755494" CREATED="1641523388190" MODIFIED="1644180812163">
<node TEXT="on the real world adversarial robustness of real-time semantic segmentation models for autonomous driving" ID="ID_621290194" CREATED="1641523393314" MODIFIED="1641523426222">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Detecting Localized Adversarial Examples: A&#xa;Generic Approach using Critical Region Analysis" ID="ID_655405044" CREATED="1652413130650" MODIFIED="1652413138859">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Perturbation Detection" ID="ID_983985712" CREATED="1641523884091" MODIFIED="1648419471576">
<font BOLD="true"/>
<node TEXT="Survey" ID="ID_554475000" CREATED="1648651800359" MODIFIED="1652412507027">
<icon BUILTIN="mindmap"/>
<node TEXT="Adversarial Example Detection for DNN Models: A Review and Experimental Comparison" ID="ID_1054333884" CREATED="1646943075067" MODIFIED="1646943080350">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="pencil"/>
<node TEXT="Artificial Intelligence Review (2022)" ID="ID_1260030442" CREATED="1652417084667" MODIFIED="1652417085620"/>
</node>
<node TEXT="Adversarial Examples Are Not Easily Detected:&#xa;Bypassing Ten Detection Methods" ID="ID_817758062" CREATED="1648419488457" MODIFIED="1648419545494">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<font BOLD="true"/>
<node TEXT="AISec" ID="ID_1277352211" CREATED="1648419525486" MODIFIED="1648419528313">
<node TEXT="Nicolas Carlini" ID="ID_1594367861" CREATED="1648419537256" MODIFIED="1648419540889"/>
<node TEXT="MNIST &amp; CIFAR10" ID="ID_452047441" CREATED="1648658395257" MODIFIED="1648658399045"/>
</node>
<node TEXT="(1) existing detection method may be defeated by constructing new loss functions" ID="ID_724192900" CREATED="1648656899060" MODIFIED="1648656912594"/>
<node TEXT="(2) propose simple guidelines for future defenses" ID="ID_861779297" CREATED="1648657000829" MODIFIED="1648657010332"/>
<node TEXT="Lessions" ID="ID_1067229954" CREATED="1648665420828" MODIFIED="1648665568192">
<node TEXT="1. randomization can increase required distortion" ID="ID_792584835" CREATED="1648665426182" MODIFIED="1648665436275">
<node TEXT="dropout randomization is the best in this paper" ID="ID_1709429014" CREATED="1648665448053" MODIFIED="1648665457804"/>
</node>
<node TEXT="2. mnist properties may not hold on CIFAR" ID="ID_1360586844" CREATED="1648665495235" MODIFIED="1648665505886"/>
<node TEXT="3. detection neural networks can be bypassed" ID="ID_1115669678" CREATED="1648665514991" MODIFIED="1648665521028"/>
<node TEXT="4. operating on raw pixel values is ineffective" ID="ID_1296076819" CREATED="1648665536389" MODIFIED="1648665546679"/>
</node>
<node TEXT="Recommendations" ID="ID_679129702" CREATED="1648665568474" MODIFIED="1648665572240">
<node TEXT="1. Evaluate using strong attack" ID="ID_344062231" CREATED="1648665573221" MODIFIED="1648665662667"/>
<node TEXT="2. Demonstrate white-attacks fail" ID="ID_1694047411" CREATED="1648665579993" MODIFIED="1648665666179"/>
<node TEXT="3. report false positive and true positive rates" ID="ID_1637052213" CREATED="1648665657349" MODIFIED="1648665675604"/>
<node TEXT="4. evaluate on more than mnist" ID="ID_455842538" CREATED="1648665693439" MODIFIED="1648665699725"/>
<node TEXT="5. release source code" ID="ID_1002874751" CREATED="1648665763729" MODIFIED="1648665766866"/>
</node>
</node>
<node TEXT="(survey) towards understanding and harnessing the effect of image transformation in adversarial deteciton" ID="ID_1343762323" CREATED="1641523891006" MODIFIED="1641523914261">
<icon BUILTIN="pencil"/>
<node TEXT="via image transformations" ID="ID_1033635622" CREATED="1641523928903" MODIFIED="1641523932870"/>
<node TEXT="this paper creates ensemble of transformations" ID="ID_446066363" CREATED="1641523933774" MODIFIED="1641523943924"/>
</node>
</node>
<node TEXT="Blackbox Model" ID="ID_969471742" CREATED="1648662655459" MODIFIED="1652412531822">
<node TEXT="Adversarial Detection without Model Information" ID="ID_1367926726" CREATED="1645108452079" MODIFIED="1645108457127">
<icon BUILTIN="pencil"/>
<node TEXT="attack detection that assumes model unavailable" ID="ID_854336755" CREATED="1645108496480" MODIFIED="1645108529519">
<icon BUILTIN="clanbomber"/>
</node>
<node TEXT="standalone detector independent of the underlying model" ID="ID_1195494420" CREATED="1645108458713" MODIFIED="1645108481695"/>
<node TEXT="energy distribution=based adversarial deteciton" ID="ID_1662739452" CREATED="1645108807054" MODIFIED="1645108816218"/>
</node>
</node>
<node TEXT="Adversarial Training or Retraining" ID="ID_768588565" CREATED="1652412536553" MODIFIED="1652413286663">
<node TEXT="GAT: Generative Adversarial Training for Adversarial Example Detection and Classification" ID="ID_413614979" CREATED="1652412540423" MODIFIED="1652412546610">
<node TEXT="ICLR20" ID="ID_271924058" CREATED="1652412547571" MODIFIED="1652412549279"/>
<node TEXT="requires adversarial training" ID="ID_777620833" CREATED="1652412549476" MODIFIED="1652413423892">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="Towards Robust Detection of Adversarial Examples" ID="ID_1959698185" CREATED="1652413287689" MODIFIED="1652413300238">
<node TEXT="NIPS18" ID="ID_1664966334" CREATED="1652413301007" MODIFIED="1652413302735">
<node TEXT="Jun Zhu" ID="ID_240279502" CREATED="1652413413727" MODIFIED="1652413417521"/>
</node>
<node TEXT="requires re-training and loss change" ID="ID_41551698" CREATED="1652413302922" MODIFIED="1652413426655">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="lacks Imagenet eval" ID="ID_229245948" CREATED="1652413311127" MODIFIED="1652413403270">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="Adversarial Example Detection and Classification With Asymmetrical Adversarial Training" ID="ID_166615137" CREATED="1652416884259" MODIFIED="1652416884815">
<node TEXT="ICLR20" ID="ID_625046782" CREATED="1652416885752" MODIFIED="1652416888325"/>
<node TEXT="has imagenet evaluation" ID="ID_1946507390" CREATED="1652417050249" MODIFIED="1652417054515"/>
<node TEXT="requires adversarial training" ID="ID_643192786" CREATED="1652416888624" MODIFIED="1652416896599">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node TEXT="Secondary Classification" ID="ID_414846675" CREATED="1648658663751" MODIFIED="1648664859598">
<node TEXT="IMPORT-Carlini" ID="ID_254123065" CREATED="1648662830176" MODIFIED="1648662837635">
<node TEXT="Grosse" ID="ID_1021114343" CREATED="1648662672493" MODIFIED="1648662681144"/>
<node TEXT="Gong" ID="ID_508989557" CREATED="1648662714118" MODIFIED="1648662716017"/>
<node TEXT="Metzen" ID="ID_202861202" CREATED="1648663289858" MODIFIED="1648663291554"/>
</node>
<node TEXT="DAD: Data-free Adversarial Defense at Test Time" ID="ID_410962157" CREATED="1649624873081" MODIFIED="1649624881705">
<icon BUILTIN="unchecked"/>
<node TEXT="WACV22" ID="ID_1229784375" CREATED="1649624875400" MODIFIED="1649624877499"/>
<node TEXT="To compare" ID="ID_992203877" CREATED="1649624877941" MODIFIED="1649624879413"/>
</node>
</node>
<node TEXT="Feature Statistics" ID="ID_574634377" CREATED="1652412650833" MODIFIED="1652416658000">
<node TEXT="Adversarial Examples Detection in Deep Networks with Convolutional Filter Statistics" ID="ID_1582866041" CREATED="1652412664191" MODIFIED="1652412847681">
<node TEXT="ICCV17" ID="ID_448242428" CREATED="1652412681539" MODIFIED="1652412684246"/>
<node TEXT="based on statistics of convolution layer outputs" ID="ID_1450410866" CREATED="1652412717720" MODIFIED="1652412757905"/>
<node TEXT="has imagenet evaluation" ID="ID_186336244" CREATED="1652412907631" MODIFIED="1652412910879"/>
<node TEXT="network" ID="ID_127665337" CREATED="1652412920720" MODIFIED="1652412922332">
<node TEXT="alexnet" ID="ID_1378246895" CREATED="1652412923157" MODIFIED="1652412925685"/>
<node TEXT="vgg16" ID="ID_477624312" CREATED="1652412926179" MODIFIED="1652412929307"/>
</node>
<node TEXT="detect these attacks" ID="ID_1404890891" CREATED="1652412841697" MODIFIED="1652412858018">
<node TEXT="L-BFGS" ID="ID_1535114174" CREATED="1652412845545" MODIFIED="1652412864879"/>
<node TEXT="EA" ID="ID_1268266112" CREATED="1652412859188" MODIFIED="1652412935827"/>
<node TEXT="Deepfool" ID="ID_137920581" CREATED="1652412947457" MODIFIED="1652412949249"/>
</node>
<node TEXT="should compare with advtrace" ID="ID_1286167900" CREATED="1652412962938" MODIFIED="1652416198745">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="SafetyNet: Detecting and Rejecting Adversarial Examples Robustly" ID="ID_1884696344" CREATED="1652470624026" MODIFIED="1652470633327">
<node TEXT="ICCV17" ID="ID_1573395527" CREATED="1652470634144" MODIFIED="1652470636095"/>
<node TEXT="consistency after modifying input" ID="ID_607783434" CREATED="1652470636372" MODIFIED="1652470646727"/>
<node TEXT="should compare with advtrace" ID="ID_1908802534" CREATED="1652470647682" MODIFIED="1652470652455">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="The Odds are Odd:&#xa;A Statistical Test for Detecting Adversarial Examples" ID="ID_480115952" CREATED="1652417603321" MODIFIED="1652417604467">
<node TEXT="ICML19" ID="ID_1571604665" CREATED="1652417606554" MODIFIED="1652417609800"/>
<node TEXT="has imagenet evaluation" ID="ID_875485912" CREATED="1652417610867" MODIFIED="1652417613767"/>
<node TEXT="can correct label" ID="ID_414060962" CREATED="1652417613979" MODIFIED="1652417616919"/>
<node TEXT="should compare with advtrace" ID="ID_115871974" CREATED="1652417617273" MODIFIED="1652417623494">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Detection of Adversarial Examples in Deep Neural&#xa;Networks with Natural Scene Statistics" ID="ID_758313134" CREATED="1652466658885" MODIFIED="1652466659678">
<node TEXT="IJCNN 20" ID="ID_1854245339" CREATED="1652466660910" MODIFIED="1652466686440"/>
<node TEXT="Natural Scene Statistics (NSS)" ID="ID_1882366566" CREATED="1652466719297" MODIFIED="1652466755378"/>
<node TEXT="100 detection accuracy" ID="ID_1468405551" CREATED="1652466916518" MODIFIED="1652466921631"/>
<node TEXT="should compare with advtrace" ID="ID_1867289169" CREATED="1652467013216" MODIFIED="1652467019152">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="ML-LOO: Detecting Adversarial Examples with Feature Attribution" ID="ID_325376604" CREATED="1652416663706" MODIFIED="1652416664276">
<node TEXT="AAAI-20" ID="ID_852918488" CREATED="1652416681128" MODIFIED="1652416683151">
<node TEXT="CIFAR10" ID="ID_881685799" CREATED="1652416683333" MODIFIED="1652416687722"/>
</node>
<node TEXT="lacks imagenet evaluation" ID="ID_655117617" CREATED="1652416688995" MODIFIED="1652416692853">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node TEXT="PCA detection" ID="ID_1905649949" CREATED="1648658633308" MODIFIED="1648664846478">
<node TEXT="IMPORT-Carlini" ID="ID_1415901383" CREATED="1648663736871" MODIFIED="1648663743514">
<node TEXT="Hendrycks" ID="ID_770849234" CREATED="1648663744453" MODIFIED="1648663748242">
<node TEXT="invalid for CIFAR" ID="ID_902001879" CREATED="1648663889873" MODIFIED="1648663893368"/>
</node>
<node TEXT="Bhagoji" ID="ID_483859539" CREATED="1648663872998" MODIFIED="1648663877393">
<node TEXT="invalid for CIFAR" ID="ID_1824432710" CREATED="1648663895319" MODIFIED="1648663900499"/>
</node>
<node TEXT="Li, Adversarial Examples Detection in Deep Networks with Convolutional Filter Statistics" ID="ID_445490815" CREATED="1648664092232" MODIFIED="1652412903754">
<node TEXT="ICCV17" ID="ID_676103905" CREATED="1648664151270" MODIFIED="1652412903756"/>
<node TEXT="eval on imagenet, but invalid for CIFAR" ID="ID_908958916" CREATED="1648664096304" MODIFIED="1648664110665"/>
<node TEXT="only L-BFGS" ID="ID_929329924" CREATED="1648664494458" MODIFIED="1648664500809"/>
<node TEXT="invalid against C&amp;W" ID="ID_1871532778" CREATED="1648664504583" MODIFIED="1648664508776"/>
</node>
</node>
</node>
<node TEXT="Distribution detection" ID="ID_1738339294" CREATED="1648658654257" MODIFIED="1648664834330">
<node TEXT="IMPORT-Carlini" ID="ID_1850530096" CREATED="1648664579597" MODIFIED="1648664583858"/>
<node TEXT="maximum mean discrepancy" ID="ID_916099158" CREATED="1648664571228" MODIFIED="1648664576071">
<node TEXT="invalid for CIFAR" ID="ID_238408726" CREATED="1648664588486" MODIFIED="1648664592948"/>
</node>
</node>
<node TEXT="Feature-Based" ID="ID_628359288" CREATED="1649724361261" MODIFIED="1649724365751">
<node TEXT="NSS" ID="ID_1395988491" CREATED="1649724367111" MODIFIED="1649724368337">
<node TEXT="Detection of Adversarial Examples in Deep Neural&#xa;Networks with Natural Scene Statistics" ID="ID_1655435367" CREATED="1649724369180" MODIFIED="1649724389592">
<icon BUILTIN="unchecked"/>
<node TEXT="IJCNN 20" ID="ID_180199044" CREATED="1649724390960" MODIFIED="1649724397382">
<node TEXT="CIFAR10" ID="ID_1361259835" CREATED="1649724414765" MODIFIED="1649724416754">
<node TEXT="100% det" ID="ID_1047349689" CREATED="1649724424103" MODIFIED="1649724425980"/>
</node>
<node TEXT="ImageNet" ID="ID_366409223" CREATED="1649724416947" MODIFIED="1649724418668">
<node TEXT="100% det" ID="ID_241413881" CREATED="1649724419737" MODIFIED="1649724421863"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Uncertainty" ID="ID_124855505" CREATED="1648664606485" MODIFIED="1649721720157">
<node TEXT="Detecting Adversarial Samples from Artifacts" ID="ID_378405709" CREATED="1646943244582" MODIFIED="1648651864607">
<icon BUILTIN="idea"/>
<icon BUILTIN="checked"/>
<node TEXT="ICML17" ID="ID_539936003" CREATED="1646943246293" MODIFIED="1646943248413">
<node TEXT="MNIST" ID="ID_331664028" CREATED="1648651892907" MODIFIED="1648651894492"/>
<node TEXT="CIFAR10" ID="ID_1817573894" CREATED="1648651894864" MODIFIED="1648651896380"/>
<node TEXT="no ILSVRC" ID="ID_1899359138" CREATED="1648651897651" MODIFIED="1649721494772">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="(1) bayesial uncertainty estimates" ID="ID_1458214406" CREATED="1648654242662" MODIFIED="1648655704847"/>
<node TEXT="(2) density (kernel) estimates" ID="ID_1715622729" CREATED="1648655684520" MODIFIED="1648655695733"/>
<node TEXT="generalizes for different architecutres and attacks" ID="ID_260062258" CREATED="1648654395847" MODIFIED="1648654416848"/>
<node TEXT="uses dropout" ID="ID_206046959" CREATED="1649721386243" MODIFIED="1649721389089"/>
<node TEXT="metric: AUC" ID="ID_477624800" CREATED="1648656304922" MODIFIED="1648656314671"/>
<node TEXT="attacks: white box FGSM BIM JSMA C&amp;W" ID="ID_18821393" CREATED="1648656317387" MODIFIED="1648656328015"/>
<node TEXT="invalid for CIFAR (C&amp;W)" ID="ID_1523903995" CREATED="1648664612425" MODIFIED="1649721739387">
<icon BUILTIN="button_cancel"/>
<node TEXT="Carlini" ID="ID_1261642556" CREATED="1649721735265" MODIFIED="1649721737050"/>
</node>
</node>
<node TEXT="Understanding Measures of Uncertainty for Adversarial Example Detection" ID="ID_383293634" CREATED="1649722538116" MODIFIED="1649722553172">
<node TEXT="UAI 2018" ID="ID_1815193457" CREATED="1649722554158" MODIFIED="1649722556824"/>
<node TEXT="no-topconf" ID="ID_297677658" CREATED="1649722569121" MODIFIED="1649722571381"/>
<node TEXT="The Roc curves in figure 14 are questionable... the curve does down by the diagonal... which seems definitely wrong" ID="ID_1518418746" CREATED="1652413581283" MODIFIED="1652413613340">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node TEXT="Normalization Detection" ID="ID_1539034569" CREATED="1648664881502" MODIFIED="1648664887878">
<node TEXT="IMPORT carlini" ID="ID_27852271" CREATED="1648664933116" MODIFIED="1648664936568"/>
<node TEXT="Dropout Randomization" ID="ID_281350781" CREATED="1648664937076" MODIFIED="1648664941720">
<node TEXT="MNIST, CIFAR" ID="ID_1747484271" CREATED="1648664975552" MODIFIED="1648664981432"/>
<node TEXT="Good on CIFAR, compromised by adaptive attack" ID="ID_1034874853" CREATED="1648664986734" MODIFIED="1648665176916"/>
<node TEXT="Artifact paper: Bayesian neural network uncertainty" ID="ID_364687895" CREATED="1648665020349" MODIFIED="1648665038968"/>
</node>
<node TEXT="Mean Blur" ID="ID_209409612" CREATED="1648665280089" MODIFIED="1648665283471">
<node TEXT="not effective defense" ID="ID_211180311" CREATED="1648665373510" MODIFIED="1648665376601"/>
</node>
</node>
<node TEXT="Generative" ID="ID_749214969" CREATED="1648662472104" MODIFIED="1648664890578">
<node TEXT="MagNet: a Two-Pronged Defense against Adversarial Examples" ID="ID_114840212" CREATED="1648662479642" MODIFIED="1648662488083">
<node TEXT="ACM CCS" ID="ID_451373269" CREATED="1648662489856" MODIFIED="1648662492228">
<node TEXT="MNIST CIFAR" ID="ID_512008405" CREATED="1648662492624" MODIFIED="1648662498796"/>
</node>
<node TEXT="lacks ImageNet evaluation" ID="ID_1118419492" CREATED="1652415997211" MODIFIED="1652416003164">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="PixelDefend: Leveraging Generative Models to Understand and Defend against Adversarial Examples" ID="ID_1844519643" CREATED="1652414053659" MODIFIED="1652414054528">
<node TEXT="ICLR18" ID="ID_1322011774" CREATED="1652414055579" MODIFIED="1652414057769"/>
<node TEXT="Fashion MNIST, CIFAR10" ID="ID_1605546242" CREATED="1652416086769" MODIFIED="1652416095174"/>
<node TEXT="lacks imagenet eval" ID="ID_609845113" CREATED="1652416095416" MODIFIED="1652416098821">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node TEXT="Multimodal" ID="ID_141398745" CREATED="1646929096139" MODIFIED="1648664910016">
<node TEXT="Detecting Adversarial Perturbations in Multi-Task Perception" ID="ID_274457308" CREATED="1646929102998" MODIFIED="1646932772460">
<icon BUILTIN="pencil"/>
<icon BUILTIN="idea"/>
<node TEXT="(1) detection scheme" ID="ID_192159417" CREATED="1646929219206" MODIFIED="1646929223629">
<node TEXT="adversarial perturbations are detected by inconsistencies between extracted edges of the input image, the depth output, and the segmentation output." ID="ID_589474324" CREATED="1646929131734" MODIFIED="1646929153597"/>
<node TEXT="heterogenious ensemble" ID="ID_812924113" CREATED="1646929154693" MODIFIED="1646929165001">
<icon BUILTIN="info"/>
</node>
</node>
<node TEXT="(2) edge consistency" ID="ID_1581857855" CREATED="1646929227157" MODIFIED="1646932558765">
<node TEXT="loss between different modalities" ID="ID_1491267282" CREATED="1646932513095" MODIFIED="1646932519209"/>
</node>
<node TEXT="(3) multi-task adv attack" ID="ID_1353003322" CREATED="1646932559964" MODIFIED="1646932565527">
<node TEXT="fooling various tasks and the detection" ID="ID_1825015452" CREATED="1646932573883" MODIFIED="1646932587870"/>
</node>
</node>
</node>
</node>
<node TEXT="Stateful Detection" ID="ID_1068348614" CREATED="1648414793478" MODIFIED="1648414796793">
<node TEXT="Stateful Detection of Black-Box Adversarial Attacks" ID="ID_1178344926" CREATED="1648414797685" MODIFIED="1648414805968">
<icon BUILTIN="unchecked"/>
<node TEXT="Carlini" ID="ID_715758266" CREATED="1648414806983" MODIFIED="1648414809938"/>
<node TEXT="against black-box attack" ID="ID_1703138784" CREATED="1648414811125" MODIFIED="1648414819549"/>
<node TEXT="(1) detect the creation procedure of black-box adversarial example" ID="ID_1999897527" CREATED="1648414821952" MODIFIED="1648415232585">
<node TEXT="keep track of past queries" ID="ID_980520047" CREATED="1648414939811" MODIFIED="1648414944678"/>
</node>
<node TEXT="(2) query blinding, attack against such stateful detection" ID="ID_1504202166" CREATED="1648415221209" MODIFIED="1648415250259">
<node TEXT="So poor, is this endless?" ID="ID_821120263" CREATED="1648415251804" MODIFIED="1648415883495">
<icon BUILTIN="info"/>
</node>
</node>
</node>
</node>
<node TEXT="Insersection" ID="ID_1915167711" CREATED="1647567802364" MODIFIED="1647567805237">
<node TEXT="semantic segmentation" ID="ID_561284915" CREATED="1647567812114" MODIFIED="1647567819386">
<node TEXT="tiggering failures; out of distribution detection by learning from local adversarial attacks in semgnatic segmentation" ID="ID_1480297231" CREATED="1647567820168" MODIFIED="1647567841158">
<icon BUILTIN="pencil"/>
</node>
</node>
</node>
<node TEXT="Robust Feature + Generation" ID="ID_537346870" CREATED="1649110439088" MODIFIED="1649110447665">
<node TEXT="A Unified Contrastive Energy-based Model for Understanding the Generative Ability of Adversarial Training" ID="ID_1489690701" CREATED="1649110448973" MODIFIED="1649110454724">
<node TEXT="ICLR22" ID="ID_1060364838" CREATED="1649110455536" MODIFIED="1649110459173"/>
</node>
</node>
</node>
<node TEXT="Adversarial Defense: Classification" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_483898562" CREATED="1642965540056" MODIFIED="1647265138437">
<edge COLOR="#7c7c00"/>
<node TEXT="Survey" ID="ID_1556055508" CREATED="1648173325254" MODIFIED="1648173329845">
<icon BUILTIN="info"/>
<node TEXT="Benchmarking Adversarial Robustness on Image Classification" ID="ID_379885724" CREATED="1642726545073" MODIFIED="1642726549438">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="2104: Deep Learning-Based Autonomous Driving Systems: A Survey of Attacks and Defenses" ID="ID_1290703897" CREATED="1619444030107" MODIFIED="1619444042107">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="2018.00401: advances in adversarial attacks and defenses in computer vision: a survey" ID="ID_493255704" CREATED="1647567861740" MODIFIED="1647567881439">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Unsolved Problems in ML Safety" ID="ID_1337188738" CREATED="1648422756392" MODIFIED="1648422764907">
<icon BUILTIN="unchecked"/>
<node TEXT="Carlini" ID="ID_1564104070" CREATED="1648422765982" MODIFIED="1648422768150"/>
</node>
</node>
<node TEXT="Benchmark/Evaluation" ID="ID_855034612" CREATED="1642431513665" MODIFIED="1648648692799">
<font BOLD="true"/>
<node TEXT="robustbench: a standardized adversarial robustness benchmark" ID="ID_1489423550" CREATED="1642431521917" MODIFIED="1642431542194">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="Critical Checkpoints for Evaluating Defence Models Against Adversarial Attack and Robustness" ID="ID_1207828835" CREATED="1647265940241" MODIFIED="1647265957212">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Adaptive Attack (Madry)" ID="ID_103436605" CREATED="1647265946214" MODIFIED="1652387977589">
<icon BUILTIN="bookmark"/>
<node TEXT="On Adaptive Attacks to Adversarial Example Defenses" ID="ID_1260395980" CREATED="1652387971803" MODIFIED="1652387973366">
<node TEXT="NIPS20" ID="ID_1453499811" CREATED="1652387978973" MODIFIED="1652387980434"/>
</node>
</node>
<node TEXT="Indicators of Attack Failure: Debugging and Improving Optimization of Adversarial Examples" ID="ID_9324905" CREATED="1648649410710" MODIFIED="1648649413156">
<icon BUILTIN="pencil"/>
<node TEXT="Nicolas" ID="ID_904426365" CREATED="1648649414535" MODIFIED="1648649419800"/>
<node TEXT="looks like NIPS preprint" ID="ID_1531408097" CREATED="1648649433839" MODIFIED="1648649442378"/>
</node>
<node TEXT="Generalized but not Robust? Comparing the Effects of Data Modification Methods on Out-of-Domain Generalization and Adversarial Robustness" ID="ID_24875098" CREATED="1649169406619" MODIFIED="1649169407241">
<node TEXT="ACL2022" ID="ID_714251955" CREATED="1649169409162" MODIFIED="1649169411614"/>
<node TEXT="(*) more data (either via additional datasets or data augmentation) benefits both OOD accuracy and adversarial robustness" ID="ID_1656697967" CREATED="1649169411884" MODIFIED="1649169438165"/>
</node>
<node TEXT="Evaluating the Adversarial Robustness of Adaptive Test-time Defenses" ID="ID_1955950293" CREATED="1652387947046" MODIFIED="1652387994467">
<icon BUILTIN="pencil"/>
<node TEXT="Hein" ID="ID_608720289" CREATED="1652387983225" MODIFIED="1652387986565"/>
<node TEXT="imitates adaptive attack paper" ID="ID_1487728187" CREATED="1652387986758" MODIFIED="1652387992706"/>
</node>
</node>
<node TEXT="Adversarial Training" ID="ID_557683107" CREATED="1641678574479" MODIFIED="1648174116710">
<font BOLD="true"/>
<node TEXT="Survey" ID="ID_178248607" CREATED="1642431445724" MODIFIED="1649101870385">
<icon BUILTIN="mindmap"/>
<node TEXT="A Survey of Robust Adversarial Training in Pattern Recognition: Fundamental, Theory, and Methodologies" ID="ID_1309339078" CREATED="1649101873071" MODIFIED="1649101883052">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="https://robustbench.github.io/" ID="ID_1384233314" CREATED="1642431450079" MODIFIED="1642431451028"/>
<node TEXT="Recent Advances in Adversarial Training for Adversarial Robustness" ID="ID_497082622" CREATED="1652390213262" MODIFIED="1652390214743">
<icon BUILTIN="unchecked"/>
<node TEXT="IJCAI21" ID="ID_611376488" CREATED="1652390215736" MODIFIED="1652390222813"/>
</node>
</node>
<node TEXT="FGSM-AT (Ian G.)" ID="ID_1386316101" CREATED="1642455770819" MODIFIED="1652389774213">
<node TEXT="Explaining and harnessing adversarial examples." ID="ID_444955732" CREATED="1642455817682" MODIFIED="1642455817682">
<node TEXT="see Attack/FGSM" ID="ID_1699730646" CREATED="1642455774488" MODIFIED="1642455811066"/>
</node>
<node TEXT="RFGSM/AT" ID="ID_775433299" CREATED="1642455858471" MODIFIED="1642455865293">
<node TEXT="Ensemble adversarial training: Attacks and defenses." ID="ID_850120172" CREATED="1642455866806" MODIFIED="1643412222998">
<icon BUILTIN="unchecked"/>
</node>
</node>
</node>
<node TEXT="PGD-AT (Standard)" ID="ID_882830443" CREATED="1641678605757" MODIFIED="1652389765399">
<node TEXT="Towards deep learning models resistant to adversarial attacks" ID="ID_634512701" CREATED="1642438883995" MODIFIED="1642438897524">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="checked"/>
<node TEXT="Very good paper." ID="ID_1535465318" CREATED="1642438919685" MODIFIED="1642438926372">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Baseline AT for defense." ID="ID_1889052826" CREATED="1642438905163" MODIFIED="1642438916299">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node TEXT="Curriculum AT" ID="ID_373950173" CREATED="1643412302659" MODIFIED="1643412307668">
<node TEXT="Curriculum Adversarial Training" ID="ID_285117304" CREATED="1643412309119" MODIFIED="1643413743828">
<icon BUILTIN="checked"/>
<node TEXT="ijcai18" ID="ID_1417853528" CREATED="1643412317231" MODIFIED="1643412322551"/>
<node TEXT="gradually increase attack strengths, namely increase PGD steps. each strengths trains for 1 epoch." ID="ID_1208154605" CREATED="1643413745409" MODIFIED="1643413812902"/>
</node>
</node>
<node TEXT="TRADES/AT" ID="ID_932779241" CREATED="1641678609578" MODIFIED="1643412167174">
<node TEXT="theoretically principled trade-off between robustness and accuracy" ID="ID_1065469295" CREATED="1642369441439" MODIFIED="1652386047710">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<node TEXT="ICML19" ID="ID_1723695982" CREATED="1652386039599" MODIFIED="1652386042121"/>
<node TEXT="commonly used baseline" ID="ID_108968911" CREATED="1642449381855" MODIFIED="1642449388727">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="TRADES+RST" ID="ID_293580920" CREATED="1652386032991" MODIFIED="1652386075191">
<node TEXT="Understanding and Mitigating the Tradeoff Between Robustness and Accuracy" ID="ID_77099741" CREATED="1652386056406" MODIFIED="1652386064221">
<icon BUILTIN="unchecked"/>
<node TEXT="ICML20" ID="ID_75040895" CREATED="1652386065166" MODIFIED="1652386067542"/>
</node>
</node>
</node>
<node TEXT="AWP" ID="ID_1537290479" CREATED="1642456071529" MODIFIED="1646671285402">
<icon BUILTIN="bookmark"/>
<node TEXT="Adversarial weight perturbation helps robust generalization." ID="ID_1348938152" CREATED="1642456074788" MODIFIED="1643412228699">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="SOTA" ID="ID_431363817" CREATED="1643412245745" MODIFIED="1643412247800"/>
</node>
<node TEXT="relevant" ID="ID_1110478692" CREATED="1652383380933" MODIFIED="1652383385957">
<node TEXT="Regularizing Neural Networks via Adversarial Model Perturbation" ID="ID_1687678334" CREATED="1652383387008" MODIFIED="1652383392750">
<node TEXT="CVPR21" ID="ID_1565038762" CREATED="1652383394260" MODIFIED="1652383395460"/>
</node>
</node>
</node>
<node TEXT="ALP" ID="ID_181360875" CREATED="1646671293886" MODIFIED="1646671296189">
<node TEXT="Adversarial logit paring" ID="ID_510147065" CREATED="1646671297160" MODIFIED="1646671312531">
<icon BUILTIN="unchecked"/>
<node TEXT="Ian goodfellow" ID="ID_469721326" CREATED="1646671305592" MODIFIED="1646671309168"/>
</node>
</node>
<node TEXT="CCAT" ID="ID_376220225" CREATED="1652389754521" MODIFIED="1652389755867">
<node TEXT="Confidence-Calibrated Adversarial Training: Generalizing to Unseen Attacks" ID="ID_1604933704" CREATED="1652389784881" MODIFIED="1652389786181">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS20" ID="ID_263637439" CREATED="1652389787761" MODIFIED="1652389789109"/>
</node>
</node>
<node TEXT="ADT" ID="ID_1571902293" CREATED="1652388120851" MODIFIED="1652388122352">
<node TEXT="Adversarial Distributional Training for&#xa;Robust Deep Learning" ID="ID_1936320095" CREATED="1652388167238" MODIFIED="1652388169372">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS20" ID="ID_635784608" CREATED="1652388170184" MODIFIED="1652388178843"/>
<node TEXT="Jun Zhu" ID="ID_1715749037" CREATED="1652388733459" MODIFIED="1652388735711"/>
<node TEXT="compares with TRADES" ID="ID_1173919543" CREATED="1652388736842" MODIFIED="1652388741288"/>
</node>
</node>
<node TEXT="GAT" ID="ID_1630974602" CREATED="1642449350297" MODIFIED="1642449352427">
<node TEXT="Guided Adversarial Attack for Evaluating and Enhancing Adversarial Defenses." ID="ID_392784781" CREATED="1642456004328" MODIFIED="1642456013081">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="nips20" ID="ID_670378629" CREATED="1642456026381" MODIFIED="1642456030253"/>
</node>
<node TEXT="GAT: Generative Adversarial Training for Adversarial Example Detection and Classification" ID="ID_1852535124" CREATED="1652412366390" MODIFIED="1652412366985">
<node TEXT="ICLR20" ID="ID_1765368903" CREATED="1652412369030" MODIFIED="1652412371681"/>
<node TEXT="requires adversarial training" ID="ID_1030310220" CREATED="1652412371857" MODIFIED="1652412484918"/>
</node>
<node TEXT="GAIRAT" ID="ID_1795824577" CREATED="1642369428185" MODIFIED="1642441201215">
<node TEXT="geometry-aware instance-reweighted adversarial training, iclr, 2020" ID="ID_255113727" CREATED="1642369496991" MODIFIED="1642441263816">
<icon BUILTIN="unchecked"/>
<node TEXT="iclr20" ID="ID_1152586640" CREATED="1643412171827" MODIFIED="1643412174065"/>
</node>
</node>
</node>
<node TEXT="NuAT" ID="ID_769833360" CREATED="1642449307525" MODIFIED="1642449312169">
<node TEXT="Towards Efficient and Effective Adversarial Training" ID="ID_1131130428" CREATED="1642449452500" MODIFIED="1642456091513">
<icon BUILTIN="help"/>
<icon BUILTIN="unchecked"/>
<node TEXT="nips21" ID="ID_694522660" CREATED="1642449458075" MODIFIED="1642449460409"/>
<node TEXT="nuclear-norm attack, single or two step" ID="ID_1355334878" CREATED="1642449462884" MODIFIED="1642449487212">
<node TEXT="why use nuclear norm?" ID="ID_676092258" CREATED="1642455675450" MODIFIED="1642455681173">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="adversarial training with nuclear-attack" ID="ID_1804260096" CREATED="1642455687824" MODIFIED="1642455695307">
<node TEXT="like TRADES" ID="ID_1339501480" CREATED="1642456101810" MODIFIED="1642456112106"/>
</node>
<node TEXT="ref: nuclear norm / trace norm" ID="ID_794259512" CREATED="1642449487562" MODIFIED="1642450088267">
<node TEXT="amount of rank-1 matrices needed to reconstruct X." ID="ID_534466284" CREATED="1642450121797" MODIFIED="1642450134497"/>
<node TEXT="https://mathoverflow.net/questions/278013/what-is-the-intuition-for-the-trace-norm-nuclear-norm" ID="ID_1243321985" CREATED="1642450093241" MODIFIED="1642450093241" LINK="https://mathoverflow.net/questions/278013/what-is-the-intuition-for-the-trace-norm-nuclear-norm"/>
<node TEXT="https://encyclopediaofmath.org/wiki/Nuclear_norm" ID="ID_1804499957" CREATED="1642450239653" MODIFIED="1642450239653" LINK="https://encyclopediaofmath.org/wiki/Nuclear_norm"/>
<node TEXT="https://en.wikipedia.org/wiki/Matrix_norm" ID="ID_252928450" CREATED="1642450331560" MODIFIED="1642450331560" LINK="https://en.wikipedia.org/wiki/Matrix_norm">
<node TEXT="Spectrum norm" ID="ID_1885026507" CREATED="1642454275615" MODIFIED="1642454282395">
<node TEXT="norm induced by p-norm" ID="ID_1777624265" CREATED="1642454340598" MODIFIED="1642454351392"/>
<node TEXT="\latex $$\|A\|_2==\sqrt{\lambda_\max(A^* A)}=\sigma_{max}(A)\leq \|A\|_F$$" ID="ID_526468254" CREATED="1642454283503" MODIFIED="1642454516305"/>
</node>
<node TEXT="Frobenius Norm" ID="ID_541944385" CREATED="1642454352691" MODIFIED="1642454364815">
<node TEXT="entry-wise norm" ID="ID_515709247" CREATED="1642454365986" MODIFIED="1642454377303"/>
<node TEXT="\latex $$\|A\|_F=\sqrt{\sum_i \sum_j |a_{ij}|^2} = \sqrt{Tr(A^*A)} = \sqrt{\sum_i^{\min(m,n)} \sigma_i^2(A)}$$" ID="ID_590705211" CREATED="1642454377531" MODIFIED="1642454469092"/>
</node>
<node TEXT="Nuclear/trace norm" ID="ID_331647121" CREATED="1642454546563" MODIFIED="1642454552498">
<node TEXT="is a Schattern norm" ID="ID_511441612" CREATED="1642454553455" MODIFIED="1642454666915">
<node TEXT="p-norm operating on singular value vector of a matrix" ID="ID_955840719" CREATED="1642454561278" MODIFIED="1642454576825"/>
<node TEXT="\latex $$\|A\|_p = \Big(\sum_i \sigma_i^p(A)\Big)^{1/p}$$" ID="ID_740352297" CREATED="1642454600205" MODIFIED="1642454655969"/>
</node>
<node TEXT="\latex $$\|A\|_* = Tr(\sqrt{A^* A}) = \sum_i^{\min(m,n)} \sigma_i(A)$$" ID="ID_1940588690" CREATED="1642454577885" MODIFIED="1642454717653"/>
<node TEXT="is a convex envelope of the rank function rank(A) -- often used in search of low rank matrices" ID="ID_1518781099" CREATED="1642454776478" MODIFIED="1642454805477"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="R-MGM" ID="ID_1819761584" CREATED="1642449346090" MODIFIED="1642449349702">
<node TEXT="Regularizer to mitigate gradient masking effect during&#xa;single-step adversarial training." ID="ID_1809245170" CREATED="1642455983073" MODIFIED="1642455989167">
<icon BUILTIN="unchecked"/>
</node>
</node>
<node TEXT="FBF" ID="ID_761176936" CREATED="1642449341180" MODIFIED="1642449345704">
<node TEXT="Fast is better than free: Revisiting adversarial training." ID="ID_336860717" CREATED="1642455952378" MODIFIED="1642455958511">
<icon BUILTIN="unchecked"/>
</node>
</node>
<node TEXT="MART" ID="ID_6479998" CREATED="1642438310728" MODIFIED="1642441191254">
<node TEXT="improving adversarial robustness requires revisiting misclassified examples" ID="ID_466710053" CREATED="1642438315292" MODIFIED="1642438341586">
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR20" ID="ID_1015903055" CREATED="1652390189576" MODIFIED="1652390191491"/>
</node>
</node>
<node TEXT="Reverse Cross Entropy" ID="ID_1881427141" CREATED="1646933225738" MODIFIED="1646933231428">
<node TEXT="Towards Robust Detection of Adversarial Examples" ID="ID_1202072415" CREATED="1646933239848" MODIFIED="1646933245962">
<icon BUILTIN="checked"/>
<node TEXT="jun zhu" ID="ID_1172403521" CREATED="1646933241345" MODIFIED="1646933242767"/>
<node TEXT="reverse cross entropy" ID="ID_1181285418" CREATED="1646933414379" MODIFIED="1646933418556">
<node TEXT="\latex $L_{CE}^\lambda (x,y)=L_{CE}(x,y)-\lambda R_y^T \log F(x)$" ID="ID_406004386" CREATED="1646934270616" MODIFIED="1646934299584"/>
</node>
<node TEXT="thresholding detect" ID="ID_1860751625" CREATED="1646933418741" MODIFIED="1646933426947"/>
<node TEXT="does not report adversarial example strength" ID="ID_1516966777" CREATED="1646934922811" MODIFIED="1646934935894">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node TEXT="SCORE" ID="ID_1726030577" CREATED="1647050867575" MODIFIED="1647050869829">
<node TEXT="Robustness and Accuracy Could Be Reconcilable by (Proper) Definition" ID="ID_143797849" CREATED="1647050872362" MODIFIED="1647050887235">
<icon BUILTIN="unchecked"/>
<node TEXT="jun zhu" ID="ID_832929345" CREATED="1647050889158" MODIFIED="1647050891004"/>
<node TEXT="not significant improvement, but looks good" ID="ID_534685146" CREATED="1647050891181" MODIFIED="1647050914976"/>
<node TEXT="SCORE facilitates the reconsciliation between robustness and accuracy, while distilling the worst-case uncertainty via robust optimization" ID="ID_1733911794" CREATED="1647050950453" MODIFIED="1647051074167"/>
</node>
</node>
<node TEXT="DAT" ID="ID_1850866585" CREATED="1647266645765" MODIFIED="1647266648557">
<node TEXT="Dynamic AT" ID="ID_1150526894" CREATED="1647266652842" MODIFIED="1647266662088"/>
</node>
<node TEXT="YOPO" ID="ID_736228642" CREATED="1647266649730" MODIFIED="1647266651496">
<node TEXT="You only propagate once" ID="ID_1231479391" CREATED="1647266663589" MODIFIED="1647266668870"/>
</node>
<node TEXT="SIHGs" ID="ID_61985594" CREATED="1647266705492" MODIFIED="1647266712121">
<node TEXT="Semi-Implicit Hybrid Gradient Methods with Application to Adversarial Robustness" ID="ID_1850162030" CREATED="1647266714067" MODIFIED="1647266723279">
<icon BUILTIN="unchecked"/>
<node TEXT="AISTATS 22" ID="ID_1001355755" CREATED="1647266739590" MODIFIED="1647266744080"/>
<node TEXT="combines DAT and YOPO" ID="ID_1398086187" CREATED="1647266724764" MODIFIED="1647266729252"/>
</node>
</node>
<node TEXT="Manifold extension" ID="ID_890151384" CREATED="1652406339560" MODIFIED="1652406342698">
<node TEXT="Dual Manifold Adversarial Robustness:&#xa;Defense against Lp and non-Lp Adversarial Attacks" ID="ID_206104462" CREATED="1652406343378" MODIFIED="1652406343884"/>
</node>
<node TEXT="not-famous" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1872674435" CREATED="1642431357219" MODIFIED="1647050857744">
<node TEXT="parl: enhancing diversity of ensemble networks to resist adversarial attacks via pairwise adversarially robust loss function" ID="ID_1007028628" CREATED="1642431798570" MODIFIED="1642432009766">
<icon BUILTIN="pencil"/>
<node TEXT="diverging layer-wise gradient direction among ensembled models in cosine similarity" ID="ID_1230731010" CREATED="1642431821906" MODIFIED="1642432005710">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="interesting idea. but storytelling needs improvement" ID="ID_925466018" CREATED="1642432015284" MODIFIED="1642432041217"/>
<node TEXT="problematic evaluation" ID="ID_899908589" CREATED="1642432187766" MODIFIED="1642432191269"/>
</node>
<node TEXT="mutual adversarial training: learning together is better than going alone" ID="ID_1562045490" CREATED="1642432441196" MODIFIED="1642432454531">
<icon BUILTIN="pencil"/>
<node TEXT="JHU Rama" ID="ID_918917928" CREATED="1642432456076" MODIFIED="1642439049709"/>
<node TEXT="knowledge distillation" ID="ID_1194675877" CREATED="1642439037294" MODIFIED="1642439040646"/>
<node TEXT="two students learn from each other" ID="ID_967309274" CREATED="1642439040809" MODIFIED="1642439045439"/>
</node>
<node TEXT="Learning Representations Robust to Group Shifts and Adversarial Examples" ID="ID_746174260" CREATED="1647265385913" MODIFIED="1647265392954">
<icon BUILTIN="pencil"/>
<node TEXT="combine with domain shift countermeasure" ID="ID_1565911703" CREATED="1647265394863" MODIFIED="1647265404217"/>
<node TEXT="distributional robust optimization" ID="ID_1633450540" CREATED="1647265404647" MODIFIED="1647265414284"/>
</node>
<node TEXT="Adversarial amplitude swap towards robust image classifiers" ID="ID_1096275822" CREATED="1649168866266" MODIFIED="1649168873830">
<icon BUILTIN="pencil"/>
<node TEXT="(1) images generated by combining amplitude spectrum of adversarial images and the phase spectrum of clean images" ID="ID_1733542887" CREATED="1649168876736" MODIFIED="1649168922732"/>
<node TEXT="(2) train on these images" ID="ID_272304925" CREATED="1649168923183" MODIFIED="1649168927908"/>
</node>
<node TEXT="A Fast and Efficient Conditional Learning for Tunable Trade-Off between Accuracy and Robustness" ID="ID_479073821" CREATED="1649623600225" MODIFIED="1649623653544">
<icon BUILTIN="pencil"/>
<icon BUILTIN="clanbomber"/>
<node TEXT="TODO" ID="ID_570405579" CREATED="1649623639892" MODIFIED="1649623641049"/>
</node>
<node TEXT="CASE-AWARE ADVERSARIAL TRAINING" ID="ID_1910807724" CREATED="1650842491224" MODIFIED="1650842493899">
<icon BUILTIN="pencil"/>
<node TEXT="Being selective to adversarial examples" ID="ID_1633971899" CREATED="1650842499542" MODIFIED="1650842511975">
<icon BUILTIN="idea"/>
</node>
<node TEXT="evaluation of this paper is problematic but the idea sounds." ID="ID_1579650742" CREATED="1650842522949" MODIFIED="1650842534822"/>
</node>
<node TEXT="Adaptive-Gravity: A Defense Against Adversarial Samples" ID="ID_828367154" CREATED="1652406798449" MODIFIED="1652406821812">
<icon BUILTIN="pencil"/>
<node TEXT="not conference" ID="ID_241729681" CREATED="1652406842767" MODIFIED="1652406845740"/>
</node>
</node>
<node TEXT="AT Speed" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1548520614" CREATED="1642441206809" MODIFIED="1647050848253">
<node TEXT="FreeAT (ATF, FAT)" ID="ID_10759055" CREATED="1642441224672" MODIFIED="1642455921912">
<node TEXT="adversarial training for free!" ID="ID_902115687" CREATED="1642441227881" MODIFIED="1642455925752">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="reuse computed gradients" ID="ID_540017524" CREATED="1642448873312" MODIFIED="1642448880329"/>
</node>
</node>
<node TEXT="N-FGSM" ID="ID_642231872" CREATED="1644180261664" MODIFIED="1644180264879">
<node TEXT="Make Some Noise: Reliable and Efficient Single-Step Adversarial Training" ID="ID_1929010940" CREATED="1644180270961" MODIFIED="1644180274378">
<icon BUILTIN="pencil"/>
<node TEXT="likely ICML submission" ID="ID_1360008454" CREATED="1644180275949" MODIFIED="1644180283810"/>
</node>
</node>
<node TEXT="Hard to say" ID="ID_560726891" CREATED="1652406214364" MODIFIED="1652406217083">
<node TEXT="Q-TART: Quickly Training for Adversarial Robustness and in-Transferability" ID="ID_1286071601" CREATED="1652406211048" MODIFIED="1652406212972">
<icon BUILTIN="pencil"/>
<node TEXT="the comparison looks problematic or lacking" ID="ID_139867221" CREATED="1652406219486" MODIFIED="1652406237251"/>
</node>
</node>
</node>
<node TEXT="AT Component" ID="ID_1828896049" CREATED="1651334135030" MODIFIED="1651334137719">
<node TEXT="BN" ID="ID_1742983557" CREATED="1651334138530" MODIFIED="1651334141052">
<node TEXT="On Fragile Features and Batch Normalization in Adversarial Training" ID="ID_1511392255" CREATED="1651334141871" MODIFIED="1651334150112">
<icon BUILTIN="pencil"/>
<node TEXT="adversarially fine-tuning just the BN layers can result in non-trivial adversarial robustness" ID="ID_1271377052" CREATED="1651334160028" MODIFIED="1651334176964"/>
</node>
</node>
</node>
<node TEXT="STAGE" ID="ID_219354908" CREATED="1642438393211" MODIFIED="1648173382480">
<node TEXT="adversarial robustness against the union of multiple perturbation models" ID="ID_1452347285" CREATED="1642438398542" MODIFIED="1648173382479">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="adversarial training and robustness for multiple perturbations" ID="ID_1832278453" CREATED="1642438422719" MODIFIED="1642438435302">
<icon BUILTIN="unchecked"/>
</node>
</node>
</node>
<node TEXT="Analysis on Adversarial Training" ID="ID_1446204242" CREATED="1648174101608" MODIFIED="1648174109263">
<font BOLD="true"/>
<node TEXT="What Do Adversarially trained Neural Networks Focus: A Fourier Domain-based Study" ID="ID_1576821107" CREATED="1648174139824" MODIFIED="1648174144917">
<icon BUILTIN="pencil"/>
<node TEXT="the difference mainly distribute in the low frequency region" ID="ID_1463528486" CREATED="1648174164987" MODIFIED="1648174174874"/>
<node TEXT="this looks like rejected CVPR paper" ID="ID_929525842" CREATED="1648174183328" MODIFIED="1648174192162"/>
</node>
<node TEXT="On the Loss Landscape of Adversarial Training:&#xa;Identifying Challenges and How to Overcome Them" ID="ID_1573445654" CREATED="1652385704648" MODIFIED="1652385714625">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS20" ID="ID_489847666" CREATED="1652385716393" MODIFIED="1652385718449"/>
<node TEXT="local Lipschitz" ID="ID_1085120369" CREATED="1652385719423" MODIFIED="1652385723167"/>
</node>
</node>
<node TEXT="Robust Generalization" LOCALIZED_STYLE_REF="styles.topic" ID="ID_125075162" CREATED="1643058798944" MODIFIED="1652390471003">
<node TEXT="Generalize better with adversarial attack / adversarial trianing" ID="ID_1604216423" CREATED="1648388384439" MODIFIED="1648388398758">
<icon BUILTIN="info"/>
</node>
<node TEXT="JHXY papers" ID="ID_360388475" CREATED="1646923800655" MODIFIED="1646923804350"/>
<node TEXT="LDS + VirtualAT" ID="ID_698882884" CREATED="1651779358642" MODIFIED="1652386374188">
<node TEXT="Distributional Smoothing with Virtual Adversarial Training" ID="ID_938557766" CREATED="1651779356728" MODIFIED="1651779367519">
<icon BUILTIN="unchecked"/>
<node TEXT="semi-supervised" ID="ID_1433077149" CREATED="1651779379379" MODIFIED="1651779382415"/>
<node TEXT="similar to adversarial training" ID="ID_803563320" CREATED="1651779371287" MODIFIED="1651779378875"/>
</node>
</node>
<node TEXT="VAT" ID="ID_696619029" CREATED="1652386387000" MODIFIED="1652386393651">
<node TEXT="Virtual Adversarial Training:&#xa;A Regularization Method for Supervised and&#xa;Semi-Supervised Learning" ID="ID_1462427172" CREATED="1652386394575" MODIFIED="1652386396842">
<icon BUILTIN="unchecked"/>
<node TEXT="TPAMI 17" ID="ID_143646316" CREATED="1652386397643" MODIFIED="1652386403583"/>
</node>
</node>
<node TEXT="Disentangling Adversarial Robustness and Generalization" ID="ID_761696774" CREATED="1652386633645" MODIFIED="1652386636131">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR19" ID="ID_1805008376" CREATED="1652386636930" MODIFIED="1652386638697"/>
<node TEXT="Hein" ID="ID_938433112" CREATED="1652386638888" MODIFIED="1652386641774"/>
</node>
<node TEXT="ICLR19" ID="ID_1404628587" CREATED="1652390504638" MODIFIED="1652390508707">
<icon BUILTIN="unchecked"/>
<node TEXT="Improving the Generalization of Adversarial Training with Domain Adaptation" ID="ID_829745669" CREATED="1652390509468" MODIFIED="1652390510277"/>
</node>
<node TEXT="A Closer Look at Accuracy vs. Robustness" ID="ID_258735997" CREATED="1652390011697" MODIFIED="1652390014362">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS20" ID="ID_1485726332" CREATED="1652390015200" MODIFIED="1652390016721"/>
<node TEXT="also local Lipschitz" ID="ID_840972996" CREATED="1652390016932" MODIFIED="1652390024926"/>
<node TEXT="refer LLR defense" ID="ID_479129206" CREATED="1652390025617" MODIFIED="1652390028534"/>
</node>
<node TEXT="RDA: Robust Domain Adaptation via Fourier Adversarial Attacking" ID="ID_1835144084" CREATED="1646923479664" MODIFIED="1646923763844">
<icon BUILTIN="unchecked"/>
<node TEXT="iccv21" ID="ID_1579318074" CREATED="1646923481418" MODIFIED="1646923482999"/>
<node TEXT="mitigate overfitting through adversarial attacks" ID="ID_559638588" CREATED="1646923815035" MODIFIED="1646923824471"/>
<node TEXT="propose fourier adversarial attacking for large magnitude perturbations" ID="ID_263843885" CREATED="1646923826789" MODIFIED="1646923848760"/>
</node>
<node TEXT="Robustness and Generalization via Generative Adversarial Training" ID="ID_949343144" CREATED="1652389512209" MODIFIED="1652389516457">
<icon BUILTIN="pencil"/>
<node TEXT="Serge" ID="ID_935059834" CREATED="1652389517309" MODIFIED="1652389521440"/>
<node TEXT="The evaluation settings do not look right" ID="ID_1277088623" CREATED="1652389530930" MODIFIED="1652389537782"/>
</node>
<node TEXT="ART-Point: Improving Rotation Robustness of Point Cloud Classifiers via Adversarial Rotation" ID="ID_893797795" CREATED="1648388362842" MODIFIED="1648388378080">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_685246872" CREATED="1648388379291" MODIFIED="1648388380602"/>
</node>
<node TEXT="AdvProp" ID="ID_465753517" CREATED="1651092418089" MODIFIED="1651092439619">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="Cihang xie" ID="ID_1667513983" CREATED="1643058809140" MODIFIED="1643058811907"/>
</node>
<node TEXT="Fast AdvProp" ID="ID_868830173" CREATED="1650809050717" MODIFIED="1651092436063">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="ICLR22, Alan" ID="ID_706047055" CREATED="1650809057964" MODIFIED="1650809065353"/>
</node>
<node TEXT="Adversarial Fine-tune with Dynamically Regulated Adversary" ID="ID_1310517906" CREATED="1652403651966" MODIFIED="1652403654504">
<icon BUILTIN="pencil"/>
<node TEXT="preserve accuracy" ID="ID_414891468" CREATED="1652403655341" MODIFIED="1652403660297"/>
<node TEXT="useful for robface project" ID="ID_1879091584" CREATED="1652403660508" MODIFIED="1652403674344">
<icon BUILTIN="info"/>
</node>
</node>
</node>
<node TEXT="Certified Robustness" ID="ID_636878532" CREATED="1641743555483" MODIFIED="1651777660583">
<node TEXT="Survey" ID="ID_1129138903" CREATED="1652390979779" MODIFIED="1652391000611">
<icon BUILTIN="mindmap"/>
<node TEXT="closely related to lipschitz" ID="ID_334887545" CREATED="1651777670185" MODIFIED="1651777679545">
<icon BUILTIN="info"/>
</node>
<node TEXT="https://github.com/AI-secure/Provable-Training-and-Verification-Approaches-Towards-Robust-Neural-Networks" ID="ID_1591662337" CREATED="1642964261893" MODIFIED="1642964261893" LINK="https://github.com/AI-secure/Provable-Training-and-Verification-Approaches-Towards-Robust-Neural-Networks"/>
</node>
<node TEXT="Lipschitz-Margin Training: Scalable Certification of Perturbation Invariance for Deep Neural Networks" ID="ID_1416035390" CREATED="1652390961778" MODIFIED="1652390964341">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS18" ID="ID_1784588171" CREATED="1652390965356" MODIFIED="1652390966833"/>
</node>
<node TEXT="PixelDP" ID="ID_108954320" CREATED="1652390832463" MODIFIED="1652390834709">
<node TEXT="Certified robustness to adversarial examples with differential privacy" ID="ID_1254880785" CREATED="1641743598637" MODIFIED="1641743613648">
<icon BUILTIN="unchecked"/>
<node TEXT="2018" OBJECT="java.lang.Long|2018" ID="ID_426962928" CREATED="1652390836806" MODIFIED="1652390838559"/>
</node>
</node>
<node TEXT="SoK: Certified Robustness for Deep Neural Networks" ID="ID_386941316" CREATED="1643035546515" MODIFIED="1643035632494">
<icon BUILTIN="pencil"/>
<icon BUILTIN="bookmark"/>
<node TEXT="TODO" ID="ID_703331117" CREATED="1643035647341" MODIFIED="1643035649305"/>
</node>
<node TEXT="Learning Smooth Neural Functions via Lipschitz Regularization" ID="ID_70062564" CREATED="1646943406180" MODIFIED="1646943409938">
<icon BUILTIN="unchecked"/>
<node TEXT="SIGGRAPH" ID="ID_913247296" CREATED="1646943412096" MODIFIED="1646943415898"/>
<node TEXT="not adv attack, but there is smooth constraint" ID="ID_1894803830" CREATED="1646943416298" MODIFIED="1646943426597"/>
</node>
<node TEXT="Random Smoothing" ID="ID_1727163116" CREATED="1652390572603" MODIFIED="1652390577552">
<node TEXT="Certified Adversarial Robustness via Randomized Smoothing" ID="ID_523090595" CREATED="1652390583126" MODIFIED="1652390735308">
<icon BUILTIN="unchecked"/>
<node TEXT="ICML19" ID="ID_1237827200" CREATED="1652390594113" MODIFIED="1652390596171"/>
<node TEXT="looks good on imagenet" ID="ID_1687581779" CREATED="1652390730831" MODIFIED="1652390733789"/>
</node>
<node TEXT="+adversarial training" ID="ID_158714235" CREATED="1652390791851" MODIFIED="1652390795357">
<node TEXT="Provably Robust Deep Learning via Adversarially&#xa;Trained Smoothed Classifiers" ID="ID_192772505" CREATED="1652390795970" MODIFIED="1652390797308">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS19" ID="ID_106543229" CREATED="1652390798152" MODIFIED="1652390807229"/>
</node>
</node>
</node>
<node TEXT="STN" ID="ID_221776785" CREATED="1648650992243" MODIFIED="1648650993535">
<node TEXT="Certified Adversarial Robustness with Additive Noise" ID="ID_501466420" CREATED="1648650110741" MODIFIED="1648650112904">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS19" ID="ID_1553192760" CREATED="1648650113913" MODIFIED="1648650116040"/>
<node TEXT="mnist, cifar10, imagenet" ID="ID_1012705291" CREATED="1648650116321" MODIFIED="1648650120446"/>
<node TEXT="(1) add gaussian noise" ID="ID_1869835989" CREATED="1648650121136" MODIFIED="1648650989490"/>
</node>
</node>
</node>
<node TEXT="Lipschitz Continuity" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1417870490" CREATED="1625980031432" MODIFIED="1651777687994">
<node TEXT="\latex $\|f(x)-f(y)\|_2 \leq L\|x-y\|_2$" ID="ID_351458712" CREATED="1630681515797" MODIFIED="1630693004280">
<node TEXT="L is called a lipschits constant" ID="ID_520258832" CREATED="1630693008693" MODIFIED="1630693015027"/>
</node>
<node TEXT="2018.12905: Lipschitz Continuity Guided Knowledge Distillation" ID="ID_467025696" CREATED="1630529969683" MODIFIED="1630529994985">
<icon BUILTIN="button_ok"/>
<node TEXT="lipschitz for neural network layers" ID="ID_759478799" CREATED="1630530162719" MODIFIED="1630530173957"/>
<node TEXT="transmitting matrix to replace SVD" ID="ID_1292515182" CREATED="1630530175016" MODIFIED="1630530183947"/>
<node TEXT="algorithm for calculating spectrum norm" ID="ID_664382898" CREATED="1630530237104" MODIFIED="1630530246050"/>
</node>
<node TEXT="Lipschitz regularity of deep neural networks:&#xa;analysis and efficient estimation" ID="ID_438196866" CREATED="1634059421008" MODIFIED="1634059422560">
<node TEXT="neurips" ID="ID_1164705869" CREATED="1634059426554" MODIFIED="1634059429951"/>
</node>
<node TEXT="SPECTRAL NORMALIZATION&#xa;FOR GENERATIVE ADVERSARIAL NETWORKS" ID="ID_1088164268" CREATED="1634059490998" MODIFIED="1634059492177"/>
<node TEXT="Exploring Generalization in Deep Learning" ID="ID_655518249" CREATED="1634060117942" MODIFIED="1634060119262"/>
<node TEXT="Distance-Based Classification with Lipschitz Functions" ID="ID_1705105058" CREATED="1634060148717" MODIFIED="1634060149837"/>
<node TEXT="Spectrally-normalized margin bounds for neural networks" ID="ID_930449787" CREATED="1634060189330" MODIFIED="1634060190422"/>
<node TEXT="EVALUATING THE ROBUSTNESS OF NEURAL NETWORKS: AN EXTREME VALUE THEORY APPROACH" ID="ID_506599121" CREATED="1634060341499" MODIFIED="1634060342665"/>
<node TEXT="Certified Defense via Latent Space Randomized&#xa;Smoothing with Orthogonal Encoders" ID="ID_1726121628" CREATED="1636487843145" MODIFIED="1636487844553"/>
<node TEXT="CLIP: Cheap Lipschitz Training&#xa;of Neural Networks&#xa;?" ID="ID_357548656" CREATED="1637078913169" MODIFIED="1637078917089">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Operator Norm regularization" ID="ID_1797178017" CREATED="1642951664771" MODIFIED="1651777720629">
<node TEXT="Spectrum norm is p=2 operator norm" ID="ID_1314274250" CREATED="1651777638137" MODIFIED="1651777646397">
<icon BUILTIN="info"/>
</node>
<node TEXT="leads to local Lipschitz continuity" ID="ID_780879026" CREATED="1651777866813" MODIFIED="1651777888036">
<icon BUILTIN="info"/>
</node>
<node TEXT="Spectral Norm Regularization for Improving the&#xa;Generalizability of Deep Learning" ID="ID_619307983" CREATED="1642951668899" MODIFIED="1642952085512">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<icon BUILTIN="pencil"/>
<node TEXT="arxiv" ID="ID_253754981" CREATED="1651777921998" MODIFIED="1651777923065"/>
<node TEXT="must read" ID="ID_680142634" CREATED="1642952052911" MODIFIED="1642952054401"/>
</node>
<node TEXT="Adversarial Training is a Form of Data-dependent&#xa;Operator Norm Regularization" ID="ID_180723104" CREATED="1642951686137" MODIFIED="1642951692337">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="NIPS" ID="ID_181517" CREATED="1651777914183" MODIFIED="1651777918254"/>
<node TEXT="must read" ID="ID_1563948349" CREATED="1642951699453" MODIFIED="1642951701325"/>
</node>
<node TEXT="Generalizable adversarial training vai spectral normalization" ID="ID_1226264876" CREATED="1651777849503" MODIFIED="1651777900024">
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR19" ID="ID_556802656" CREATED="1651777901080" MODIFIED="1651777904537">
<node TEXT="stanford" ID="ID_809462126" CREATED="1651777930713" MODIFIED="1651777932103"/>
</node>
</node>
<node ID="ID_1517611527" CREATED="1651777950303" MODIFIED="1651778003458">
<icon BUILTIN="pencil"/>
<icon BUILTIN="bookmark"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    A Closer Look at Accuracy vs. Robustness
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Local Linearization" ID="ID_1233851547" CREATED="1651777730897" MODIFIED="1651777734332">
<node TEXT="LLR" ID="ID_1146656651" CREATED="1649718768395" MODIFIED="1649718769633">
<node TEXT="Adversarial Robustness through Local Linearization" ID="ID_618965152" CREATED="1649718772985" MODIFIED="1649719241554">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="checked"/>
<node TEXT="NIPS2019" ID="ID_1911774428" CREATED="1649718783335" MODIFIED="1649718787286">
<node TEXT="CIFAR10" ID="ID_978997744" CREATED="1649718816814" MODIFIED="1649718818893">
<node TEXT="8/255" ID="ID_65937772" CREATED="1649718873312" MODIFIED="1649718880412"/>
</node>
<node TEXT="Imagenet" ID="ID_126765112" CREATED="1649718819060" MODIFIED="1649718820896">
<node TEXT="4/255 and 16/255" ID="ID_69038178" CREATED="1649718882447" MODIFIED="1649718889894"/>
</node>
</node>
<node TEXT="faster than standard AT" ID="ID_741473848" CREATED="1649718806757" MODIFIED="1649718813463"/>
<node TEXT="actually local lipschitz regularization" ID="ID_252839524" CREATED="1649718787519" MODIFIED="1651777694752">
<font BOLD="true"/>
</node>
</node>
</node>
</node>
<node TEXT="Activation function" ID="ID_958488576" CREATED="1642440628351" MODIFIED="1642440653680">
<node ID="ID_240078001" CREATED="1642440660898" MODIFIED="1642440669751">
<icon BUILTIN="unchecked"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <h1 http-equiv="content-type" content="text/html; charset=utf-8" class="title mathjax">
      Sorting out Lipschitz function approximation
    </h1>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Architecture" ID="ID_678438754" CREATED="1648336299559" MODIFIED="1648336302516">
<node TEXT="Enhancing Classifier Conservativeness and Robustness by Polynomiality" ID="ID_1098684448" CREATED="1648336306289" MODIFIED="1648336446986">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1434170291" CREATED="1648336449172" MODIFIED="1648336451497"/>
<node TEXT="change softmax function into softRmax" ID="ID_1077545148" CREATED="1648336452007" MODIFIED="1648336460004"/>
<node TEXT="robustness without gradient obfuscation or gradient masking" ID="ID_1071224180" CREATED="1648336465198" MODIFIED="1648336477406"/>
</node>
</node>
<node TEXT="Additional Data or self-supervised" ID="ID_1095198265" CREATED="1652386462667" MODIFIED="1652406991573">
<node TEXT="Unlabeled Data Improves Adversarial Robustness" ID="ID_743548425" CREATED="1652386466043" MODIFIED="1652386493938">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS19" ID="ID_759747469" CREATED="1652386494843" MODIFIED="1652386496590"/>
</node>
<node TEXT="Using Multiple Self-Supervised Tasks Improves Model Robustness" ID="ID_1872365850" CREATED="1652406997507" MODIFIED="1652406998193">
<node TEXT="ICLR22w" ID="ID_180057987" CREATED="1652406998970" MODIFIED="1652407001231"/>
</node>
</node>
<node TEXT="Bayesian Learning" ID="ID_680956486" CREATED="1652389099277" MODIFIED="1652389105519">
<node TEXT="Bayesian Adversarial Learning" ID="ID_1382395834" CREATED="1652389119775" MODIFIED="1652389122328">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS18" ID="ID_1370787734" CREATED="1652389123088" MODIFIED="1652389124598"/>
</node>
</node>
<node TEXT="Generative Replacement" ID="ID_814019873" CREATED="1642953253003" MODIFIED="1642953259726">
<node TEXT="Adversarially Robust Classification by Conditional Generative Model Inversion" ID="ID_811483312" CREATED="1642953266238" MODIFIED="1642953325665">
<icon BUILTIN="pencil"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="Reconstruct / Revert" ID="ID_855700329" CREATED="1647567976256" MODIFIED="1649187182307">
<font BOLD="true"/>
<node TEXT="delving into deep image prior for adversarial defense: a novel reconstruction based defense framework" ID="ID_1101165014" CREATED="1647567984454" MODIFIED="1647568005112">
<icon BUILTIN="unchecked"/>
<node TEXT="acm mm" ID="ID_1121719140" CREATED="1647568006277" MODIFIED="1647568007837"/>
</node>
<node TEXT="Reverse Engineering of Imperceptible Adversarial Image Perturbations" ID="ID_715406747" CREATED="1649178389420" MODIFIED="1649178397390">
<node TEXT="ICLR22" ID="ID_154190135" CREATED="1649181964456" MODIFIED="1649181966990">
<node TEXT="Yifan Gong" ID="ID_1397398867" CREATED="1649181968859" MODIFIED="1649181973066"/>
</node>
</node>
</node>
<node TEXT="Activation Intervene" ID="ID_1088268480" CREATED="1647266487705" MODIFIED="1647266501053">
<node TEXT="Improving Robustness of Convolutional Neural Networks Using Element-Wise Activation Scaling" ID="ID_1441795510" CREATED="1647266502455" MODIFIED="1647266508111">
<icon BUILTIN="pencil"/>
<node TEXT="some previous works uniformly scales the activation by a factor" ID="ID_232037888" CREATED="1647266510850" MODIFIED="1647266531034">
<icon BUILTIN="idea"/>
<node TEXT="reduces spectrum norm hence reducing the move distance" ID="ID_1718563172" CREATED="1647266533786" MODIFIED="1647266565135"/>
<node TEXT="however shrinking the whole space does not sound like something introducing robustness at once" ID="ID_796258217" CREATED="1647266565481" MODIFIED="1647266593801"/>
</node>
</node>
</node>
<node TEXT="Distillation" ID="ID_1398688048" CREATED="1647265149729" MODIFIED="1647265152310">
<node TEXT="Transferring Adversarial Robustness Through Robust Representation Matching" ID="ID_1195174599" CREATED="1647265153085" MODIFIED="1647265159703">
<icon BUILTIN="unchecked"/>
<node TEXT="USENIX22" ID="ID_492775537" CREATED="1647265161385" MODIFIED="1647265163646"/>
<node TEXT="adversarial trianing is too slow" ID="ID_282502797" CREATED="1647265173521" MODIFIED="1647265180634"/>
<node TEXT="student learns the robust representations from the teacher network" ID="ID_640049187" CREATED="1647265180865" MODIFIED="1647265202101"/>
</node>
</node>
<node TEXT="Late Fusion" ID="ID_1556837406" CREATED="1649633628459" MODIFIED="1649633630829">
<node TEXT="Training-Free Robust Multimodal Learning via Sample-Wise Jacobian Regularization" ID="ID_266079583" CREATED="1649633640001" MODIFIED="1649633641760">
<icon BUILTIN="pencil"/>
<node TEXT="looks like rejected ECCV" ID="ID_1470248094" CREATED="1649633642769" MODIFIED="1649633646853"/>
</node>
</node>
<node TEXT="Patch Defense" ID="ID_1143099045" CREATED="1648180904182" MODIFIED="1648180907628">
<node TEXT="Towards Practical Certifiable Patch Defense with Vision Transformer" ID="ID_881632170" CREATED="1648180952088" MODIFIED="1648180959464">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_379980426" CREATED="1648180955529" MODIFIED="1648180957199">
<node TEXT="CIFAR10" ID="ID_1111330517" CREATED="1648260769781" MODIFIED="1648260772173"/>
<node TEXT="ILSVRC2012" ID="ID_1681375880" CREATED="1648260772525" MODIFIED="1648260774984"/>
</node>
<node TEXT="existing methods sacrifice clean accuracy" ID="ID_1058867948" CREATED="1648260201789" MODIFIED="1648260211829"/>
<node TEXT="derandomized smoothing with vision transformer" ID="ID_1531792988" CREATED="1648260239300" MODIFIED="1648260251088"/>
<node TEXT="reconstruct the architecture of the ViT" ID="ID_919736557" CREATED="1648260337506" MODIFIED="1648260350021"/>
<node TEXT="significant certified performance increase" ID="ID_1314933884" CREATED="1648260356330" MODIFIED="1648260367449"/>
</node>
<node TEXT="Segment and Complete: Defending Object Detectors against Adversarial Patch&#xa;Attacks with Robust Patch Detection" ID="ID_1711043287" CREATED="1651106458637" MODIFIED="1651106460975">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22, Rama" ID="ID_791015543" CREATED="1651106462507" MODIFIED="1651106466241"/>
<node TEXT="Object Detection" ID="ID_913953101" CREATED="1651106467157" MODIFIED="1651106469805"/>
</node>
<node TEXT="Certified Patch Robustness via Smoothed&#xa;Vision Transformers" ID="ID_1393882387" CREATED="1652400808342" MODIFIED="1652400810377">
<icon BUILTIN="unchecked"/>
<node TEXT="Madry" ID="ID_863710066" CREATED="1652400811169" MODIFIED="1652400813869"/>
</node>
</node>
<node TEXT="Pruning" ID="ID_391041991" CREATED="1649633795729" MODIFIED="1649633798101">
<node TEXT="Masking Adversarial Damage: Finding Adversarial Saliency for Robust and Sparse Network" ID="ID_688752599" CREATED="1649633799104" MODIFIED="1649633804914">
<node TEXT="CVPR22" ID="ID_717543729" CREATED="1649633805807" MODIFIED="1649633807168"/>
</node>
</node>
<node TEXT="Defend Natural Neural net" ID="ID_450620251" CREATED="1652391201216" MODIFIED="1652391212735">
<node TEXT="Energy-based" ID="ID_178062976" CREATED="1652391213403" MODIFIED="1652391216434">
<node TEXT="EBM" ID="ID_736436137" CREATED="1652391219838" MODIFIED="1652391221731">
<node TEXT="Stochastic Security: Adversarial Defense Using Long-Run Dynamics of Energy-Based Models" ID="ID_1034618598" CREATED="1652391217228" MODIFIED="1652391217918">
<node TEXT="ICLR21" ID="ID_1031214705" CREATED="1652391223926" MODIFIED="1652391226381"/>
<node TEXT="CIFAR10, SVHN, CIFAR100" ID="ID_546101918" CREATED="1652391230653" MODIFIED="1652391239968"/>
<node TEXT="does not look like generalizing to ImageNet" ID="ID_1853413986" CREATED="1652391241476" MODIFIED="1652391248135"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Adversarial Defense: Intersection" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_1324883012" CREATED="1641521506805" MODIFIED="1647265134040">
<edge COLOR="#0000ff"/>
<font NAME="Gentium"/>
<node TEXT="Contrastive Learning" ID="ID_230158987" CREATED="1649169110339" MODIFIED="1649169131686">
<font BOLD="true"/>
<node TEXT="Task-Agnostic Robust Representation Learning" ID="ID_1035556100" CREATED="1649169241964" MODIFIED="1649169251838">
<icon BUILTIN="pencil"/>
<node TEXT="Philip Torr" ID="ID_461361807" CREATED="1649169244394" MODIFIED="1649169250158"/>
<node TEXT="Robustness at the representation level" ID="ID_1452405899" CREATED="1649169266414" MODIFIED="1649169273770"/>
</node>
</node>
<node TEXT="Face Recognition" ID="ID_1083375375" CREATED="1643132265039" MODIFIED="1649102907633">
<font BOLD="true"/>
<node TEXT="RobFR: Benchmarking Adversarial Robustness on Face Recognition" ID="ID_765372057" CREATED="1642726609490" MODIFIED="1642726640122">
<icon BUILTIN="pencil"/>
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="RobFR" ID="ID_956441703" CREATED="1643132279319" MODIFIED="1643132284675">
<node TEXT="https://github.com/ShawnXYang/Face-Robustness-Benchmark" ID="ID_232276308" CREATED="1643132270554" MODIFIED="1643132270554" LINK="https://github.com/ShawnXYang/Face-Robustness-Benchmark"/>
</node>
<node TEXT="Powerful Physical Adversarial Examples Against Practical Face Recognition Systems" ID="ID_1220714793" CREATED="1649102916078" MODIFIED="1649102921474">
<icon BUILTIN="unchecked"/>
<node TEXT="WACV22" ID="ID_1744027013" CREATED="1649102922446" MODIFIED="1649102925180"/>
<node TEXT="physical face attack" ID="ID_624278487" CREATED="1649102935226" MODIFIED="1649102939514"/>
</node>
<node TEXT="Face Verification Bypass" ID="ID_1487209530" CREATED="1649103065074" MODIFIED="1649103067123">
<icon BUILTIN="pencil"/>
<node TEXT="looks like course project" ID="ID_683520176" CREATED="1649103068291" MODIFIED="1649103072896"/>
</node>
</node>
<node TEXT="Deep Metric Learning" ID="ID_818044827" CREATED="1649169031775" MODIFIED="1649169036193">
<font BOLD="true"/>
<node TEXT="see Track: AdvRank" ID="ID_442150394" CREATED="1647049171530" MODIFIED="1650810568792"/>
</node>
<node TEXT="3D/PCL" ID="ID_259895259" CREATED="1649103944879" MODIFIED="1649103947165">
<node TEXT="Robust Structured Declarative Classifiers for 3D Point Clouds: Defending Adversarial Attacks with Implicit Gradients" ID="ID_1085580757" CREATED="1649103947887" MODIFIED="1649103955450">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Audio" ID="ID_1537810138" CREATED="1652405551815" MODIFIED="1652405553184">
<node TEXT="From Environmental Sound Representation to Robustness of 2D CNN Models Against Adversarial Attacks" ID="ID_1291962884" CREATED="1652405557694" MODIFIED="1652405558165"/>
</node>
<node TEXT="Deepfake Detection" ID="ID_1986728438" CREATED="1646948353156" MODIFIED="1646948357613">
<node TEXT="Towards Adversarially Robust Deepfake Detection: An Ensemble Approach" ID="ID_1602704665" CREATED="1646948358406" MODIFIED="1646948364713">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Generative" ID="ID_1297276236" CREATED="1649630202035" MODIFIED="1649630208049">
<node TEXT="Adversarially robust segmentation models learn perceptually-aligned gradients" ID="ID_1579052965" CREATED="1649630171570" MODIFIED="1649630175331">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Towards Understanding the Generative Capability of&#xa;Adversarially Robust Classifiers" ID="ID_1415803889" CREATED="1652391381081" MODIFIED="1652391391590">
<icon BUILTIN="unchecked"/>
<node TEXT="ICCV21" ID="ID_1011049112" CREATED="1652391392316" MODIFIED="1652391393701"/>
</node>
</node>
<node TEXT="Hashing" ID="ID_254620335" CREATED="1652404685642" MODIFIED="1652404687334">
<node TEXT="Centralized Adversarial Learning for Robust Deep Hashing" ID="ID_1749995901" CREATED="1652404688365" MODIFIED="1652404695527">
<icon BUILTIN="pencil"/>
<node TEXT="what&apos;s its relationship to advrank, robrank, robdml?" ID="ID_299223389" CREATED="1652404703545" MODIFIED="1652404717797">
<icon BUILTIN="info"/>
</node>
</node>
</node>
<node TEXT="Image Denoising" ID="ID_688438108" CREATED="1642369253403" MODIFIED="1642965597652">
<node TEXT="Towards adversarially robust deep image denoising" ID="ID_912787227" CREATED="1642369258886" MODIFIED="1642369271962">
<icon BUILTIN="pencil"/>
<node TEXT="present observation-based zero-mean attack" ID="ID_948789581" CREATED="1642369294389" MODIFIED="1642369302053"/>
<node TEXT="present hybrid adversarial training" ID="ID_409590623" CREATED="1642369302455" MODIFIED="1642369331464">
<node TEXT="using augmented data" ID="ID_632082886" CREATED="1642369349160" MODIFIED="1642369354932"/>
</node>
</node>
</node>
<node TEXT="Multi-Task learning" ID="ID_1182441257" CREATED="1642442091094" MODIFIED="1642965600653">
<node TEXT="Adversarial Robustness in Multi-Task Learning: Promises and Illusions" ID="ID_429718797" CREATED="1642442095758" MODIFIED="1642442212412">
<icon BUILTIN="checked"/>
<node TEXT="AAAI22" ID="ID_1679691953" CREATED="1642442198116" MODIFIED="1642442200599"/>
<node TEXT="blindly adding auxiliary tasks or weighing the tasks provides a false sense of robustness." ID="ID_459008675" CREATED="1642442206193" MODIFIED="1642442252721"/>
<node TEXT="the choice of the task to incorporate in the loss function are important factors that can be leveraged to yield more robust models" ID="ID_1336811666" CREATED="1642442253918" MODIFIED="1642442277862"/>
</node>
</node>
<node TEXT="Object Detection" ID="ID_1293793866" CREATED="1649169048366" MODIFIED="1649169051401">
<node TEXT="Defending From Physically-Realizable Adversarial Attacks Through Internal Over-Activation Analysis" ID="ID_1601765509" CREATED="1649169051973" MODIFIED="1649169053897">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Defending Against Person Hiding Adversarial Patch Attack with a Universal White Frame" ID="ID_1354705736" CREATED="1652403552952" MODIFIED="1652403560360">
<icon BUILTIN="pencil"/>
<node TEXT="rejected by NIPS21" ID="ID_1706619708" CREATED="1652403561081" MODIFIED="1652403565159"/>
</node>
</node>
<node TEXT="Openset Recognition" ID="ID_417719416" CREATED="1646947995726" MODIFIED="1646948002223">
<node TEXT="Open-set Adversarial Defense with Clean-Adversarial Mutual Learning" ID="ID_109010590" CREATED="1646948002881" MODIFIED="1646948362746">
<icon BUILTIN="unchecked"/>
<node TEXT="IJCV" ID="ID_1364434112" CREATED="1646948008562" MODIFIED="1646948010165"/>
</node>
</node>
<node TEXT="Reinforcement Learning" ID="ID_628850521" CREATED="1647267813992" MODIFIED="1647267819607">
<node TEXT="SOUND ADVERSARIAL AUDIO-VISUAL NAVIGATION" ID="ID_1280103442" CREATED="1647267824706" MODIFIED="1647267830036">
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR22" ID="ID_197663134" CREATED="1647267826223" MODIFIED="1647267827774"/>
</node>
</node>
<node TEXT="Text Classifiation" ID="ID_1972370487" CREATED="1652383484193" MODIFIED="1652383511660">
<node TEXT="Adversarial Training Methods for Semi-Supervised Text Classification" ID="ID_388847845" CREATED="1652383489196" MODIFIED="1652383507765">
<node TEXT="ICLR17" ID="ID_597381726" CREATED="1652383513623" MODIFIED="1652383517724"/>
<node TEXT="Ian G." ID="ID_638274130" CREATED="1652383520276" MODIFIED="1652383522779"/>
</node>
</node>
<node TEXT="Unsupervised Domain Adaptation" ID="ID_1641764072" CREATED="1647265453252" MODIFIED="1647265458995">
<node TEXT="Exploring Adversarially Robust Training for Unsupervised Domain Adaptation" ID="ID_1050578370" CREATED="1647265459656" MODIFIED="1647265470596">
<icon BUILTIN="pencil"/>
<node TEXT="Vishal" ID="ID_707250183" CREATED="1647265465095" MODIFIED="1647265466451"/>
</node>
</node>
</node>
<node TEXT="Classification" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_98760508" CREATED="1642438760894" MODIFIED="1642438768351">
<edge COLOR="#7c7c00"/>
<node TEXT="MLP" ID="ID_636086784" CREATED="1648181297496" MODIFIED="1648181311232">
<font BOLD="true"/>
<node TEXT="MAE" ID="ID_436521303" CREATED="1648181301355" MODIFIED="1648181305556">
<node TEXT="kaiming" ID="ID_1886584456" CREATED="1648181306970" MODIFIED="1648181308419"/>
</node>
</node>
<node TEXT="Convolutional" ID="ID_1658183940" CREATED="1578642663386" MODIFIED="1647052962389">
<font NAME="Gentium" BOLD="true"/>
<node TEXT="AlexNet" ID="ID_1739289131" CREATED="1583301886777" MODIFIED="1587526865018">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Inception" ID="ID_398924458" CREATED="1583301891146" MODIFIED="1587526865018">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="VGG" ID="ID_814941213" CREATED="1587736264512" MODIFIED="1587736265783"/>
<node TEXT="ResNet" ID="ID_1353740519" CREATED="1578642692753" MODIFIED="1587526865019">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="WideResNet" ID="ID_758079042" CREATED="1642438822215" MODIFIED="1642438825478">
<node TEXT="Wide residual networks" ID="ID_1046817230" CREATED="1642438826280" MODIFIED="1642438829049">
<icon BUILTIN="unchecked"/>
<node TEXT="BMVC" ID="ID_899326377" CREATED="1642438830435" MODIFIED="1642438832022"/>
</node>
</node>
<node TEXT="DenseNet" ID="ID_1895941471" CREATED="1587736276618" MODIFIED="1587736279489"/>
<node TEXT="MnasNet" ID="ID_1730669639" CREATED="1645044794503" MODIFIED="1645044796316"/>
<node TEXT="ResNeXT" ID="ID_1649086845" CREATED="1587736266999" MODIFIED="1587736272048">
<node TEXT="1912.12165: ResNetX: a more disordered and deeper network architecture" ID="ID_186791272" CREATED="1591523319161" MODIFIED="1591523330569"/>
</node>
<node TEXT="ResNeST" ID="ID_1760624702" CREATED="1587736272364" MODIFIED="1587736275764">
<node TEXT="2004.08955: resnest: split-attention networks" ID="ID_1911521356" CREATED="1587736301189" MODIFIED="1587736313392"/>
</node>
<node TEXT="SAN (attention)" ID="ID_679867425" CREATED="1588156186802" MODIFIED="1588156191656">
<node TEXT="2004.13621: exploring self-attention for image recognition (CVPR20)" ID="ID_1562997206" CREATED="1588156197557" MODIFIED="1588156222403">
<icon BUILTIN="button_ok"/>
<node TEXT="imagenet: 79 / 94" ID="ID_427959757" CREATED="1590746403401" MODIFIED="1590746414048"/>
</node>
</node>
<node TEXT="Mobile / Size" ID="ID_898062646" CREATED="1587874073174" MODIFIED="1587874096857">
<node TEXT="SqueezeNet" ID="ID_467799444" CREATED="1587736289183" MODIFIED="1587736291729"/>
<node TEXT="ShuffleNet" ID="ID_1464600180" CREATED="1587736285399" MODIFIED="1587736288612"/>
<node TEXT="2004.08796: MicroDenseNet: when residual learning meets dense aggregation: rethinking the aggregation of deep neural networks" ID="ID_1947283650" CREATED="1587874102107" MODIFIED="1587874133889"/>
<node TEXT="MNasNet" ID="ID_569027083" CREATED="1610423796313" MODIFIED="1610423799445"/>
</node>
<node TEXT="EfficientNet" ID="ID_1080342228" CREATED="1610423804197" MODIFIED="1610423806621">
<node TEXT="(NAS)" ID="ID_438598210" CREATED="1610423811764" MODIFIED="1610423818305"/>
</node>
<node TEXT="EfficientNet v2" ID="ID_1698384616" CREATED="1617628875847" MODIFIED="1617628880397">
<node TEXT="EfficientNetV2: Smaller Models and Faster Training" ID="ID_830544397" CREATED="1617628885983" MODIFIED="1617628887839"/>
</node>
<node TEXT="RepVGG" ID="ID_193953382" CREATED="1610423823055" MODIFIED="1610423825133">
<node TEXT="2101: RepVGG: Making VGG-style ConvNets Great Again" ID="ID_90567394" CREATED="1610423826848" MODIFIED="1610424005147">
<icon BUILTIN="bookmark"/>
<node TEXT="only 3x3 convolution and relu, due to highly optimized convolution kernels in cudnn and mkl" ID="ID_1136570214" CREATED="1610424029790" MODIFIED="1610424056958"/>
</node>
</node>
<node TEXT="Manual Network Arch Search" ID="ID_1644165991" CREATED="1590798285515" MODIFIED="1590798297350">
<node TEXT="2003.03828: &#x3a0;&#x2212;nets: Deep Polynomial Neural Networks (cvpr20)" ID="ID_1971909483" CREATED="1590798302462" MODIFIED="1590798316921"/>
<node TEXT="2003.13866: Dataless Model Selection with the Deep Frame Potential (cvpr20 oral)" ID="ID_1001428483" CREATED="1590749590119" MODIFIED="1590749604523">
<node TEXT="Manual network Architecture search" ID="ID_906467454" CREATED="1590749906309" MODIFIED="1590749914137"/>
</node>
<node TEXT="RepLKNet" ID="ID_1993802123" CREATED="1649130309608" MODIFIED="1649130316321">
<font BOLD="true"/>
<node TEXT="Scaling Up Your Kernels to 31x31: Revisiting Large Kernel Design in CNNs" ID="ID_303103818" CREATED="1649130250951" MODIFIED="1649130256171">
<node TEXT="CVPR22" ID="ID_1730768826" CREATED="1649130257812" MODIFIED="1649130259170">
<node TEXT="Jian Sun" ID="ID_1653020634" CREATED="1649130409044" MODIFIED="1649130411134">
<node TEXT="Xiangyu Zhang" ID="ID_1209636585" CREATED="1649130422447" MODIFIED="1649130425362"/>
</node>
</node>
<node TEXT="shrink the performance gap to swin transformer using CNN" ID="ID_738786023" CREATED="1649130342676" MODIFIED="1649130358342"/>
<node TEXT="in contrast to small-kernel CNNs, large-kernel CNNs have much larger effective receptive fields and higher shape bias rather than texture bias." ID="ID_739960812" CREATED="1649130317521" MODIFIED="1649130382485"/>
</node>
</node>
</node>
</node>
<node TEXT="Transformer" ID="ID_1624253019" CREATED="1642438779008" MODIFIED="1642953058542">
<font BOLD="true"/>
<node TEXT="Attention Mechanism" ID="ID_813294848" CREATED="1618848447541" MODIFIED="1645045062292">
<icon BUILTIN="info"/>
<node TEXT="transformer leverages the attention mechanism" ID="ID_1216045906" CREATED="1645045014525" MODIFIED="1645045025091"/>
<node TEXT="transformer is the first tarnsduction model relying entirely on self-attention to compute representations of its input and output without using sequence aligned RNNs or convolution" ID="ID_968856136" CREATED="1645045025325" MODIFIED="1645045056171"/>
<node TEXT="Learning Long-term Dependencies Using Cognitive Inductive Biases in Self-attention RNNs (bengio)" ID="ID_1557678117" CREATED="1618848455421" MODIFIED="1645045075829">
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="Image Transformer (ViT)" ID="ID_585506553" CREATED="1652404976637" MODIFIED="1652404983160">
<node TEXT="Image Transformer" ID="ID_1634333693" CREATED="1652404984422" MODIFIED="1652404989054">
<node TEXT="ICML18" ID="ID_158873981" CREATED="1652404989904" MODIFIED="1652404994222"/>
</node>
</node>
<node TEXT="Self-Attention" ID="ID_9436642" CREATED="1652405630017" MODIFIED="1652405634186">
<node TEXT="Stand-Alone Self-Attention in Vision Models" ID="ID_1255591840" CREATED="1652405635039" MODIFIED="1652405640666"/>
</node>
<node TEXT="LocalViT" ID="ID_325620572" CREATED="1652405624037" MODIFIED="1652405627728">
<node TEXT="2104: LocalViT: Bringing Locality to Vision Transformers" ID="ID_1986446305" CREATED="1619337180035" MODIFIED="1619337196848"/>
</node>
<node TEXT="Swin Transformer" ID="ID_180866711" CREATED="1642393909569" MODIFIED="1642393914798">
<node TEXT="Swin Transformer: Hierarchical Vision Transformer using Shifted Windows" ID="ID_828104298" CREATED="1647053000391" MODIFIED="1647053007099">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="iccv21 best paper" ID="ID_1559238235" CREATED="1645044840627" MODIFIED="1645044844407"/>
</node>
</node>
<node TEXT="Acceleration / Efficiency" ID="ID_870822946" CREATED="1649101702095" MODIFIED="1649101708626">
<node TEXT="Automated Progressive Learning for Efficient Training of Vision Transformers" ID="ID_375053278" CREATED="1649101709513" MODIFIED="1649101715507">
<node TEXT="CVPR22" ID="ID_1580181622" CREATED="1649101716937" MODIFIED="1649101718166"/>
</node>
</node>
<node TEXT="Adaptive Split-Fusion Transformer" ID="ID_315100861" CREATED="1652401232769" MODIFIED="1652401236117">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="MiniViT" ID="ID_1122712443" CREATED="1652406434654" MODIFIED="1652406437076">
<node TEXT="MiniViT: Compressing Vision Transformers with Weight Multiplexing" ID="ID_1344779054" CREATED="1652406431608" MODIFIED="1652406432695">
<node TEXT="CVPR22" ID="ID_158269939" CREATED="1652406439183" MODIFIED="1652406440398"/>
</node>
</node>
</node>
<node TEXT="Capsule Net" ID="ID_1659335587" CREATED="1645045179791" MODIFIED="1645045184178">
<node TEXT="quantum capsule networks" ID="ID_1165020573" CREATED="1641523224758" MODIFIED="1641523259186">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Loss Function" ID="ID_1059926176" CREATED="1642438781602" MODIFIED="1642438785478">
<node TEXT="Cross Entropy (Softmax + NLL)" ID="ID_172299886" CREATED="1642949643007" MODIFIED="1647265748825"/>
<node TEXT="Focal Loss" ID="ID_1080205774" CREATED="1582943123957" MODIFIED="1587526865020">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="works well when there is imbalance in the number of training samples in each class, such as in long-tail cases" ID="ID_573374922" CREATED="1647265757735" MODIFIED="1647265781110"/>
</node>
<node TEXT="Relational Surrogate Loss Learning" ID="ID_1946575254" CREATED="1646942717232" MODIFIED="1646942719745">
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR22" ID="ID_206379015" CREATED="1646942721247" MODIFIED="1646942722686"/>
</node>
<node TEXT="2002.09437: calibrating deep neural networks using focal loss" ID="ID_1015553994" CREATED="1582942837140" MODIFIED="1587526865020">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: analysis on focal loss" ID="ID_1741336699" CREATED="1582943021519" MODIFIED="1587526864918">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: miscalibration -- mismatch between a network&apos;s prediction and confidence" ID="ID_408342363" CREATED="1582943050142" MODIFIED="1587526865021">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: experiment and analysis" ID="ID_1293300308" CREATED="1582943078917" MODIFIED="1587526865021">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1. focal loss + temprature scaling (distillation?) yields best model" ID="ID_1334742972" CREATED="1582943088656" MODIFIED="1587526865022">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="2004.09805: AMC-Loss: Angular Margin contrastive loss for improved explainability in image classificatoin" ID="ID_47403602" CREATED="1587527702351" MODIFIED="1587527731932">
<node TEXT="Auxiliary losses like center loss, contrastive loss, and triplet loss do not take into account the intrinsic angular distribution exhibited by the low- and high-level feature representations" ID="ID_344420536" CREATED="1587527746821" MODIFIED="1587527801702"/>
</node>
<node TEXT="2004.11362: supervised contrastive learning" ID="ID_1359274675" CREATED="1587733099040" MODIFIED="1587733132435">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="idea"/>
<node TEXT="inspiration from self-supervised learning" ID="ID_321932978" CREATED="1587733116442" MODIFIED="1587733126175"/>
<node TEXT="modify batch contrastive loss" ID="ID_1727969225" CREATED="1587733150878" MODIFIED="1587733158022"/>
</node>
<node TEXT="2104: Orthogonal Projection Loss" ID="ID_1777245204" CREATED="1619588465371" MODIFIED="1619588478176">
<icon BUILTIN="button_ok"/>
<node TEXT="orthogonal restriction" ID="ID_1997736262" CREATED="1619588556419" MODIFIED="1619588564023"/>
</node>
<node TEXT="Cyclical Focal Loss" ID="ID_478118261" CREATED="1647265797112" MODIFIED="1647265803109">
<icon BUILTIN="pencil"/>
<node TEXT="extension to focal loss" ID="ID_942111109" CREATED="1647265807566" MODIFIED="1647265821995"/>
</node>
</node>
<node TEXT="Long-Tail" ID="ID_314928185" CREATED="1649104418933" MODIFIED="1649104421848">
<node TEXT="Nested Collaborative Learning for Long-Tailed Visual Recognition" ID="ID_908771433" CREATED="1649104478693" MODIFIED="1649104479517">
<node TEXT="CVPR22" ID="ID_1585249354" CREATED="1649104480327" MODIFIED="1649104481946"/>
</node>
</node>
<node TEXT="Semi-Supervised" ID="ID_831162860" CREATED="1649168478733" MODIFIED="1649168482062">
<node TEXT="SimMatch: Semi-supervised Learning with Similarity Matching" ID="ID_1319700790" CREATED="1649168482746" MODIFIED="1649168490533">
<node TEXT="CVPR22" ID="ID_1919676331" CREATED="1649168491873" MODIFIED="1649168493259"/>
</node>
<node TEXT="Distributional Smoothing with Virtual Adversarial Training" ID="ID_926635272" CREATED="1651779266264" MODIFIED="1651779346626">
<icon BUILTIN="unchecked"/>
</node>
</node>
<node TEXT="Few-Shot" ID="ID_186985544" CREATED="1649628667512" MODIFIED="1649628669364">
<node TEXT="Matching Feature Sets for Few-Shot Image Classification" ID="ID_1191271272" CREATED="1649628671810" MODIFIED="1649628677111">
<node TEXT="CVPR22" ID="ID_471448143" CREATED="1649628678874" MODIFIED="1649628680449"/>
<node TEXT="a set-based representation intrinsitically builds a richer representation of images from the base classes, which can subsequently better transfer to the few-shot classes." ID="ID_1034599613" CREATED="1649628688797" MODIFIED="1649628722058"/>
</node>
</node>
<node TEXT="Metrics" ID="ID_192942420" CREATED="1650818395071" MODIFIED="1650818396898">
<node TEXT="Machine Learning State-of-the-Art with Uncertainties" ID="ID_1885109529" CREATED="1650818397628" MODIFIED="1650818404166">
<node TEXT="ICLR22" ID="ID_1915085190" CREATED="1650818405061" MODIFIED="1650818406686"/>
</node>
</node>
<node TEXT="Modality" ID="ID_1720917515" CREATED="1652404465688" MODIFIED="1652404467605">
<node TEXT="iCAR: Bridging Image Classification and Image-text Alignment for Visual Recognition" ID="ID_675153263" CREATED="1652404468560" MODIFIED="1652404474604">
<icon BUILTIN="pencil"/>
<node TEXT="MSRA" ID="ID_418050697" CREATED="1652404475280" MODIFIED="1652404476614"/>
</node>
</node>
<node TEXT="Regularization" ID="ID_277201350" CREATED="1652383343786" MODIFIED="1652383346875">
<node TEXT="Data Augmentation" ID="ID_1121776606" CREATED="1643057931229" MODIFIED="1643057946544">
<node ID="ID_1264772918" CREATED="1643057954153" MODIFIED="1643057958105">
<icon BUILTIN="unchecked"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    On Feature Normalization and Data Augmentation
  </body>
</html>
</richcontent>
<node TEXT="cvpr21" ID="ID_109505516" CREATED="1643057959966" MODIFIED="1643057961249"/>
<node TEXT="the moments are often removed (e.g. mean and stdev)" ID="ID_776849456" CREATED="1643057961500" MODIFIED="1643057991230"/>
<node TEXT="but moments extracted from instance normalization and positional normalization can roughly capture style and shape information of an image" ID="ID_1102689926" CREATED="1643057991834" MODIFIED="1643058019401"/>
</node>
<node TEXT="Deep AutoAugment" ID="ID_1383049991" CREATED="1649130129509" MODIFIED="1649130129980">
<node TEXT="ICLR22" ID="ID_1196117385" CREATED="1649130131526" MODIFIED="1649130135185"/>
</node>
</node>
<node TEXT="Regularization" ID="ID_1344390864" CREATED="1652401123503" MODIFIED="1652401127762">
<node TEXT="Regularizing Neural Networks via Adversarial Model Perturbation" ID="ID_321938074" CREATED="1652383357338" MODIFIED="1652383359351">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR21" ID="ID_1992628429" CREATED="1652383360278" MODIFIED="1652383361397"/>
</node>
</node>
<node TEXT="Natural Corruption" ID="ID_28401889" CREATED="1652401091594" MODIFIED="1652401143417">
<node TEXT="Deeper Insights into ViTs Robustness towards&#xa;Common Corruptions" ID="ID_567021160" CREATED="1652401144086" MODIFIED="1652401168378">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Understanding The Robustness in Vision Transformers" ID="ID_255240365" CREATED="1652403100297" MODIFIED="1652403107057">
<icon BUILTIN="pencil"/>
</node>
</node>
</node>
</node>
<node TEXT="Deep Metric Learning" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_464750159" CREATED="1641242970936" MODIFIED="1642456761439">
<edge COLOR="#ff00ff"/>
<font NAME="Gentium"/>
<node TEXT="Survey" ID="ID_1192003468" CREATED="1641520461677" MODIFIED="1648175016651">
<icon BUILTIN="info"/>
<font NAME="Gentium"/>
<node TEXT="2002.08473: revisiting training strategies and generalization performance in deep metric learning" ID="ID_585062864" CREATED="1582857197676" MODIFIED="1651955230479">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="revisit most widely used DML objective functions" ID="ID_1601875277" CREATED="1582857225897" MODIFIED="1587526865066">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="study parameter choices and batch sampling" ID="ID_1014314628" CREATED="1582857260610" MODIFIED="1587526865067">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="propose simple, yet effective training regularization method" ID="ID_1639876989" CREATED="1582857276291" MODIFIED="1587526865068">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="randomly perform a switch operation within tuples by exchanging negative samples x_n with the positive x_p in a given ranking loss formulation with fixed probability." ID="ID_1882096693" CREATED="1582859272250" MODIFIED="1587526865069">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="this regularization pushes the samples of the same class apart, thus enabling a DML model to capture extra non-balel discriminative features. Simultaneously, this process dampens the compression included by strong discriminative training signals." ID="ID_31907685" CREATED="1582859349016" MODIFIED="1587526865070">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="conclusions" ID="ID_1270249032" CREATED="1582858876452" MODIFIED="1587526865071">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="Generalization performance in DML exhibits strong inverse correlation to the decay of the singulra value spectrum of a learned representation." ID="ID_256125729" CREATED="1582858880428" MODIFIED="1587526865071">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="A Metric Learning Reality Check" ID="ID_894569151" CREATED="1622801160347" MODIFIED="1651955232297">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="ECCV20" ID="ID_606475431" CREATED="1648173389648" MODIFIED="1648173392107"/>
<node TEXT="flaws in experimental methodology" ID="ID_506127366" CREATED="1622801174483" MODIFIED="1622801197501"/>
<node TEXT="actual improvements over time have been marginal at best" ID="ID_116707694" CREATED="1622801198120" MODIFIED="1622801209887"/>
</node>
<node TEXT="Do Different Deep Metric Learning Losses Lead to Similar Learned Features?" ID="ID_1616573694" CREATED="1651953803779" MODIFIED="1651955233765">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="ICCV21" ID="ID_454000109" CREATED="1651953811961" MODIFIED="1651953813423"/>
<node TEXT="Is the follow-up of the above two" ID="ID_405385083" CREATED="1651954108419" MODIFIED="1651954115324"/>
<node TEXT="(1) ranking based and classification based losses guide the network to learn different features" ID="ID_262045487" CREATED="1651955243876" MODIFIED="1651955265916"/>
<node TEXT="(2) losses pay attention on seemingly undesired properties such as the car&apos;s color or the sun evevation" ID="ID_1063365417" CREATED="1651955266308" MODIFIED="1651955290112">
<node TEXT="deep learning not learning the correct representation" ID="ID_399149197" CREATED="1651955294943" MODIFIED="1651955310166"/>
</node>
</node>
</node>
<node TEXT="Loss Functions" ID="ID_1262041414" CREATED="1641520453737" MODIFIED="1641520995604">
<font NAME="Gentium"/>
<node TEXT="Group-based?" ID="ID_1898475549" CREATED="1641524460135" MODIFIED="1648342703726">
<node TEXT="LiftStructure" ID="ID_362567920" CREATED="1641242980713" MODIFIED="1641520995605">
<font NAME="Gentium"/>
</node>
<node TEXT="Multi-Similarity" ID="ID_1503083507" CREATED="1641242994307" MODIFIED="1641520995605">
<font NAME="Gentium"/>
</node>
<node TEXT="The Group Loss++: A deeper look into group loss for deep metric learning" ID="ID_1054572484" CREATED="1649630904421" MODIFIED="1649630909296">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
<node TEXT="TPAMI22" ID="ID_284202166" CREATED="1649630912115" MODIFIED="1649630915767"/>
<node TEXT="must read for the next dml defense" ID="ID_284287769" CREATED="1649630920598" MODIFIED="1649630927648"/>
</node>
<node TEXT="Fewer is More: A Deep Graph Metric Learning&#xa;Perspective Using Fewer Proxies" ID="ID_644836695" CREATED="1651104693409" MODIFIED="1651104695646">
<icon BUILTIN="unchecked"/>
<node TEXT="NIPS 2020, Xidian" ID="ID_1554646182" CREATED="1651104697189" MODIFIED="1651104705491"/>
</node>
</node>
<node TEXT="Language Guidance" ID="ID_1378629774" CREATED="1648175946093" MODIFIED="1648175949975">
<node TEXT="Integrating Language Guidance into Vision-based Deep Metric Learning" ID="ID_430933197" CREATED="1648175953686" MODIFIED="1648175965810">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1619280443" CREATED="1648175968437" MODIFIED="1648175970177"/>
<node TEXT="incorporates CLIP" ID="ID_1036654181" CREATED="1648175971072" MODIFIED="1648175974568"/>
<node TEXT="relevant to cross modal retrieval" ID="ID_174450250" CREATED="1648176008155" MODIFIED="1648176014577"/>
</node>
</node>
<node TEXT="Proxy-based" ID="ID_879816648" CREATED="1641524445335" MODIFIED="1648174881472">
<font BOLD="true"/>
<node TEXT="ProxyAnchor" ID="ID_219040076" CREATED="1648175402677" MODIFIED="1648175404916"/>
<node TEXT="ProxyNCA" ID="ID_1083149893" CREATED="1648175267078" MODIFIED="1648175270261"/>
<node TEXT="SoftTriplet" ID="ID_1233815845" CREATED="1648175406007" MODIFIED="1648175409028"/>
<node TEXT="SoftTriple" ID="ID_1960504946" CREATED="1651105766571" MODIFIED="1651105770536">
<node TEXT="SoftTriple Loss: Deep Metric Learning Without Triplet Sampling" ID="ID_125134877" CREATED="1651105777730" MODIFIED="1651105786986">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
<node TEXT="ICCV" ID="ID_77776449" CREATED="1651105788988" MODIFIED="1651105796809"/>
<node TEXT="very important for class-dml connection" ID="ID_1938160440" CREATED="1651105797409" MODIFIED="1651105808116">
<icon BUILTIN="info"/>
</node>
</node>
</node>
<node TEXT="NIR" ID="ID_1355356333" CREATED="1648175262641" MODIFIED="1648175264469">
<node TEXT="Non-isotropy Regularization for Proxy-based Deep Metric Learning" ID="ID_600017005" CREATED="1648175115791" MODIFIED="1648175119219">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_465005643" CREATED="1648175120835" MODIFIED="1648175124398">
<node TEXT="google" ID="ID_836335995" CREATED="1648175127003" MODIFIED="1648175128157"/>
<node TEXT="brings minor but clear improvements" ID="ID_1261363441" CREATED="1648175526275" MODIFIED="1648175541699"/>
</node>
<node TEXT="existing proxy-methods solely optimize for sample proxy distance. Given the inherent non-bijectiveness of used distance functions, this can induce locally isotropic sample distributions, leading to crucial semantic context being missed due to difficulties resolving local structures and intraclass relation between samples" ID="ID_372034559" CREATED="1648175542683" MODIFIED="1648175608867"/>
<node TEXT="NIR normalization can be used as a plugin" ID="ID_1894248115" CREATED="1648175617817" MODIFIED="1648175629956"/>
</node>
</node>
</node>
<node TEXT="Triplet-based" ID="ID_1287246293" CREATED="1641524449462" MODIFIED="1648174882328">
<font BOLD="true"/>
<node TEXT="Triplet" ID="ID_430004220" CREATED="1641242977534" MODIFIED="1641520995604">
<font NAME="Gentium"/>
</node>
<node TEXT="CircleLoss" ID="ID_1293728558" CREATED="1648174903893" MODIFIED="1652385120654">
<node TEXT="Circle Loss: A Unified Perspective of Pair Similarity Optimization" ID="ID_1136191375" CREATED="1652385123360" MODIFIED="1652385130236">
<node TEXT="CVPR20" ID="ID_1759135943" CREATED="1652385182714" MODIFIED="1652385184033"/>
<node TEXT="Both DML and Face" ID="ID_744193257" CREATED="1652385185136" MODIFIED="1652385189666"/>
</node>
</node>
</node>
<node TEXT="Classification-based" ID="ID_336668153" CREATED="1641524453412" MODIFIED="1648174884961">
<font BOLD="true"/>
</node>
<node TEXT="Hyperbolic" ID="ID_1875171674" CREATED="1648342666602" MODIFIED="1648342670818">
<node TEXT="Hyperbolic Vision Transformers: Combining Improvements in Metric Learning" ID="ID_10937222" CREATED="1648342671777" MODIFIED="1648342675156">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1214282304" CREATED="1648342678953" MODIFIED="1648342680507"/>
<node TEXT="(1) hyperbolic embedding" ID="ID_258527249" CREATED="1648342781429" MODIFIED="1648342791655"/>
<node TEXT="(2) incorporate transformers" ID="ID_1951501276" CREATED="1648342792051" MODIFIED="1648342798988"/>
</node>
</node>
</node>
<node TEXT="Hard Mining" ID="ID_865388421" CREATED="1652143301505" MODIFIED="1652143305242">
<node TEXT="Hard negative examples are hard, but useful." ID="ID_1077076790" CREATED="1652143306190" MODIFIED="1652143318726">
<icon BUILTIN="unchecked"/>
<node TEXT="ECCV20" ID="ID_450723202" CREATED="1652143307865" MODIFIED="1652143310672"/>
</node>
</node>
<node TEXT="Unsupervised" ID="ID_306535149" CREATED="1651955198172" MODIFIED="1651955202355">
<node TEXT="Self-Taught Metric Learning without Labels" ID="ID_1409325339" CREATED="1651955203092" MODIFIED="1651955220215">
<icon BUILTIN="unchecked"/>
<node TEXT="ICCV21" ID="ID_1413045773" CREATED="1651955211477" MODIFIED="1651955214565"/>
</node>
</node>
<node TEXT="w/ Graph" ID="ID_1539093308" CREATED="1649102316384" MODIFIED="1649102319359">
<node TEXT="AVSL" ID="ID_1818687879" CREATED="1649102378417" MODIFIED="1649102381099">
<node TEXT="Attributable Visual Similarity Learning" ID="ID_1493044485" CREATED="1649102320359" MODIFIED="1649102337908">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_858349431" CREATED="1649102328579" MODIFIED="1649102329874"/>
<node TEXT="CUB, Cars, SOP" ID="ID_596571641" CREATED="1649102341554" MODIFIED="1649102372902">
<node TEXT="" ID="ID_1789373764" CREATED="1649102362284" MODIFIED="1649102362284"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Face Recognition" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" POSITION="right" ID="ID_153988282" CREATED="1641242964517" MODIFIED="1642456756940">
<edge COLOR="#00ff00"/>
<font NAME="Gentium"/>
<node TEXT="Face Identification by default." ID="ID_1812222459" CREATED="1651184495295" MODIFIED="1651184504310">
<icon BUILTIN="info"/>
</node>
<node TEXT="?" ID="ID_1110446588" CREATED="1643132611953" MODIFIED="1652403477029">
<node TEXT="Rethinking feature discrimination and polymerization for large-scale recognition" ID="ID_952906687" CREATED="1643132616302" MODIFIED="1643132624041">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="L2-constrained softmax loss for discriminative face verification" ID="ID_1859004489" CREATED="1643132643711" MODIFIED="1643132824906">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="Deepface: Closing the gap to human-level performance in face verification" ID="ID_380808482" CREATED="1643132687451" MODIFIED="1643132826578">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT=" Deep learning face representation from predicting 10,000 classes" ID="ID_743085930" CREATED="1643132700263" MODIFIED="1643132828298">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="Large-Margin Softmax Loss for Convolutional Neural Networks" ID="ID_1455743436" CREATED="1643132782704" MODIFIED="1643132830122">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="A discriminative feature learning approach for deep face recognition" ID="ID_531526310" CREATED="1643132817470" MODIFIED="1643132831906">
<icon BUILTIN="unchecked"/>
<node TEXT="center loss" ID="ID_485839035" CREATED="1643132819959" MODIFIED="1643132822269"/>
</node>
<node TEXT="Range loss for deep face recognition with long-tail" ID="ID_1684893432" CREATED="1643133019948" MODIFIED="1643133022451">
<icon BUILTIN="unchecked"/>
</node>
</node>
<node TEXT="Dataset" ID="ID_180339389" CREATED="1641503238612" MODIFIED="1641520995596">
<font NAME="Gentium"/>
<node TEXT="Train" ID="ID_1770192462" CREATED="1651184251978" MODIFIED="1651184253253">
<node TEXT="Casia webface" ID="ID_229244818" CREATED="1641503245897" MODIFIED="1641520995597">
<font NAME="Gentium"/>
<node TEXT="Learning Face Representation from Scratch" ID="ID_1940194677" CREATED="1641503251609" MODIFIED="1642395357411">
<icon BUILTIN="checked"/>
<font NAME="Gentium" ITALIC="false"/>
<node TEXT="no large scale dataset publically available, so propose semi-automatical way to collect face images" ID="ID_1189323012" CREATED="1641512559678" MODIFIED="1641520995597">
<font NAME="Gentium"/>
<node TEXT="CASIA-WebFace contains 1e4 subjects and 5e5 images." ID="ID_265616413" CREATED="1641512655754" MODIFIED="1641520995598">
<font NAME="Gentium"/>
</node>
</node>
<node TEXT="use a 11-layer CNN to learn discriminative feature. simply a multi-class classification baseline" ID="ID_1930071035" CREATED="1641512691353" MODIFIED="1641521098034"/>
<node TEXT="evaluate accuracy on LFW and YTF" ID="ID_421884912" CREATED="1641512706845" MODIFIED="1641520995599">
<font NAME="Gentium"/>
</node>
</node>
</node>
<node TEXT="MS1MV2" ID="ID_756106822" CREATED="1651188277249" MODIFIED="1651188283263"/>
<node TEXT="webface260m" ID="ID_1589368342" CREATED="1642953447155" MODIFIED="1642953451237">
<node TEXT="WebFace260M: A Benchmark Unveiling the Power of Million-Scale Deep Face Recognition CVPR21" ID="ID_982203647" CREATED="1642953498775" MODIFIED="1642953558370">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<node TEXT="webface260m noisy" ID="ID_165220438" CREATED="1642953571444" MODIFIED="1642953577010"/>
<node TEXT="webface42m cleaned" ID="ID_1437922589" CREATED="1642953577195" MODIFIED="1642953580844">
<node TEXT="still GPU unfriendly" ID="ID_856230850" CREATED="1642953879711" MODIFIED="1642953884075"/>
</node>
<node TEXT="webface12m (30%)" ID="ID_1426062574" CREATED="1642953859522" MODIFIED="1642953864180"/>
<node TEXT="webface4m (10%)" ID="ID_1864465016" CREATED="1642953864416" MODIFIED="1642953870493">
<node TEXT="looks slightly friendly" ID="ID_1191849266" CREATED="1642953871189" MODIFIED="1642953913872"/>
<node TEXT="we can only start here" ID="ID_1943833487" CREATED="1642953914888" MODIFIED="1642953918594"/>
</node>
</node>
<node TEXT="WebFace260M: A Benchmark for Million-Scale Deep Face Recognition" ID="ID_798146357" CREATED="1650817482839" MODIFIED="1650817484031">
<node TEXT="PAMI" ID="ID_1742142448" CREATED="1650817485299" MODIFIED="1650817487159"/>
<node TEXT="2204.10149" OBJECT="java.lang.Double|2204.10149" ID="ID_1650106439" CREATED="1650817499428" MODIFIED="1650817500727"/>
</node>
<node TEXT="https://www.face-benchmark.org/index.html" ID="ID_16909553" CREATED="1642953463291" MODIFIED="1642953463291" LINK="https://www.face-benchmark.org/index.html"/>
</node>
</node>
<node TEXT="Test" ID="ID_1363165444" CREATED="1651184245626" MODIFIED="1651184249026">
<node TEXT="LFW" ID="ID_1898554892" CREATED="1651184258526" MODIFIED="1651184260423"/>
<node TEXT="YTF" ID="ID_556061465" CREATED="1651184260798" MODIFIED="1651184263116"/>
</node>
</node>
<node TEXT="Multi-Class Classification" FOLDED="true" ID="ID_1466249430" CREATED="1641243301668" MODIFIED="1641520995599">
<font NAME="Gentium"/>
<node TEXT="Softmax Loss" ID="ID_625024996" CREATED="1651184098700" MODIFIED="1651184101967"/>
<node TEXT="L2-Softmax loss" ID="ID_1655703042" CREATED="1651184103282" MODIFIED="1651184106549">
<node TEXT="hypersphere" ID="ID_766784144" CREATED="1651184117000" MODIFIED="1651184119666"/>
</node>
<node TEXT="Center Loss" ID="ID_1925391381" CREATED="1641519035510" MODIFIED="1641520995599">
<font NAME="Gentium"/>
</node>
<node TEXT="SphereFace" ID="ID_312670993" CREATED="1641251036526" MODIFIED="1641520995600">
<font NAME="Gentium"/>
</node>
<node TEXT="CosFace" ID="ID_704538771" CREATED="1641251030859" MODIFIED="1642953952990">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium"/>
</node>
<node TEXT="ArcFace" ID="ID_411135218" CREATED="1652384378593" MODIFIED="1652384382019">
<node TEXT="ArcFace: additive angular margin loss for deep face recognition 2019" ID="ID_970043870" CREATED="1641242901225" MODIFIED="1642953945231">
<icon BUILTIN="checked"/>
<icon BUILTIN="bookmark"/>
<font NAME="Gentium"/>
<node TEXT="the two tracks of face recognition are multi-class classifier and embedding learning" ID="ID_611518023" CREATED="1641519055619" MODIFIED="1641520995601">
<font NAME="Gentium"/>
<node TEXT="problem for classification" ID="ID_1895607582" CREATED="1641519117849" MODIFIED="1641520995602">
<font NAME="Gentium"/>
<node TEXT="size of linear layer linearly increases with identity number" ID="ID_1714580233" CREATED="1641519128504" MODIFIED="1641520995602">
<font NAME="Gentium"/>
</node>
<node TEXT="learned features not discriminative enough in open-set setting" ID="ID_645674933" CREATED="1641519155123" MODIFIED="1641520995602">
<font NAME="Gentium"/>
</node>
</node>
<node TEXT="problem for embedding" ID="ID_1624119885" CREATED="1641519123374" MODIFIED="1641520995602">
<font NAME="Gentium"/>
<node TEXT="combinatorial explosion in triplets. increase number of iterations" ID="ID_167265516" CREATED="1641519176112" MODIFIED="1641520995602">
<font NAME="Gentium"/>
</node>
<node TEXT="semi-hard sample mining is difficult for effective training" ID="ID_1816861001" CREATED="1641519234769" MODIFIED="1641520995602">
<font NAME="Gentium"/>
</node>
</node>
</node>
<node TEXT="united formulation for sphereface, cosface and arcface" ID="ID_1920762886" CREATED="1641519314414" MODIFIED="1641520995602">
<font NAME="Gentium"/>
<node TEXT="\latex $$L_4=-\frac{1}{N}\sum_{i=1}^N \log \frac{&#xa;\exp(s\cos(m_1 \theta_{y_i} + m_2 )-m_3)&#xa;}{&#xa;\sum_j \exp(s\cos(m_1 \theta_{j} + m_2 )-m_3)&#xa;}$$" ID="ID_407559158" CREATED="1641519332807" MODIFIED="1641520995603">
<font NAME="Gentium"/>
</node>
<node TEXT="sphere face: multiplicative angular margin m_1" ID="ID_1612863665" CREATED="1641519483413" MODIFIED="1641520995603">
<font NAME="Gentium"/>
</node>
<node TEXT="arcface: additive angular margin m2" ID="ID_479788384" CREATED="1641519516270" MODIFIED="1641520995603">
<font NAME="Gentium"/>
</node>
<node TEXT="cosface: additive cosine margin m3" ID="ID_1469158039" CREATED="1641519549998" MODIFIED="1641520995603">
<font NAME="Gentium"/>
</node>
</node>
<node TEXT="https://github.com/deepinsight/insightface/tree/master/recognition/arcface_torch" ID="ID_1831504394" CREATED="1641519654585" MODIFIED="1641520995603">
<font NAME="Gentium"/>
</node>
</node>
</node>
<node TEXT="ArcFace Extension" ID="ID_1324013539" CREATED="1642950760442" MODIFIED="1642950766638">
<node TEXT="KappaFace: Adaptive Additive Angular Margin Loss for Deep Face Recognition" ID="ID_1118636562" CREATED="1642950767435" MODIFIED="1642950783515">
<icon BUILTIN="pencil"/>
<node TEXT="looks fair" ID="ID_1297020447" CREATED="1642951154422" MODIFIED="1642951157854"/>
<node TEXT="adaptive margin." ID="ID_1051061321" CREATED="1642951148542" MODIFIED="1642951154038"/>
<node TEXT="von Mises-Fisher distribution" ID="ID_1463580715" CREATED="1642951187531" MODIFIED="1642951195717"/>
<node TEXT="Final Eq.15" ID="ID_704402950" CREATED="1642951232067" MODIFIED="1642951250251"/>
</node>
<node TEXT="ArcFace-SCF: Spherical confidence learning for face recognition. (CVPR 2021)" ID="ID_1651655230" CREATED="1642951059696" MODIFIED="1642951075607">
<icon BUILTIN="unchecked"/>
</node>
</node>
<node TEXT="P2SGrad" ID="ID_1528859822" CREATED="1652384387406" MODIFIED="1652384390022">
<node TEXT="P2SGrad: Refined Gradients for Optimizing Deep Face Models" ID="ID_1929819605" CREATED="1652384391411" MODIFIED="1652384397158">
<node TEXT="CVPR19" ID="ID_1963132669" CREATED="1652384399361" MODIFIED="1652384401100"/>
</node>
</node>
<node TEXT="CircleLoss" ID="ID_1011919739" CREATED="1652385140954" MODIFIED="1652385143212">
<node TEXT="Circle Loss: A Unified Perspective of Pair Similarity Optimization" ID="ID_1292289619" CREATED="1652385147157" MODIFIED="1652385149315">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR20" ID="ID_1109536073" CREATED="1652385150869" MODIFIED="1652385166238"/>
<node TEXT="Both DML and Face" ID="ID_1366793448" CREATED="1652385166575" MODIFIED="1652385169170"/>
</node>
</node>
<node TEXT="Curricularface: adaptive curriculum learning loss for deep face recognition. CVPR2020" ID="ID_1026306137" CREATED="1642950986727" MODIFIED="1642953955842">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="AdaFace" ID="ID_1721671990" CREATED="1649628757671" MODIFIED="1649628776836">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<node TEXT="AdaFace: Quality Adaptive Margin for Face Recognition" ID="ID_419790283" CREATED="1649628760371" MODIFIED="1649628761250">
<node TEXT="CVPR22" ID="ID_1909513529" CREATED="1649628762396" MODIFIED="1649628763820">
<node TEXT="Oral" ID="ID_405483472" CREATED="1649628765388" MODIFIED="1649628766520"/>
</node>
<node TEXT="we propose a new loss function that emphasizes samples of different difficulties based on their image quality" ID="ID_1401366888" CREATED="1649629032892" MODIFIED="1649629048086"/>
</node>
</node>
<node TEXT="Efficiency" ID="ID_783283599" CREATED="1649105830242" MODIFIED="1649105832685">
<node TEXT="Killing Two Birds with One Stone:Efficient and Robust Training of Face Recognition CNNs by Partial FC" ID="ID_556846223" CREATED="1649105833331" MODIFIED="1649105842171">
<icon BUILTIN="pencil"/>
</node>
</node>
</node>
<node TEXT="Similarity Embedding" ID="ID_290828706" CREATED="1641243309778" MODIFIED="1651184179798">
<font NAME="Gentium"/>
<node TEXT="FaceNet" ID="ID_466452383" CREATED="1641242953447" MODIFIED="1641520995604">
<font NAME="Gentium"/>
</node>
</node>
<node TEXT="Face Verification" ID="ID_33628603" CREATED="1651184509665" MODIFIED="1651184513925"/>
<node TEXT="Key Point" ID="ID_1651017374" CREATED="1650830438788" MODIFIED="1650830442804">
<node TEXT="Not All Tokens Are Equal: Human-centric Visual Analysis via&#xa;Token Clustering Transformer" ID="ID_1267200711" CREATED="1650830443573" MODIFIED="1650830471615">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_256081483" CREATED="1650830472906" MODIFIED="1650830474075"/>
<node TEXT="Important" ID="ID_1925146044" CREATED="1650830479672" MODIFIED="1650830501355">
<icon BUILTIN="info"/>
<font BOLD="true"/>
</node>
<node TEXT="token clustering" ID="ID_1084636872" CREATED="1650830509672" MODIFIED="1650830512102"/>
</node>
</node>
<node TEXT="Bias" ID="ID_1657534476" CREATED="1651188079795" MODIFIED="1651188082281"/>
<node TEXT="Privacy" ID="ID_191145216" CREATED="1651188082570" MODIFIED="1651188086484"/>
</node>
<node TEXT="Track: AdvRank" FOLDED="true" POSITION="right" ID="ID_67713290" CREATED="1561352085890" MODIFIED="1642394393230">
<icon BUILTIN="idea"/>
<font NAME="Gentium" SIZE="10" BOLD="true"/>
<edge COLOR="#ff00ff"/>
<node TEXT="1806.04425: Ranking Robustness Under Ad" ID="ID_562405105" CREATED="1561555589182" MODIFIED="1636485900673">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="document manipulation for candidate attack" ID="ID_1231428087" CREATED="1561555592821" MODIFIED="1587728924172">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="mainly analysis robustness" ID="ID_55534459" CREATED="1561555648216" MODIFIED="1587728924173">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="manual documentation manipulation instead of automatic adversarial example generation" ID="ID_798730521" CREATED="1561555609750" MODIFIED="1587728924174">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="targets at RankSVM and LambdaMART models instead of neural networks" ID="ID_255349278" CREATED="1561555658663" MODIFIED="1587728924175">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1808.03908: Adversarial Personalized Rank" ID="ID_83109239" CREATED="1561555800953" MODIFIED="1636485894233">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="recommendation system" ID="ID_1396811583" CREATED="1561555811711" MODIFIED="1587728924177">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="enhance robustness of a recommender model with adversarial training (adv examples)" ID="ID_527243554" CREATED="1561555835768" MODIFIED="1587728924178">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="APR on Matrix Factorization" ID="ID_662100047" CREATED="1561556113144" MODIFIED="1587728924179">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1809: AdvBox: FaceNet Attack" ID="ID_1888665729" CREATED="1561556172335" MODIFIED="1636485887561">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="query attack" ID="ID_1680208164" CREATED="1561556180000" MODIFIED="1587728924182">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="https://github.com/advboxes/AdvBox/blob/master/applications/face_recogn" ID="ID_951928489" CREATED="1561556196446" MODIFIED="1587728924182" LINK="https://github.com/advboxes/AdvBox/blob/master/applications/face_recogn">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="\latex $L=\sqrt{\sum (E-E_t)^2}" ID="ID_79735084" CREATED="1561556315382" MODIFIED="1587728924183">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1812.00552: Universal Perturbation Attack Against Image Retrieval" ID="ID_1919220467" CREATED="1561556406527" MODIFIED="1587728924183">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="directly attacks triplet ranking loss function" ID="ID_491701428" CREATED="1561556424133" MODIFIED="1587728924185">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="but what if the triplet fell into the zero-gradient region" ID="ID_839129884" CREATED="1561556433741" MODIFIED="1587728924186">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="using PGD+momentum for optimization" ID="ID_948653721" CREATED="1561556446589" MODIFIED="1587728924187">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="studies query attack" ID="ID_1697473744" CREATED="1561556462589" MODIFIED="1587728924188">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2019-ICMR: Who&#x2019;s Afraid of Adversarial Queries?" ID="ID_1703113745" CREATED="1561556482168" MODIFIED="1636485880766">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="query attack" ID="ID_1123142555" CREATED="1561556568742" MODIFIED="1587728924189">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="\latex $\max ||f(x)-f(x+v)||_2^2$" ID="ID_1830202115" CREATED="1561556572709" MODIFIED="1587728924190">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="aims at image retrieval and didn&apos;t mention embedding learning" ID="ID_1516625397" CREATED="1561556595981" MODIFIED="1587728924190">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1907: unsupervised adversarial attacks on deep feature-based retrieval with GAN" ID="ID_9436679" CREATED="1588230731425" MODIFIED="1588230756582">
<node TEXT="generate adversarial perturbation with GAN" ID="ID_1151385661" CREATED="1588230816064" MODIFIED="1588230824347"/>
</node>
<node TEXT="1812.00552: Universal Perturbation Attack Against Image Retrieval (iccv19)" ID="ID_1472505886" CREATED="1573474834671" MODIFIED="1587728924195">
<icon BUILTIN="messagebox_warning"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: universal perturbation for untargeted attack" ID="ID_1954548078" CREATED="1573474853539" MODIFIED="1587728924196">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: currupts pairwise or listwise ranking" ID="ID_1847715376" CREATED="1573474863063" MODIFIED="1587728924197">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="untargeted, focusing on fooling" ID="ID_665084129" CREATED="1573475747222" MODIFIED="1587728924198">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1901.10650: Metric Attack and Defense for Person Re-identificatio" ID="ID_490211267" CREATED="1592543067606" MODIFIED="1636485967109">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
<font BOLD="true"/>
<node TEXT="Song Bai" ID="ID_692240773" CREATED="1636485956725" MODIFIED="1636485961987"/>
<node TEXT="attack the metric in person reID" ID="ID_34707268" CREATED="1592543081709" MODIFIED="1592543098277"/>
<node TEXT="push (untargeted) and pull (targeted)" ID="ID_1286430581" CREATED="1592543098573" MODIFIED="1592543112360"/>
</node>
<node TEXT="1908.09613: targeted mismatch adversarial attack (iccv19)" ID="ID_671271001" CREATED="1573471637067" MODIFIED="1636486108618">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: targeted mismatch" ID="ID_1563475526" CREATED="1573471664345" MODIFIED="1587728924192">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: distance objective" ID="ID_688655227" CREATED="1573472458185" MODIFIED="1587728924193">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="not systematic enough" ID="ID_205991126" CREATED="1573472480708" MODIFIED="1587728924194">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="THIS is a threat to us" ID="ID_988648076" CREATED="1573472496549" MODIFIED="1587728924195">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Advpattern (iccv19)" ID="ID_932771683" CREATED="1576809427350" MODIFIED="1587728924198">
<icon BUILTIN="messagebox_warning"/>
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="ours: Adversarial Ranking Attack and Defense (arxiv 2002.11293)" ID="ID_933056745" CREATED="1576809387016" MODIFIED="1592543327861">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" SIZE="10" BOLD="true"/>
<node TEXT="ECCV2020" ID="ID_1213034229" CREATED="1576809470567" MODIFIED="1610427018812">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2004.04199: transferable, controllable, and inconspicious adversarial attacks on person re-identification with deep-misranking (CVPR20)" ID="ID_693496331" CREATED="1590743469098" MODIFIED="1590743579415">
<icon BUILTIN="button_ok"/>
<node TEXT="person reID" ID="ID_548309734" CREATED="1590743636274" MODIFIED="1590743640590"/>
<node TEXT="use triplet form attack objective" ID="ID_1324326527" CREATED="1590743509489" MODIFIED="1590743528991"/>
<node TEXT="untargeted" ID="ID_862183953" CREATED="1590743529382" MODIFIED="1590743531961"/>
<node TEXT="learning-to-mis-rank" ID="ID_172715084" CREATED="1590805595723" MODIFIED="1590805600800"/>
</node>
<node TEXT="Vulnerability of Person Re Identification Models to Metric Adversarial Attacks (cvpr20w)" ID="ID_666911505" CREATED="1592544846016" MODIFIED="1620715760339">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="messagebox_warning"/>
<node TEXT="self metric attack" ID="ID_1983736472" CREATED="1592544863127" MODIFIED="1592544870094">
<node TEXT="maximum shift distance" ID="ID_1843892614" CREATED="1592544925404" MODIFIED="1592544929867"/>
<node TEXT="untargeted" ID="ID_1551966366" CREATED="1592544982949" MODIFIED="1592544984988"/>
</node>
<node TEXT="furthest negative attack" ID="ID_165661813" CREATED="1592544870439" MODIFIED="1592544877498">
<node TEXT="push from orig, pull from furthest cluster" ID="ID_765025792" CREATED="1592545364041" MODIFIED="1592545375413"/>
<node TEXT="disturb the ranking" ID="ID_225845829" CREATED="1592544987176" MODIFIED="1592544995331"/>
<node TEXT="untargeted" ID="ID_1161241988" CREATED="1592544996728" MODIFIED="1592544998361"/>
</node>
<node TEXT="adversarial training for metric" ID="ID_881036760" CREATED="1592544877759" MODIFIED="1592544885179"/>
</node>
<node TEXT="2012.14057: PERSONRE IDENTIFICATION WITH ADVERSARIAL TRIPLET EMBEDDING" ID="ID_974503367" CREATED="1610427222890" MODIFIED="1620715752859">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="not peer reviewed" ID="ID_911035452" CREATED="1610427251959" MODIFIED="1610427256067"/>
<node TEXT="\latex l_\text{triplet}(\tilde{a}, p, n)" ID="ID_1528343305" CREATED="1610427320982" MODIFIED="1610427368752"/>
<node TEXT="better reid performance? weird" ID="ID_912255527" CREATED="1610427378471" MODIFIED="1610427386018"/>
</node>
<node TEXT="2103.02927 QAIR: Practical Query-efficient Black-Box Attacks for Image Retrieval" ID="ID_1939119283" CREATED="1615294198999" MODIFIED="1620715422259">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_ok"/>
<node TEXT="query-based attack against image retrieval to subvert the top-k retrieval" ID="ID_788475675" CREATED="1615294409842" MODIFIED="1615294424860">
<node TEXT="random incorrect result" ID="ID_1404780182" CREATED="1615294426968" MODIFIED="1615294439236"/>
</node>
<node TEXT="gradient optimization + a relevance-based loss" ID="ID_1805130536" CREATED="1615294463470" MODIFIED="1615294478666">
<node TEXT="uses RGF (random gradient free)" ID="ID_1977339570" CREATED="1615294685532" MODIFIED="1615294698319"/>
</node>
<node TEXT="recursive model stealing method" ID="ID_334621555" CREATED="1615294479086" MODIFIED="1615294494768">
<node TEXT="provides transfer prior" ID="ID_1010296679" CREATED="1615294496388" MODIFIED="1615294503336"/>
</node>
<node TEXT="stealing and attack is separated steps: what if we treat it as a multi-arm bandit?" ID="ID_1679102936" CREATED="1615294783533" MODIFIED="1615294824361">
<icon BUILTIN="idea"/>
<node TEXT="explore and exploit" ID="ID_645430104" CREATED="1615294813742" MODIFIED="1615294819339"/>
</node>
</node>
<node TEXT="RobRank: adversarial attack and defense in deep ranking" ID="ID_990446170" CREATED="1636485853836" MODIFIED="1650810489070">
<font BOLD="true"/>
</node>
<node TEXT="RobDML: Enhancing Adversarial Robustness for Deep Metric Learning" ID="ID_1927729885" CREATED="1636485850581" MODIFIED="1650810510794">
<font BOLD="true"/>
<node TEXT="CVPR22" ID="ID_1223380826" CREATED="1650810506474" MODIFIED="1650810507766"/>
</node>
</node>
<node TEXT="3D + PCL Recog" FOLDED="true" POSITION="left" ID="ID_1606327696" CREATED="1583132559918" MODIFIED="1649103272801">
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#007c7c"/>
<node TEXT="Dataset" ID="ID_750669130" CREATED="1583137147235" MODIFIED="1587526865232">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="PartNet: A large scale benchmark for fine-grained and hierarchical part-level 3D object understanding" ID="ID_367911268" CREATED="1583137156179" MODIFIED="1587526865232">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: large-scale dataset of 3D objects annotated with fine-trained, instance-level, and hierarchical 3D part information." ID="ID_1810204510" CREATED="1583137993707" MODIFIED="1587526865232">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: 3d part recognition: fine-rained semantic segmentation, hierarchical semantic segmentation, and instance segmentation" ID="ID_447922824" CREATED="1583141336957" MODIFIED="1587526865233">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="generic" ID="ID_67683623" CREATED="1608878793211" MODIFIED="1608878794674">
<node TEXT="2012.09688&#xa;PCT: Point Cloud Transformer" ID="ID_760061240" CREATED="1608878795342" MODIFIED="1608878805569"/>
<node TEXT="2012.09164 Point Transformer" ID="ID_1856683145" CREATED="1608879617939" MODIFIED="1608880161951">
<icon BUILTIN="bookmark"/>
<node TEXT="self-attention network to PCL" ID="ID_1162420612" CREATED="1608880164884" MODIFIED="1608880172010"/>
</node>
</node>
<node TEXT="classification" ID="ID_1683843111" CREATED="1583132565754" MODIFIED="1610426338023">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="PointNet: deep learning on Point Sets for 3D classification and segmentation (2017)" ID="ID_535445133" CREATED="1583132578641" MODIFIED="1587526865234">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: 3d classification and segmentation" ID="ID_369477718" CREATED="1583132646461" MODIFIED="1587526865235">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: previous works transform such data to regular 3D voxel grids or collections of images" ID="ID_509541120" CREATED="1583132656157" MODIFIED="1587526865236">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="renders data unecessarily voluminous and causes issues" ID="ID_1650499462" CREATED="1583132774211" MODIFIED="1587526865237">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="how: novel type of neural network" ID="ID_1582863497" CREATED="1583132794431" MODIFIED="1587526865238">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="directly consumes 3d point cloud" ID="ID_904651994" CREATED="1583132807372" MODIFIED="1587526865238">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="respects the permutation invariance of point in the input" ID="ID_80693188" CREATED="1583132818955" MODIFIED="1587526865239">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="Detail" ID="ID_592119659" CREATED="1583132853503" MODIFIED="1587526865239">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="properties of point set" ID="ID_923628661" CREATED="1583133482219" MODIFIED="1587526865239">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1. unordered" ID="ID_59901348" CREATED="1583133494060" MODIFIED="1587526865240">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2. interaction among points" ID="ID_905514121" CREATED="1583133498167" MODIFIED="1587526865240">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="3. invariance under transformations" ID="ID_932819479" CREATED="1583133522841" MODIFIED="1587526865241">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="e.g. translation and rotation" ID="ID_1003384585" CREATED="1583133566689" MODIFIED="1587526865241">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="regularize transformation to be close to orthogonal matrix" ID="ID_1980629096" CREATED="1583133585186" MODIFIED="1587526865242">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="An orthogonal transformation will not lose information in the input, thus is desired" ID="ID_285705617" CREATED="1583133597956" MODIFIED="1587526865242">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
</node>
<node TEXT="PointNet++: deep hierarchical feature learning on point sets in a metric space (NIPS)" ID="ID_1305155138" CREATED="1583133688195" MODIFIED="1587526865243">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: 3d classification and segmentation" ID="ID_1391695258" CREATED="1583136577356" MODIFIED="1587526865244">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: pointnet does not capture local structures induced by the metric space points live in" ID="ID_1734373925" CREATED="1583136583902" MODIFIED="1587526865245">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: introduce a hierarchical neural network that applies pointnet recursively on a nested partitioning of the input point set; propoes novel set learning layers to adaptively combine features from multiple scales" ID="ID_574546543" CREATED="1583136619272" MODIFIED="1587526865246">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.10876: PointAugment: an auto-agumentation framework for point cloud classification (CVPR2020)" ID="ID_1995300558" CREATED="1583310893208" MODIFIED="1587526865246">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: regularization in PCL classification" ID="ID_682948316" CREATED="1583310928954" MODIFIED="1587526865247">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: regularization" ID="ID_1191365248" CREATED="1583311604535" MODIFIED="1587526865248">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: sample-aware and takes adversarial learning stategy to jointly optimize the augmentor and the classifier" ID="ID_1638101526" CREATED="1583311614272" MODIFIED="1587526865248">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="augmentor can learn samples that best fit the classifier" ID="ID_1705315529" CREATED="1583311657637" MODIFIED="1587526865249">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="EllipsoidNet: Ellipsoid Representation for Point Cloud Classification andSegmentation (cvpr21)" ID="ID_1008449076" CREATED="1615297913979" MODIFIED="1615297923495">
<node TEXT="new 2d representation" ID="ID_1336906720" CREATED="1615297924304" MODIFIED="1615297941283"/>
</node>
<node TEXT="Generalization" ID="ID_1470361340" CREATED="1650816123033" MODIFIED="1650816126078">
<node TEXT="MetaSets: Meta-Learning on Point Sets for Generalizable Representations" ID="ID_1880007762" CREATED="1650816127428" MODIFIED="1650816134358">
<node TEXT="CVPR21" ID="ID_1972344411" CREATED="1650816135730" MODIFIED="1650816137097"/>
</node>
</node>
</node>
<node TEXT="Semgentation" ID="ID_98283900" CREATED="1610426338878" MODIFIED="1610426343353">
<node TEXT="2003.05593:Learning to Segment 3D Point Clouds in 2D Image Space (cvpr20 oral)" ID="ID_1194509251" CREATED="1590748289620" MODIFIED="1590748552801"/>
<node TEXT="2003.14032 PolarNet: An Improved Grid Representation for Online LiDAR Point Clouds Semantic Segmentation (cvpr20)" ID="ID_273525175" CREATED="1590806996838" MODIFIED="1590807009853"/>
<node TEXT="2012.10217&#xa;SegGroup: Seg-Level Supervision for 3D Instance and Semantic Segmentation" ID="ID_746665270" CREATED="1608872980932" MODIFIED="1608873093023"/>
<node TEXT="2101: Boundary-Aware Geometric Encoding for Semantic Segmentation of Point Clouds (AAAI21)" ID="ID_18229318" CREATED="1610426385894" MODIFIED="1610426427874"/>
</node>
<node TEXT="Shape + PCL" ID="ID_692723578" CREATED="1587978723669" MODIFIED="1587978729471">
<node TEXT="2004.09411: shape-oriented convolutional neural network for point cloud analysis (AAAI20)" ID="ID_648541054" CREATED="1587978730554" MODIFIED="1587978867499"/>
<node TEXT="2004.04242: deep manifold prior" ID="ID_1198757401" CREATED="1589766955822" MODIFIED="1589766965285"/>
<node TEXT="2005.08144: high-dimentsional convolutional networks for geometric pattern recognition" ID="ID_551842404" CREATED="1590025587186" MODIFIED="1590025610132"/>
</node>
<node TEXT="Depth" ID="ID_1948167092" CREATED="1617622791190" MODIFIED="1617622792504">
<node TEXT="Depth Estimation" ID="ID_310201956" CREATED="1583313577977" MODIFIED="1587526865249">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.12319: semantically-guided representation learning for self-supervised monocular depth" ID="ID_123925389" CREATED="1583319217945" MODIFIED="1587526865249">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: depth estimation" ID="ID_512544818" CREATED="1583319382866" MODIFIED="1587526865250">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: leverage more directly semantic structure to guide geometric representation learning" ID="ID_1129284074" CREATED="1583319389083" MODIFIED="1587526865251">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="Depth Representation" ID="ID_940024603" CREATED="1617622797830" MODIFIED="1617622801976">
<node TEXT="CVPR21: S2R-DepthNet: Learning a Generalizable Depth-specific Structural Representation" ID="ID_339705407" CREATED="1617622803213" MODIFIED="1617622810654"/>
</node>
</node>
<node TEXT="Motion Prediction" ID="ID_1468545131" CREATED="1585273931386" MODIFIED="1587526865251">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2003.06594: collaborative motion prediction via neural motion message passing (CVPR20)" ID="ID_922645949" CREATED="1585273939963" MODIFIED="1587526865251">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="model the interaction among traffic actors, which could cooperate with each other to avoid collisions or form groups" ID="ID_1866499715" CREATED="1585273974008" MODIFIED="1587526865252">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="Social GAN: Socially Acceptable Trajectories&#xa;with Generative Adversarial Networks (cvpr20)" ID="ID_707374796" CREATED="1592546647558" MODIFIED="1592546660119"/>
</node>
<node TEXT="Tracking" ID="ID_1225202497" CREATED="1590746031233" MODIFIED="1590746035544">
<node TEXT="2005.13888: P2B: Point-to-Box Network for 3D Object Tracking in Point Clouds (cvpr20 oral)" ID="ID_247548698" CREATED="1590746036605" MODIFIED="1590746049896"/>
</node>
<node TEXT="2D-&gt;3D / Reconstruction" ID="ID_85275942" CREATED="1587974524881" MODIFIED="1587975275112">
<node TEXT="3D portrait" ID="ID_1817633511" CREATED="1587974528844" MODIFIED="1587974534327">
<node TEXT="2004.11598: Deep 3D portrait from a single image" ID="ID_788527202" CREATED="1587974536965" MODIFIED="1587974549308">
<node TEXT="unsupervised" ID="ID_588496742" CREATED="1587974550811" MODIFIED="1587974553381"/>
</node>
</node>
</node>
<node TEXT="Unsupervised" ID="ID_1108189941" CREATED="1590749952334" MODIFIED="1590749956189">
<node TEXT="2004.01176: Learning Unsupervised Hierarchical Part Decomposition of 3D Objects from a Single RGB Image (cvpr20)" ID="ID_953085415" CREATED="1590749956974" MODIFIED="1590749967809"/>
</node>
<node TEXT="Object Detection" ID="ID_359173144" CREATED="1634062757685" MODIFIED="1648337996840">
<node TEXT="Voxel Transformer for 3D Object Detection" ID="ID_1112299622" CREATED="1634062767074" MODIFIED="1634062773616"/>
<node TEXT="LiDAR Snowfall Simulation for Robust 3D Object Detection" ID="ID_1716092506" CREATED="1649103277309" MODIFIED="1649103282821">
<node TEXT="CVPR22" ID="ID_685730559" CREATED="1649103284187" MODIFIED="1649103285288"/>
</node>
</node>
<node TEXT="Interpolation" ID="ID_687015313" CREATED="1648337997035" MODIFIED="1648338000713">
<node TEXT="IDEA-Net: Dynamic 3D Point Cloud Interpolation via Deep Embedding Alignment" ID="ID_1606213792" CREATED="1648338005697" MODIFIED="1648338009130">
<node TEXT="CVPR22" ID="ID_80360502" CREATED="1648338010355" MODIFIED="1648338011775"/>
</node>
</node>
</node>
<node TEXT="Action Recognition" FOLDED="true" POSITION="left" ID="ID_941361807" CREATED="1647567341551" MODIFIED="1647567345392">
<edge COLOR="#ff0000"/>
<node TEXT="Architecture" ID="ID_694731282" CREATED="1647567346659" MODIFIED="1647567355095">
<node TEXT="non-local neural networks" ID="ID_595201499" CREATED="1647567356067" MODIFIED="1647567369739">
<icon BUILTIN="checked"/>
<node TEXT="CVPR18" ID="ID_556242427" CREATED="1647567364801" MODIFIED="1647567366157"/>
<node TEXT="kaiming he" ID="ID_598417815" CREATED="1647567408886" MODIFIED="1647567410210"/>
<node TEXT="deal with long-range dependency problem that the traditional CNN is not very good at" ID="ID_883791225" CREATED="1647567373082" MODIFIED="1647567395434"/>
</node>
</node>
<node TEXT="Framework" ID="ID_164836852" CREATED="1651953412715" MODIFIED="1651953414547">
<node TEXT="Cross-modal Representation Learning for Zero-shot Action Recognition" ID="ID_1630052458" CREATED="1651953420845" MODIFIED="1651953421503">
<node TEXT="CVPR22, Microsoft" ID="ID_125282230" CREATED="1651953423268" MODIFIED="1651953426559"/>
</node>
</node>
</node>
<node TEXT="Alpha Matting" FOLDED="true" POSITION="left" ID="ID_1289342378" CREATED="1649104749744" MODIFIED="1649104752541">
<edge COLOR="#0000ff"/>
<node TEXT="2003.07711: F, B, Alpha Matting (ECCV20 submitted)" ID="ID_98155221" CREATED="1585103366049" MODIFIED="1587526865049">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: estimating opacity mask for objects" ID="ID_40944389" CREATED="1585103398418" MODIFIED="1587526865049">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: existing networks only predict the alpha matte" ID="ID_349450024" CREATED="1585103433174" MODIFIED="1587526865050">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: a low-cost modification to alpha matting networks" ID="ID_688016212" CREATED="1585103410787" MODIFIED="1587526865051">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2004.00626 Background Matting: The World is Your Green Screen (cvpr20)" ID="ID_1311753286" CREATED="1590805662549" MODIFIED="1590805676803"/>
</node>
<node TEXT="AR/VR Demands" FOLDED="true" POSITION="left" ID="ID_598464569" CREATED="1649106345613" MODIFIED="1649106350552">
<edge COLOR="#00ff00"/>
<node TEXT="Relighting" ID="ID_147972340" CREATED="1649106351568" MODIFIED="1649106353702">
<node TEXT="Face Relighting with Geometrically Consistent Shadows" ID="ID_1062796447" CREATED="1649106354508" MODIFIED="1649106359170">
<node TEXT="CVPR22" ID="ID_1173806280" CREATED="1649106360283" MODIFIED="1649106361411"/>
</node>
</node>
</node>
<node TEXT="Backdoor Attack &amp; Defense" LOCALIZED_STYLE_REF="default" FOLDED="true" POSITION="left" ID="ID_1279237598" CREATED="1642964328030" MODIFIED="1648336230488">
<edge COLOR="#ff0000"/>
<node TEXT="Attack" ID="ID_481030829" CREATED="1642965505734" MODIFIED="1642965507796">
<node TEXT="Classification" ID="ID_1068695249" CREATED="1643059216060" MODIFIED="1643059221252">
<node TEXT="* Madry backdoor" ID="ID_684891535" CREATED="1643059222128" MODIFIED="1644181613323"/>
<node TEXT="Imperceptible and Multi-channel Backdoor Attack against Deep Neural Networks" ID="ID_1408798505" CREATED="1644181613544" MODIFIED="1644181621740">
<icon BUILTIN="pencil"/>
<node TEXT="exploiting DCT steganography" ID="ID_645033989" CREATED="1644181646727" MODIFIED="1644181655902"/>
</node>
<node TEXT="Poison Ink: Robust and Invisible Backdoor Attack" ID="ID_216800788" CREATED="1647567532543" MODIFIED="1647567536305">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Federated Learning (FL)" ID="ID_63486999" CREATED="1642964624292" MODIFIED="1642964972961">
<node TEXT="Attack of the Tails:&#xa;Yes, You Really Can Backdoor Federated Learning" ID="ID_1636233737" CREATED="1642964633771" MODIFIED="1642964637902">
<icon BUILTIN="checked"/>
<node TEXT="NIPS20" ID="ID_315153104" CREATED="1642964641665" MODIFIED="1642964643166"/>
<node TEXT="there are already many attacks and defenses in FL" ID="ID_1297351488" CREATED="1642964663241" MODIFIED="1642964670655"/>
<node TEXT="this paper breaks the sota defense" ID="ID_1011383288" CREATED="1642964675173" MODIFIED="1642964680787"/>
<node TEXT="susceptible to attack. defense remains open problem" ID="ID_92977436" CREATED="1642964925932" MODIFIED="1642964939810"/>
</node>
<node ID="ID_124537533" CREATED="1642965130724" MODIFIED="1642965135500">
<icon BUILTIN="pencil"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Backdoor Attacks on Federated Learning with Lottery Ticket Hypothesis
  </body>
</html>
</richcontent>
<node TEXT="21.09" OBJECT="java.lang.Double|21.09" ID="ID_874215275" CREATED="1642965148120" MODIFIED="1642965212458"/>
</node>
<node TEXT="DBA: Distributed Backdoor Attacks against Federated Learning" ID="ID_1786745650" CREATED="1642965312934" MODIFIED="1642965322325">
<icon BUILTIN="unchecked"/>
<node TEXT="iclr21" ID="ID_1366820704" CREATED="1642965323484" MODIFIED="1642965326580"/>
</node>
</node>
<node TEXT="Automobile" ID="ID_1807292891" CREATED="1646928903901" MODIFIED="1646928910994">
<node TEXT="Clean-Annotation Backdoor Attack against Lane Detection Systems in the Wild" ID="ID_444510611" CREATED="1646928911957" MODIFIED="1646928920668">
<icon BUILTIN="pencil"/>
<node TEXT="physical attack against lane deteciton" ID="ID_78672113" CREATED="1646928932402" MODIFIED="1646929021292"/>
<node TEXT="clean-annotation approach" ID="ID_638994819" CREATED="1646929021555" MODIFIED="1646929029315"/>
</node>
</node>
<node TEXT="Tracking" ID="ID_633333846" CREATED="1644181581603" MODIFIED="1644181584344">
<node TEXT="Few-Shot Backdoor Attacks on Visual Object Tracking" ID="ID_1944039915" CREATED="1644181500877" MODIFIED="1644181516065">
<icon BUILTIN="checked"/>
<node TEXT="ICLR22" ID="ID_1379553453" CREATED="1644181502731" MODIFIED="1644181504177"/>
<node TEXT="extension of backdoor" ID="ID_1434989681" CREATED="1644181504456" MODIFIED="1644181513607"/>
</node>
</node>
<node TEXT="Contrastive Learning" ID="ID_1079011502" CREATED="1647267727724" MODIFIED="1647267731689">
<node TEXT="Indiscriminate Poisoning Attacks on Unsupervised Contrastive Learning" ID="ID_103472433" CREATED="1647267739350" MODIFIED="1647267743429">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Poisoning and Backdooring Contrastive Learning" ID="ID_199714317" CREATED="1648425561210" MODIFIED="1648425562006">
<node TEXT="Carlini" ID="ID_1124457398" CREATED="1648425564654" MODIFIED="1648425567024"/>
<node TEXT="CLIP" ID="ID_653822881" CREATED="1648425569833" MODIFIED="1648425571800"/>
</node>
</node>
</node>
<node TEXT="Defense" ID="ID_1943792971" CREATED="1642964418531" MODIFIED="1642965511845">
<node TEXT="Survey" ID="ID_330468423" CREATED="1647048705270" MODIFIED="1647048707607">
<node TEXT="A Survey of Neural Trojan Attacks and Defenses in Deep Learning" ID="ID_941695665" CREATED="1646945552550" MODIFIED="1646945574224">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Classification" ID="ID_1654847796" CREATED="1647048721032" MODIFIED="1647048723750">
<node TEXT="Adversarial Fine-tuning for Backdoor Defense: Connect Adversarial Examples to Triggered Samples" ID="ID_580929337" CREATED="1646947640727" MODIFIED="1646947646923">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Face Recog" ID="ID_569214033" CREATED="1647266119655" MODIFIED="1647266122422">
<node TEXT="Resurrecting Trust in Facial Recognition: Mitigating Backdoor Attacks in Face Recognition to Prevent Potential Privacy Breaches" ID="ID_880337110" CREATED="1647266127523" MODIFIED="1647266130023">
<icon BUILTIN="pencil"/>
<node TEXT="(1) using transfer learning" ID="ID_1190567128" CREATED="1647266131286" MODIFIED="1647266137581"/>
<node TEXT="(2) using selective image perturbation" ID="ID_1274323880" CREATED="1647266137898" MODIFIED="1647266144697"/>
</node>
</node>
<node TEXT="Federated Learning (FL)" ID="ID_1134675768" CREATED="1642965172314" MODIFIED="1642965518360">
<node TEXT="Mitigating Backdoor Attacks in Federated Learning" ID="ID_709693725" CREATED="1642965179275" MODIFIED="1642965184157">
<icon BUILTIN="pencil"/>
<node TEXT="20.11" OBJECT="java.lang.Double|20.11" ID="ID_839247018" CREATED="1642965197783" MODIFIED="1642965208595"/>
<node TEXT="Performance too good to be true?" ID="ID_813710886" CREATED="1642965276263" MODIFIED="1642965282360"/>
</node>
<node TEXT="Certifiably Robust Federated Learning against Backdoor Attacks" ID="ID_741627861" CREATED="1642965359811" MODIFIED="1642965362229">
<icon BUILTIN="unchecked"/>
<node TEXT="icml21" ID="ID_359293148" CREATED="1642965364960" MODIFIED="1642965369038"/>
</node>
<node TEXT="FederatedReverse: A Detection and Defense Method Against Backdoor Attacks in Federated Learning" ID="ID_1755199424" CREATED="1643036656254" MODIFIED="1643036673341">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="Defending against Backdoors in Federated Learning with Robust Learning Rate AAAI21" ID="ID_1421685316" CREATED="1643036746252" MODIFIED="1643036830465">
<icon BUILTIN="unchecked"/>
<node TEXT="at high level, our defense is based on carefully adjusting the aggregation server&apos;s learning rate, per dimension and per round, based on the sign infirmation of agents&apos; updates" ID="ID_735008317" CREATED="1643036783427" MODIFIED="1643036817264"/>
</node>
</node>
<node TEXT="Filtering" ID="ID_1962822246" CREATED="1646924812678" MODIFIED="1646924814463">
<node TEXT="Towards Effective and Robust Neural Trojan Defenses via Input Filtering" ID="ID_199989646" CREATED="1646924820093" MODIFIED="1646924826803">
<icon BUILTIN="pencil"/>
<node TEXT="(1) variational input filtering: lossy data compression" ID="ID_193654578" CREATED="1647266258774" MODIFIED="1647266324880"/>
<node TEXT="(2) adversarial input filtering: adversarial learning" ID="ID_1607964159" CREATED="1647266267852" MODIFIED="1647266332225"/>
</node>
</node>
</node>
</node>
<node TEXT="Capsule Network" FOLDED="true" POSITION="left" ID="ID_1447197719" CREATED="1648341702034" MODIFIED="1648341707064">
<edge COLOR="#00ffff"/>
<node TEXT="HP-Capsule: Unsupervised Face Part Discovery by Hierarchical Parsing Capsule Network" ID="ID_1960737685" CREATED="1648341745946" MODIFIED="1648341753103">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_76112470" CREATED="1648341754220" MODIFIED="1648341755486"/>
<node TEXT="capsule networks are designed to present the objects by a set of parts and their relationships, which provide an insight into the procedure of visual perception" ID="ID_1555851286" CREATED="1648341756627" MODIFIED="1648341784222">
<icon BUILTIN="info"/>
</node>
</node>
</node>
<node TEXT="Comp. Linguistics" FOLDED="true" POSITION="left" ID="ID_865579939" CREATED="1610008291436" MODIFIED="1642394269419">
<edge COLOR="#0000ff"/>
<node TEXT="Language Representation" ID="ID_1139506578" CREATED="1583319666984" MODIFIED="1648343280371">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="ELMo" ID="ID_1144092497" CREATED="1583321768820" MODIFIED="1587526864978">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="BERT" ID="ID_970622765" CREATED="1583321766003" MODIFIED="1587526864978">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="XLNet" ID="ID_214926633" CREATED="1583321774388" MODIFIED="1587526864978">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="ALBERT" ID="ID_1612144981" CREATED="1583321771476" MODIFIED="1587526864978">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2012.10873&#xa;Sequence-to-Sequence Contrastive Learning for Text Recognition" ID="ID_936047798" CREATED="1608874619355" MODIFIED="1608874629781"/>
<node TEXT="NAACL: Revisiting Document Representations for Large-Scale Zero-Shot Learning" ID="ID_849682508" CREATED="1619332223950" MODIFIED="1619332231274"/>
</node>
<node TEXT="sentence embedding" ID="ID_1959395028" CREATED="1610008316533" MODIFIED="1610008319960">
<node TEXT="1908: Sentence-BERT: Sentence Embeddings using Siamese BERT-Networks" ID="ID_1743352469" CREATED="1610008346393" MODIFIED="1610008369033"/>
<node TEXT="EMNLP: on the sentence embeddings from pre-trained language models" ID="ID_413909105" CREATED="1619332268062" MODIFIED="1619332289375"/>
</node>
<node TEXT="Sequence Modeling" ID="ID_351729279" CREATED="1618848122097" MODIFIED="1645045266470">
<node TEXT="vanilla RNN" ID="ID_470266436" CREATED="1645045269394" MODIFIED="1645045280217"/>
<node TEXT="LSTM" ID="ID_1361137265" CREATED="1645045281093" MODIFIED="1645045282870"/>
<node TEXT="GRU" ID="ID_1280881087" CREATED="1645045275154" MODIFIED="1645045276501"/>
<node TEXT="TCN" ID="ID_789045066" CREATED="1618848128609" MODIFIED="1618848130007">
<node TEXT="An Empirical Evaluation of Generic Convolutional and Recurrent Networksfor Sequence Modeling" ID="ID_1211179302" CREATED="1618848130953" MODIFIED="1618848146516">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node TEXT="Continual Learning" FOLDED="true" POSITION="left" ID="ID_28623311" CREATED="1649110273675" MODIFIED="1649110277766">
<edge COLOR="#ff00ff"/>
<node TEXT="Probing Representation Forgetting in Supervised and Unsupervised Continual Learning" ID="ID_73452629" CREATED="1649110278617" MODIFIED="1649110284775"/>
</node>
<node TEXT="Cross-Modal Retrieval" FOLDED="true" POSITION="left" ID="ID_22749108" CREATED="1576988430324" MODIFIED="1642431082632">
<font NAME="Gentium" BOLD="false"/>
<edge COLOR="#7c0000"/>
<node TEXT="Image-Text" ID="ID_22916358" CREATED="1649103179989" MODIFIED="1649103183136">
<node TEXT="VSE Family" ID="ID_730414508" CREATED="1652383580081" MODIFIED="1652383582802">
<node TEXT="Unifying ..." ID="ID_1819265540" CREATED="1586240031907" MODIFIED="1587526865103">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="VSE++" ID="ID_612993504" CREATED="1586240024674" MODIFIED="1587526865105">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="sota" ID="ID_516293267" CREATED="1586240099321" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="flickr30k" ID="ID_546511377" CREATED="1586240101669" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 52.9" ID="ID_550891352" CREATED="1586240111612" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 39.6" ID="ID_884008793" CREATED="1586240112874" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="ms-coco" ID="ID_613323924" CREATED="1586240106613" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1k" ID="ID_1421179749" CREATED="1586240136945" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 64.6" ID="ID_1633553206" CREATED="1586240142100" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 52.0" ID="ID_1077994917" CREATED="1586240148569" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="5k" ID="ID_1512427449" CREATED="1586240138622" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 41.3" ID="ID_1725968697" CREATED="1586240155222" MODIFIED="1587526865106">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 30.3" ID="ID_481195263" CREATED="1586240162060" MODIFIED="1587526865107">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="SS" ID="ID_1699285095" CREATED="1587025688098" MODIFIED="1587526865107">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="VSE infty" ID="ID_1318319088" CREATED="1652383593326" MODIFIED="1652383596175">
<node TEXT="Learning the Best Pooling Strategy for Visual Semantic Embedding" ID="ID_182415412" CREATED="1652383597008" MODIFIED="1652383603736">
<node TEXT="CVPR21" ID="ID_478107870" CREATED="1652383607066" MODIFIED="1652383608206"/>
</node>
</node>
</node>
<node TEXT="SCAN (ECCV18): stacked cross attention for image-text matching" ID="ID_1439311497" CREATED="1583370940211" MODIFIED="1587526865107">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image-text matching" ID="ID_645118810" CREATED="1586239421227" MODIFIED="1587526865108">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: simply aggregates the similarity of all possible pairs of regions and words without attending differentially to more and less important words or regions" ID="ID_1265894543" CREATED="1586239425245" MODIFIED="1587526865110">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: discover full latent alignment using both image regions and words in a sentence as contect and infer image-text similarity" ID="ID_1636622031" CREATED="1586239793548" MODIFIED="1587526865120">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="sota" ID="ID_845468264" CREATED="1586239941699" MODIFIED="1587526865121">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="flickr30k" ID="ID_217483876" CREATED="1586239945220" MODIFIED="1587526865121">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 67.4" ID="ID_1486644891" CREATED="1586239975412" MODIFIED="1587526865121">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 48.6" ID="ID_1708382186" CREATED="1586240008708" MODIFIED="1587526865121">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="mscoco" ID="ID_140444515" CREATED="1586239949723" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1k" ID="ID_1577581954" CREATED="1586240056672" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 72.7" ID="ID_10874274" CREATED="1586240060731" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 58.8" ID="ID_1159300585" CREATED="1586240075534" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="5k" ID="ID_1654579943" CREATED="1586240057954" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 50.4" ID="ID_1519119507" CREATED="1586240081649" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 38.6" ID="ID_590281563" CREATED="1586240089142" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="(IJCAI-19 RDAN): Multi-level visua;-semantic alighments with relation-wise dual attention network for image and text matching" ID="ID_1620531353" CREATED="1583370943434" MODIFIED="1587526865122">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image-text matching" ID="ID_1600229973" CREATED="1586241116612" MODIFIED="1587526865124">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: abstract workds such as actions" ID="ID_182389430" CREATED="1586241120623" MODIFIED="1587526865125">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: RDAN model" ID="ID_210476871" CREATED="1586241159657" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="sota" ID="ID_975868300" CREATED="1586240949378" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="flickr30k" ID="ID_1658409783" CREATED="1586240952468" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 68.1" ID="ID_1999984019" CREATED="1586240956592" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 54.1" ID="ID_1298566819" CREATED="1586240968485" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="mscoco" ID="ID_984904686" CREATED="1586240980411" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1k" ID="ID_783199489" CREATED="1586241018437" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 74.6" ID="ID_1273929468" CREATED="1586240983982" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 61.6" ID="ID_1540729136" CREATED="1586240991529" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="1907.09748: (PFAN): Position Focused Attention Network for Image-Text Matching" ID="ID_470316208" CREATED="1583370946887" MODIFIED="1587526865126">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image-text matching" ID="ID_428335848" CREATED="1586240188876" MODIFIED="1587526865128">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: PFAN to investigate the object position clue to enhance visual-text joint-embedding learning" ID="ID_1835652632" CREATED="1586240194100" MODIFIED="1587526865129">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="sota" ID="ID_1110318065" CREATED="1586240329549" MODIFIED="1587526865129">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="flickr30k" ID="ID_689076008" CREATED="1586240331929" MODIFIED="1587526865129">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 70.0" ID="ID_770808018" CREATED="1586240337972" MODIFIED="1587526865130">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 50.4" ID="ID_1485095872" CREATED="1586240347463" MODIFIED="1587526865130">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="mscoco" ID="ID_1563565673" CREATED="1586240334413" MODIFIED="1587526865130">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1k" ID="ID_48740691" CREATED="1586240355540" MODIFIED="1587526865130">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="s-i: 76.5" ID="ID_947149652" CREATED="1586240361408" MODIFIED="1587526865130">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="i-s: 61.6" ID="ID_522407065" CREATED="1586240366364" MODIFIED="1587526865130">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="1909.02701: VSRN: visual semantic reasoning for image-text matching (ICCV19)" ID="ID_761207430" CREATED="1587527540837" MODIFIED="1587527570936"/>
<node TEXT="1911.07528: Ladder Loss" ID="ID_879911839" CREATED="1609680873410" MODIFIED="1609680889225">
<icon BUILTIN="bookmark"/>
<font BOLD="true"/>
<node TEXT="1904.09626: deep metric learning beyond binary supervision" ID="ID_1691858159" CREATED="1609681031569" MODIFIED="1609681049923"/>
</node>
<node TEXT="2002.10016: deep multimodal image-text embeddings for automatic cross-media retrieval" ID="ID_1544665375" CREATED="1583309899207" MODIFIED="1587526865130">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image-text retrieval" ID="ID_546012275" CREATED="1583309919471" MODIFIED="1587526865131">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="the author is having fun" ID="ID_1862649220" CREATED="1583309936837" MODIFIED="1587526865132">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.08510: expressing objects just like words: recurrent visual embedding for image-text matching" ID="ID_18086236" CREATED="1583370893620" MODIFIED="1587526865133">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image-text matching" ID="ID_869083995" CREATED="1583371002035" MODIFIED="1587526865133">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: existing works ignore the connections between the objects that are semantically related; these objectis may collectively determine whether the image corresponds to a text or not" ID="ID_1942940677" CREATED="1583371027768" MODIFIED="1587526865135">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: propose a dual path recurrent neural network which processes images and sentences symmetrically by recurrent neural networks" ID="ID_327990430" CREATED="1583371074343" MODIFIED="1587526865136">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2003.03772: IMRAM: iterative matching with recurrent attention memory for cross-modal image-text retrieval (CVPR20)" ID="ID_1607556695" CREATED="1589849966539" MODIFIED="1589850001017">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
<node TEXT="sota" ID="ID_645097460" CREATED="1589850066064" MODIFIED="1589850069372"/>
<node TEXT="f30k" ID="ID_952774624" CREATED="1589850089969" MODIFIED="1589850092250">
<node TEXT="74, 79" ID="ID_943817756" CREATED="1589850094969" MODIFIED="1589850105582"/>
</node>
<node TEXT="coco" ID="ID_897203196" CREATED="1589850092464" MODIFIED="1589850093338">
<node TEXT="76, 61" ID="ID_1993874426" CREATED="1589850107320" MODIFIED="1589850115454"/>
</node>
</node>
<node TEXT="2004.09144: transformer reasoning network for image-text matching and retrieval" ID="ID_1179239931" CREATED="1587527423924" MODIFIED="1587527513062">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="2004.12070: hyperref MMNas" ID="ID_883213530" CREATED="1588074737070" MODIFIED="1589849965229">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="f30k" ID="ID_924866619" CREATED="1588074753608" MODIFIED="1588074755550">
<node TEXT="78.3 / 60.7" ID="ID_1428749887" CREATED="1588074757897" MODIFIED="1588074771032"/>
</node>
<node TEXT="maybe the new SOTA. Apr. 2020" ID="ID_544717570" CREATED="1588074809536" MODIFIED="1588074816258"/>
</node>
<node TEXT="2005.09801: fashionBERT: text and image matching with adaptive loss for cross-modal retrieval" ID="ID_1184415285" CREATED="1590025829965" MODIFIED="1590025860526">
<node TEXT="no eval on f30k or coco" ID="ID_27965399" CREATED="1590025862784" MODIFIED="1590025868262"/>
</node>
<node TEXT="BFAN: Focus your attention:A bidirectional focal attention network for image-text match (ACM MM)" ID="ID_321497763" CREATED="1590802159181" MODIFIED="1590802206958"/>
<node TEXT="2003.03669: Adaptive Offline Quintuplet Loss for Image-Text Matching" ID="ID_177378639" CREATED="1590748813000" MODIFIED="1590748827292">
<node TEXT="f30k: 73 / 54" ID="ID_112288869" CREATED="1590802112238" MODIFIED="1590802125493"/>
<node TEXT="coco: 77 / 96" ID="ID_613186739" CREATED="1590802125927" MODIFIED="1590802145018"/>
</node>
<node TEXT="2004.01095: MCEN: Bridging Cross-Modal Gap between Cooking Recipes and Dish Images with Latent Variable Model (cvpr20)" ID="ID_576418454" CREATED="1590749993141" MODIFIED="1590750006537"/>
<node TEXT="2004.00277 Graph Structured Network for Image-Text Matching (cvpr20)" ID="ID_1616725398" CREATED="1590806554894" MODIFIED="1590806586098">
<icon BUILTIN="button_ok"/>
<node TEXT="f30K: 76 / 57" ID="ID_1331739001" CREATED="1590806678424" MODIFIED="1590806686909"/>
<node TEXT="coco: 78 / 63" ID="ID_290191269" CREATED="1590806687324" MODIFIED="1590806696701"/>
<node TEXT="sota May 2020" ID="ID_1480141631" CREATED="1590806725154" MODIFIED="1590806728113"/>
</node>
<node TEXT="2010.01666: multi-modal retrieval using graph neural networks" ID="ID_1676547166" CREATED="1608010937162" MODIFIED="1608010981920">
<node TEXT="no comparison; inclined to qualitative evaluations" ID="ID_288207961" CREATED="1608010983165" MODIFIED="1608010996471"/>
</node>
<node TEXT="Cross-Modal Common Representation Learning with Triplet Loss Functions" ID="ID_1827284412" CREATED="1646944290551" MODIFIED="1646944296357">
<icon BUILTIN="pencil"/>
<node TEXT="fair" ID="ID_1712372002" CREATED="1646944300395" MODIFIED="1646944304415"/>
</node>
</node>
<node TEXT="Image-Recipe" ID="ID_552619459" CREATED="1650809005463" MODIFIED="1650809008428">
<node TEXT="Transformer Decoders with MultiModal Regularization for Cross-Modal Food Retrieval" ID="ID_778443774" CREATED="1650809009512" MODIFIED="1650809016539">
<node TEXT="CVPR22w" ID="ID_253625686" CREATED="1650809019210" MODIFIED="1650809021151"/>
</node>
</node>
<node TEXT="Cross-lingual" ID="ID_1275207266" CREATED="1607576963588" MODIFIED="1649103229953">
<node TEXT="2012.05107: towards zero-shot cross-lingual image retrieval" ID="ID_33321188" CREATED="1607576975335" MODIFIED="1607576995999">
<icon BUILTIN="button_ok"/>
<node TEXT="multi-modal representations using cross-lingual pre-training on the text side" ID="ID_761823260" CREATED="1607577008171" MODIFIED="1607577023025"/>
<node TEXT="objective function which tightens the text embedding clusters by pushing dissimilar texts from each other" ID="ID_278870167" CREATED="1607577035945" MODIFIED="1607577055909"/>
</node>
</node>
<node TEXT="Audio-Image" ID="ID_1999112869" CREATED="1619443906882" MODIFIED="1649103236875">
<node TEXT="Talk, Don&apos;t Write: A Study of Direct Speech-Based Image Retrieval" ID="ID_1895575295" CREATED="1619443912720" MODIFIED="1619443918857"/>
</node>
<node TEXT="Video-Text" ID="ID_387413475" CREATED="1649103145467" MODIFIED="1649103148698">
<node TEXT="X-Pool: Cross-Modal Language-Video Attention for Text-Video Retrieval" ID="ID_363825595" CREATED="1649103154257" MODIFIED="1649103155281">
<node TEXT="CVPR22" ID="ID_1553240026" CREATED="1649103156491" MODIFIED="1649103158193"/>
</node>
<node TEXT="Disentangled Representation Learning for Text-Video Retrieval" ID="ID_1578456483" CREATED="1649168592201" MODIFIED="1649168592807">
<node TEXT="ECCV22" ID="ID_1572394517" CREATED="1649168594046" MODIFIED="1649168598874"/>
</node>
</node>
<node TEXT="Variant: Continual Learning" ID="ID_131519106" CREATED="1619336659546" MODIFIED="1619336666539">
<node TEXT="CVPR21w: Continual learning in cross-modal retrieval" ID="ID_834969689" CREATED="1619336667454" MODIFIED="1619336675662"/>
</node>
</node>
<node TEXT="Dataset Distillation" FOLDED="true" POSITION="left" ID="ID_265073126" CREATED="1648338075798" MODIFIED="1648343254704">
<icon BUILTIN="idea"/>
<edge COLOR="#ff00ff"/>
<node TEXT="Dataset Distillation by Matching Training Trajectories" ID="ID_1232754909" CREATED="1648338079783" MODIFIED="1648338088477">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_951175501" CREATED="1648338090454" MODIFIED="1648338092141"/>
</node>
</node>
<node TEXT="DeepFake" FOLDED="true" POSITION="left" ID="ID_81253926" CREATED="1587729755009" MODIFIED="1642394342542">
<edge COLOR="#007c7c"/>
<node TEXT="Dataset" ID="ID_1886590251" CREATED="1615347464586" MODIFIED="1615347466780">
<node TEXT="ForgeryNet: A Versatile Benchmark for Comprehensive Forgery Analysis (cvpr21)" ID="ID_1435934248" CREATED="1615347467706" MODIFIED="1615347479218"/>
</node>
<node TEXT="Survey" ID="ID_1081735994" CREATED="1587729760036" MODIFIED="1587729764469">
<node TEXT="2004.11138: the creation and detection of deepfakes: a survey" ID="ID_229656376" CREATED="1587729766371" MODIFIED="1587729930927">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2004.12626: preliminary forensics analysis of deepfake images" ID="ID_913965" CREATED="1588068509056" MODIFIED="1588068524708"/>
<node TEXT="Seeing is Living? Rethinking the Security of Facial Liveness Verification in the Deepfake Era" ID="ID_461047989" CREATED="1647267019291" MODIFIED="1647267027027">
<icon BUILTIN="unchecked"/>
<node TEXT="USENIX22" ID="ID_1148613248" CREATED="1647267021829" MODIFIED="1647267024253"/>
</node>
</node>
<node TEXT="Create" ID="ID_1812140643" CREATED="1589767913067" MODIFIED="1648338593271">
<node TEXT="2005.05535: deepfacelab: a simple flexible and extensible face swapping framework" ID="ID_1258022523" CREATED="1589767931055" MODIFIED="1589767949771"/>
<node TEXT="2004.07165 A recurrent cycle consistency loss for progressive face-to-face synthesis (FG 2020 oral)" ID="ID_1521921331" CREATED="1590805531639" MODIFIED="1590805549067"/>
</node>
<node TEXT="Detection" ID="ID_1897134747" CREATED="1588389497295" MODIFIED="1619337612320">
<node TEXT="2003.08685: leveraging frequency analysis for deep fake image recognition" ID="ID_252600993" CREATED="1588389509200" MODIFIED="1588389530379">
<node TEXT="frequency analysis" ID="ID_818362087" CREATED="1588389590906" MODIFIED="1588389598678"/>
</node>
<node TEXT="2003.08645: detecting deepfakes with metric learning" ID="ID_1878482148" CREATED="1588389618001" MODIFIED="1588389634521"/>
<node TEXT="2005.05632: detecting cnn-generated facial images in real-world scenarios (CVPR20w)" ID="ID_1299699785" CREATED="1589767828944" MODIFIED="1589767856220"/>
<node TEXT="Multi-attentional Deepfake Detection (cvpr21)" ID="ID_781694096" CREATED="1615298559426" MODIFIED="1615298567054">
<node TEXT="bincls -&gt; fine-grained cls" ID="ID_1081064126" CREATED="1615298568075" MODIFIED="1615298788725"/>
<node TEXT="multi-attentional" ID="ID_634769208" CREATED="1615298789067" MODIFIED="1615298801115"/>
</node>
<node TEXT="CVPR21: Improving the Efficiency and Robustness of Deepfakes Detection through Precise Geometric Features" ID="ID_1786812758" CREATED="1619337614061" MODIFIED="1619337626336"/>
<node TEXT="Self-supervised Learning of Adversarial Example: Towards Good Generalizations for Deepfake Detection" ID="ID_1441114547" CREATED="1648338608774" MODIFIED="1648338611335">
<node TEXT="CVPR22" ID="ID_409760991" CREATED="1648338612963" MODIFIED="1648338614477"/>
</node>
<node TEXT="ObjectFormer for Image Manipulation Detection and Localization" ID="ID_926927385" CREATED="1649102118496" MODIFIED="1649102119754">
<node TEXT="CVPR22" ID="ID_1894520939" CREATED="1649102122404" MODIFIED="1649102123634"/>
</node>
<node TEXT="Detecting Deepfakes with Self-Blended Images" ID="ID_1330201072" CREATED="1650818257543" MODIFIED="1650818258952">
<node TEXT="CVPR22" ID="ID_678441173" CREATED="1650818260394" MODIFIED="1650818261846"/>
</node>
<node TEXT="Voice-Face Homogeneity Tells Deepfake" ID="ID_153357596" CREATED="1646942317853" MODIFIED="1646942325559">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_725170575" CREATED="1646942319120" MODIFIED="1646942321768"/>
</node>
</node>
</node>
<node TEXT="Diffusion Models" FOLDED="true" POSITION="left" ID="ID_543295574" CREATED="1645111157251" MODIFIED="1645111160424">
<edge COLOR="#00ffff"/>
<node TEXT="https://lilianweng.github.io/lil-log/2021/07/11/diffusion-models.html" ID="ID_626991727" CREATED="1645111188223" MODIFIED="1645111188223" LINK="https://lilianweng.github.io/lil-log/2021/07/11/diffusion-models.html"/>
<node TEXT="Diffusion Models Beat GANs on Image Synthesis" ID="ID_1871658132" CREATED="1645111161757" MODIFIED="1645111172504">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Pseudo Numerical Methods for Diffusion Models on Manifolds" ID="ID_1101187024" CREATED="1647264822390" MODIFIED="1647264900126">
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR22" ID="ID_489004075" CREATED="1647264824614" MODIFIED="1647264826627"/>
<node TEXT="standard diffusion is very slow and involves too many steps for inference" ID="ID_688183538" CREATED="1647264827569" MODIFIED="1647264848261"/>
<node TEXT="DDPMs should be treated as solving differential equations on manifolds" ID="ID_80114006" CREATED="1647264848863" MODIFIED="1647264863012"/>
<node TEXT="significantly speedup inference and made it more practical" ID="ID_1736885770" CREATED="1647264863514" MODIFIED="1647264895412"/>
</node>
</node>
<node TEXT="Dimension Reduction" FOLDED="true" POSITION="left" ID="ID_703196004" CREATED="1648336542877" MODIFIED="1648336545945">
<edge COLOR="#00ff00"/>
<node TEXT="PCA" ID="ID_279239785" CREATED="1648336547244" MODIFIED="1648336549636"/>
<node TEXT="t-SNE" ID="ID_372306146" CREATED="1648336550147" MODIFIED="1648336553078"/>
<node TEXT="h-NNE" ID="ID_530951729" CREATED="1648336553298" MODIFIED="1648336598952">
<node TEXT="Hierarchical Nearest Neighbor Graph Embedding for Efficient Dimensionality&#xa;Reduction" ID="ID_1074126704" CREATED="1648336600093" MODIFIED="1648336616328">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1719348477" CREATED="1648336618438" MODIFIED="1648336619839"/>
<node TEXT="no-optimization" ID="ID_708278320" CREATED="1648336626204" MODIFIED="1648336628663"/>
</node>
</node>
</node>
<node TEXT="Domain Adaptation" FOLDED="true" POSITION="left" ID="ID_1179225618" CREATED="1582887416803" MODIFIED="1642394196054">
<font NAME="Gentium" BOLD="false"/>
<edge COLOR="#ff0000"/>
<node TEXT="Concept" ID="ID_276280133" CREATED="1650832531233" MODIFIED="1650832534030">
<icon BUILTIN="info"/>
<node TEXT="domain adaptation" ID="ID_1897127261" CREATED="1650832537525" MODIFIED="1650832545079">
<node TEXT="input distribution p(x) changes between training and test" ID="ID_1783851732" CREATED="1650832546158" MODIFIED="1650832566190"/>
<node TEXT="transfer learning posterior p(y|x) changes between training and test" ID="ID_542172298" CREATED="1650832567330" MODIFIED="1650832588622"/>
</node>
</node>
<node TEXT="Openset" ID="ID_652345966" CREATED="1587030820215" MODIFIED="1587526864981">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2004.04388: Towards Inheritable Models for Open-Set Domain Adaptation (CVPR20)" ID="ID_882848776" CREATED="1587030828036" MODIFIED="1587526864982">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.08675: unsupervised domain adaptation via discriminative manifold embedding and alignment" ID="ID_1783103324" CREATED="1582887489485" MODIFIED="1587526864982">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: domain adaptation" ID="ID_364211866" CREATED="1582887681503" MODIFIED="1587526864984">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why:" ID="ID_767525254" CREATED="1582887693719" MODIFIED="1587526864984">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1. hardcoded pseudo labels on the target domain are risky to the intrinsic data structure" ID="ID_1466382959" CREATED="1582887707992" MODIFIED="1587526864985">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2. batch-wise trianing manner in deep learning limites the description of the global structure" ID="ID_333992277" CREATED="1582887727498" MODIFIED="1587526864986">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="how: Riemannian manifold learning framework is proposed to achieve transferability and discriminability." ID="ID_932362154" CREATED="1582887750636" MODIFIED="1587526864986">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.11770: rethinking the hyperparameters for fine-tuning (ICLR20)" ID="ID_687246947" CREATED="1583323656825" MODIFIED="1587526864987">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: examines fine-tuning" ID="ID_126121824" CREATED="1583323676904" MODIFIED="1587526864988">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="findings" ID="ID_1484997220" CREATED="1583323715662" MODIFIED="1587526864988">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1. momentum affects fine-tuning performance as suggested by previous theoretical findings" ID="ID_409173341" CREATED="1583323717892" MODIFIED="1587526864988">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2. learning reate is not only dataset dependent, but also sensitive to the similarity between source and target domain" ID="ID_406913796" CREATED="1583323812333" MODIFIED="1587526864989">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="3. reference-based regularization that keeps models close the the initial model does not necessarily apply for &quot;dissimilar&quot; datasets." ID="ID_409659201" CREATED="1583323868185" MODIFIED="1587526864990">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="2002.04206: dual-triplet metric learning for unsupervised domain adaptation in video-based face recognition" ID="ID_1852118217" CREATED="1583375335641" MODIFIED="1587526864991">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2004.04393: Universal Source-Free Domain Adaptation (CVPR20)" ID="ID_678847990" CREATED="1587030701603" MODIFIED="1587526864992">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2004.08878: uncertainty-aware consistency regularization for cross-domain semantic segmentation" ID="ID_1060047510" CREATED="1587873167086" MODIFIED="1587873185991"/>
<node TEXT="2004.09403: class distribution alignment for adversarial domain adaptation" ID="ID_1913880236" CREATED="1588082459830" MODIFIED="1588082478774"/>
<node TEXT="2012.12302&#xa;Flexible deep transfer learning by separate feature embeddings and manifold alignment" ID="ID_1781903227" CREATED="1608871572614" MODIFIED="1608871585302">
<node TEXT="a bit similar to cross-modal alignment" ID="ID_867737025" CREATED="1608871591299" MODIFIED="1608871600207"/>
</node>
<node TEXT="2012.11807&#xa;Learning Disentangled Semantic Representation for Domain Adaptation" ID="ID_1101645023" CREATED="1608871972133" MODIFIED="1608871978855"/>
<node TEXT="Auto-Transfer: Learning to Route Transferrable Representations ICLR22" ID="ID_275822091" CREATED="1644180783785" MODIFIED="1644180796263"/>
<node TEXT="Fine-Tuning can Distort Pretrained Features and Underperform Out-of-Distribution" ID="ID_1001645107" CREATED="1647264390521" MODIFIED="1647264398367">
<icon BUILTIN="unchecked"/>
<node TEXT="ICLR22" ID="ID_248597371" CREATED="1647264392555" MODIFIED="1647264395025"/>
</node>
<node TEXT="Does Robustness on ImageNet Transfer to Downstream Tasks?" ID="ID_442746527" CREATED="1650842399693" MODIFIED="1650842400541">
<node TEXT="CVPR22" ID="ID_391235371" CREATED="1650842401650" MODIFIED="1650842402914"/>
</node>
<node TEXT="Adaptive Domain Adaptation" ID="ID_926597894" CREATED="1649130053250" MODIFIED="1649130058744">
<node TEXT="Learning Distinctive Margin toward Active Domain Adaptation" ID="ID_1937782410" CREATED="1649130059707" MODIFIED="1649130060490">
<node TEXT="CVPR22" ID="ID_1742639409" CREATED="1649130061738" MODIFIED="1649130063012"/>
<node TEXT="similar to SVM" ID="ID_1059316080" CREATED="1649130063284" MODIFIED="1649130069027"/>
</node>
</node>
<node TEXT="Unsupervised Domain Adaptation" ID="ID_1180792051" CREATED="1650824963676" MODIFIED="1650824970144">
<node TEXT="Safe Self-Refinement for Transformer-based Domain Adaptation" ID="ID_1500347419" CREATED="1650824973786" MODIFIED="1650824979577">
<node TEXT="CVPR22" ID="ID_439499097" CREATED="1650824980672" MODIFIED="1650824982140"/>
</node>
</node>
</node>
<node TEXT="Facial Expression" FOLDED="true" POSITION="left" ID="ID_1665603625" CREATED="1648335974432" MODIFIED="1648336221787">
<edge COLOR="#0000ff"/>
<node TEXT="Towards Semi-Supervised Deep Facial Expression Recognition with An Adaptive Confidence Margin" ID="ID_1230150992" CREATED="1648335980975" MODIFIED="1648336159200">
<icon BUILTIN="unchecked"/>
<font ITALIC="false"/>
<node TEXT="CVPR22" ID="ID_1243722840" CREATED="1648336163039" MODIFIED="1648336170532"/>
<node TEXT="semi-supervised" ID="ID_1980840722" CREATED="1648336171391" MODIFIED="1648336176030"/>
<node TEXT="better leverage unlabeled data" ID="ID_316244842" CREATED="1648336176523" MODIFIED="1648336184906"/>
</node>
</node>
<node TEXT="GAN Inverstion" FOLDED="true" POSITION="left" ID="ID_1643977837" CREATED="1608014251316" MODIFIED="1642394381732">
<edge COLOR="#0000ff"/>
<node TEXT="2004.00049: in-domain gan inversion for real image editing" ID="ID_789901011" CREATED="1608014256819" MODIFIED="1608014290154">
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="2007.06600: closed-form factorization of latent semantics in GANs" ID="ID_190477062" CREATED="1624973853281" MODIFIED="1624973903147">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="launch"/>
<node TEXT="I like this paper" ID="ID_807310527" CREATED="1624973921489" MODIFIED="1624973924563"/>
<node TEXT="SeFa" ID="ID_485890930" CREATED="1624974410069" MODIFIED="1624974411493"/>
</node>
<node TEXT="2012.09036&#xa;Improved StyleGAN Embedding: Where are the Good Latents?" ID="ID_1195062919" CREATED="1608879346483" MODIFIED="1608879350891"/>
<node TEXT="2103.03243 Anycost GANs for Interactive Image Synthesis and Editing (cvpr21)" ID="ID_1065575004" CREATED="1615296747093" MODIFIED="1615296755894">
<node TEXT="elastic resolution + computation reduction" ID="ID_1366331350" CREATED="1615296878773" MODIFIED="1615296919734"/>
</node>
</node>
<node TEXT="Graph Network" FOLDED="true" POSITION="left" ID="ID_753946836" CREATED="1582940931218" MODIFIED="1651953060730">
<font NAME="Gentium" BOLD="false"/>
<edge COLOR="#7c7c00"/>
<node TEXT="GNN" ID="ID_1274111681" CREATED="1651953050512" MODIFIED="1651953052009">
<node TEXT="2002.05544: superpixel image classification with graph attention networks" ID="ID_332176668" CREATED="1583375009739" MODIFIED="1587526865279">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Second-Order Pooling for Graph Neural Networks (tpami20)" ID="ID_445336652" CREATED="1615351166321" MODIFIED="1615351177272">
<node TEXT="pooling method" ID="ID_564494170" CREATED="1615351183774" MODIFIED="1615351187164"/>
</node>
</node>
<node TEXT="GCN" ID="ID_1722177383" CREATED="1601902008395" MODIFIED="1601902010750"/>
</node>
<node TEXT="Image Compression" FOLDED="true" POSITION="left" ID="ID_1511931365" CREATED="1648343124472" MODIFIED="1648343127424">
<edge COLOR="#7c0000"/>
<node TEXT="ELIC: Efficient Learned Image Compression with&#xa;Unevenly Grouped Space-Channel Contextual Adaptive Coding" ID="ID_391462928" CREATED="1648343132187" MODIFIED="1648343146503">
<node TEXT="CVPR22" ID="ID_1107484924" CREATED="1648343143028" MODIFIED="1648343144450"/>
</node>
<node TEXT="Unified Multivariate Gaussian Mixture for Efficient Neural Image Compression" ID="ID_525288991" CREATED="1648343235650" MODIFIED="1648343236782">
<node TEXT="CVPR22" ID="ID_1065954996" CREATED="1648343238753" MODIFIED="1648343240041"/>
</node>
</node>
<node TEXT="Image Retrieval" FOLDED="true" POSITION="left" ID="ID_1006799943" CREATED="1649630594458" MODIFIED="1649630598110">
<edge COLOR="#007c00"/>
<node TEXT="Correlation Verification for Image Retrieval" ID="ID_1013972134" CREATED="1649630600849" MODIFIED="1649630693321">
<node TEXT="CVPR22" ID="ID_1345324489" CREATED="1649630694449" MODIFIED="1649630695940"/>
</node>
<node TEXT="Learning Super-Features for Image Retrieval" ID="ID_829824427" CREATED="1644181270753" MODIFIED="1644181364623">
<icon BUILTIN="checked"/>
<node TEXT="ICLR22" ID="ID_1211217354" CREATED="1644181278285" MODIFIED="1644181280219"/>
<node TEXT="not DML" ID="ID_1043681772" CREATED="1644181273835" MODIFIED="1644181275311"/>
<node TEXT="combine local and global features" ID="ID_1751979954" CREATED="1644181344128" MODIFIED="1644181349980"/>
<node TEXT="existing methods suffer from problems in the use of local features" ID="ID_111945206" CREATED="1644181350883" MODIFIED="1644181360186"/>
</node>
</node>
<node TEXT="Instance Segmentation" FOLDED="true" POSITION="left" ID="ID_1759449865" CREATED="1649104217760" MODIFIED="1649104221244">
<edge COLOR="#7c7c00"/>
<node TEXT="Main" ID="ID_1417928999" CREATED="1578733356683" MODIFIED="1649104710111">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="Mask R-CNN" ID="ID_1460418401" CREATED="1590805638982" MODIFIED="1590805641615"/>
<node TEXT="2003.13954 FGN: Fully Guided Network for Few-Shot Instance Segmentation (cvpr20)" ID="ID_1454655339" CREATED="1590975022052" MODIFIED="1590975029134"/>
<node TEXT="2012.07177&#xa;Simple Copy-Paste is a Strong Data Augmentation Method for Instance Segmentation" ID="ID_792173050" CREATED="1608880556971" MODIFIED="1608880566837"/>
</node>
<node TEXT="Descriptors" ID="ID_1696499735" CREATED="1649104065329" MODIFIED="1649104223403">
<node TEXT="Contour" ID="ID_395676166" CREATED="1649104068494" MODIFIED="1649104069863">
<node TEXT="Eigencontours: Novel Contour Descriptors Based on Low-Rank Approximation" ID="ID_362471390" CREATED="1649104078004" MODIFIED="1649104078579">
<node TEXT="CVPR22" ID="ID_642950560" CREATED="1649104080637" MODIFIED="1649104081863"/>
<node TEXT="contour is one of the most important object descriptiors, along with texture and color" ID="ID_961927899" CREATED="1649104082894" MODIFIED="1649104097966"/>
</node>
</node>
</node>
</node>
<node TEXT="Machine Learning" FOLDED="true" POSITION="left" ID="ID_1123557953" CREATED="1642430639041" MODIFIED="1642430641511">
<edge COLOR="#7c007c"/>
<node TEXT="Gaussian Process" ID="ID_1656444615" CREATED="1642430730867" MODIFIED="1642430733848">
<node TEXT="deep neural networks as point estimates for deep gaussian processes" ID="ID_1084429400" CREATED="1642430711652" MODIFIED="1642430742869">
<icon BUILTIN="unchecked"/>
</node>
</node>
</node>
<node TEXT="Model Compression" FOLDED="true" POSITION="left" ID="ID_325472209" CREATED="1649171031656" MODIFIED="1649171034930">
<edge COLOR="#00007c"/>
<node TEXT="Interspace Pruning: Using Adaptive Filter Representations to Improve Training of Sparse CNNs" ID="ID_1136994469" CREATED="1649171035811" MODIFIED="1649171042670">
<node TEXT="CVPR22" ID="ID_1181157881" CREATED="1649171043941" MODIFIED="1649171045542"/>
</node>
<node TEXT="Leaner and Faster: Two-Stage Model Compression for Lightweight&#xa;Text-Image Retrieval" ID="ID_880443205" CREATED="1651953324409" MODIFIED="1651953325391">
<node TEXT="NAACL" ID="ID_854685267" CREATED="1651953326299" MODIFIED="1651953329267"/>
</node>
</node>
<node TEXT="Multimodal Learning" FOLDED="true" POSITION="left" ID="ID_423909848" CREATED="1649104367617" MODIFIED="1649104370772">
<edge COLOR="#ff0000"/>
<node TEXT="Balanced Multimodal Learning via On-the-fly Gradient Modulation" ID="ID_1018564287" CREATED="1649104376663" MODIFIED="1649104377816">
<node TEXT="CVPR22" ID="ID_735774105" CREATED="1649104378958" MODIFIED="1649104379977"/>
</node>
</node>
<node TEXT="MultiTask Learning" FOLDED="true" POSITION="left" ID="ID_1771310570" CREATED="1652383917582" MODIFIED="1652383921441">
<edge COLOR="#ff0000"/>
<node TEXT="Gradient Surgery for Multi-Task Learning" ID="ID_686455197" CREATED="1652383922344" MODIFIED="1652383931965">
<icon BUILTIN="bookmark"/>
<node TEXT="NIPS20" ID="ID_926576648" CREATED="1652383934990" MODIFIED="1652383936487"/>
<node TEXT="we propose a form of gradient surgery that projects a task&apos;s gradient onto the normal plane of the gradient of any other task that has a conflicting gradient" ID="ID_1875166652" CREATED="1652383936902" MODIFIED="1652383975982"/>
</node>
</node>
<node TEXT="Network Arch Search" FOLDED="true" POSITION="left" ID="ID_1465080986" CREATED="1576827705751" MODIFIED="1650817686281">
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#00ffff"/>
<node TEXT="Lottery (winning ticket hypothesis)" ID="ID_157689053" CREATED="1589771412424" MODIFIED="1589771423223"/>
<node TEXT="NAS + General" ID="ID_345720050" CREATED="1589771335033" MODIFIED="1610423263436">
<node TEXT="1912.09640 ATOMNAS: FINE-GRAINED END TO END NEURAL ARCHITECTURE SEARCH (iclr20)" ID="ID_551298402" CREATED="1591523824249" MODIFIED="1591523859249"/>
<node TEXT="2001.01431: deeper insights into weight sharing in neural architecture search" ID="ID_788581693" CREATED="1578641715823" MODIFIED="1587526865297">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="as training every child model from scratch is very time-consuming, recent works leverage weight-sharing to speed up the model evaluation procedure" ID="ID_210487548" CREATED="1578641819751" MODIFIED="1587526865299">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="these approaches greatly reduce computation by maintaining a single copy of weights on the super-net and share the weights among every child model" ID="ID_320246153" CREATED="1578641854870" MODIFIED="1587526865306">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="weight-sharing has no theoretical guarantee and its impact has not been well studied before" ID="ID_1082606016" CREATED="1578641968342" MODIFIED="1587526865308">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="comprehensive and reveal the impact of weight-sharing" ID="ID_575085259" CREATED="1578641994032" MODIFIED="1587526865308">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1. best-performing models from different runes or even from consecutive epoches within the same run have significant variance" ID="ID_806597637" CREATED="1578642011022" MODIFIED="1587526865309">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2. even with high variance, we can extract valuable information from training the super-net with shared weight s" ID="ID_1259033553" CREATED="1578642053527" MODIFIED="1587526865310">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="3. inference between child models is a main factor that induces high variance" ID="ID_824472334" CREATED="1578642091423" MODIFIED="1587526865312">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="4. properly reducing the degree of weight sharing could effectively reduce variance" ID="ID_1319017164" CREATED="1578642112783" MODIFIED="1587526865312">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="2002.07376: picking winning tickets before training by preserving gradient flow" ID="ID_285121795" CREATED="1582337040844" MODIFIED="1587526865313">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: prune networks at initialization" ID="ID_242187066" CREATED="1582337065578" MODIFIED="1587526865314">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: overparameterization has been shown to benefit both the optimization and generalization of neural networks, but large networks are resource hungry at both training and test time." ID="ID_982652785" CREATED="1582337086688" MODIFIED="1587526865315">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: simple but effective pruning criterion termed Gradient Signal Preservation (GraSP)" ID="ID_456233465" CREATED="1582337135230" MODIFIED="1587526865316">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.08104: neural networks on random graphs" ID="ID_1492714029" CREATED="1583370632367" MODIFIED="1587526865316">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: evaluation of neural networks with architectures corresponding to random graphs of various types" ID="ID_189291229" CREATED="1583370645715" MODIFIED="1587526865317">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2004.05020: ModuleNet: Knowledge-inherited Neural Architecture search" ID="ID_1957978051" CREATED="1587029726858" MODIFIED="1587526865318">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="1912.12522 NASEVALUATION IS FRUSTRATINGLY HARD (ICLR20)" ID="ID_1102573147" CREATED="1591523757273" MODIFIED="1591523776195"/>
<node TEXT="2101: Unchain the Search Space with Hierarchical Differentiable Architecture Search" ID="ID_770027754" CREATED="1610423275136" MODIFIED="1610423282492"/>
<node TEXT="2101: Neural Architecture Search via Combinatorial Multi-Armed Bandit" ID="ID_1528572141" CREATED="1610424093363" MODIFIED="1610424103413"/>
<node TEXT="Contrastive Neural Architecture Search with Neural Architecture Comparators (cvpr21)" ID="ID_866640575" CREATED="1615348935373" MODIFIED="1615348942277"/>
<node TEXT="SpiderNet: Hybrid Differentiable-Evolutionary Architecture Search via Train-Free Metrics" ID="ID_704263392" CREATED="1650817696173" MODIFIED="1650817696822">
<node TEXT="CVPR22w" ID="ID_250147196" CREATED="1650817697711" MODIFIED="1650817699427"/>
</node>
</node>
<node TEXT="NAS + Other" ID="ID_1734029148" CREATED="1589771343435" MODIFIED="1589771346550">
<node TEXT="other=SISR" ID="ID_12673974" CREATED="1589771348054" MODIFIED="1589771353001">
<node TEXT="2003.04619: hierarchical neural architecture search for single image super-resolution" ID="ID_1727215487" CREATED="1589771353984" MODIFIED="1589771370713"/>
</node>
<node TEXT="other=modality" ID="ID_621571662" CREATED="1589771456800" MODIFIED="1589771460809">
<node TEXT="2004.12070: deep multimodal neural architecture search" ID="ID_1501292654" CREATED="1588072764031" MODIFIED="1588072782358"/>
</node>
<node TEXT="other=multitask" ID="ID_1291820834" CREATED="1590975521172" MODIFIED="1590975526893">
<node TEXT="2003.14058 MTL-NAS: Task-Agnostic Neural Architecture Search towards General-Purpose Multi-Task Learning (cvpr20)" ID="ID_755508472" CREATED="1590975527584" MODIFIED="1590975541542"/>
</node>
<node TEXT="otehr=captioning" ID="ID_341214713" CREATED="1608876510692" MODIFIED="1608876515099">
<node TEXT="2012.09742&#xa;AutoCaption: Image Captioning with Neural Architecture Search" ID="ID_1588438121" CREATED="1608876515864" MODIFIED="1608876526467"/>
</node>
</node>
</node>
<node TEXT=" Object Detection" FOLDED="true" POSITION="left" ID="ID_876510464" CREATED="1576825429577" MODIFIED="1647264593943">
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#7c7c00"/>
<node TEXT="Main" ID="ID_602285743" CREATED="1590025482391" MODIFIED="1590025486305">
<node TEXT="R-CNN" ID="ID_1685978685" CREATED="1578733403630" MODIFIED="1587526865053">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Fast R-CNN" ID="ID_694782039" CREATED="1578733411700" MODIFIED="1587526865053">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Faster R-CNN" ID="ID_642482113" CREATED="1578733414892" MODIFIED="1587526865053">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="SSD" ID="ID_164154080" CREATED="1578733432308" MODIFIED="1587526865054">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="YOLO" ID="ID_1721203552" CREATED="1578733429892" MODIFIED="1587526865054">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2004.10934: YOLOv4: optimal speed and accuracy of object detection" ID="ID_1418219399" CREATED="1587731285906" MODIFIED="1587731405695">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Mask R-CNN" ID="ID_365573714" CREATED="1578733418308" MODIFIED="1587526865054">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2005.09973: dynamic refinement network for oriented and densely packed object detection" ID="ID_1703277358" CREATED="1590025459045" MODIFIED="1590025479923"/>
<node TEXT="2012.03544: end to end object detection with fully convolutional network" ID="ID_1523511969" CREATED="1607496830511" MODIFIED="1607496848085"/>
<node TEXT="2012.03519: fine-trained dynamic head for object detection" ID="ID_70012465" CREATED="1607496916818" MODIFIED="1607496930453"/>
<node TEXT="2012: Implicit Feature Pyramid Network for Object Detection" ID="ID_659232211" CREATED="1610428420327" MODIFIED="1610428428770">
<node TEXT="Jian Sun" ID="ID_1833196048" CREATED="1610428429754" MODIFIED="1610428432093"/>
</node>
<node TEXT="R(Det)^2: Randomized Decision Routing for Object Detection" ID="ID_1075041282" CREATED="1649628031567" MODIFIED="1649628032450">
<node TEXT="CVPR22" ID="ID_1565974246" CREATED="1649628035054" MODIFIED="1649628038569"/>
</node>
</node>
<node TEXT="Performance Metric" ID="ID_898832267" CREATED="1649101258896" MODIFIED="1649101263049">
<node TEXT="Optimal Correction Cost for Object Detection Evaluation" ID="ID_1091207762" CREATED="1649101264499" MODIFIED="1649101269915">
<node TEXT="CVPR22" ID="ID_607658609" CREATED="1649101272943" MODIFIED="1649101274253"/>
</node>
</node>
<node TEXT="Long-tail" ID="ID_234088557" CREATED="1590798731797" MODIFIED="1590798734352">
<node TEXT="2003.05176: Equalization Loss for Long-Tailed Object Recognition (CVPR20)" ID="ID_731786534" CREATED="1590798735443" MODIFIED="1590798753647">
<icon BUILTIN="button_ok"/>
<node TEXT="improvement over mask rcnn" ID="ID_532045701" CREATED="1590798821368" MODIFIED="1590798828465"/>
</node>
<node TEXT="CVPR21: Adaptive Class Suppression Loss for Long-Tail Object Detection" ID="ID_1259869264" CREATED="1617622572423" MODIFIED="1617622580480">
<node TEXT="existing methods usually divide the whole categories into several groups and treat each group with different strategies" ID="ID_1294091078" CREATED="1617622648162" MODIFIED="1617622669345">
<node TEXT="breaking the limitation of manual grouping" ID="ID_1252367187" CREATED="1617622670643" MODIFIED="1617622683444"/>
</node>
</node>
</node>
<node TEXT="Retailing" ID="ID_725089971" CREATED="1588391641383" MODIFIED="1588391642960">
<node TEXT="2003.08230: rethinking object detection in retail stores" ID="ID_524544258" CREATED="1588391644632" MODIFIED="1588391661192"/>
</node>
<node TEXT="Camouflaged" ID="ID_1233579435" CREATED="1648388054502" MODIFIED="1648388062210">
<node TEXT="Zoom In and Out: A Mixed-scale Triplet Network&#xa;for Camouflaged Object Detection" ID="ID_290360895" CREATED="1648388067795" MODIFIED="1648388070326">
<node TEXT="CVPR22" ID="ID_1320931684" CREATED="1648388071940" MODIFIED="1648388074017"/>
</node>
</node>
<node TEXT="w/ Pretrained" ID="ID_713615493" CREATED="1649102563690" MODIFIED="1649102570371">
<node TEXT="Learning to Prompt for Open-Vocabulary Object Detection with Vision-Language Model" ID="ID_1568893151" CREATED="1649102571287" MODIFIED="1649102577177">
<node TEXT="CVPR22" ID="ID_863951710" CREATED="1649102578216" MODIFIED="1649102579357"/>
</node>
</node>
</node>
<node TEXT="Open Set Recognition" FOLDED="true" POSITION="left" ID="ID_323999523" CREATED="1634132952545" MODIFIED="1647267889167">
<edge COLOR="#7c007c"/>
<node TEXT="Open-Set Recognition: A Good Closed-Set Classifier is All You Need" ID="ID_1603928418" CREATED="1634132957520" MODIFIED="1634132962452">
<node TEXT="vgg group" ID="ID_419228120" CREATED="1634132963805" MODIFIED="1634132966497"/>
<node TEXT="arxiv" ID="ID_1399907050" CREATED="1634132966842" MODIFIED="1634132967732"/>
</node>
<node TEXT="PMAL: Open Set Recognition via Robust Prototype Mining" ID="ID_1567699679" CREATED="1648174626207" MODIFIED="1648174710258">
<icon BUILTIN="unchecked"/>
<icon BUILTIN="idea"/>
<node TEXT="AAAI21" ID="ID_1093847305" CREATED="1648174632155" MODIFIED="1648174634120"/>
<node TEXT="Open-Set is somewhat similar to deep metric learning variant" ID="ID_162349135" CREATED="1648174806913" MODIFIED="1648174820515"/>
<node TEXT="besides recognizing predefined classes, the system needs to reject the unknowns" ID="ID_1080431228" CREATED="1648174634902" MODIFIED="1648174647137"/>
<node TEXT="prototype learning is a potential manner to handle the problem as its ability to improve intra-class compactness of representations is much needed in discrimination between the known and the unknowns" ID="ID_1737399656" CREATED="1648174672305" MODIFIED="1648174703066"/>
<node TEXT="using mining strategy to avoid inteference of noise" ID="ID_879408251" CREATED="1648174763868" MODIFIED="1648174797459">
<icon BUILTIN="yes"/>
</node>
<node TEXT="can we borrow these to face recognition?" ID="ID_290608270" CREATED="1648174835615" MODIFIED="1648174856270">
<icon BUILTIN="help"/>
</node>
</node>
</node>
<node TEXT="Ordinal Regression" FOLDED="true" POSITION="left" ID="ID_408572005" CREATED="1576825445602" MODIFIED="1647264584581">
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#00ffff"/>
<node TEXT="OR-CNN" ID="ID_646788311" CREATED="1648336827721" MODIFIED="1648336832137"/>
<node TEXT="2004.12260: learning to autofocus (CVPR20)" ID="ID_1326141945" CREATED="1588068686019" MODIFIED="1588068859199">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="idea"/>
<node TEXT="ordinal regression" ID="ID_1337834731" CREATED="1588068727223" MODIFIED="1588068730887"/>
</node>
<node TEXT="Learning probabilistic ordinal embeddings for&#xa;uncertainty-aware regression." ID="ID_131283044" CREATED="1648337155121" MODIFIED="1648337161044">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR21" ID="ID_230574498" CREATED="1648337163491" MODIFIED="1648337165185"/>
</node>
<node TEXT="Moving Window Regression: A Novel Approach to Ordinal Regression" ID="ID_728124155" CREATED="1648336835391" MODIFIED="1648336844125">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_360837902" CREATED="1648336845156" MODIFIED="1648336846512"/>
</node>
<node TEXT="Unimodal-Concentrated Loss: Fully Adaptive Label Distribution Learning for Ordinal Regression" ID="ID_1643259552" CREATED="1649623420719" MODIFIED="1649623421988">
<node TEXT="CVPR22" ID="ID_1439656873" CREATED="1649623426704" MODIFIED="1649623428734"/>
</node>
</node>
<node TEXT="Privacy ML" FOLDED="true" POSITION="left" ID="ID_1174539463" CREATED="1590802979023" MODIFIED="1647265584135">
<edge COLOR="#7c0000"/>
<node TEXT="Encrypt / Decrypt" ID="ID_892914237" CREATED="1590803103499" MODIFIED="1590803111112">
<node TEXT="2004.05523 DeepEDN: A Deep Learning-based Image Encryption and Decryption Network for Internet of Medical Things" ID="ID_779071488" CREATED="1590802983849" MODIFIED="1590802993502"/>
</node>
<node TEXT="Membership Inference" ID="ID_1965495393" CREATED="1591523395980" MODIFIED="1591523401694">
<node TEXT="2001.00071 PrivGAN: Protecting GANs from membership inference attacks at low cos" ID="ID_370428646" CREATED="1591523402373" MODIFIED="1591523413026"/>
<node TEXT="Label-only membership inference attacks" ID="ID_1310996729" CREATED="1648422783790" MODIFIED="1648422789621">
<node TEXT="Carlini" ID="ID_1163854114" CREATED="1648422791804" MODIFIED="1648422793928"/>
</node>
</node>
<node TEXT="Physical Mail" ID="ID_1223992131" CREATED="1608872127823" MODIFIED="1608872130574">
<node TEXT="2012.11803&#xa; Modeling Deep Learning Based Privacy Attacks on Physical Mail" ID="ID_924300325" CREATED="1608872131773" MODIFIED="1608872140902"/>
</node>
<node TEXT="Forensics" ID="ID_135402985" CREATED="1618846988345" MODIFIED="1618846991727">
<node TEXT="2003.06951: camera tarce erasing (CVPR20)" ID="ID_586456534" CREATED="1585108821256" MODIFIED="1587526865295">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="camera trace is a unique noise produced in digital imaging process" ID="ID_703034002" CREATED="1585108844004" MODIFIED="1587526865296">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="Model Inversion (Privacy)" ID="ID_1887850078" CREATED="1646937279593" MODIFIED="1647265571606">
<node TEXT="Label-Only Model Inversion Attacks via Boundary Repulsion" ID="ID_1711729671" CREATED="1646937292008" MODIFIED="1646937298623">
<icon BUILTIN="checked"/>
<font ITALIC="false"/>
</node>
<node TEXT="Do Gradient Inversion Attacks Make Federated Learning Unsafe?" ID="ID_539563592" CREATED="1646948139524" MODIFIED="1646948148268">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="Assessing Privacy Risks from Feature Vector Reconstruction Attacks" ID="ID_456202389" CREATED="1646948272071" MODIFIED="1646948274903"/>
<node TEXT="Privacy Leakage of Adversarial Training Models in Federated Learning Systems" ID="ID_1739403016" CREATED="1647266811240" MODIFIED="1647266814655">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22w" ID="ID_1710159890" CREATED="1647266815467" MODIFIED="1647266817920"/>
<node TEXT="adversarially trained nets are vulnerable to inversion attack" ID="ID_1271286519" CREATED="1647266824910" MODIFIED="1647266840002"/>
</node>
<node TEXT="GradViT: Gradient Inversion of Vision Transformers" ID="ID_619347180" CREATED="1648387184629" MODIFIED="1648387190460">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_227494600" CREATED="1648387192118" MODIFIED="1648387193540">
<node TEXT="NVIDIA" ID="ID_739150767" CREATED="1648387416314" MODIFIED="1648387417986"/>
</node>
<node TEXT="transformer inversion" ID="ID_423330045" CREATED="1648387195131" MODIFIED="1648387198487"/>
</node>
<node TEXT="Exploiting Explanations for Model Inversion Attacks" ID="ID_1411419993" CREATED="1642456774974" MODIFIED="1642456783648">
<icon BUILTIN="unchecked"/>
<node TEXT="iccv21" ID="ID_588513264" CREATED="1642456785239" MODIFIED="1642456786804"/>
<node TEXT="attackers can construct sensitive information (such as faces) merely from model predictions" ID="ID_111964935" CREATED="1642456788665" MODIFIED="1642456829436"/>
</node>
<node TEXT="WHEN MACHINE LEARNING ISN&#x2019;T PRIVATE" ID="ID_1244120972" CREATED="1648419587624" MODIFIED="1648419588530">
<node TEXT="USENIX" ID="ID_105372756" CREATED="1648419590393" MODIFIED="1648419592312"/>
<node TEXT="Carlini" ID="ID_901631564" CREATED="1648419592553" MODIFIED="1648419594517"/>
</node>
<node TEXT="Counterfactual Memorization in&#xa;Neural Language Models" ID="ID_426897548" CREATED="1648425614069" MODIFIED="1648425614886">
<node TEXT="Carlini" ID="ID_1948661446" CREATED="1648425616141" MODIFIED="1648425617588"/>
</node>
<node TEXT="GradViT: Gradient Inversion of Vision Transformers" ID="ID_1338574288" CREATED="1649126363591" MODIFIED="1649126364829">
<node TEXT="CVPR22" ID="ID_512975228" CREATED="1649126365880" MODIFIED="1649126366979"/>
</node>
</node>
<node TEXT="model watermark" ID="ID_854641187" CREATED="1647567587011" MODIFIED="1647567593099">
<node TEXT="exloring structure consistency for deep model watermarking" ID="ID_489056739" CREATED="1647567594240" MODIFIED="1647567607226">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Differential Privacy" ID="ID_1268713155" CREATED="1648337762797" MODIFIED="1648337766246">
<node TEXT="Mixed Differential Privacy in Computer Vision" ID="ID_1969874071" CREATED="1648337767049" MODIFIED="1648337770249">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1404369978" CREATED="1648337773035" MODIFIED="1648337774527"/>
</node>
</node>
</node>
<node TEXT="Self-Supervised Learning" FOLDED="true" POSITION="left" ID="ID_714759247" CREATED="1650825914367" MODIFIED="1651953578841">
<edge COLOR="#7c007c"/>
<node TEXT="Survey" ID="ID_1775463249" CREATED="1650825920877" MODIFIED="1650825925562">
<icon BUILTIN="info"/>
<node TEXT="Empirical Evaluation and Theoretical Analysis for Representation Learning: A Survey" ID="ID_704481907" CREATED="1650825926968" MODIFIED="1650825934945">
<node TEXT="IJCAI survey 2022" ID="ID_210152171" CREATED="1650825935831" MODIFIED="1650825944162"/>
<node TEXT="generic representation learning, involving both CV and CL" ID="ID_1395139495" CREATED="1650825945439" MODIFIED="1650825953912"/>
</node>
</node>
<node TEXT="Contrastive Learning Image" ID="ID_1556051530" CREATED="1646924377310" MODIFIED="1652404146054">
<node TEXT="Self-Damaging Contrastive Learning" ID="ID_374319956" CREATED="1646924383175" MODIFIED="1646924388224">
<node TEXT="icml21" ID="ID_1688743346" CREATED="1646924391163" MODIFIED="1646924393200"/>
<node TEXT="create a dynamic self-competitor model to contrast with the target model, which is a pruned version of the latter. during training, contrasting the two models will lead to adaptive online mining of the most easily forgotten samples for the current target model, and implicitly emphasize them more in the contrastive loss" ID="ID_1977443801" CREATED="1646924393453" MODIFIED="1646924451450"/>
</node>
<node TEXT="BatchFormer: Learning to Explore Sample Relationships for Robust Representation Learning" ID="ID_1047906963" CREATED="1646937425094" MODIFIED="1646937435486">
<icon BUILTIN="unchecked"/>
<node TEXT="cvpr22" ID="ID_1302140754" CREATED="1646937436755" MODIFIED="1646937438057"/>
</node>
<node TEXT="Adversarial Masking for Self-Supervised Learning" ID="ID_161503963" CREATED="1644181760956" MODIFIED="1644181769832">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Self-Supervised Image Representation Learning with Geometric Set Consistency" ID="ID_504378487" CREATED="1649104770916" MODIFIED="1649104771843">
<node TEXT="CVPR22" ID="ID_1149198306" CREATED="1649104772859" MODIFIED="1649104773959"/>
<node TEXT="under the guidance of 3D geometric consistency" ID="ID_1279411864" CREATED="1649104781206" MODIFIED="1649104789146"/>
</node>
<node TEXT="Rethinking Minimal Sufficient Representation in Contrastive Learning" ID="ID_1841880552" CREATED="1649167986341" MODIFIED="1649167986833">
<node TEXT="CVPR22" ID="ID_724279159" CREATED="1649167988007" MODIFIED="1649167990006"/>
<node TEXT="since all supervision information for one view comes from the other view, contrastive learning approximately obtains the manimal sufficient representation which contains the shared information and eliminates the non-shared information between views" ID="ID_257277147" CREATED="1649167993722" MODIFIED="1649168038621"/>
<node TEXT="consifering the diversity of the down stream tasks, it cannot be guaranteeed that all ttask-relevant information is shared betwen views" ID="ID_1688262861" CREATED="1649168038849" MODIFIED="1649168055121"/>
</node>
<node TEXT="Unified Contrastive Learning in Image-Text-Label Space" ID="ID_520382759" CREATED="1649623175917" MODIFIED="1649623182532">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_1550402425" CREATED="1649623178078" MODIFIED="1649623179606"/>
</node>
<node TEXT="On the Importance of Asymmetry for Siamese Representation Learning" ID="ID_1127640615" CREATED="1649624328307" MODIFIED="1649624329557">
<node TEXT="CVPR22" ID="ID_123639312" CREATED="1649624332043" MODIFIED="1649624333693"/>
</node>
<node TEXT="Use All The Labels: A Hierarchical Multi-Label Contrastive Learning Framework" ID="ID_1062716650" CREATED="1652404192259" MODIFIED="1652404192706">
<node TEXT="CVPR22" ID="ID_1803319439" CREATED="1652404193774" MODIFIED="1652404194847"/>
</node>
</node>
<node TEXT="Contrastive Learning Video" ID="ID_1516905874" CREATED="1649102713240" MODIFIED="1652404149496">
<node TEXT="sound and visual representation learning with multiple pretraining tasks" ID="ID_1372709479" CREATED="1641524234751" MODIFIED="1641524246105">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Frame-wise Action Representations for Long Videos via Sequence Contrastive Learning" ID="ID_1235235307" CREATED="1649102802918" MODIFIED="1649102803748">
<node TEXT="CVPR22" ID="ID_1379516385" CREATED="1649102806472" MODIFIED="1649102807610"/>
<node TEXT="Sequence contrastive learning" ID="ID_413817863" CREATED="1649102807882" MODIFIED="1649102813865"/>
</node>
<node TEXT="CenterCLIP: Token Clustering for Efficient Text-Video Retrieval" ID="ID_391932937" CREATED="1651952987924" MODIFIED="1651952988800">
<node TEXT="SIGIR" ID="ID_787589457" CREATED="1651952989777" MODIFIED="1651952991331"/>
</node>
<node TEXT="TransRank: Self-supervised Video Representation Learning&#xa;via Ranking-based Transformation Recognition" ID="ID_746328177" CREATED="1651953611730" MODIFIED="1651953612644">
<node TEXT="CVPR22" ID="ID_516236195" CREATED="1651953613554" MODIFIED="1651953614875"/>
</node>
<node TEXT="In Defense of Image Pre-Training for&#xa;Spatiotemporal Recognition" ID="ID_889274972" CREATED="1651953774184" MODIFIED="1651953776596">
<icon BUILTIN="pencil"/>
<node TEXT="cihang xie" ID="ID_1074268195" CREATED="1651953777761" MODIFIED="1651953779253"/>
</node>
</node>
</node>
<node TEXT="Scene Graph" FOLDED="true" POSITION="left" ID="ID_691684807" CREATED="1582940936028" MODIFIED="1651953021589">
<font NAME="Gentium" BOLD="false"/>
<edge COLOR="#007c7c"/>
<node TEXT="Generation" ID="ID_1992268449" CREATED="1582940945725" MODIFIED="1590024968949">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1912.07414 Learning Canonical Representations forScene Graph to Image Generation" ID="ID_1230724059" CREATED="1591523975753" MODIFIED="1591523987076"/>
<node TEXT="2002.11949: unbiased scene graph generalization from biased training" ID="ID_1016607145" CREATED="1582940950980" MODIFIED="1587526865280">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: scene graph generation (SGG)" ID="ID_1614294683" CREATED="1582940994088" MODIFIED="1587526865281">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: collapsing diversity hurts downstream tasks due to the training bias. However, debiasing is not trivial because traditional methods call tell good bias and bad bias" ID="ID_476245187" CREATED="1582941002281" MODIFIED="1587526865282">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: draw counterfactual causality from the trained graph" ID="ID_538665732" CREATED="1582941507255" MODIFIED="1587526865283">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2005.08230: graph density-aware losses for novel compositions in scene graph generation (CVPR20)" ID="ID_168569159" CREATED="1590024970199" MODIFIED="1590024994252"/>
<node TEXT="Exploiting Edge-Oriented Reasoning for 3D Point-based Scene Graph Analysis (cvpr21)" ID="ID_110979499" CREATED="1615348622406" MODIFIED="1615348629899"/>
<node TEXT="Probabilistic Modeling of Semantic Ambiguity for Scene Graph Generation (cvpr21)" ID="ID_1895253076" CREATED="1615351040893" MODIFIED="1615351048231"/>
</node>
<node TEXT="Image Retrieval" ID="ID_1768896" CREATED="1610425686042" MODIFIED="1610425689498">
<node TEXT="2012: Image-to-Image Retrieval by Learning Similarity between Scene Graphs (AAAI21)" ID="ID_1384437389" CREATED="1610425690361" MODIFIED="1610425706901">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node TEXT="Semantic Segmentation" FOLDED="true" POSITION="left" ID="ID_710086984" CREATED="1641524074732" MODIFIED="1641524079818">
<edge COLOR="#007c7c"/>
<node TEXT="Main" ID="ID_1927721844" CREATED="1578733352755" MODIFIED="1649104722464">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1505.04597: U-Net: convolutional networks for biomedical image segmentation (U-Net)" ID="ID_1662911562" CREATED="1578733451101" MODIFIED="1587526865046">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: segmentation" ID="ID_525154704" CREATED="1578733494076" MODIFIED="1587526865046">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how" ID="ID_564416828" CREATED="1578733499940" MODIFIED="1587526865046">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="training strategy that relies on strong use of data augmentation" ID="ID_1775116489" CREATED="1578733508732" MODIFIED="1587526865046">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="architecture: contracting path to capture context and a symmetric expanding path that enables precise localization" ID="ID_1230729181" CREATED="1578733542669" MODIFIED="1587526865047">
<icon BUILTIN="wizard"/>
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="1912.08193 PointRend: Image Segmentation as Rendering (kaiming)" ID="ID_1360164242" CREATED="1591524011593" MODIFIED="1591524026866">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2012.05007: group-wise semantic mining for weakly supervised semantic segmentation" ID="ID_871835183" CREATED="1607580365071" MODIFIED="1607580381423">
<node TEXT="input images are represented as graph nodes, and the underlying relations between a pair of images are characterized by an efficient co-attention mechnism" ID="ID_701602240" CREATED="1607580528072" MODIFIED="1607580553437"/>
</node>
<node TEXT="2101: Rethinking Semantic Segmentation from a Sequence-to-Sequence Perspective with Transformers" ID="ID_782123948" CREATED="1610424579200" MODIFIED="1610424586216"/>
<node TEXT="Multi-Source Domain Adaptation with Collaborative Learning for Semantic Segmentation (cvpr21)" ID="ID_1873517908" CREATED="1615358252825" MODIFIED="1615358259859">
<node TEXT="arXiv:2103.04717" ID="ID_1286107433" CREATED="1615358260849" MODIFIED="1615358265830"/>
</node>
<node TEXT="Tree Energy Loss: Towards Sparsely Annotated Semantic Segmentation" ID="ID_1072029066" CREATED="1648342536462" MODIFIED="1648342539439">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR22" ID="ID_366650528" CREATED="1648342540556" MODIFIED="1648342541821"/>
</node>
<node TEXT="towards unsupervised open world semantic segmentation" ID="ID_886126595" CREATED="1641524113165" MODIFIED="1641524125507">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Generalization Gap" ID="ID_1078614193" CREATED="1649622999156" MODIFIED="1649623010400">
<node TEXT="Pin the Memory: Learning to Generalize Semantic Segmentation" ID="ID_565752448" CREATED="1649623011621" MODIFIED="1649623013281">
<node TEXT="CVPR22" ID="ID_259258542" CREATED="1649623015267" MODIFIED="1649623016760"/>
<node TEXT="meta-learning to shrink the generalization gap" ID="ID_536241004" CREATED="1649623017156" MODIFIED="1649623033341"/>
</node>
</node>
</node>
<node TEXT="Weakly supervised semantic segmentation" ID="ID_898901373" CREATED="1617621463991" MODIFIED="1617621470599">
<node TEXT="CVPR21: Background-Aware Pooling and Noise-Aware Loss for Weakly-Supervised Semantic Segmentation" ID="ID_438120929" CREATED="1617621471500" MODIFIED="1617621479134">
<node TEXT="bounding boxes do not specify object boundaries" ID="ID_630389636" CREATED="1617622064739" MODIFIED="1617622078587">
<node TEXT="background-aware pooling, focusing more on aggregating foreground features, for high quality pseudo segmentation labels" ID="ID_1930311428" CREATED="1617622086378" MODIFIED="1617622241077"/>
</node>
<node TEXT="noisy label especially at object boundaries" ID="ID_1470006065" CREATED="1617622216454" MODIFIED="1617622256519">
<node TEXT="noise-aware loss, less susceptible to incorrect labels" ID="ID_707624454" CREATED="1617622261098" MODIFIED="1617622288020"/>
</node>
</node>
</node>
</node>
<node TEXT="Vision-Language Pretraining" FOLDED="true" POSITION="left" ID="ID_1438251706" CREATED="1646945591468" MODIFIED="1646945596520">
<edge COLOR="#00ff00"/>
<node TEXT="Survey" ID="ID_1995640100" CREATED="1650816017282" MODIFIED="1650816021941">
<icon BUILTIN="info"/>
<node TEXT="Vision-and-Language Pretrained Models: A Survey" ID="ID_1852780037" CREATED="1650816023897" MODIFIED="1650816029291">
<node TEXT="IJCAI22" ID="ID_653641959" CREATED="1650816029905" MODIFIED="1650816032042"/>
</node>
</node>
<node TEXT="CLIP" ID="ID_410734693" CREATED="1646945598394" MODIFIED="1646945599795">
<node TEXT="CLIP and adversarial robustness?" ID="ID_484691074" CREATED="1647048643037" MODIFIED="1647048648456"/>
</node>
<node TEXT="ALIGN" ID="ID_883337544" CREATED="1646945681124" MODIFIED="1646945684050"/>
<node TEXT="FILIP" ID="ID_712868502" CREATED="1646945686665" MODIFIED="1646945695226"/>
<node TEXT="Wukong: 100 Million Large-scale Chinese Cross-modal Pre-training Dataset and A Foundation Framework" ID="ID_579762046" CREATED="1646945599992" MODIFIED="1646945607267">
<icon BUILTIN="pencil"/>
</node>
<node TEXT="Conditional Prompt Learning for Vision-Language Models" ID="ID_1017315336" CREATED="1648394772288" MODIFIED="1648394773329">
<node TEXT="CVPR22" ID="ID_1728549709" CREATED="1648394774156" MODIFIED="1648394775509"/>
</node>
</node>
<node TEXT="-------------------------------------------------------------------------------------------------" POSITION="left" ID="ID_1156109860" CREATED="1647264433260" MODIFIED="1647264435598">
<edge COLOR="#007c7c"/>
</node>
<node FOLDED="true" POSITION="left" ID="ID_950327353" CREATED="1583319653266" MODIFIED="1651953012174">
<icon BUILTIN="clanbomber"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#ff0000">Visual Representation</font>
    </p>
  </body>
</html>
</richcontent>
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#0000ff"/>
<node TEXT="Overview" ID="ID_1167874158" CREATED="1590806838024" MODIFIED="1590806839723">
<node TEXT="2003.14323 How Useful is Self-Supervised Pretraining for Visual Tasks? (cvpr20)" ID="ID_519934474" CREATED="1590806840836" MODIFIED="1590806854620">
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="VilBERT" ID="ID_957701734" CREATED="1587025741828" MODIFIED="1587526864956">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2002.12247: learning representations by predicting bags of visual words" ID="ID_685769333" CREATED="1583321744328" MODIFIED="1587526864958">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="inspired by works such as BERT" ID="ID_1309476354" CREATED="1583321965567" MODIFIED="1587526864959">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.12204: visual commonsense R-CNN" ID="ID_1137945207" CREATED="1583322744508" MODIFIED="1587526864959">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: unsupervised image representation learning" ID="ID_1588030107" CREATED="1583322756046" MODIFIED="1587526864961">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: causual intervention instead of likelihood" ID="ID_730361703" CREATED="1583322771097" MODIFIED="1587526864961">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.08822: automatic shortcut removal for self-supervised representation learning" ID="ID_578694955" CREATED="1583371245245" MODIFIED="1587526864962">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: shortcut removal" ID="ID_1445945142" CREATED="1583371264607" MODIFIED="1587526864963">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: feature extractor quickly learns to exploit low-level visual features such as color aberrations or watermarks and then fails to learn useful semantic representations." ID="ID_1421063789" CREATED="1583371269457" MODIFIED="1587526864964">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: propose a generral framework for removing shortcut features automatically" ID="ID_1875733064" CREATED="1583371305795" MODIFIED="1587526864965">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="featues which are the first to be exploited for solving the pretext task may also be the most vulnerable to an adversary trained to make the task harder" ID="ID_1567244054" CREATED="1583371377480" MODIFIED="1587526864965">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="inspire" ID="ID_1863504064" CREATED="1583371333031" MODIFIED="1587526864966">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1. can we use this for defending backdoor attacks?" ID="ID_1048102066" CREATED="1583371340220" MODIFIED="1587526864966">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="CNN Design" ID="ID_862186943" CREATED="1585106325434" MODIFIED="1587526864975">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2003.07064: on translation invarince in CNNs: convolutional layers can exloit absolute spaticla location (CVPR20)" ID="ID_390066153" CREATED="1585106337791" MODIFIED="1587526864975">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: challenges common assumption that conv layers in mordern cnns are translation invariant" ID="ID_1361111330" CREATED="1585106372608" MODIFIED="1587526864976">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: simply solution to remove spacial location encoding which improves translation invariance" ID="ID_1140045256" CREATED="1585106448598" MODIFIED="1587526864977">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="SSL + Seg" ID="ID_492254333" CREATED="1608873116375" MODIFIED="1608873127333">
<icon BUILTIN="bookmark"/>
<node TEXT="2012.10017&#xa;Self-supervised Learning with Fully Convolutional Networks" ID="ID_1663487289" CREATED="1608873134291" MODIFIED="1608873147948">
<icon BUILTIN="bookmark"/>
<node TEXT="TODO" ID="ID_1840485165" CREATED="1608873253436" MODIFIED="1608873259135"/>
</node>
<node TEXT="2012.10782 Three Ways to Improve Semantic Segmentation with Self-Supervised Depth Estimation" ID="ID_1421017440" CREATED="1608876253867" MODIFIED="1608876263820"/>
</node>
<node TEXT="Contrastive Learning" ID="ID_1392293712" CREATED="1587525264357" MODIFIED="1608873469776">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1807.03748: representation learning with contrastive predictive coding" ID="ID_1253445477" CREATED="1587525461612" MODIFIED="1587526865011">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="idea"/>
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="1808.06670: Deep InfoMax: learning deep representations by mutual information estimation and maximization" ID="ID_217646590" CREATED="1587525665592" MODIFIED="1587526865012">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="1906.05849: contrastive multiview coding" ID="ID_1614202070" CREATED="1587525736685" MODIFIED="1587526865013">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="1911.05722: momentum contrast for unsupervised visual representation learning" ID="ID_1238694894" CREATED="1587525770018" MODIFIED="1587526865014">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2002.05709: a simple framework for contrastive learning of visual representations (SimCLR; Hinton)" ID="ID_1250600595" CREATED="1583374010828" MODIFIED="1587526864966">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="why: new design without requiring specilized architectures of a memory bank" ID="ID_370821688" CREATED="1583374047212" MODIFIED="1587526864967">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="leanred" ID="ID_846072559" CREATED="1583374126481" MODIFIED="1587526864968">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1. composition of data augmentations plays a critical role in defining effective predictive tasksk" ID="ID_1341361310" CREATED="1583374129257" MODIFIED="1587526864972">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2. learnable non-linear transformation between the representation and te contrastive loss substantially improves the quality of the learned representations." ID="ID_1281836915" CREATED="1583374153130" MODIFIED="1587526864973">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="3. contrastive learning benefits from larger batch sizes and more training steps" ID="ID_1812551547" CREATED="1583374178545" MODIFIED="1587526864974">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="Linear classifier achived 76% top-1 accuracy on imagenet (resnet-50 level)" ID="ID_1747290173" CREATED="1583374524671" MODIFIED="1587526864974">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2005.10243: what makes for good views for contrastive learning (ECCV20 pending)" ID="ID_1614318742" CREATED="1590025350198" MODIFIED="1590025366771">
<node TEXT="73% top-1 with resnet50" ID="ID_1829948905" CREATED="1590025371557" MODIFIED="1590025380525"/>
</node>
<node TEXT="2012.02733: hierarchical semantic aggregation for contrastive representation learning" ID="ID_589593958" CREATED="1607501107784" MODIFIED="1607501838132">
<icon BUILTIN="idea"/>
<node TEXT="extending the contastive loss to allow for multiple positives per anchor, and explicitly pulling semantically similar images/patches together at the ealier layers as well as the last embedding space." ID="ID_1943023790" CREATED="1607501132802" MODIFIED="1607501195028">
<node TEXT="finally the deep metric learning things went into self supervised learning" ID="ID_98688493" CREATED="1607501850340" MODIFIED="1607501866361"/>
</node>
<node TEXT="76.4% top-1 imagenet resnet50" ID="ID_7070633" CREATED="1607502434202" MODIFIED="1607502456285"/>
</node>
<node TEXT="2012.00868: towards good practices in self-supervised representation learning" ID="ID_1573142090" CREATED="1607504770502" MODIFIED="1607504924955">
<icon BUILTIN="button_ok"/>
<node TEXT="will be useful" ID="ID_842726168" CREATED="1607504927137" MODIFIED="1607504930568"/>
</node>
<node TEXT="2012.09962 Information-Preserving Contrastive Learning for Self-Supervised Representations" ID="ID_34214238" CREATED="1608873535652" MODIFIED="1608873549183"/>
<node TEXT="2104: Solving Inefficiency of Self-supervised Representation Learning" ID="ID_181911350" CREATED="1619335582053" MODIFIED="1619335593237"/>
<node TEXT="Compressive Visual Representations" ID="ID_1029907368" CREATED="1637078797486" MODIFIED="1637078798398"/>
<node TEXT="Selective-Supervised Contrastive Learning with Noisy Labels" ID="ID_1783068898" CREATED="1648388675713" MODIFIED="1648388676964">
<node TEXT="CVPR22" ID="ID_243171848" CREATED="1648388678752" MODIFIED="1648388680202"/>
</node>
</node>
<node TEXT="Self-sup/Unsup ^contrastive" ID="ID_1338043497" CREATED="1587525929533" MODIFIED="1608873579210">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="Unsupervised Visual Representation Learning by Context Prediction (ICCV15)" ID="ID_1364798211" CREATED="1587525933209" MODIFIED="1587526864996">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Unsupervised Representation Learning by Predicting Image Rotations (ICLR18)" ID="ID_302272034" CREATED="1587525945834" MODIFIED="1587526864998">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Revisiting Self-Supervised Visual Representation Learning (CVPR19)" ID="ID_1851432561" CREATED="1587526021778" MODIFIED="1587526865001">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="S^4L: Self-Supervised Semi-Supervised Learning (ICCV19)" ID="ID_1305351225" CREATED="1587526028684" MODIFIED="1587526865003">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Representation Learning by Learning to Count" ID="ID_92259082" CREATED="1587526139156" MODIFIED="1587526865005">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="1708.02901: Transitive Invariance for Self-supervised Visual Representation Learning" ID="ID_521099661" CREATED="1587526154532" MODIFIED="1587526865006">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="1708.07860: Multi-task Self-Supervised Visual Learning" ID="ID_327244794" CREATED="1587526165992" MODIFIED="1587526865008">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="Unsupervised Learning of Visual Representations using Videos" ID="ID_962768992" CREATED="1587526196989" MODIFIED="1587526865009">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="1711.09082 Cross-Domain Self-supervised Multi-task Feature Learning using Synthetic Imagery" ID="ID_1646797968" CREATED="1587526226463" MODIFIED="1587526865009">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2004.08744: are we pretraining it right? digging deeper into visio-linguistic pretraining" ID="ID_1495624359" CREATED="1587874349262" MODIFIED="1587874375901"/>
<node TEXT="2012.11552&#xa;Online Bag-of-Visual-Words Generation for Unsupervised Representation Learning" ID="ID_888759027" CREATED="1608873661573" MODIFIED="1608873667241"/>
<node TEXT="2012.07477&#xa;Aggregative Self-Supervised Feature Learning" ID="ID_512897160" CREATED="1608880527980" MODIFIED="1608880533968"/>
<node TEXT="Summary Link https://github.com/jason718/awesome-self-supervised-learning" ID="ID_1526610547" CREATED="1587526334629" MODIFIED="1587526865015">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT=" Exploring Simple Siamese Representation Learning" ID="ID_909252643" CREATED="1622799434567" MODIFIED="1622799464047">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
<node TEXT="SimSiam (Kaiming)" ID="ID_117976638" CREATED="1622799465242" MODIFIED="1622799468479"/>
</node>
<node TEXT="Robust Representation Learning via Perceptual Similarity Metrics" ID="ID_1487769108" CREATED="1625979864633" MODIFIED="1625979869931">
<node TEXT="ICML21" ID="ID_1000239169" CREATED="1625979871025" MODIFIED="1625979875056"/>
</node>
</node>
<node TEXT="Video" ID="ID_889528288" CREATED="1583378938529" MODIFIED="1607578075176">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="(AVD) 1907.05640: AVD: Adversasrial video distillation" ID="ID_886310214" CREATED="1588300578517" MODIFIED="1588300601393"/>
<node TEXT="2002.07442: video-level representation learning" ID="ID_1408106855" CREATED="1583378944803" MODIFIED="1587526864979">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="existing methods do not consider video-level temporal evolution of spatio-temporal features" ID="ID_491439577" CREATED="1583379032120" MODIFIED="1587526864979">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2003.07990: watching the world go by: representation learning from unlabeled videos" ID="ID_1970577986" CREATED="1588389196457" MODIFIED="1588389464536">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
<node TEXT="Video Noise Contrastive Estination" ID="ID_317299846" CREATED="1588389293264" MODIFIED="1588389302662"/>
<node TEXT="Learn single image represetnations" ID="ID_231581972" CREATED="1588389315572" MODIFIED="1588389323801"/>
</node>
<node TEXT="2012.05057: contrastive transformation for self-supervised correspondence learning" ID="ID_692111184" CREATED="1607578079992" MODIFIED="1607578095435"/>
</node>
<node TEXT="AI Interpretation" ID="ID_1077913505" CREATED="1588155861085" MODIFIED="1588155864859">
<node TEXT="2004.13166: a disentangling invertible interpretation network for explaining latent representations (cvpr20)" ID="ID_627043223" CREATED="1588155865845" MODIFIED="1588155904934"/>
</node>
<node TEXT="ZeroShot" ID="ID_107339849" CREATED="1590805778541" MODIFIED="1590805781439">
<node TEXT="2004.00587 Symmetry and Group in Attribute-Object Compositions (cvpr20)" ID="ID_551480333" CREATED="1590805782179" MODIFIED="1590805799905"/>
</node>
<node TEXT="PCL Representation" ID="ID_965391199" CREATED="1590743125097" MODIFIED="1608871105532">
<node TEXT="2005.14169: self-supervised modal and view invariant feature learning" ID="ID_939413624" CREATED="1590743132226" MODIFIED="1590743147677"/>
<node TEXT="2012.13089&#xa; P4Contrast: Contrastive Learning with Pairs of Point-Pixel Pairs for RGB-D Scene Understanding" ID="ID_1359907490" CREATED="1608871275332" MODIFIED="1608871286097"/>
</node>
</node>
<node TEXT="Classification" FOLDED="true" POSITION="left" ID="ID_1412187905" CREATED="1576825420762" MODIFIED="1648335928432">
<icon BUILTIN="clanbomber"/>
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#00ff00"/>
<node TEXT="Regularization" FOLDED="true" ID="ID_988535058" CREATED="1583203860700" MODIFIED="1587526864992">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="Dropout" ID="ID_866290927" CREATED="1583031604086" MODIFIED="1587526864993">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="Dropout" ID="ID_1113358480" CREATED="1610427519107" MODIFIED="1610427520772"/>
<node TEXT="AutoDropout" ID="ID_459697994" CREATED="1610427521086" MODIFIED="1610427523036">
<node TEXT="2101.01761: AutoDropout: Learning Dropout Patterns to Regularize Deep Networks" ID="ID_1293707654" CREATED="1610427524206" MODIFIED="1610427596453">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node TEXT="BN" ID="ID_1518330716" CREATED="1589848189712" MODIFIED="1589848191283"/>
<node TEXT="2003.08761: exemplar normalization for learning deep representation (CVPR20)" ID="ID_1042826961" CREATED="1589848192135" MODIFIED="1589848220914"/>
<node TEXT="Freature Map Distortion" ID="ID_1684406378" CREATED="1583031607508" MODIFIED="1587526864993">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.11022: beyond dropout: feature map distortion to regularize deep neural networks" ID="ID_869809840" CREATED="1583031617531" MODIFIED="1587526864993">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: regularization" ID="ID_600365259" CREATED="1583203993250" MODIFIED="1587526864994">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: dropout is not optimal" ID="ID_1763653148" CREATED="1583204000285" MODIFIED="1587526864994">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: feature map distortion" ID="ID_898184361" CREATED="1583204006482" MODIFIED="1587526864995">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="2004.00197: Controllable Orthogonalization in Training DNNs (cvpr20)" ID="ID_1732543263" CREATED="1590750120947" MODIFIED="1590750132910"/>
<node TEXT="2004.04690 Orthogonal Over-Parameterized Training (tech report)" ID="ID_887184168" CREATED="1590805194879" MODIFIED="1590805205903"/>
<node TEXT="2010.03630: revisiting batch normalization for improving corruption robustness" ID="ID_1477723543" CREATED="1608016599643" MODIFIED="1608016616867"/>
<node TEXT="Obstruction" ID="ID_1719343203" CREATED="1589849065156" MODIFIED="1590750090223">
<node TEXT="2003.04490: compositional convolutional neural networks: a deep architecture with innate robustness to partial occlusion (CVPR20)" ID="ID_1050753055" CREATED="1589849073868" MODIFIED="1589849112130">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2004.01180: Learning to See Through Obstructions (cvpr20)" ID="ID_940698694" CREATED="1590749400960" MODIFIED="1590749448553"/>
</node>
</node>
<node TEXT="Understanding / Interpreting" FOLDED="true" ID="ID_754960494" CREATED="1588389142265" MODIFIED="1588389151163">
<node TEXT="2003.06254: what information does a resnet compress? (ICLR19)" ID="ID_893276080" CREATED="1588389152245" MODIFIED="1588389170326"/>
<node TEXT="2003.08907: overinterpretation reveals image classification models pathologies" ID="ID_1565761508" CREATED="1588390501286" MODIFIED="1588390521792"/>
</node>
<node TEXT="Fine-Grained Classification" ID="ID_1016015353" CREATED="1583301869719" MODIFIED="1587526865022">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.10191: learning attentive pairwise interaction for fine-grained classification" ID="ID_1094724002" CREATED="1583301910446" MODIFIED="1587526865023">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: fine-grained classification" ID="ID_914034467" CREATED="1583302155021" MODIFIED="1587526865024">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="most approaches address this difficulty by learning discriminative representation of individual input image" ID="ID_227751742" CREATED="1583302169694" MODIFIED="1587526865025">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="why: humans can effectively identify contrastive clues by comparing image pairs" ID="ID_908593746" CREATED="1583302163571" MODIFIED="1587526865026">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: propose a simple but effective attentive pairwise interaction network" ID="ID_345233600" CREATED="1583302211523" MODIFIED="1587526865026">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="capture semantic differences in the input image pair" ID="ID_29190113" CREATED="1583302471900" MODIFIED="1587526865027">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
</node>
<node TEXT="Uncertainty" ID="ID_1593235677" CREATED="1583204058384" MODIFIED="1587526865028">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="evidential deep learning" ID="ID_1081116980" CREATED="1583204063027" MODIFIED="1587526865028">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1806.01768 Evidential Deep Learning to Quantify ClassificationUncertainty" ID="ID_1842644628" CREATED="1591524639495" MODIFIED="1591524649537"/>
</node>
</node>
<node TEXT="Semisupervised / Distillation" ID="ID_855185476" CREATED="1588403718444" MODIFIED="1588403725537">
<node TEXT="1912.11006: Data-Free Adversarial Distillation" ID="ID_319061813" CREATED="1592551041725" MODIFIED="1592551053539"/>
<node TEXT="2003.08797: teacher-student chain for efficient semi-supervised histology image classification" ID="ID_1686725012" CREATED="1588403734483" MODIFIED="1588403766395"/>
<node TEXT="2004.05937: knowledge distillation and student-teacher learning for visual intelligence: a review and new outlooks" ID="ID_1042517206" CREATED="1589767067029" MODIFIED="1589767092525">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2003.13960 Neural Networks Are More Productive Teachers Than Human Raters: Active Mixup for Data-Efficient Knowledge Distillation from a Blackbox Model (cvpr20)" ID="ID_951173961" CREATED="1590974991129" MODIFIED="1590975003016"/>
<node TEXT="2003.13964 Regularizing Class-wise Predictions via Self-knowledge Distillation (cvpr20)" ID="ID_355874115" CREATED="1590975707007" MODIFIED="1590975715804"/>
</node>
<node TEXT="Few Shot and Zero Shot (intersects with domain adaptation?)" ID="ID_1897333203" CREATED="1585109268143" MODIFIED="1587526865035">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2003.06777: deepEMD: few-shot image classification with differentiable earth mover&apos;s distance and structured classifiers (CVPR20 oral)" ID="ID_1492716874" CREATED="1585109873688" MODIFIED="1587526865036">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="Interlace" FOLDED="true" ID="ID_1204322663" CREATED="1578642682210" MODIFIED="1587526865036">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="+ Fuzzy system" ID="ID_1242767321" CREATED="1578641399520" MODIFIED="1587526865036">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2001.01686: a deep neuro-fuzzy network for image classification" ID="ID_522465421" CREATED="1578641432239" MODIFIED="1587526865037">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="neural network + fuzzy systems i.e. fuzzy reasoning rules into connectionist networks." ID="ID_1242073246" CREATED="1578641455223" MODIFIED="1587526865037">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="existing neuro-fuzzy systems are shallow" ID="ID_19713722" CREATED="1578641511191" MODIFIED="1587526865038">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="this paper: end-to-end deep neuro-fuzzy model" ID="ID_139433883" CREATED="1578641529382" MODIFIED="1587526865039">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="performance: reasonable. didn&apos;t outperform the SOTA." ID="ID_1480859010" CREATED="1578641555919" MODIFIED="1587526865040">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="+ Frequency domain" ID="ID_203110491" CREATED="1578643422676" MODIFIED="1587526865041">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2001.01034: frequentnet: a new deep learning baseline for image classification" ID="ID_225364341" CREATED="1578643437651" MODIFIED="1587526865041">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="instead of using principal component vectors as the filter vector in the PCANet, we use basis vectors in discrete fourier analysis and wavelets analysis as our filter vectors" ID="ID_1649164143" CREATED="1578643464355" MODIFIED="1587526865041">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="comparable performance, no need for optimization" ID="ID_655258806" CREATED="1578643529771" MODIFIED="1587526865043">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="Sentiment" ID="ID_1764328525" CREATED="1588231332268" MODIFIED="1588231337437">
<node TEXT="1907.06160: smile, behappy :) emoji embedding for visual sentiment analysis" ID="ID_8388311" CREATED="1588231343272" MODIFIED="1588231363911"/>
</node>
</node>
<node TEXT="Data Balancing" FOLDED="true" ID="ID_922155363" CREATED="1590803542752" MODIFIED="1590803548634">
<node TEXT="2004.06524 Contrastive Examples for Addressing the Tyranny of the Majority" ID="ID_727783853" CREATED="1590803549545" MODIFIED="1590803558905"/>
</node>
<node TEXT="Misc Tricks" ID="ID_1670691713" CREATED="1588299328559" MODIFIED="1588299332688"/>
<node TEXT="Age" ID="ID_1783971737" CREATED="1576824706684" MODIFIED="1619589755077">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="(CVPR18) Mean-variance loss for deep age estimation from a face" ID="ID_570719696" CREATED="1576824872783" MODIFIED="1587526865146">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: age estimation" ID="ID_103064711" CREATED="1576824901758" MODIFIED="1587526865147">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: regression does not leverage a distribution&apos;s robustness in representing labels with ambiguity such as ages" ID="ID_748099145" CREATED="1576824922877" MODIFIED="1587526865147">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: new loss function, dubbed mean-variance loss for age estimation via distribution learning" ID="ID_1109675725" CREATED="1576824959038" MODIFIED="1587526865149">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="the new loss function is used in conjunction with standard softmax loss" ID="ID_817138255" CREATED="1576825020135" MODIFIED="1587526865149">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="mean-variance are calculated based on the softmax output" ID="ID_747756389" CREATED="1576825164399" MODIFIED="1587526865150">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="result &amp; conclusion" ID="ID_227373699" CREATED="1576825050735" MODIFIED="1587526865151">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="the proposed loss is useful for obtaining concentrated yet accurate label distribution estimation" ID="ID_1302883693" CREATED="1576825335673" MODIFIED="1587526865151">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="SOTA" ID="ID_14198386" CREATED="1576825377657" MODIFIED="1587526865152">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="PML: Progressive Margin Loss for Long-tailed Age Classification (cvpr21)" ID="ID_285767618" CREATED="1615296048667" MODIFIED="1615296062785">
<node TEXT="refine age label pattern: intra-class variance, inter-class and class center." ID="ID_1312092738" CREATED="1615296080066" MODIFIED="1615296116347"/>
</node>
</node>
</node>
<node TEXT="Face / ReID" FOLDED="true" POSITION="left" ID="ID_764967541" CREATED="1576824448954" MODIFIED="1648335926319">
<icon BUILTIN="clanbomber"/>
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#00007c"/>
<node TEXT="Face Recognition" ID="ID_1959392251" CREATED="1576824686204" MODIFIED="1618846679952">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="FaceNet" ID="ID_341503004" CREATED="1586242055136" MODIFIED="1618846620145">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="FaceNet: A Unified Embedding for Face Recognition and Clusteri" ID="ID_1735787567" CREATED="1591524402876" MODIFIED="1591524404311"/>
</node>
<node TEXT="SphereFace" ID="ID_522295324" CREATED="1586242051206" MODIFIED="1587526865155">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1704.08063 SphereFace: Deep Hypersphere Embedding for Face Recognitio" ID="ID_1706347379" CREATED="1591524483553" MODIFIED="1591524488816"/>
</node>
<node TEXT="NormFace" ID="ID_1778455805" CREATED="1591524497198" MODIFIED="1591524500124">
<node TEXT="1704.06369 L2Hypersphere Embedding for Face Verification" ID="ID_1744807412" CREATED="1591524500815" MODIFIED="1591524518872"/>
</node>
<node TEXT="CosFace" ID="ID_1204305450" CREATED="1591524600368" MODIFIED="1591524602023">
<node TEXT="1801.09414 CosFace: Large Margin Cosine Loss for Deep Face Recognitio" ID="ID_1005890114" CREATED="1591524602887" MODIFIED="1591524613200"/>
</node>
<node TEXT="ArcFace" ID="ID_1384370250" CREATED="1586242047014" MODIFIED="1587526865156">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1801.07698 ArcFace: Additive Angular Margin Loss for Deep Face Recognition" ID="ID_1166653832" CREATED="1591524583413" MODIFIED="1591524589368"/>
</node>
<node TEXT="2002.11841: towards universal representation learning for deep face recognition (CVPR20)" ID="ID_314846989" CREATED="1583323254520" MODIFIED="1587526865157">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: face recognition" ID="ID_1394471379" CREATED="1583323271914" MODIFIED="1587526865158">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: deal with larger variation unseen in the given training data without leveraging taret domain knowledge" ID="ID_1337323170" CREATED="1583323289908" MODIFIED="1587526865159">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: universal representation learning framework" ID="ID_1893031000" CREATED="1583323306563" MODIFIED="1587526865160">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2002.02603: adaptive deep metric embeddings for person re-identification under occlusions" ID="ID_1445762544" CREATED="1583372229841" MODIFIED="1587526865161">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: ReID under occlusion" ID="ID_45779603" CREATED="1583372379031" MODIFIED="1587526865162">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: most existing methods use local features to deal with occlusions. however, they usually independently extract features from the local regions of an image without considering the relationship among different local regions" ID="ID_821874618" CREATED="1583372387908" MODIFIED="1587526865163">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: learns the spetial dependencies between the local regions and extracts the discriminative feature representation" ID="ID_104428063" CREATED="1583372437764" MODIFIED="1587526865165">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="CircleLoss" ID="ID_327025357" CREATED="1586242067191" MODIFIED="1587526865165">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="2004.00288 CurricularFace: Adaptive Curriculum Learning Loss for Deep Face Recognition (cvpr20)" ID="ID_770885226" CREATED="1590806051356" MODIFIED="1590806063475"/>
<node TEXT="2002.10826: deep representation learning on long-tailed data: a learnable embedding augmentation perspective" ID="ID_1701130658" CREATED="1582944492550" MODIFIED="1587526865077">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: facilitate representation learning for long-tail data" ID="ID_58774325" CREATED="1582944838770" MODIFIED="1587526865077">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: head classes and tail classes present different distribution patterns" ID="ID_939186214" CREATED="1582944852918" MODIFIED="1587526865078">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="head classes have large spetial span" ID="ID_1071784992" CREATED="1582944873347" MODIFIED="1587526865079">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="tail classes have small spatial span" ID="ID_493054403" CREATED="1582944887469" MODIFIED="1587526865079">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="how: construct each feature into a feature cloud" ID="ID_1446270120" CREATED="1582944897710" MODIFIED="1587526865080">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2012.05010: strong but simple baseline with dual granularity triplet loss for visible-thermal person re-identification" ID="ID_260138729" CREATED="1607578149584" MODIFIED="1607578176988">
<node TEXT="center-based loss for intra-class compactness and inter-class discrimination from the coarse level." ID="ID_160635367" CREATED="1607579060503" MODIFIED="1607579093763"/>
</node>
<node TEXT="2004.09329: robust partial matching for personal search in the wild (CVPR20)" ID="ID_72012387" CREATED="1589766849502" MODIFIED="1589766888722"/>
</node>
<node TEXT="Cross-Domain" ID="ID_1156990348" CREATED="1618847613125" MODIFIED="1618847618741">
<node TEXT="axv: Cross-Domain Similarity Learning for Face Recognition in Unseen Domains" ID="ID_70329937" CREATED="1618847629720" MODIFIED="1618847643677">
<icon BUILTIN="idea"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="Unsup REID" ID="ID_1913546122" CREATED="1608880346716" MODIFIED="1608880351472">
<node TEXT="2012.09071 Joint Generative and Contrastive Learning for Unsupervised PersonRe-identification" ID="ID_776788813" CREATED="1608880352252" MODIFIED="1608880365103"/>
</node>
<node TEXT="Attribute / Expression" ID="ID_1049418507" CREATED="1576988415194" MODIFIED="1587526865165">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="Facial Expressoin" ID="ID_360070738" CREATED="1583236533369" MODIFIED="1587526865166">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.10392: suppressing uncertainties for large-scale facial expression recognition" ID="ID_1880101450" CREATED="1583236545401" MODIFIED="1587526865166">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: expression recognition" ID="ID_1030455155" CREATED="1583236567218" MODIFIED="1587526865167">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: ambiguous expressions, low-quality facial images, subjectiveness of annotators" ID="ID_1750473674" CREATED="1583236573265" MODIFIED="1587526865168">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: Self-Cure Network (SCN) which suppresses uncertainty and prevents network from overfitting facial images" ID="ID_1502125933" CREATED="1583236610165" MODIFIED="1587526865169">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="a self-attention mechanism over mini-batch to weight each training sample with a ranking regularization" ID="ID_1180017773" CREATED="1583236665438" MODIFIED="1587526865170">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="a relabeling mechanism to modify the labels of these samples in the lowest-ranked group" ID="ID_1597526201" CREATED="1583236686119" MODIFIED="1587526865171">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
</node>
<node TEXT="Face attribute" ID="ID_1431255114" CREATED="1590744867769" MODIFIED="1590744871072">
<node TEXT="2005.11576: hierarchical feature embedding for attribute recognition (CVPR20)" ID="ID_1471001187" CREATED="1590744878334" MODIFIED="1590745133137"/>
</node>
</node>
<node TEXT="Vehicle ReID" ID="ID_1487169938" CREATED="1590803654764" MODIFIED="1590803659017">
<node TEXT="2004.06305: VehicleNet: Learning Robust Visual Representation for Vehicle Re-identification" ID="ID_1381154777" CREATED="1590803659717" MODIFIED="1590803671137"/>
<node TEXT="2004.06271: The Devil is in the Details: Self-Supervised Attention for Vehicle Re-Identification" ID="ID_1878470047" CREATED="1590803731542" MODIFIED="1590803743255"/>
</node>
</node>
<node TEXT="Image Editing" FOLDED="true" POSITION="left" ID="ID_65977927" CREATED="1576830540818" MODIFIED="1642394376414">
<font NAME="Gentium" BOLD="false"/>
<edge COLOR="#ff0000"/>
<node TEXT="Fundamental" ID="ID_1355970702" CREATED="1619335831487" MODIFIED="1619335834763">
<node TEXT="CVPR21: Surrogate Gradient Field for Latent Space Manipulation" ID="ID_592809931" CREATED="1619335835845" MODIFIED="1619335849013"/>
</node>
<node TEXT="Text-guided" ID="ID_96180274" CREATED="1576830552417" MODIFIED="1587526865271">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1912.06203: ManiGan, Text-guided image manipulation" ID="ID_1256240562" CREATED="1576830556977" MODIFIED="1587526865272">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: edit parts of an image" ID="ID_1251551865" CREATED="1576830600090" MODIFIED="1587526865272">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: GAN" ID="ID_724188695" CREATED="1576830608609" MODIFIED="1587526865273">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="two key components" ID="ID_1561560255" CREATED="1576830618737" MODIFIED="1587526865273">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="text-image affine combination module" ID="ID_1986990785" CREATED="1576830628497" MODIFIED="1587526865273">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="select region to be edited" ID="ID_929413564" CREATED="1576830658330" MODIFIED="1587526865273">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="original image feature -- text-irrelevant contents" ID="ID_1498613519" CREATED="1576830684170" MODIFIED="1587526865274">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="detail correction module" ID="ID_1261711347" CREATED="1576830637049" MODIFIED="1587526865275">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="rectifies mismatched atrributes and completes the missing conetnt" ID="ID_1490051208" CREATED="1576830667121" MODIFIED="1587526865275">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Text-Image Synthesis" ID="ID_1506766368" CREATED="1588403169157" MODIFIED="1588403180098">
<node TEXT="2003.08152: SwapText: Image Based Texts Transfer in Scenes" ID="ID_1133281286" CREATED="1588403187099" MODIFIED="1588403216138"/>
</node>
<node TEXT="hair" ID="ID_538662627" CREATED="1590804467312" MODIFIED="1590804469002">
<node TEXT="2004.06848 Intuitive, Interactive Beard and Hair Synthesis with Generative Models (cvpr20)" ID="ID_1110805262" CREATED="1590804469671" MODIFIED="1590804486111"/>
</node>
<node TEXT="anime" ID="ID_862113376" CREATED="1590804588447" MODIFIED="1590804590452">
<node TEXT="2004.06718 Line Art Correlation Matching Network for Automatic Animation Colorization" ID="ID_1382110194" CREATED="1590804591161" MODIFIED="1590804601517"/>
</node>
<node TEXT="2012.09290&#xa;Self-Supervised Sketch-to-Image Synthesis" ID="ID_1510941603" CREATED="1608878874818" MODIFIED="1608878886235"/>
<node TEXT="2012.09841&#xa;Taming Transformers for High-Resolution Image Synthesis" ID="ID_1075081377" CREATED="1608879204035" MODIFIED="1608879212785"/>
</node>
<node FOLDED="true" POSITION="left" ID="ID_1579389920" CREATED="1588216741582" MODIFIED="1642394113994"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#ff0000">Multi-modal representation</font>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<edge COLOR="#00ff00"/>
<node TEXT="vision + language" ID="ID_125347534" CREATED="1590803820236" MODIFIED="1590803826356">
<node TEXT="ECCV 2004.06165: Oscar: Object-Semantics Aligned Pre-training for Vision-Language Tasks" ID="ID_1961572401" CREATED="1590803830019" MODIFIED="1624970126864">
<icon BUILTIN="button_ok"/>
<node TEXT="object tags detected in images as anchor points to ease the learning of alignment" ID="ID_566666734" CREATED="1624969340351" MODIFIED="1624969359544"/>
<node TEXT="better exploit the data" ID="ID_1271537844" CREATED="1624969362160" MODIFIED="1624969372056"/>
</node>
<node TEXT="2012.04638: TAP: text-aware pre-training for text-vqa and text-caption" ID="ID_695841892" CREATED="1607585260060" MODIFIED="1607585278493"/>
<node TEXT="CVPR21: Seeing Out of tHe bOx: End-to-End Pre-training for Vision-Language Representation Learning" ID="ID_1908888029" CREATED="1619443299843" MODIFIED="1619443304341"/>
</node>
<node TEXT="vision + audio" ID="ID_1536975940" CREATED="1590803809232" MODIFIED="1590803819977">
<node TEXT="2004.14326: seeing voices and hearing voices: learning discriminative embeddings using cross-modal self-supervisoin" ID="ID_538501606" CREATED="1588216764991" MODIFIED="1588216790938"/>
</node>
</node>
<node TEXT="Image Translation" FOLDED="true" POSITION="left" ID="ID_357087353" CREATED="1578730821872" MODIFIED="1642394370987">
<font NAME="Gentium" BOLD="false"/>
<edge COLOR="#7c7c00"/>
<node TEXT="supervised (high cost, hard to collect datasets)" ID="ID_237136676" CREATED="1578731997076" MODIFIED="1587739146931">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="unsupervised (trend)" ID="ID_1557448649" CREATED="1578732013668" MODIFIED="1587739144269">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1611.07004: image-to-image translation with conditional adversarial networks (pix2pix)" ID="ID_1094627182" CREATED="1578733910383" MODIFIED="1587526865258">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image2image" ID="ID_1085578152" CREATED="1578734011039" MODIFIED="1587526865259">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="synthesizing photos from label maps, reconstructing objects from edge maps, and colorizing images, etc" ID="ID_107157893" CREATED="1578734013695" MODIFIED="1587526865260">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how" ID="ID_972590250" CREATED="1578734328913" MODIFIED="1587526865260">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="unlike an unconditional GAN, both the generator and discriminator observe the input edge map" ID="ID_616869719" CREATED="1578734330801" MODIFIED="1587526865260">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="1703.00848: unsupervised image-to-image translation networks (UNIT)" ID="ID_238819949" CREATED="1578732824912" MODIFIED="1587526865261">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image2image" ID="ID_669411987" CREATED="1578732858399" MODIFIED="1587526865262">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: one could not infer nothing about the joint distribution from the marginal distributions without additional assumptions" ID="ID_1265579302" CREATED="1578732867400" MODIFIED="1587526865262">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how" ID="ID_1837408627" CREATED="1578732939441" MODIFIED="1587526865263">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="a shared latent space assumption" ID="ID_1637292457" CREATED="1578732949864" MODIFIED="1587526865263">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="\latex $x=F(z)$ and $y=G(z)$" ID="ID_299303300" CREATED="1578733302020" MODIFIED="1587526865264">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="1703.10593: unpaired image-to-image translation using cycle-consistent adversarial networks (CycleGAN)" ID="ID_1314183603" CREATED="1578730839098" MODIFIED="1587526865264">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image2image" ID="ID_1221528038" CREATED="1578730918321" MODIFIED="1587526865265">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: for many tasks, paired training data will not be available" ID="ID_1802399406" CREATED="1578730926440" MODIFIED="1587526865265">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how" ID="ID_916251376" CREATED="1578731174561" MODIFIED="1587526865266">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="\latex $G:X\mapsto Y$ and $F:Y\mapsto X$." ID="ID_1780938702" CREATED="1578731177906" MODIFIED="1587526865266">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="\latex encourages $F(G(x))\approx x$ and $G(F(y))\approx y$" ID="ID_1678479191" CREATED="1578731300618" MODIFIED="1587526865267">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="X and Y are two image domains" ID="ID_1805211088" CREATED="1578731553708" MODIFIED="1587526865268">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="e.g. X is real photo, Y is Monet drawing" ID="ID_1187467562" CREATED="1578731566746" MODIFIED="1587526865268">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="1704.02510: unsupervised dual learning for image-to-image translation (DualGAN)" ID="ID_1845127710" CREATED="1578731481907" MODIFIED="1587526865269">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: image2image" ID="ID_1358328979" CREATED="1578731514419" MODIFIED="1587526865270">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: supervised dataset is too expensive" ID="ID_1747758867" CREATED="1578731518114" MODIFIED="1587526865270">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how" ID="ID_212449039" CREATED="1578731739268" MODIFIED="1587526865271">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="\latex $\|u - G_B(G_A(u))\|$" ID="ID_1913991511" CREATED="1578731742156" MODIFIED="1587526865271">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="\latex $\|v-G_A(G_B(v))\|$" ID="ID_156850288" CREATED="1578731812204" MODIFIED="1587526865271">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="2003.04858: unpaired image-to-image translation using adversarial consistency loss" ID="ID_1428222599" CREATED="1589848867450" MODIFIED="1589848886750"/>
<node TEXT="2012.04932: lipschitz regularized cycleGAN for improving semantic robustness in unpaired image-to-image translation" ID="ID_154605440" CREATED="1607580729207" MODIFIED="1607580753918">
<node TEXT="gradient penalty loss to the generators, which encourages semantically consistent transformations" ID="ID_1720777318" CREATED="1607580755080" MODIFIED="1607580780414"/>
</node>
<node TEXT="2104: Style-Aware Normalized Loss for Improving Arbitrary Style Transfer" ID="ID_410723494" CREATED="1619334048525" MODIFIED="1619334061818"/>
</node>
<node TEXT="Style Transfer" ID="ID_1023438827" CREATED="1588389882528" MODIFIED="1588389887049">
<node TEXT="2003.08436: collaborative distillation for ultra-resolution universal style transfer (cvpr20)" ID="ID_1508846396" CREATED="1588389888513" MODIFIED="1588389977445"/>
</node>
<node TEXT="Colorize" ID="ID_120611080" CREATED="1587739148965" MODIFIED="1587739156325">
<node TEXT="2004.09484: bringing old photos back to life (CVPR20 Oral)" ID="ID_1797644713" CREATED="1587739157845" MODIFIED="1587739177365"/>
</node>
</node>
<node TEXT="Image Generation" FOLDED="true" POSITION="left" ID="ID_1134997009" CREATED="1576827671482" MODIFIED="1642394336267">
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#7c007c"/>
<node TEXT="Overview" ID="ID_1253581434" CREATED="1590026500990" MODIFIED="1590026503690">
<node TEXT="2005.09165: regularization methods for generative adversarial networks: an overview of recent studies" ID="ID_626951677" CREATED="1590026504674" MODIFIED="1590026522304"/>
</node>
<node TEXT="Fundamental" ID="ID_1467097134" CREATED="1619335747557" MODIFIED="1619335754213">
<node TEXT="GAN" ID="ID_877691193" CREATED="1583383137812" MODIFIED="1587526865254">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="DCGAN" ID="ID_1279105302" CREATED="1587612323853" MODIFIED="1587612329617"/>
<node TEXT="Wasserstein GAN" ID="ID_863069188" CREATED="1588229343795" MODIFIED="1588229356097"/>
</node>
<node TEXT="Mode Collapse" ID="ID_647784765" CREATED="1608879369842" MODIFIED="1608879373272">
<node TEXT="2012.08687&#xa;StrokeGAN: Reducing Mode Collapse in Chinese Font Generation via Stroke Encoding" ID="ID_185507986" CREATED="1608879374397" MODIFIED="1608879382885"/>
</node>
<node TEXT="Generative Adversarial Network" ID="ID_428477126" CREATED="1583383133874" MODIFIED="1588229285343">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1907.05681: Adversarial Lipschitz Regularization (ICLR20)" ID="ID_96899344" CREATED="1588229293628" MODIFIED="1588229452535">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2003.06211: Semantic pyramic for image generation (CVPR20)" ID="ID_816605908" CREATED="1588388947876" MODIFIED="1588388975251"/>
<node TEXT="2004.12411: disentangled image generation through structured noise injection (CVPR20)" ID="ID_758344505" CREATED="1588068639509" MODIFIED="1588068658805"/>
<node TEXT="Realness" ID="ID_1635107425" CREATED="1583383141056" MODIFIED="1587526865254">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.05512: real or not real, that is a question (ICLR20)" ID="ID_57675523" CREATED="1583383150308" MODIFIED="1587526865254">
<icon BUILTIN="idea"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="treating realness as a random variable that can be estimated from multiple angles" ID="ID_1146544359" CREATED="1583383161968" MODIFIED="1587526865254">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="provides stronger guidance to the generator" ID="ID_298296022" CREATED="1583383202890" MODIFIED="1587526865255">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="(qualitative results look promising)" ID="ID_1444326722" CREATED="1583383345514" MODIFIED="1587526865256">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="2004.06014: training end-to-end single image generators without gans" ID="ID_1253123156" CREATED="1588081959640" MODIFIED="1588081973459"/>
<node TEXT="2004.10289 Panoptic-based image synthesis (CVPR20)" ID="ID_328182071" CREATED="1587612278910" MODIFIED="1587612293497"/>
<node TEXT="Facial" ID="ID_4493218" CREATED="1587978026544" MODIFIED="1587978027829">
<node TEXT="2004.11660: disentangled and controllable face image generation via 3d imitative contrastive learning (CVPR20 oral)" ID="ID_776492962" CREATED="1587978028637" MODIFIED="1587978053574">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="fairness/balancing" ID="ID_1730712152" CREATED="1607581007442" MODIFIED="1607581011390">
<node TEXT="2012.04842: improving the fiarness of deep generative models without retraining" ID="ID_1850197479" CREATED="1607581012232" MODIFIED="1607581028477"/>
</node>
<node TEXT="fidelity" ID="ID_1550734610" CREATED="1607584457540" MODIFIED="1607584459560">
<node TEXT="2012.05217: positional encoding as spatial inductive bias in gans" ID="ID_315851811" CREATED="1607584460708" MODIFIED="1607584476956">
<node TEXT="such positional encoding is indispensable for generating images with high fidelity" ID="ID_171010780" CREATED="1607584478612" MODIFIED="1607584598119"/>
</node>
</node>
<node TEXT="2012.09793&#xa;SceneFormer: Indoor Scene Generation with Transformers" ID="ID_295290094" CREATED="1608876440959" MODIFIED="1608876451311"/>
<node TEXT="2104: GANcraft: Unsupervised 3D Neural Rendering of Minecraft Worlds" ID="ID_1179165361" CREATED="1619336341425" MODIFIED="1619336351819"/>
</node>
<node TEXT="adversarial augmentation" ID="ID_1748002969" CREATED="1620969865572" MODIFIED="1620969871525">
<node TEXT="cvpr21: When Human Pose Estimation Meets Robustness:Adversarial Algorithms and Benchmarks" ID="ID_425668398" CREATED="1620969872373" MODIFIED="1620969881029"/>
</node>
<node TEXT="Application" ID="ID_280559333" CREATED="1590025737783" MODIFIED="1590025741335">
<node TEXT="Anonymization" ID="ID_570546115" CREATED="1590025742418" MODIFIED="1590025746828">
<node TEXT="2005.09544: CIAGAN: conditional identity anonymization generative adversarial networks (CVPR20)" ID="ID_651028875" CREATED="1590025747793" MODIFIED="1590025772804"/>
</node>
</node>
</node>
<node TEXT="Causual &amp; Induction &amp; Common Sense" FOLDED="true" POSITION="left" ID="ID_1958356012" CREATED="1582337801426" MODIFIED="1642394325220">
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#00007c"/>
<node TEXT="Causual" ID="ID_324034028" CREATED="1587731828478" MODIFIED="1587731830847">
<node TEXT="2002.06838: hierarchical rule induction network for abstract visual reasoning" ID="ID_150581699" CREATED="1582337816081" MODIFIED="1587526865326">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: visual reasoning for Raven&apos;s Progressive Matrices (RPM) test." ID="ID_726926607" CREATED="1582337835010" MODIFIED="1587526865327">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: existing work fails to recognize the complex attribute patterns inside or across rows/columns of RPM" ID="ID_793614613" CREATED="1582337858933" MODIFIED="1587526865328">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: hierarchical rule induction network, by imitating human induction strategies." ID="ID_451344589" CREATED="1582337910538" MODIFIED="1587526865328">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="2004.12906: towards causal generative senne models via competition of experts" ID="ID_162170802" CREATED="1588074941514" MODIFIED="1588074966266"/>
</node>
<node TEXT="Common Sense" ID="ID_686038090" CREATED="1587731879900" MODIFIED="1587731884082">
<node TEXT="2004.10796: visual commonsense graphs: reasoning about the dynamic context of a still image" ID="ID_1005080176" CREATED="1587732569445" MODIFIED="1587732693054">
<node TEXT="database: predict previous and following events" ID="ID_99087495" CREATED="1587732695996" MODIFIED="1587732747861">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node TEXT="GAN Generate" FOLDED="true" POSITION="left" ID="ID_176549857" CREATED="1624457228132" MODIFIED="1642394239449">
<edge COLOR="#007c00"/>
<node TEXT="2106.10410 Deep Generative Learning via Schr &#x308;odinger Bridge" ID="ID_1481005194" CREATED="1624457232722" MODIFIED="1624457250506">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Learning" FOLDED="true" POSITION="left" ID="ID_876811772" CREATED="1583309966895" MODIFIED="1642394191143">
<font NAME="Gentium" SIZE="10" BOLD="false"/>
<edge COLOR="#7c7c00"/>
<node TEXT="Federated Learning (distributed learning; protecting privacy)" ID="ID_647280840" CREATED="1583309969788" MODIFIED="1587526864937">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.09843: practical and bilateral privacy-preserving federated learning" ID="ID_564507506" CREATED="1583310044407" MODIFIED="1587526864938">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: federated learning" ID="ID_985779415" CREATED="1583310061113" MODIFIED="1587526864939">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: protect more privacy" ID="ID_1208071786" CREATED="1583310065998" MODIFIED="1587526864939">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="Online Learning / Novel Class Discovery" ID="ID_1490111028" CREATED="1583373522722" MODIFIED="1587526864940">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.05714: automatically discovering and learning new visual categories with ranking statistics (ICLR20, VGG Oxford)" ID="ID_1826828246" CREATED="1583373530690" MODIFIED="1587526864941">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="why: it&apos;s challenging" ID="ID_1259104861" CREATED="1583373565655" MODIFIED="1587526864941">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="Active Learning" ID="ID_853331882" CREATED="1608876358857" MODIFIED="1608876362122">
<node TEXT="2012.10467&#xa; Minimax Active Learning" ID="ID_1952916971" CREATED="1608876363892" MODIFIED="1608876372836"/>
</node>
<node TEXT="Meta Learning" ID="ID_929306258" CREATED="1583378173046" MODIFIED="1587526864942">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.04162: incremental learning for metric-based meta-learniers" ID="ID_415175776" CREATED="1583378216948" MODIFIED="1587526864942">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="Continual Learning" ID="ID_131447938" CREATED="1587731579088" MODIFIED="1587731583163">
<node TEXT="2004.10862: continual learning of object instances (CVPRw)" ID="ID_1507005369" CREATED="1587731584263" MODIFIED="1587731605443">
<node TEXT="focus on car, and incrementally learn to distinguish different car instances" ID="ID_1166289498" CREATED="1587731618521" MODIFIED="1587731648648"/>
</node>
<node TEXT="2004.11545: dropout as an imlicit gating mechanism for continual learning" ID="ID_475097263" CREATED="1587975571838" MODIFIED="1587975586113"/>
</node>
<node TEXT="Incremental Learning" ID="ID_657680929" CREATED="1590752080935" MODIFIED="1590752087255">
<node TEXT="2004.00713: Memory-Efficient Incremental Learning Through Feature Adaptation" ID="ID_1766164041" CREATED="1590752093902" MODIFIED="1590752108458"/>
</node>
<node TEXT="unsup domain adaptation" ID="ID_108358759" CREATED="1610422300921" MODIFIED="1610422305959">
<node TEXT="2101: Unsupervised Domain Adaptation of Black-Box Source Models" ID="ID_1885176581" CREATED="1610422307004" MODIFIED="1610422319037"/>
</node>
</node>
<node TEXT="Intersection Defense" FOLDED="true" POSITION="left" ID="ID_1275267930" CREATED="1622799700597" MODIFIED="1642394158922">
<edge COLOR="#007c00"/>
<node TEXT="DML" ID="ID_927008795" CREATED="1622802719468" MODIFIED="1622802720343">
<node TEXT="Rubbish" ID="ID_1247032023" CREATED="1622802723397" MODIFIED="1624543493448">
<icon BUILTIN="button_cancel"/>
<node TEXT="Exploring Adversarial Robustness of Deep Metric Learning" ID="ID_1387572831" CREATED="1622802739471" MODIFIED="1622802740450">
<node TEXT="https://arxiv.org/pdf/2102.07265.pdf" ID="ID_645482518" CREATED="1622802726240" MODIFIED="1622802732742"/>
</node>
</node>
</node>
<node TEXT="ObjDet" ID="ID_1203593785" CREATED="1624539286957" MODIFIED="1624539290639">
<node TEXT="(cvpr21) Robust and Accurate Object Detection via Adversarial Learning" ID="ID_1638273128" CREATED="1624539298716" MODIFIED="1624539311743">
<icon BUILTIN="button_ok"/>
<node TEXT="adv example for data augmentation" ID="ID_174762110" CREATED="1624540876004" MODIFIED="1624540886409"/>
<node TEXT="improve obj det performance" ID="ID_946608558" CREATED="1624540886713" MODIFIED="1624540892578"/>
<node TEXT="problems" ID="ID_1871024848" CREATED="1624543458906" MODIFIED="1624543461653">
<node TEXT="what if the object detector faces fierce attack?" ID="ID_1992748864" CREATED="1624543462322" MODIFIED="1624543474430"/>
</node>
</node>
<node TEXT="Class-Aware Robust Adversarial Training for Object Detection" ID="ID_1436320901" CREATED="1637079310725" MODIFIED="1637079312062">
<node TEXT="cvpr21" ID="ID_473812172" CREATED="1637079313024" MODIFIED="1637079324552"/>
</node>
</node>
<node TEXT="Long Tail" ID="ID_1900018509" CREATED="1637080123685" MODIFIED="1637080125542">
<node TEXT="Adversarial Robustness under Long-Tailed Distribution" ID="ID_445119755" CREATED="1637080126444" MODIFIED="1637080127831"/>
</node>
<node TEXT="NAS" ID="ID_1020944876" CREATED="1622799704993" MODIFIED="1622799707782">
<node TEXT="(arxiv) Anti-Bandit Neural Architecture Search for Model Defense" ID="ID_442328463" CREATED="1622799708489" MODIFIED="1622799715416"/>
<node TEXT="On the Security Risks of AutoML" ID="ID_995855368" CREATED="1634132827800" MODIFIED="1634132833674">
<node TEXT="usenix" ID="ID_1006455161" CREATED="1634132834750" MODIFIED="1634132836275"/>
</node>
</node>
<node TEXT="Pretraining" ID="ID_1606119561" CREATED="1624457889540" MODIFIED="1624457891689">
<node TEXT="(axv) Pre-training also Transfers Non-Robustness" ID="ID_407973177" CREATED="1624457892410" MODIFIED="1624457900981"/>
</node>
<node TEXT="Lane Det" ID="ID_1868366935" CREATED="1625974177570" MODIFIED="1625974179767">
<node TEXT="On Robustness of Lane Detection Models to Physical-World Adversarial Attacks in Autonomous Driving" ID="ID_1156882871" CREATED="1625974180616" MODIFIED="1625974189184">
<node TEXT="arxiv" ID="ID_1572806282" CREATED="1625974189946" MODIFIED="1625974192312"/>
</node>
</node>
<node TEXT="Generative" ID="ID_1768680988" CREATED="1625980568602" MODIFIED="1625980571772">
<node TEXT="Inverting Adversarially Robust Networks for Image Synthesis" ID="ID_110239613" CREATED="1625980572708" MODIFIED="1625980577239">
<node TEXT="arxiv" ID="ID_879836352" CREATED="1625980578364" MODIFIED="1625980579990"/>
<node TEXT="robust features aligned to human perception" ID="ID_209232762" CREATED="1625980580226" MODIFIED="1625980587834"/>
<node TEXT="helps image reconstruction" ID="ID_43664767" CREATED="1625980588137" MODIFIED="1625980609125"/>
</node>
</node>
<node TEXT="Fed / IoT" ID="ID_260240737" CREATED="1636487462569" MODIFIED="1636487466382">
<node TEXT="2-in-1 Accelerator: Enabling Random Precision Switch for&#xa;Winning Both Adversarial Robustness and Efficiency" ID="ID_1939163476" CREATED="1636487467425" MODIFIED="1636487474183"/>
</node>
<node TEXT="advtrain / fairness" ID="ID_820016406" CREATED="1636487558836" MODIFIED="1636487563215">
<node TEXT="Trade-off Between Accuracy, Robustness, and Fairness of Deep Classifiers" ID="ID_1785636070" CREATED="1636487564115" MODIFIED="1636487570060"/>
</node>
</node>
<node FOLDED="true" POSITION="left" ID="ID_164822436" CREATED="1619332448153" MODIFIED="1642394149974"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#ff0000">Adversarial Defense</font>
    </p>
  </body>
</html>
</richcontent>
<font SIZE="10" BOLD="true"/>
<edge COLOR="#00007c"/>
<node TEXT="Robustness Metric" ID="ID_680776114" CREATED="1625979162858" MODIFIED="1625979166960">
<node TEXT="Residual Error: a New Performance Measure for Adversarial Robustness" ID="ID_194335772" CREATED="1625979167723" MODIFIED="1625979173388">
<node TEXT="arxiv" ID="ID_819279647" CREATED="1625979175191" MODIFIED="1625979176147"/>
</node>
<node TEXT="Indicators of Attack Failure: Debugging and Improving Optimization of Adversarial Examples" ID="ID_1957501644" CREATED="1625979476870" MODIFIED="1625979503638">
<icon BUILTIN="button_ok"/>
<node TEXT="arxiv" ID="ID_1170392465" CREATED="1625979483056" MODIFIED="1625979484055"/>
<node TEXT="N. Carlini" ID="ID_961715346" CREATED="1625979509014" MODIFIED="1625979515723"/>
</node>
</node>
<node TEXT="Activation" ID="ID_114568529" CREATED="1634132525038" MODIFIED="1634132530043">
<node TEXT="Smooth Adversarial Training" ID="ID_1239940491" CREATED="1634062515869" MODIFIED="1634132555779">
<icon BUILTIN="button_ok"/>
<node TEXT="change activation function" ID="ID_1242144569" CREATED="1634062520694" MODIFIED="1634062527225"/>
<node TEXT="alan" ID="ID_1106059463" CREATED="1634132550553" MODIFIED="1634132552389"/>
</node>
<node TEXT="PARAMETERIZING ACTIVATION FUNCTIONS FOR ADVERSARIAL ROBUSTNESS" ID="ID_1566461952" CREATED="1634132530624" MODIFIED="1634132539112"/>
</node>
<node TEXT="Adv Train" ID="ID_12463402" CREATED="1619332590105" MODIFIED="1619332594362">
<node TEXT="Global Rank" ID="ID_1925881249" CREATED="1622803455076" MODIFIED="1622803476768">
<node TEXT="https://github.com/RobustBench/robustbench" ID="ID_1408727362" CREATED="1622801050275" MODIFIED="1622801058519">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="2104: dual head adversarial training" ID="ID_658666709" CREATED="1619332595421" MODIFIED="1619332610785">
<icon BUILTIN="button_cancel"/>
<node TEXT="Adding another head means increasing the overall model capacity. Larger model capacity is known to benefit robustness. The actually contribution from this work is questionable." ID="ID_1001904050" CREATED="1619332627953" MODIFIED="1619332672688"/>
</node>
<node TEXT="ijcai-19: Improving the Robustness of Deep Neural Networks viaAdversarial Training with Triplet Loss" ID="ID_129115295" CREATED="1622802646544" MODIFIED="1622802654904">
<node TEXT="classification" ID="ID_910084798" CREATED="1622802655866" MODIFIED="1622802664804"/>
<node TEXT="using triplet for adv example" ID="ID_151428576" CREATED="1622802665135" MODIFIED="1622802671305"/>
</node>
<node TEXT="Uncovering the Limits of Adversarial Trainingagainst Norm-Bounded Adversarial Examples" ID="ID_586218461" CREATED="1622803446387" MODIFIED="1622803489684">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="Adversarial Robustness through Local Linearization" ID="ID_1848483043" CREATED="1634062147637" MODIFIED="1634062149383"/>
<node TEXT="Multi-stage Optimization based Adversarial Training" ID="ID_681059043" CREATED="1625550234262" MODIFIED="1625550239652">
<node TEXT="arxiv" ID="ID_1223417208" CREATED="1625550243051" MODIFIED="1625550244622"/>
<node TEXT="under similar amount of training overhead, combination of benign + fgsm + pgd adversarial examples is better than merely pgd for adversarial training" ID="ID_360602510" CREATED="1625550244825" MODIFIED="1625550287117">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="Boosting Fast Adversarial Training with Learnable&#xa;Adversarial Initialization" ID="ID_1510703135" CREATED="1634058997606" MODIFIED="1634059004061">
<node TEXT="TIP" ID="ID_1853022103" CREATED="1634059006767" MODIFIED="1634059008398"/>
<node TEXT="initialization stage" ID="ID_1430748561" CREATED="1634059008759" MODIFIED="1634059019669"/>
<node TEXT="using GAN" ID="ID_1938240131" CREATED="1634059020435" MODIFIED="1634059023063"/>
<node TEXT="not interested" ID="ID_1724281834" CREATED="1634059023419" MODIFIED="1634059026127"/>
</node>
<node TEXT="Adversarial Robustness through Local Linearization" ID="ID_918321317" CREATED="1634060604933" MODIFIED="1634060606259"/>
<node TEXT="Robust fine-tuning of zero-shot models" ID="ID_1645339936" CREATED="1634065440213" MODIFIED="1634065441265"/>
<node TEXT="Robustness and Generalization via Generative Adversarial Training" ID="ID_1723315291" CREATED="1634065799185" MODIFIED="1634065800217">
<node TEXT="strong" ID="ID_168612437" CREATED="1634065802365" MODIFIED="1634065806429"/>
<node TEXT="use gan for adversarial training" ID="ID_1665647308" CREATED="1634065959502" MODIFIED="1634065967049"/>
</node>
<node TEXT="Adversarial Examples Improve Image Recognition" ID="ID_1280278119" CREATED="1634066181027" MODIFIED="1636486961689">
<icon BUILTIN="button_ok"/>
<node TEXT="AdvProp" ID="ID_1481379353" CREATED="1634066183951" MODIFIED="1634066186265"/>
</node>
<node TEXT="Robustness May Be at Odds with Accuracy" ID="ID_1216983011" CREATED="1636486955870" MODIFIED="1636486985569">
<icon BUILTIN="button_ok"/>
<node TEXT="Madry" ID="ID_896433447" CREATED="1636486988872" MODIFIED="1636486990503"/>
</node>
<node TEXT="ADVERSARIAL TRAINING MAY BE&#xa;A DOUBLE&#xa;-EDGED SWORD" ID="ID_873246889" CREATED="1636487927897" MODIFIED="1636487930807">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Rethinking Softmax Cross-Entropy Loss for Adversarial Robustness" ID="ID_1012068221" CREATED="1637076556002" MODIFIED="1637077525706">
<icon BUILTIN="button_ok"/>
<node TEXT="iclr20" ID="ID_455112842" CREATED="1637076568850" MODIFIED="1637076573308"/>
</node>
<node TEXT="Boosting Adversarial Training with&#xa;Hypersphere Embedding" ID="ID_810748400" CREATED="1637077518861" MODIFIED="1637077522793">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Gradient-Guided Dynamic Efficient Adversarial Training*" ID="ID_294225703" CREATED="1637078449979" MODIFIED="1637078451419"/>
<node TEXT="Overfitting" ID="ID_1626319647" CREATED="1637080059730" MODIFIED="1637080062043">
<node TEXT="Exploring Memorization in Adversarial Training" ID="ID_1711786581" CREATED="1637080063060" MODIFIED="1637080067568">
<icon BUILTIN="button_ok"/>
<node TEXT="Jun Zhu" ID="ID_962307317" CREATED="1637080068415" MODIFIED="1637080071195"/>
</node>
</node>
</node>
<node TEXT="Adversarial Regularization" ID="ID_289665507" CREATED="1636486968429" MODIFIED="1636486972960">
<node TEXT="ARCH: Efficient Adversarial Regularized Training with Caching" ID="ID_1059826116" CREATED="1636486976372" MODIFIED="1636487132562">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Towards Improving Adversarial Training of NLP Models" ID="ID_803839388" CREATED="1636487325603" MODIFIED="1636487326981">
<node TEXT="A2T" ID="ID_911171622" CREATED="1636487367570" MODIFIED="1636487369219"/>
</node>
</node>
<node TEXT="Certified" ID="ID_1236277584" CREATED="1636487583184" MODIFIED="1636487586775">
<node TEXT="" ID="ID_623639304" CREATED="1636487588214" MODIFIED="1636487588214"/>
</node>
<node TEXT="Input tramsformation" ID="ID_1213476674" CREATED="1624456431013" MODIFIED="1624456435135">
<node TEXT="arxiv: Self-Supervised Iterative Contextual Smoothing forEfficient Adversarial Defense againstGray- and Black-Box Attack" ID="ID_1485740796" CREATED="1624456436488" MODIFIED="1624456447293"/>
</node>
<node TEXT="Regularization" ID="ID_619021427" CREATED="1625980053065" MODIFIED="1625980056180">
<node TEXT="Adversarial Robustness via Fisher-Rao Regularization" ID="ID_649861640" CREATED="1625980057350" MODIFIED="1625980323173">
<icon BUILTIN="help"/>
<node TEXT="minor improvements" ID="ID_1916236876" CREATED="1625980295412" MODIFIED="1625980299586"/>
<node TEXT="compared to madry, trades, self-adaptive, etc" ID="ID_653230823" CREATED="1625980299959" MODIFIED="1625980315417"/>
</node>
</node>
<node TEXT="Purturbation Analysis" ID="ID_559378753" CREATED="1624457681416" MODIFIED="1624457691163">
<node TEXT="(axv) Delving into the pixels of adversarial samples" ID="ID_469623777" CREATED="1624457692101" MODIFIED="1624457704845"/>
</node>
<node TEXT="Semantic Continuity" ID="ID_1461544046" CREATED="1622799081144" MODIFIED="1622799088846">
<node TEXT="(arxiv) An Experimental Study of Semantic Continuity for Deep Learning Models" ID="ID_703257006" CREATED="1622799089949" MODIFIED="1622799101500">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="zero-shot" ID="ID_1393387700" CREATED="1618847917641" MODIFIED="1618847920321">
<node TEXT="BREEDS: Benchmarks for Subpopulation Shift (madry)" ID="ID_294145792" CREATED="1618847904374" MODIFIED="1618847940000"/>
</node>
<node TEXT="GCE" ID="ID_1268717162" CREATED="1637078487916" MODIFIED="1637078489797">
<node TEXT="improving adversarial robustness via guided complement entropy" ID="ID_1138878565" CREATED="1637078490305" MODIFIED="1637078508623">
<icon BUILTIN="idea"/>
<icon BUILTIN="button_ok"/>
<node TEXT="iccv19" ID="ID_1133047544" CREATED="1637078502597" MODIFIED="1637078503935"/>
</node>
</node>
</node>
<node TEXT="Intersection Attack" FOLDED="true" POSITION="left" ID="ID_1233458939" CREATED="1622799217079" MODIFIED="1642394135458">
<edge COLOR="#7c0000"/>
<node TEXT="Transformer" ID="ID_1624340256" CREATED="1634131797665" MODIFIED="1634131800516">
<node TEXT="Towards Transferable Adversarial Attacks on Vision Transformers" ID="ID_74442504" CREATED="1634131802049" MODIFIED="1634131803417">
<node TEXT="not interested  in yet another attack" ID="ID_1639130592" CREATED="1634131807568" MODIFIED="1634131815192"/>
</node>
</node>
<node TEXT="Obj Det" ID="ID_366936846" CREATED="1633717091302" MODIFIED="1633717094723">
<node TEXT="Adversarial Semantic Contour for Object Detection" ID="ID_1975788414" CREATED="1633717096914" MODIFIED="1633717103404"/>
<node TEXT="DPA: Learning Robust Physical Adversarial Camouflages for Object Detectors" ID="ID_1799741814" CREATED="1634135142792" MODIFIED="1634135147834"/>
</node>
<node TEXT="PCL" ID="ID_923447776" CREATED="1622799231913" MODIFIED="1622799235175">
<node TEXT="(arxiv)&#xa;Nudge Attacks on Point-Cloud DNNs" ID="ID_631907652" CREATED="1622799236106" MODIFIED="1622799258165"/>
</node>
<node TEXT="VOT" ID="ID_1277911165" CREATED="1622799567851" MODIFIED="1622799571692">
<node TEXT="(arxiv) Efficient Adversarial Attacks for Visual Object Tracking" ID="ID_1329953902" CREATED="1622799572654" MODIFIED="1622799580656"/>
</node>
<node TEXT="GAN" ID="ID_1363056297" CREATED="1622799907640" MODIFIED="1622799909221">
<node TEXT="(CVPR18) Generative Adversarial Perturbations" ID="ID_1463619225" CREATED="1622799910398" MODIFIED="1622799913983"/>
</node>
<node TEXT="Caption" ID="ID_1725538475" CREATED="1625974036482" MODIFIED="1625974038362">
<node ID="ID_1074272390" CREATED="1625974024351" MODIFIED="1625974024351"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <h1 http-equiv="content-type" content="text/html; charset=utf-8" class="title mathjax">
      Controlled Caption Generation for Images Through Adversarial Attacks
    </h1>
  </body>
</html>
</richcontent>
<node TEXT="arxiv" ID="ID_1834624347" CREATED="1625974041603" MODIFIED="1625974042595"/>
</node>
</node>
</node>
<node FOLDED="true" POSITION="left" ID="ID_1921860778" CREATED="1619332443607" MODIFIED="1642394128518"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#ff0000">Adversarial Attack</font>
    </p>
  </body>
</html>
</richcontent>
<font SIZE="10" BOLD="true"/>
<edge COLOR="#00ffff"/>
<node TEXT="White-box" ID="ID_1555511443" CREATED="1622799936449" MODIFIED="1622799939620">
<node TEXT="adaptive (nips20)" ID="ID_1782873595" CREATED="1622799940581" MODIFIED="1622799955723">
<node TEXT="On Adaptive Attacks to Adversarial Example Defenses" ID="ID_1281128118" CREATED="1622799956556" MODIFIED="1624535034048">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Guided Adversarial Attack for Evaluating and Enhancing Adversarial Defenses" ID="ID_908146186" CREATED="1637077606067" MODIFIED="1637077608878">
<icon BUILTIN="button_ok"/>
<node TEXT="for evaluation" ID="ID_935717765" CREATED="1637077611688" MODIFIED="1637077614905"/>
</node>
</node>
<node TEXT="Universal Perturbation" ID="ID_634572915" CREATED="1625124226520" MODIFIED="1625124230206"/>
<node TEXT="Transfer-Based" ID="ID_494458451" CREATED="1625124222725" MODIFIED="1625124225459">
<node TEXT="Energy Attack: On Transferring Adversarial Examples" ID="ID_356940553" CREATED="1634132074901" MODIFIED="1634132081830">
<icon BUILTIN="button_ok"/>
<node TEXT="interesting read" ID="ID_265871245" CREATED="1634132085727" MODIFIED="1634132088974"/>
<node TEXT="might be useful for adversarial singularity" ID="ID_501568016" CREATED="1634132100695" MODIFIED="1634132112687"/>
</node>
</node>
<node TEXT="Score-based" ID="ID_400143656" CREATED="1624534954707" MODIFIED="1624534958664">
<node TEXT="1807.07978: prior convictions: black-box adversarial attacks with bandits and priors" ID="ID_587323234" CREATED="1624534968771" MODIFIED="1624535002949">
<icon BUILTIN="button_ok"/>
<node TEXT="(madry, bandit)" ID="ID_568480575" CREATED="1624534959717" MODIFIED="1624534966375"/>
<node TEXT="beats NES" ID="ID_185327310" CREATED="1624535008411" MODIFIED="1624535013577"/>
</node>
</node>
<node TEXT="Decision-Based" ID="ID_1856973949" CREATED="1622798951703" MODIFIED="1622798955974">
<node TEXT="(arxiv) SurFree: a fast surrogate-free black-box attack" ID="ID_1326434288" CREATED="1622798956806" MODIFIED="1622798964074">
<node TEXT="designs a better algorithm than HSJA, GeoDA, QEBA." ID="ID_606031593" CREATED="1622798981685" MODIFIED="1622798998173"/>
</node>
<node TEXT="PopSkipJump: Decision-Based Attack for Probabilistic Classifiers" ID="ID_482453773" CREATED="1625980429996" MODIFIED="1625980436055">
<node TEXT="icml21" ID="ID_659808398" CREATED="1625980437389" MODIFIED="1625980439692"/>
<node TEXT="off-the-shelf randomized defenses, and show that they offer almost no extra robustness to decision-based attacks" ID="ID_678200780" CREATED="1625980440497" MODIFIED="1625980452402"/>
</node>
</node>
<node TEXT="Perturbation Bound" ID="ID_960819597" CREATED="1620970275608" MODIFIED="1620970288993">
<node TEXT="Optical" ID="ID_1950780074" CREATED="1620970291928" MODIFIED="1625124563391">
<node TEXT="ijcai21: AVA: Adversarial Vignetting Attack against Visual Recognition" ID="ID_1511950717" CREATED="1620970303456" MODIFIED="1620970312059"/>
</node>
<node TEXT="Patch" ID="ID_1249707226" CREATED="1625124563660" MODIFIED="1625124565109">
<node TEXT="Inconspicuous Adversarial Patches for FoolingImage Recognition Systems on Mobile Devices" ID="ID_1475567657" CREATED="1625124567553" MODIFIED="1625124574655">
<node TEXT="arxiv" ID="ID_296280509" CREATED="1625124575338" MODIFIED="1625124576547"/>
</node>
</node>
<node TEXT="Human" ID="ID_311535723" CREATED="1625549037670" MODIFIED="1625549042313">
<node TEXT="Demiguise Attack: Crafting Invisible Semantic Adversarial Perturbations with Perceptual Similarity" ID="ID_1778079381" CREATED="1625549045982" MODIFIED="1625549053121">
<node TEXT="arxiv" ID="ID_1349359786" CREATED="1625549055349" MODIFIED="1625549056799"/>
</node>
</node>
<node TEXT="One Pixel" ID="ID_1664614596" CREATED="1625979025746" MODIFIED="1625979028728">
<node TEXT="Analyzing Adversarial Robustness of Deep Neural Networks in Pixel Space: a Semantic Perspective" ID="ID_690165764" CREATED="1625979029699" MODIFIED="1625979030960">
<node TEXT="arxiv" ID="ID_405716477" CREATED="1625979031653" MODIFIED="1625979033179"/>
</node>
</node>
</node>
<node TEXT="Model Extraction" ID="ID_1951028996" CREATED="1619588087975" MODIFIED="1619588092054">
<node TEXT="axv: Thief, Beware of What Get You There: Towards Understanding Model Extraction Attack" ID="ID_1217398408" CREATED="1619588092869" MODIFIED="1619588159518">
<node TEXT="extracting model parameters" ID="ID_760799169" CREATED="1620969904355" MODIFIED="1620969908372"/>
</node>
</node>
<node TEXT="Other Model" ID="ID_458790248" CREATED="1625549273966" MODIFIED="1625549276362">
<node TEXT="DVS-Attacks: Adversarial Attacks on Dynamic Vision Sensors for Spiking Neural Networks" ID="ID_835067979" CREATED="1625549278405" MODIFIED="1625549284762">
<node TEXT="IJCNN" ID="ID_686123720" CREATED="1625549286440" MODIFIED="1625549288651"/>
</node>
</node>
<node TEXT="Transformer" ID="ID_735844601" CREATED="1637078583230" MODIFIED="1637078585847">
<node TEXT="Adversarial Token Attacks on Vision Transformers" ID="ID_163205597" CREATED="1637078586866" MODIFIED="1637078588081"/>
<node TEXT="Robustness Evaluation of Transformer-based Form Field Extractors via Form Attacks" ID="ID_750372851" CREATED="1637078618966" MODIFIED="1637078619794"/>
</node>
</node>
<node TEXT="Deep Metric Learning" FOLDED="true" POSITION="left" ID="ID_1326637759" CREATED="1576827604321" MODIFIED="1642394121127" COLOR="#ff0000">
<font NAME="Gentium" SIZE="10" BOLD="true"/>
<edge COLOR="#ff00ff"/>
<node TEXT="DML Loss" ID="ID_820417736" CREATED="1587973956142" MODIFIED="1619442792710">
<node TEXT="Contrastive" ID="ID_1945215396" CREATED="1587973802684" MODIFIED="1587973804570"/>
<node TEXT="Triplet" ID="ID_1734553454" CREATED="1587973804934" MODIFIED="1587973809741">
<node TEXT="1404.4661: learning fine-grained image similarty with deep ranking (TripLet loss)" ID="ID_206305037" CREATED="1578732547983" MODIFIED="1619442965509">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: fine-grained image similarity" ID="ID_978815820" CREATED="1578732581936" MODIFIED="1587526865073">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="triplet loss function" ID="ID_1869177197" CREATED="1578732571959" MODIFIED="1587526865074">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="triplet sampling" ID="ID_1008793220" CREATED="1578732576430" MODIFIED="1587526865074">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="1412.6622 DEEP METRIC LEARNING USINGTRIPLET NETWORK" ID="ID_643974275" CREATED="1591524453183" MODIFIED="1591524465655"/>
<node TEXT="1503.03832 FaceNet: A Unified Embedding for Face Recognition and Clusteri" ID="ID_542096784" CREATED="1591524372955" MODIFIED="1619442961268">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Lift-Structure (stanford online product)" ID="ID_1943685129" CREATED="1587973959756" MODIFIED="1587973975433">
<node TEXT="Deep Metric Learning via Lifted Structured Feature Embedding (cvpr16)" ID="ID_887643436" CREATED="1592536937971" MODIFIED="1592536947863">
<node TEXT="sop dataset" ID="ID_861157127" CREATED="1592536951302" MODIFIED="1592536955031"/>
<node TEXT="much better than triplet" ID="ID_1941999654" CREATED="1592537061299" MODIFIED="1592537065167"/>
</node>
</node>
<node TEXT="2004.04674 Fisher Discriminant Triplet and Contrastive Losses for Training Siamese Networks (IJCNN20)" ID="ID_1004361213" CREATED="1590805285281" MODIFIED="1590805305310"/>
<node TEXT="2003.13911 Proxy Anchor Loss for Deep Metric Learning (cvpr20)" ID="ID_1377753998" CREATED="1590975224797" MODIFIED="1590975249538">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="idea"/>
</node>
<node TEXT="2004.09073: catsim: a categorical image similarity metric" ID="ID_1455926815" CREATED="1587736120119" MODIFIED="1587736135961"/>
<node TEXT="2002.10857: circle loss: a unified perspective of pair similarity optimization" ID="ID_4869963" CREATED="1583310262185" MODIFIED="1587526865074">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: new loss for DML" ID="ID_1242658509" CREATED="1583310292894" MODIFIED="1587526865075">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: existing methods are not flexible because the penalty strength on every single similarity score is restricted to be equal" ID="ID_7995539" CREATED="1583310300921" MODIFIED="1587526865075">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="Recall@k Surrogate Loss with Large Batches and Similarity Mixup" ID="ID_567374328" CREATED="1630532130548" MODIFIED="1630532138978">
<node TEXT="surrogate loss for recall @ k" ID="ID_1180948827" CREATED="1630532139766" MODIFIED="1630532146391"/>
</node>
</node>
<node TEXT="DML Loss / Classification Family" ID="ID_1369397391" CREATED="1633708396322" MODIFIED="1633708413053"/>
<node TEXT="DML Loss / Ranking Family" ID="ID_1149596026" CREATED="1633708413358" MODIFIED="1633708419757"/>
<node TEXT="DML Loss / AP Family" ID="ID_139878808" CREATED="1633708420018" MODIFIED="1633708425765">
<node TEXT="Robust and Decomposable Average Precision&#xa;for Image Retrieval" ID="ID_350044663" CREATED="1633708429434" MODIFIED="1633708430879">
<node TEXT="nips21" ID="ID_1051433659" CREATED="1633708460621" MODIFIED="1633708463091"/>
</node>
</node>
<node TEXT="Adaptive Boundary" ID="ID_1226818663" CREATED="1619444239750" MODIFIED="1619444243080">
<node TEXT="Ladder Loss" ID="ID_1340507838" CREATED="1619444243882" MODIFIED="1619444246444"/>
<node TEXT="WWW: Towards Self-Adaptive Metric Learning On the Fly" ID="ID_1869520762" CREATED="1619444246723" MODIFIED="1619444256387">
<node TEXT="adaptive-bound triplet loss" ID="ID_275187321" CREATED="1619444264449" MODIFIED="1619444269311"/>
</node>
<node TEXT="margin loss" ID="ID_1261847034" CREATED="1624367804103" MODIFIED="1624367806595"/>
</node>
<node TEXT="Sampling Technique" ID="ID_1742785525" CREATED="1587973843928" MODIFIED="1587973998163">
<node TEXT="ICCV17: Sampling Matters in Deep Embedding Learning" ID="ID_458572746" CREATED="1619442756718" MODIFIED="1619442778392">
<icon BUILTIN="button_ok"/>
<node TEXT="Margin Loss" ID="ID_807215101" CREATED="1619442768919" MODIFIED="1619442771665"/>
<node TEXT="Distance Sampling" ID="ID_813665416" CREATED="1619442772242" MODIFIED="1619442775514"/>
</node>
<node TEXT="2004.11624: dynamic sampling for deep metric sampling" ID="ID_507331720" CREATED="1587973999405" MODIFIED="1587974018399">
<node TEXT="beginning: easy samples" ID="ID_436980983" CREATED="1587974020142" MODIFIED="1587974026031"/>
<node TEXT="afterwards: hard samples" ID="ID_1627753454" CREATED="1587974026399" MODIFIED="1587974030452"/>
<node TEXT="effect: slight" ID="ID_936606003" CREATED="1587974030721" MODIFIED="1602567119970">
<node TEXT="my speculation: rewards for reduced optimization difficulty" ID="ID_263748870" CREATED="1587974053199" MODIFIED="1602567119971"/>
</node>
</node>
</node>
<node TEXT="Framework" ID="ID_919702781" CREATED="1592551254183" MODIFIED="1592551255807">
<node TEXT="1912.11194 a simple and effective framework for pairwise deep metric learning" ID="ID_524705375" CREATED="1592551271711" MODIFIED="1592551283247"/>
</node>
<node TEXT="Tricks" ID="ID_776179147" CREATED="1588229013471" MODIFIED="1588229015557">
<node TEXT="1907.11854: a benchmark on tricks for large-scale image retrieval" ID="ID_1121039947" CREATED="1588229016764" MODIFIED="1588229032598"/>
<node TEXT="Ensemble of Loss Functions to Improve Generalizability of Deep Metric Learning methods" ID="ID_914967764" CREATED="1625549371652" MODIFIED="1625549445797">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="Retrieval / Hashing" ID="ID_1132355353" CREATED="1581830794031" MODIFIED="1634062912486">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2004.09695: image retrieval using multi-scale cnn features pooling" ID="ID_299921439" CREATED="1587548648904" MODIFIED="1587548666408"/>
<node TEXT="2005.06653: structured query-based image retrieval using scene graphs (CVPR20)" ID="ID_1650443600" CREATED="1589767524451" MODIFIED="1589767546122"/>
<node TEXT="Self-supervised Product Quantization for Deep Unsupervised Image Retrieval" ID="ID_1724528022" CREATED="1634062915182" MODIFIED="1634062926580">
<node TEXT="self-supervised" ID="ID_688619887" CREATED="1634062937501" MODIFIED="1634062942012"/>
</node>
</node>
<node TEXT="Sketch-Based Image Retrieval (SBIR)" ID="ID_1068455912" CREATED="1583236761284" MODIFIED="1587526865080">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="2002.10310: sketch less for more: on the fly fine-grained sketch based image retrieval" ID="ID_260933629" CREATED="1583236777762" MODIFIED="1587526865081">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: SBIR" ID="ID_18563923" CREATED="1583236819951" MODIFIED="1587526865081">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="why: most users struggle to draw a complete and faithful sketch" ID="ID_1106236943" CREATED="1583236822904" MODIFIED="1587526865082">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="reformulate the SBIR goal: retrieving the target photo with the least number of strockes possible" ID="ID_49522387" CREATED="1583236860035" MODIFIED="1587526865083">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="how: propose an on-the-fly design that starts retrieving as soon as the user starts drawing." ID="ID_1554794643" CREATED="1583236885772" MODIFIED="1587526865083">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="reinforcement learning-based cross-modal retrieval framework" ID="ID_1698132132" CREATED="1583236919902" MODIFIED="1587526865084">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="directly optimizes rank of the ground-truth photo" ID="ID_1436078871" CREATED="1583289962815" MODIFIED="1587526865085">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="novel reward scheme that circumvents the problems related to irrelevant sketch strockes" ID="ID_144876328" CREATED="1583289986700" MODIFIED="1587526865086">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
</node>
<node TEXT="2004.10780: diagram image retrieval using sketch-based deep learning and transfer learning" ID="ID_184168498" CREATED="1587732885522" MODIFIED="1587732904528"/>
<node TEXT="CVPR21: More Photos are All You Need: Semi-Supervised Learning for Fine-Grained Sketch Based Image Retrieval" ID="ID_503039662" CREATED="1619588722085" MODIFIED="1619588730434"/>
</node>
<node TEXT="Product / Fashion" ID="ID_1038213316" CREATED="1581830785579" MODIFIED="1588229533486">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="1912.08967 Fashion Outfit Complementary Item Retrieval (L. Davis)" ID="ID_1895526025" CREATED="1591524104440" MODIFIED="1591524120580"/>
<node TEXT="2002.02814: Fine-Grained Fashion Similarity Learning byAttribute-Specific Embedding Network" ID="ID_1742589164" CREATED="1581830814619" MODIFIED="1587526865087">
<font NAME="Gentium" BOLD="false"/>
<node TEXT="what: fine-grained embedding" ID="ID_151923931" CREATED="1581830853705" MODIFIED="1587526865088">
<font NAME="Gentium" BOLD="false"/>
</node>
<node TEXT="how: two attention modules + triplet ranking loss" ID="ID_1693413521" CREATED="1581830861418" MODIFIED="1587526865088">
<font NAME="Gentium" BOLD="false"/>
</node>
</node>
<node TEXT="1907.05007: semi-supervised feature-level attribute manipulation for fashion image retrieval (BMVC)" ID="ID_848668805" CREATED="1588229534723" MODIFIED="1588229566804"/>
<node TEXT="2005.12739: An Effective Pipeline for a Real-world Clothes Retrieval System (CVPR20w)" ID="ID_130313313" CREATED="1590745908898" MODIFIED="1590745924437"/>
</node>
<node TEXT="Domain Adaptation" ID="ID_1898156399" CREATED="1624371195889" MODIFIED="1624371199524">
<node TEXT="2103.07503: cross-domain similarity learning for face recognition in unseen domains" ID="ID_1504784041" CREATED="1624371201605" MODIFIED="1624371226979">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node TEXT="Explainable AI" FOLDED="true" POSITION="left" ID="ID_1535392614" CREATED="1646937960988" MODIFIED="1650817806076">
<edge COLOR="#7c0000"/>
<node TEXT="Class Activation Map" ID="ID_47285759" CREATED="1651102989796" MODIFIED="1651103008396">
<node TEXT="CAM" ID="ID_1199847666" CREATED="1651102994318" MODIFIED="1651102997899">
<node TEXT="Learning Deep Features for Discriminative Localization" ID="ID_675900606" CREATED="1651103009874" MODIFIED="1651103025782">
<icon BUILTIN="unchecked"/>
<node TEXT="CVPR, bolei zhou" ID="ID_1929585737" CREATED="1651103017313" MODIFIED="1651103020547"/>
</node>
</node>
</node>
<node TEXT="Do Explanations Explain? Model Knows Best" ID="ID_1458677290" CREATED="1646937968275" MODIFIED="1646937983298">
<icon BUILTIN="checked"/>
<node TEXT="CVPR22" ID="ID_164811359" CREATED="1646937985052" MODIFIED="1646937987156"/>
<node TEXT="one peculiar observation is that these explanations (attributions) point to different features as being important" ID="ID_1757747697" CREATED="1646942060828" MODIFIED="1646942079638"/>
</node>
<node TEXT="What can we learn from misclassified ImageNet&#xa;images?" ID="ID_1435212181" CREATED="1642952852302" MODIFIED="1642952861609">
<icon BUILTIN="pencil"/>
<node TEXT="misclassifications are rarely across superclasses, but mainly among subclasses with a superclass" ID="ID_426581992" CREATED="1642952902122" MODIFIED="1642952932522"/>
<node TEXT="ensemble networks trained each only on subclasses of a given superclass perform better than the same network trained on all subclasses of all superclasses" ID="ID_1950788253" CREATED="1642952937590" MODIFIED="1642952961617"/>
</node>
</node>
<node FOLDED="true" POSITION="left" ID="ID_742784641" CREATED="1558004886033" MODIFIED="1642394387258" COLOR="#ff0000"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <u><font color="#000000">Adversarial Attack &amp; Defense</font></u>
    </p>
  </body>
</html>
</richcontent>
<font NAME="Gentium" SIZE="10" BOLD="true"/>
<edge COLOR="#00ff00"/>
<node TEXT="Overviews" FOLDED="true" ID="ID_1394264125" CREATED="1559914265823" MODIFIED="1590025174942">
<edge COLOR="#007c7c"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1610.00768: Technical Report on thecleverhans v2.1.0 Adversarial Examples Library" ID="ID_748489351" CREATED="1559914312935" MODIFIED="1587728923614">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1712.07107: Adversarial Examples: Attacks and Defenses forDeep Learning" ID="ID_1555442514" CREATED="1559914272254" MODIFIED="1587728923615">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Taxonomy" ID="ID_362304347" CREATED="1570522753813" MODIFIED="1587728923616">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Methods for generating adversarial attacks" ID="ID_144083033" CREATED="1570522759379" MODIFIED="1587728923617">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Application of adv examples" ID="ID_1247024985" CREATED="1570522767884" MODIFIED="1587728923617">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Countermeasure" ID="ID_825197249" CREATED="1570522788539" MODIFIED="1587728923618">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="reactive (detect)" ID="ID_1941239122" CREATED="1570534339280" MODIFIED="1587728923618">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial detection" ID="ID_608995534" CREATED="1570534362808" MODIFIED="1587728923619">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="input reconstruction" ID="ID_4732809" CREATED="1570534368742" MODIFIED="1587728923619">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="network verification" ID="ID_18208506" CREATED="1570534373582" MODIFIED="1587728923620">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="proactive (robustify)" ID="ID_464577717" CREATED="1570534347815" MODIFIED="1587728923620">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="defensive distillation" ID="ID_1771113282" CREATED="1570534384031" MODIFIED="1587728923621">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="adversarial training" ID="ID_1781388111" CREATED="1570534391853" MODIFIED="1587728923622">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="classifier robustifying" ID="ID_615220444" CREATED="1570534397983" MODIFIED="1587728923622">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="ensemble" ID="ID_1868321529" CREATED="1570534420654" MODIFIED="1587728923623">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="almost all defenses are shown to be effective only for part of attacks. They tend not to be defensive for some strong and unseen attacks. Most defenses target adversarial examples in the computer vision task. However, with the development of adversarial examples in other areas, new defenses for these areas, especially for satefy-critical systems are urgently required." ID="ID_1164657603" CREATED="1570535840916" MODIFIED="1587728923623">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Challenges and discussions" ID="ID_1524017623" CREATED="1570522849372" MODIFIED="1587728923625">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="transferability" ID="ID_338674199" CREATED="1570522854364" MODIFIED="1587728923625">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="existence of adversarial examples" ID="ID_1404827896" CREATED="1570522858219" MODIFIED="1587728923625">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="robustness evaluation" ID="ID_561711080" CREATED="1570522865268" MODIFIED="1587728923626">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="2002.08347: on adaptive attacks to adversarial example defenses (madry)" ID="ID_524322170" CREATED="1583144714560" MODIFIED="1587976264820">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="idea"/>
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Not mainly for overview, but it contains a good overview" ID="ID_886145061" CREATED="1583144763112" MODIFIED="1587728923627">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2002.03080: an empirical evaluation of perturbation-based defenses" ID="ID_529205848" CREATED="1581824478162" MODIFIED="1587728923806">
<icon BUILTIN="idea"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: empirical study on defenses" ID="ID_591403012" CREATED="1581826211912" MODIFIED="1587728923807">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: adversarial training + noise, a new combination that has not been well-experimented" ID="ID_207373419" CREATED="1581826223684" MODIFIED="1587728923807">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2004.11488: adversarial machine learning: an interpretation perspective" ID="ID_366513320" CREATED="1587976267628" MODIFIED="1587977482674">
<icon BUILTIN="button_ok"/>
<node TEXT="interpretation" ID="ID_90070977" CREATED="1587977627815" MODIFIED="1587977630223"/>
</node>
<node TEXT="2005.08087: universal adversarial perturbations: a survey" ID="ID_1173268807" CREATED="1590025185659" MODIFIED="1590025203217"/>
<node TEXT="Benchmarking Adversarial Robustness on Image Classificatio (CVPR20 Oral)" ID="ID_1233249218" CREATED="1592532059679" MODIFIED="1592532074528">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
<node TEXT="very good overview and benchmark" ID="ID_1253299584" CREATED="1592532080958" MODIFIED="1592532087933"/>
</node>
</node>
<node TEXT="Implementations" FOLDED="true" ID="ID_41330030" CREATED="1558005425332" MODIFIED="1587728923555">
<edge COLOR="#00ffff"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Cleverhans" ID="ID_746902806" CREATED="1559914199029" MODIFIED="1587728923627">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="AdvBox (baidu, lowquality)" ID="ID_1678136169" CREATED="1561349376127" MODIFIED="1587728923628">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Classical Countermeasure" FOLDED="true" ID="ID_284324386" CREATED="1561349799627" MODIFIED="1587728923628">
<edge COLOR="#00ff00"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="2004-KDD: the data is actively manipulated by an adversary seeking to make the classifier produce false negatives." ID="ID_209173360" CREATED="1561349815443" MODIFIED="1587728923634">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2008-USENIX: an adversary can exploit statistical machine learning for spam filters." ID="ID_606697317" CREATED="1561349906770" MODIFIED="1587728923635">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2015-ICITR: In adversarial retrieval settings such as the web, authors may consistently try to promote their documents in rankings by changing them." ID="ID_906537638" CREATED="1561350129876" MODIFIED="1587728923637">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Threat Model" FOLDED="true" ID="ID_1750640445" CREATED="1558005333577" MODIFIED="1587728923637">
<edge COLOR="#00ff00"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="White Box" ID="ID_1430128574" CREATED="1558006808405" MODIFIED="1587728923638">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Gradient Matrix" ID="ID_1751601438" CREATED="1558062987030" MODIFIED="1587728923639">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Jacobian Matrix" ID="ID_1223620675" CREATED="1558062993605" MODIFIED="1587728923639">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Black Box" ID="ID_528174848" CREATED="1558006811884" MODIFIED="1587728923639">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Transferability" ID="ID_53179049" CREATED="1558063023734" MODIFIED="1587728923639">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Heuristics" ID="ID_653046167" CREATED="1558063034134" MODIFIED="1587728923639">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Attacking Goal" FOLDED="true" ID="ID_959185437" CREATED="1558005406931" MODIFIED="1587728923639">
<edge COLOR="#ff00ff"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Targeted" ID="ID_386346547" CREATED="1558006817038" MODIFIED="1587728923640">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="l-bfgs" ID="ID_269894182" CREATED="1573459358422" MODIFIED="1587728923640">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Untargeted" ID="ID_1636373551" CREATED="1558006820005" MODIFIED="1587728923640">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="FDA" ID="ID_63231784" CREATED="1573459328812" MODIFIED="1587728923640">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="PGD" ID="ID_209190113" CREATED="1573459337014" MODIFIED="1587728923640">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="I-FGSM" ID="ID_589674131" CREATED="1573459338884" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="FGSM" ID="ID_1927168939" CREATED="1573459346555" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="feature map" ID="ID_1589879561" CREATED="1573459371473" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="deep dream" ID="ID_754049716" CREATED="1573459383757" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="FDA" ID="ID_810242782" CREATED="1573459388869" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Representation" ID="ID_1703289404" CREATED="1559913926153" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Reprogramming" ID="ID_471254326" CREATED="1559916898079" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1806.11146: ADVERSARIAL REPROGRAMMING OF NEURALNETWORKS" ID="ID_883709240" CREATED="1559916909726" MODIFIED="1587728923641">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Constraints" FOLDED="true" ID="ID_60598965" CREATED="1559913884408" MODIFIED="1587728923642">
<edge COLOR="#7c007c"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="L0" ID="ID_1216066492" CREATED="1559914004779" MODIFIED="1587728923643">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="e.g. one pixel attack" ID="ID_811355674" CREATED="1576809033375" MODIFIED="1587728923643">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1710.08864: One Pixel Attack for Fooling Deep Neural Networks" ID="ID_18540235" CREATED="1559916150020" MODIFIED="1587728923644">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="L1" ID="ID_1339316581" CREATED="1559914008577" MODIFIED="1587728923645">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="L2" ID="ID_1198766517" CREATED="1559914009962" MODIFIED="1587728923646">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="DeepFool" ID="ID_1300843082" CREATED="1561351233266" MODIFIED="1587728923646">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="C&amp;W" ID="ID_801556665" CREATED="1559914465931" MODIFIED="1587728923646">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="L_infty" ID="ID_416103713" CREATED="1559914012586" MODIFIED="1587728923646">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="FGSM" ID="ID_471384623" CREATED="1559914475170" MODIFIED="1587728923647">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="PGD" ID="ID_1490451340" CREATED="1559914478850" MODIFIED="1587728923647">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="other Imperceptible" ID="ID_1629107429" CREATED="1559916271702" MODIFIED="1587728923647">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="SpatialTransform" ID="ID_589077836" CREATED="1559916277326" MODIFIED="1587728923648">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Noise/No-constraint" ID="ID_1562273407" CREATED="1559914448185" MODIFIED="1587728923648">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1412.1897: Deep Neural Networks are Easily Fooled:High Confidence Predictions for Unrecognizable Images" ID="ID_232126791" CREATED="1559914548492" MODIFIED="1587728923649">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Patch" ID="ID_1611004019" CREATED="1559914445650" MODIFIED="1587728923650">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="change network parameter, or say the most sensitive chunk of parameters" ID="ID_1438864662" CREATED="1573481877041" MODIFIED="1587728923651">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Bit-Flip Attack: Crushing Neural Network withProgressive Bit Search (1903.12269)" ID="ID_232411264" CREATED="1573481896343" MODIFIED="1587728923652">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="perceptual ball" ID="ID_321441539" CREATED="1588301242265" MODIFIED="1588301251795">
<node TEXT="1912.09405" OBJECT="java.lang.Double|1912.09405" ID="ID_1120246546" CREATED="1588301252661" MODIFIED="1588301259256"/>
</node>
<node TEXT="color chanel" ID="ID_1319217052" CREATED="1610425930336" MODIFIED="1610425932625">
<node TEXT="2012: Color Channel Perturbation Attacks for Fooling Convolutional Neural Networks and A Defense Against Such Attacks" ID="ID_1390707950" CREATED="1610425933773" MODIFIED="1610425941083"/>
</node>
<node TEXT="0 (Natural Example)" ID="ID_7750835" CREATED="1591524293430" MODIFIED="1591524305220">
<node TEXT="1907.07174 Natural Adversarial Examples" ID="ID_1690718419" CREATED="1591524319340" MODIFIED="1591524325530">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Wavelet" ID="ID_153513697" CREATED="1630681611464" MODIFIED="1630681619754">
<node TEXT="Improving Visual Quality of Unrestricted Adversarial&#xa;Examples with Wavelet-VAE" ID="ID_168318858" CREATED="1630681624666" MODIFIED="1630681632713"/>
</node>
</node>
<node TEXT="General Attack / White Box" FOLDED="true" ID="ID_134213718" CREATED="1561350695063" MODIFIED="1589620657826">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Gradient + Imperceptible" ID="ID_785468311" CREATED="1602922766242" MODIFIED="1602922776076">
<node TEXT="L-BFGS" ID="ID_1722032306" CREATED="1559913949602" MODIFIED="1587728923655">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="[Framework] 1312.6199: Intriguing properties of neural networks" ID="ID_1358708013" CREATED="1559914349063" MODIFIED="1587728923656">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="This is the first paper that proposed adversarial example. The method for generating adversarial example is not popular nowadays because second-order optimization is difficult and time-consuming." ID="ID_1821334282" CREATED="1559914405472" MODIFIED="1587728923657">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="This paper also pointed out that training on adversarial examples can regularize the model, but at that time such training procedure was expensive." ID="ID_1998955468" CREATED="1561350914135" MODIFIED="1587728923658">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="FGSM" ID="ID_108520076" CREATED="1559913955785" MODIFIED="1587728923659">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1412.6572: Explaining and Harnessing Adversarial Examples" ID="ID_1674845937" CREATED="1559914589222" MODIFIED="1587728923660">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="[SOTA] Local Linearity, fast, transferability, over-the-air" ID="ID_1146241564" CREATED="1559914602476" MODIFIED="1587728923661">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="DeepFool" ID="ID_911976518" CREATED="1559913959057" MODIFIED="1587728923662">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1511.04599: DeepFool: a simple and accuratemethod to fool deep neural networks" ID="ID_110412706" CREATED="1559914711847" MODIFIED="1587728923663">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Also proposed a simple robustness measure" ID="ID_447335203" CREATED="1561351253279" MODIFIED="1587728923664">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="FeatureAdversary" ID="ID_1590160700" CREATED="1559914175630" MODIFIED="1587728923664">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1511.05122: Adversarial Manipulation of Deep representations" ID="ID_402791000" CREATED="1561351312591" MODIFIED="1587728923665">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="We concentrate on the internal layers of DNN representations, to produce a new class of adversarial images that differs qualitatively from others." ID="ID_132555590" CREATED="1561351320567" MODIFIED="1587728923667">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="\latex $$\arg\min_I \| \phi(I) - \phi(I_g) \|_2^2 $$" ID="ID_134723516" CREATED="1581130628545" MODIFIED="1587728923669">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="\latex s.t. $\|I-I_s||_\infty &lt; \delta $" ID="ID_1466200379" CREATED="1581130669985" MODIFIED="1587728923670">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="special: Multiplicative" ID="ID_853469362" CREATED="1602922778128" MODIFIED="1602922953676">
<node TEXT="MULTAV: MULTIPLICATIVE ADVERSARIAL VIDEOS" ID="ID_1330073572" CREATED="1602922812885" MODIFIED="1602922815227">
<node TEXT="https://arxiv.org/pdf/2009.08058.pdf" ID="ID_937292308" CREATED="1602922784796" MODIFIED="1602922798948"/>
<node TEXT="additive $x+r$, multiplicative $xr$" ID="ID_1990052241" CREATED="1602922831185" MODIFIED="1602922867699"/>
<node TEXT="multiplicative perturbation follows a different distribution compared to the additive" ID="ID_315353559" CREATED="1602922868934" MODIFIED="1602922895648">
<node TEXT="multiplicative perturbation breaks (trained using additive perturbation) defensive model?" ID="ID_722602426" CREATED="1602922897216" MODIFIED="1602922931705"/>
</node>
</node>
</node>
</node>
<node TEXT="JSMA" ID="ID_307686437" CREATED="1559913961593" MODIFIED="1587728923670">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1511.07528: The Limitations of Deep Learning ing Adversarial Settings" ID="ID_251120516" CREATED="1559914737517" MODIFIED="1587728923672">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Jacobian Saliency Map Attack" ID="ID_1244998892" CREATED="1561351393202" MODIFIED="1587728923673">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="PGD" ID="ID_935184612" CREATED="1559913970097" MODIFIED="1587728923673">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="BIM/I-FGSM" ID="ID_255359627" CREATED="1559914121700" MODIFIED="1587728923674">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1607.02533: Adversarial examples in the physical world" ID="ID_1557056509" CREATED="1559915217156" MODIFIED="1587728923674">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="LLCM more efficient than FGSM, but less transferable" ID="ID_1231776129" CREATED="1559915237940" MODIFIED="1587728923675">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Attacks InceptionV3, LLCM is not popular anymore" ID="ID_159191029" CREATED="1561352242913" MODIFIED="1587728923677">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Proposed destruction rate for adv examples during black box attack" ID="ID_624733922" CREATED="1561352402048" MODIFIED="1587728923678">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="FGSM performs well when performing over-the-air attack. FGSM is robust to artificial transformations." ID="ID_1052955870" CREATED="1561352434833" MODIFIED="1587728923679">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1611.01236 adversarial learning at scale" ID="ID_550211341" CREATED="1581130244761" MODIFIED="1587728923679">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="PGD" ID="ID_229755283" CREATED="1559914127075" MODIFIED="1587728923684">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1706.06083: Towards Deep Learning Models (Madry, PGD)" ID="ID_1386709557" CREATED="1561471688089" MODIFIED="1587728923684">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1804.03286: On the Robustness of the CVPR 2018 White-Box Adversarial Example Defenses" ID="ID_293588442" CREATED="1559916781437" MODIFIED="1587728923685">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Beat Pixel Deflection" ID="ID_1029281929" CREATED="1559916803927" MODIFIED="1587728923685">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Beat HGR" ID="ID_1085915829" CREATED="1559916828285" MODIFIED="1587728923686" HGAP_QUANTITY="18.499999865889553 pt" VSHIFT_QUANTITY="3.7499998882412946 pt">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
</node>
<node TEXT="C&amp;W" ID="ID_1860884694" CREATED="1559913980393" MODIFIED="1587728923686">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1607.04311: Defensive Distillation is Not Robust to Adversarial Examples" ID="ID_108980418" CREATED="1559915320989" MODIFIED="1587728923687">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1608.04644: Towards Evaluating the Robust ness of Neural Networks (C&amp;W)" ID="ID_51684375" CREATED="1559915474069" MODIFIED="1587728923688">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Iterative; Beats defensive distillation." ID="ID_565637178" CREATED="1560150653756" MODIFIED="1587728923689">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="generates both targeted / untargeted examples" ID="ID_612139881" CREATED="1570537490452" MODIFIED="1587728923691">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="tested defensive distillation with both targetted and untargetted examples" ID="ID_1551424814" CREATED="1570537500098" MODIFIED="1587728923691">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="[Improve] 1811.09600: Decoupling Direction and Norm for Efficient Gradient-BasedL2Adversarial Attacks and Defenses (DDN attack, sota L2 attack)" ID="ID_1839868858" CREATED="1559917060465" MODIFIED="1587728923692">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="improved version of C&amp;W by decoupling direction and norm" ID="ID_838056216" CREATED="1561459478783" MODIFIED="1587728923693">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="both targetted and untargetted attack are performed" ID="ID_1976288131" CREATED="1570537565604" MODIFIED="1587728923694">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="examing defense using untargetted attacks" ID="ID_492201479" CREATED="1570538744762" MODIFIED="1587728923696">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="\latex $\tilde{J}(x,y,\theta)  = J(\tilde{x},y,\theta)$" ID="ID_582371869" CREATED="1570539804590" MODIFIED="1587728923697">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="inspired by FGSM defense and madry defense" ID="ID_948732359" CREATED="1570539839734" MODIFIED="1587728923697">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
</node>
<node TEXT="EAD" ID="ID_1647864314" CREATED="1559916049139" MODIFIED="1587728923698">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1709.04114: EAD: Elastic-Net Attacks to Deep Neural Networks via Adversarial Examples" ID="ID_473621683" CREATED="1559916055427" MODIFIED="1587728923700">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="formulate the process of attacking DNNs via adversarial examples as an elastic-net regularized optimization problem." ID="ID_1106552789" CREATED="1561354828351" MODIFIED="1587728923701">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="MI-FGSM" ID="ID_1000387654" CREATED="1559916090588" MODIFIED="1587728923702">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1710.06081: Boosting Adversarial Attacks with Momentum" ID="ID_611500676" CREATED="1559916104259" MODIFIED="1587728923703">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Incorporate momentum when updating the perturbation with gradient" ID="ID_1893260648" CREATED="1561354906686" MODIFIED="1587728923704">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="One-Pixel / Sparse" ID="ID_1155988369" CREATED="1561354936496" MODIFIED="1588856216200">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1710.08864: One Pixel Attack for Fooling Deep Neural Networks" ID="ID_142096256" CREATED="1561354948864" MODIFIED="1587728923705">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="generate one-pixel perturbation based on differential evolution" ID="ID_142886429" CREATED="1561354987576" MODIFIED="1587728923706">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="requries less information (than black box attack) and can fool more types of networks due to the inherent features of differential evolution." ID="ID_1780505784" CREATED="1561355020039" MODIFIED="1587728923707">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1909.05040: Sparse and Imperceivable Adversarial Attacks (iccv19)" ID="ID_67289234" CREATED="1573479511957" MODIFIED="1587728923708">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: black-box attack, harder to identify" ID="ID_1965818580" CREATED="1573479741444" MODIFIED="1587728923708">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Patchwise Dense" ID="ID_754279269" CREATED="1559914507035" MODIFIED="1588896747435">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1712.09665: Adversarial Patch" ID="ID_1771600178" CREATED="1559916213942" MODIFIED="1587728923710">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Universal, targeted" ID="ID_274088729" CREATED="1559916222614" MODIFIED="1587728923711">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Perceptible perturbation, or say very obvious" ID="ID_1100859954" CREATED="1561355111232" MODIFIED="1587728923712">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="not visually natural" ID="ID_1356368478" CREATED="1561459713601" MODIFIED="1587728923712">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2019-AAAI: Perceptual-Sensitive GAN for Generating Adversarial Patches" ID="ID_1462269814" CREATED="1559917095586" MODIFIED="1587728923712">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="improved fidelity compared to the previous methods" ID="ID_1214609760" CREATED="1561459809796" MODIFIED="1587728923714">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1910.10053: Attacking Optical Flow (iccv19)" ID="ID_508138437" CREATED="1573482073093" MODIFIED="1587728923714">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="attacks optical flow, or say self-driving cars" ID="ID_301650456" CREATED="1573482089753" MODIFIED="1587728923715">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="extension of adversarial patch" ID="ID_1078135581" CREATED="1573482101800" MODIFIED="1587728923716">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1812.02843: Fooling Network Interpretation in Image Classification (iccv19)" ID="ID_206279307" CREATED="1573482615226" MODIFIED="1587728923716">
<font NAME="Gentium" SIZE="10"/>
<node TEXT=" Practi-cal methods such as adversarial patches have been shownto be extremely effective in causing misclassification. How-ever, these patches are highlighted using standard networkinterpretation algorithms, thus revealing the identity of theadversary. We show that it is possible to create adversarialpatches which not only fool the prediction, but also changewhat  we  interpret  regarding  the  cause  of  the  prediction.Moreover,  we introduce our attack as a controlled settingto measure the accuracy of interpretation algorithms." ID="ID_1109242075" CREATED="1573482643169" MODIFIED="1587728923717">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="FDA: Feature disruptive attack (iccv19, 1909.04385)" ID="ID_1239853116" CREATED="1573459422195" MODIFIED="1587728923718">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: untargeted classification attack" ID="ID_135860595" CREATED="1573459436289" MODIFIED="1587728923719">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: disrupt feature map, instead of softmax or pre-softmax output" ID="ID_1398714056" CREATED="1573459446367" MODIFIED="1587728923721">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="PGD is the comparison target" ID="ID_595275744" CREATED="1573459467248" MODIFIED="1587728923723">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="evaluation metric is fooling rate" ID="ID_1431614764" CREATED="1573459488231" MODIFIED="1587728923725">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="applied to style-transfer and image caption generation" ID="ID_321175544" CREATED="1573459661497" MODIFIED="1587728923726">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="FDA may be the strongest white-box adversary" ID="ID_1331171149" CREATED="1573459739933" MODIFIED="1587728923727">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Is madry defense resistent to FDA?" ID="ID_1592963151" CREATED="1573459818030" MODIFIED="1587728923728">
<icon BUILTIN="idea"/>
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2004.12385: towards feature space adversarial attack" ID="ID_1446760513" CREATED="1588081584428" MODIFIED="1588081601241">
<node TEXT="different from feature adversary" ID="ID_977067347" CREATED="1588081603334" MODIFIED="1588081614051"/>
</node>
<node TEXT="Confidence" ID="ID_1964464105" CREATED="1561466952849" MODIFIED="1587728923742">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="2019-ICLR: evaluating methodology for attacks against confidence thresholding models" ID="ID_402304810" CREATED="1561466970968" MODIFIED="1587728923742">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="proposed the MaxConfidence family of attacks, which are optimal in a variety of theoretical settings, including one realistic setting: attacks against linear models." ID="ID_1688841277" CREATED="1561467002512" MODIFIED="1587728923743">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Reprogramming" ID="ID_1387754556" CREATED="1559916879007" MODIFIED="1587728923744">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1806.11146:&#xa;ADVERSARIAL REPROGRAMMING OF NEURALNETWORKS" ID="ID_637353434" CREATED="1561359609575" MODIFIED="1587728923744">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="This attack finds a single adversarial perturbation, that can be added to all test-time inputs to a machine learning model in order to cause the model to perform a task chosen by the adversary -- even if the model was not trained to do this task." ID="ID_1241248952" CREATED="1561386779088" MODIFIED="1587728923745">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Initialization" ID="ID_939379885" CREATED="1588388759711" MODIFIED="1588388763063">
<node TEXT="2003.06878: output diversified initialization for adversarial attacks" ID="ID_957742821" CREATED="1588388767156" MODIFIED="1588388787116">
<node TEXT="random initializtion stragety, different from PGD&apos;s randinit" ID="ID_415516933" CREATED="1588388801385" MODIFIED="1588388815576"/>
</node>
</node>
<node TEXT="Misc" ID="ID_1048108569" CREATED="1561354805695" MODIFIED="1587728923746">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1708.06131: Evasion attacks against machine" ID="ID_134757570" CREATED="1561354814869" MODIFIED="1587728923746">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Irregular Bound" ID="ID_665959527" CREATED="1588301303023" MODIFIED="1589848521377">
<node TEXT="1912.09405: adversarial perturbations on the perceptual ball" ID="ID_499557806" CREATED="1588301308711" MODIFIED="1588301320589">
<node TEXT="semi-sparse" ID="ID_1190918843" CREATED="1588301447426" MODIFIED="1588301449679"/>
<node TEXT="perceptual ball" ID="ID_1822585902" CREATED="1589848493625" MODIFIED="1589848497244"/>
</node>
<node TEXT="2003.08757: adversarial camouflage: hiding physical-world attacks with natural styles" ID="ID_1484894043" CREATED="1589848556807" MODIFIED="1589848577898"/>
</node>
<node TEXT="other imperceptible constraint" ID="ID_860532938" CREATED="1573479137875" MODIFIED="1587728923747">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1904.08489 SEMANTIC ADVERSARIAL ATTACKS: PARAMETRIC TRANSFORMATION STHAT FOOL DEEP CLASSIFIERS" ID="ID_231684801" CREATED="1573479146345" MODIFIED="1588896832398">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="other non-imperceptile constraint" ID="ID_848752314" CREATED="1573481609848" MODIFIED="1587728923748">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="What Else Can Fool Deep Learning?Addressing Color Constancy Errors on Deep Neural Network Performance (iccv19)" ID="ID_662245401" CREATED="1573481623146" MODIFIED="1587728923749">
<font NAME="Gentium" SIZE="10"/>
<node TEXT=" Specifically, we explore how strong color castscaused  by  incorrectly  applied  computational  color  con-stancy &#x2013; referred to as white balance (WB) in photography&#x2013; negatively impact the performance of DNNs targeting im-age segmentation and classification. In addition, we discusshow existing image augmentation methods used to improvethe  robustness  of  DNNs  are  not  well  suited  for  modelingWB errors." ID="ID_1338052480" CREATED="1573481618321" MODIFIED="1587728923750">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="(NIPS) Functional Adversarial Attacks" ID="ID_915385988" CREATED="1576825793758" MODIFIED="1587728923752">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="* Breaking Defense" ID="ID_974968585" CREATED="1583148000827" MODIFIED="1588230275962">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="BPDA. backward pass differentiable approximation" ID="ID_1084692436" CREATED="1583148006348" MODIFIED="1587728923791">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="EOT. expectation over transformation" ID="ID_888153588" CREATED="1583148024272" MODIFIED="1587728923792">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Transferability" FOLDED="true" ID="ID_361544450" CREATED="1559915095612" MODIFIED="1607499387507">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1605.07277: Transferability in Machine Learn ing: from Phenomena to Black-Box Attacks using Adversarial Samples" ID="ID_1918320910" CREATED="1559914956034" MODIFIED="1587728923757">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="ian goodfellow" ID="ID_639294304" CREATED="1630682246707" MODIFIED="1630682251088"/>
<node TEXT="Intorduce new transferability attack betwene previously unexplored pairs of machine learning model classes, most notably SVMs and decision trees." ID="ID_218675879" CREATED="1561351969521" MODIFIED="1587728923758">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="use the target model to label a synthetic dataset" ID="ID_1007831863" CREATED="1630682294802" MODIFIED="1630682305478"/>
</node>
<node TEXT="Metrics of Attack/Transferability" ID="ID_1818786540" CREATED="1588081877174" MODIFIED="1588300632215">
<node TEXT="2004.12771: adversarial fooling beyond &quot;flipping the label&quot;" ID="ID_789321996" CREATED="1588081885882" MODIFIED="1588081900679"/>
<node TEXT="1907.06291: measuring the transferability of adversarial examples" ID="ID_1572977272" CREATED="1588300673837" MODIFIED="1588300695551"/>
</node>
<node TEXT="2003.06468: GeoDA: a geometric framework for black-box adversarial attacks (CVPR20)" ID="ID_258085346" CREATED="1588388130491" MODIFIED="1588388152884">
<node TEXT="generating better black box examples using an iterative method" ID="ID_1947495948" CREATED="1588388293432" MODIFIED="1588388312376"/>
<node TEXT="no consideration for defense" ID="ID_1992069662" CREATED="1588388344878" MODIFIED="1588388350233"/>
</node>
<node TEXT="Transfer-based (using gradient)" ID="ID_644647593" CREATED="1589620635590" MODIFIED="1589848368422">
<node TEXT="Framework" ID="ID_1134912756" CREATED="1561351557400" MODIFIED="1587728923753">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1602.02697:&#xa;Practical Black-Box Attacks against Machine Learning" ID="ID_1066484530" CREATED="1559915115156" MODIFIED="1587728923754">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="train a local network to substitute the target DNN" ID="ID_1964718210" CREATED="1561351597548" MODIFIED="1587728923755">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="breaks gradient masking. Gradient masking methods construct models that does not have useful gradients, e.g. by using a nearest neighbor classifier instead of DNN." ID="ID_14267178" CREATED="1561351637998" MODIFIED="1587728923756">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Universal Perturbation" ID="ID_1378797055" CREATED="1559915172149" MODIFIED="1587728923759">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1610.08401: Universal adversarial perturbations" ID="ID_1770040090" CREATED="1559915543984" MODIFIED="1587728923760">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Image agnostic, (to some extent) network-agnostic" ID="ID_1801290774" CREATED="1559915575902" MODIFIED="1587728923760">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="algorithm: for each datapoint update perturbation" ID="ID_450106672" CREATED="1561353046061" MODIFIED="1587728923761">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Ensemble" ID="ID_1818709558" CREATED="1559915667767" MODIFIED="1587728923762">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1611.02770: Delving into Transferable Adversarial Examples and Black-Box Attacks" ID="ID_307074291" CREATED="1559915684944" MODIFIED="1587728923762">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="ensemble based approach to generate transferable adversarial example" ID="ID_1427545201" CREATED="1561354258213" MODIFIED="1587728923763">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="DaST: Data-free Substitute Training for Adversarial Attacks (cvpr20)" ID="ID_434777205" CREATED="1592534011318" MODIFIED="1592534023444">
<node TEXT="getting rid of the data requirement of training a substitute model" ID="ID_1151953674" CREATED="1592534026160" MODIFIED="1592534053735"/>
</node>
<node TEXT="Invariant to Spatial Transformation" ID="ID_1062700919" CREATED="1559916349471" MODIFIED="1587728923766">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1801.02612: SPATIALLY TRANSFORMED ADVERSARIAL EXAMPLES" ID="ID_1050946702" CREATED="1559916359502" MODIFIED="1587728923767">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="perturbations generated through spatial transformation could result in large L-p distance, but are still perceptually realistic." ID="ID_435389943" CREATED="1561355191554" MODIFIED="1587728923768">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1904.02884: Evading Defenses to Trans" ID="ID_1507621206" CREATED="1561467598406" MODIFIED="1587728923768">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="By optimizing a perturbation over an ensemble of translated images, the generated adversarial examples is less sensitive to the white-box model being attacked and has better transferability." ID="ID_1270714773" CREATED="1561467649284" MODIFIED="1587728923769">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="vr-IGSM" ID="ID_1511616588" CREATED="1559916561635" MODIFIED="1587728923770">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1802.09707: UNDERSTANDING AND ENHANCING THE TRANSFERABILITY OF ADVERSARIAL EXAMPLES" ID="ID_384772740" CREATED="1559916570801" MODIFIED="1587728923770">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Model-specific factors (network architecture, model capacity) and local smoothness of loss function." ID="ID_771322903" CREATED="1559916679445" MODIFIED="1587728923771">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Variance-Reduced attack, since it utilizes the variance-reduced gradient to generate adversarial example" ID="ID_1736451568" CREATED="1561356546988" MODIFIED="1587728923771">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="DI-FGSM (CVPR18)" ID="ID_1787086477" CREATED="1559916711844" MODIFIED="1587728923772">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1803.06978: Improving Transferability of Adversarial Examples with Input Diversity" ID="ID_1299119049" CREATED="1559916720331" MODIFIED="1587728923773">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="What: improve success rate of transfer (black-box attacks)" ID="ID_673000179" CREATED="1573024021986" MODIFIED="1587728923773">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="HOW: apply random transformation to input image at each iteration while generating adv examples" ID="ID_166314307" CREATED="1573024082795" MODIFIED="1587728923774">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="diverse input patterns by random transformation at each iteration" ID="ID_1981414916" CREATED="1559916740091" MODIFIED="1587728923775">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="ILA Intermediate Level Aattack" ID="ID_282187084" CREATED="1573479991799" MODIFIED="1587728923778">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1811.08458 Intermediate Level Adversarial Attack for Enhanced Transferability" ID="ID_1701029226" CREATED="1573480003890" MODIFIED="1587728923779">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="(ILA) 1907.10823: enhancing adversarial example transferability with an intermediate level attack" ID="ID_1417133075" CREATED="1588300197151" MODIFIED="1588300226773"/>
</node>
<node TEXT="GAP: 1712.02328: Generative Adversarial Perturbations (CVPR18)" ID="ID_1182828441" CREATED="1590743842156" MODIFIED="1590743862367">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2005.09257: patch attack for automatic check-out" ID="ID_416993693" CREATED="1590026223741" MODIFIED="1590026236426">
<node TEXT="visible" ID="ID_112886145" CREATED="1590026237258" MODIFIED="1590026241687"/>
</node>
<node TEXT="2012.03528: backpropagating linearly improves transferability of adversarial examples" ID="ID_978305450" CREATED="1607499508881" MODIFIED="1607499599433">
<icon BUILTIN="idea"/>
<node TEXT="linBP" ID="ID_1305787641" CREATED="1607499903744" MODIFIED="1607499905753"/>
<node TEXT="based on fgsm linear interpretation" ID="ID_1397223675" CREATED="1607499536697" MODIFIED="1607499549985"/>
<node TEXT="backpropagates as if some nonlinaer activations are not encountered in the forward pass" ID="ID_986259889" CREATED="1607499550359" MODIFIED="1607499594852"/>
</node>
<node TEXT="Improving Transferability of Adversarial Patches on Face Recognition with Generative Models" ID="ID_1625860796" CREATED="1625124992841" MODIFIED="1625124999409">
<node TEXT="cvpr21" ID="ID_1026732990" CREATED="1625125000285" MODIFIED="1625125001771"/>
</node>
<node TEXT="Universality" ID="ID_1581439948" CREATED="1607499388129" MODIFIED="1607499390534">
<node TEXT="2003.05549: Frequency-Tuned Universal Adversarial Attacks" ID="ID_1542878169" CREATED="1590798427388" MODIFIED="1590798438836"/>
<node TEXT="Universal Adversarial perturbations are not bugs, they are features (cvpr20w)" ID="ID_1860666419" CREATED="1592550004548" MODIFIED="1592550018902">
<node TEXT="targeted UAP without utilizing the original training data" ID="ID_1003842802" CREATED="1592550029870" MODIFIED="1592550062529"/>
</node>
</node>
<node TEXT="Targeted Transfer" ID="ID_1007082528" CREATED="1610424876825" MODIFIED="1610424880487">
<node TEXT="2101: Patch-wise++ Perturbation for Adversarial Targeted Attacks" ID="ID_998535622" CREATED="1610424881267" MODIFIED="1610424887535"/>
</node>
<node TEXT="Different Budget" ID="ID_111051403" CREATED="1610426129096" MODIFIED="1610426141331">
<node TEXT="2101: Local Black-box Adversarial Attacks: A QueryEfficient Approach" ID="ID_1652410173" CREATED="1610426142059" MODIFIED="1610426152403">
<node TEXT="transferability black-box" ID="ID_994247943" CREATED="1610426196711" MODIFIED="1610426202715"/>
</node>
</node>
</node>
<node TEXT="Black-Box" FOLDED="true" ID="ID_1050735842" CREATED="1607499390918" MODIFIED="1607499392845">
<node TEXT="Heuristics" ID="ID_885416992" CREATED="1559915741489" MODIFIED="1587728923764">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1612.06299: Simple Black-Box Adversarial Perturbations for Deep Networks" ID="ID_821612543" CREATED="1559915754359" MODIFIED="1587728923764">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="greedy local search" ID="ID_982078968" CREATED="1561354322810" MODIFIED="1587728923765">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Doesn&apos;t rely on transferability" ID="ID_1164989057" CREATED="1561354327034" MODIFIED="1587728923765">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Curls&amp;Whey" ID="ID_1512288405" CREATED="1561467284606" MODIFIED="1587728923780">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1904.01160: Curls &amp; Whey: Boosting BlackoBox Adversarial Attacks" ID="ID_1080206742" CREATED="1561467294390" MODIFIED="1587728923780">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="two defects exist in black-box iterative attacks that generate adversarial examples by incrementally adjusting the noise-adding direction for each step." ID="ID_1052106769" CREATED="1561467337651" MODIFIED="1587728923780">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="existing iterative attacks add noises monotonically along the direction of gradient ascent, resulting in a lack of diversity and adaptability of the generated iterative trajectories" ID="ID_470280916" CREATED="1561467367482" MODIFIED="1587728923782">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="it is trivial to perform adversarial attack by adding excessive noises, but currently there is no refinement mechanism to squeeze redundant noises." ID="ID_1490782555" CREATED="1561467399571" MODIFIED="1587728923783">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Decision-based (can only access predicted class) / Score-based (can only access logits)" ID="ID_1457456080" CREATED="1589620988209" MODIFIED="1590743962884">
<node TEXT="blackbox (sampling/decision based/other)" ID="ID_213774286" CREATED="1573476383742" MODIFIED="1587728923784">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Guessing Smart: Biased Sampling for Efficient Black-Box Adversarial Attacks (iccv19)" ID="ID_569780747" CREATED="1573476402389" MODIFIED="1587728923784">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="enhanced version of boundary" ID="ID_1203597663" CREATED="1573476412436" MODIFIED="1587728923786">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Universal Adversarial Perturbation via Prior Driven Uncertainty Approximati (iccv19)" ID="ID_335539948" CREATED="1573477460014" MODIFIED="1587728923789">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: unsupervised universal perturbation generation leveraging uncertainty." ID="ID_190679532" CREATED="1573477476395" MODIFIED="1587728923790">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1907.11684: On the Design of Black-box Adversarial Examples by Leveraging Gradient-free Optimization and Operator Splitting Method (iccv19)" ID="ID_1478104715" CREATED="1573483325211" MODIFIED="1594800812640">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1712.04248 DECISION-BASED ADVERSARIAL ATTACKS: RELIABLE ATTACKS AGAINST BLACK-BOX MACHINE LEARNING MODELS (ICLR18) (aka Boundary Attack)" ID="ID_979140996" CREATED="1573476451734" MODIFIED="1592662996544">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="idea"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="boundary attack" ID="ID_1842429854" CREATED="1573477453827" MODIFIED="1587728923789">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="uniform sampling" ID="ID_1260783299" CREATED="1594361707781" MODIFIED="1594361713441"/>
<node TEXT="go along the decision boundary and find the smallest adversarial perturbation" ID="ID_986130919" CREATED="1594361713698" MODIFIED="1594361738947"/>
<node TEXT="s" ID="ID_1084600961" CREATED="1594801190034" MODIFIED="1600060052491"/>
<node TEXT="focus on minimizing the perturbation during the iterations" ID="ID_1376993891" CREATED="1594801219179" MODIFIED="1594801243067"/>
</node>
<node TEXT="1807.04457: query-efficient hard-label black-box attack: an optimization-based approach" ID="ID_1465994500" CREATED="1594363324897" MODIFIED="1594363353784">
<node TEXT="need to calculate the distance of a point to the decision boundary or estimate the predicted probability by the hard-label outputs, which are less efficient as demonstrated in the expeirments." ID="ID_1988494716" CREATED="1594363379584" MODIFIED="1594363413394"/>
</node>
<node TEXT="1904.04433: efficient decision-based black-box adversarial attacks on face recognition" ID="ID_1518592252" CREATED="1594361745346" MODIFIED="1594361780938">
<node TEXT="evolutionary attack" ID="ID_1355824180" CREATED="1594361782683" MODIFIED="1594361787726"/>
<node TEXT="simple and efficient variant of covariance matrix adaptation evolution strategy" ID="ID_1348847327" CREATED="1594363430680" MODIFIED="1594363450834">
<node TEXT="CMA-ES" ID="ID_132452342" CREATED="1594363559203" MODIFIED="1594363561481"/>
<node TEXT="(1+1)-CMA-ES" ID="ID_1302221682" CREATED="1594363561759" MODIFIED="1594363568343"/>
</node>
<node TEXT="can model the local geometries of the search directions and reduce the dimension of the search space." ID="ID_729836703" CREATED="1594801529157" MODIFIED="1594801549362"/>
<node TEXT="initialize from an example that is already adversarial" ID="ID_900329267" CREATED="1594801090383" MODIFIED="1594801109957"/>
<node TEXT="try to find a minimum aversarial perturbation during the iterations" ID="ID_1774698098" CREATED="1594801112729" MODIFIED="1594801127406"/>
<node TEXT="P_c is called the ecolution path. Exponentially decayed search directions." ID="ID_13264358" CREATED="1594801127770" MODIFIED="1594801155316"/>
<node TEXT="eval metric: MSE (since success rate is not a thing)" ID="ID_1717251798" CREATED="1594801161533" MODIFIED="1594801173837"/>
</node>
<node TEXT="2005.14137: QEBA: query-efficient boundary-based blackbox attack (CVPR20)" ID="ID_1542300076" CREATED="1590743987484" MODIFIED="1590744009964"/>
</node>
<node TEXT="no-box (no score no decision) extreme condition" ID="ID_1506761842" CREATED="1607503373386" MODIFIED="1607503545630">
<node TEXT="2012.02525: practical no-box adversarial attacks against DNNs" ID="ID_1347192597" CREATED="1607503392679" MODIFIED="1607503548305">
<icon BUILTIN="bookmark"/>
<node TEXT="the attacker can only gather a small number of examples from the same problem domain" ID="ID_372689439" CREATED="1607503431081" MODIFIED="1607503454317"/>
</node>
</node>
</node>
<node TEXT="Analysis" FOLDED="true" ID="ID_585918457" CREATED="1561468158595" MODIFIED="1607497436436">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1502.02590: Analysis of classifiers&#x2019; robustness" ID="ID_282179492" CREATED="1561468168547" MODIFIED="1587728923810">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="theoretical framework for analysing classifier robustness" ID="ID_988543480" CREATED="1561468186067" MODIFIED="1587728923812">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="capacity alone helps" ID="ID_522912766" CREATED="1561468198114" MODIFIED="1587728923813">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="more capacity and stronger adversaries decrease transferability" ID="ID_1842455577" CREATED="1561468214394" MODIFIED="1587728923813">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1605.07262: Measuring Neural Net Robustness" ID="ID_1543416807" CREATED="1561469872432" MODIFIED="1587728923814">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="propose metric for measuring the robustness of a neural net" ID="ID_498380011" CREATED="1561469916359" MODIFIED="1587728923815">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="existing approaches to improving robustness &quot;overgit&quot; to adversarial examples generated using a specific algorithm." ID="ID_523008412" CREATED="1561469927477" MODIFIED="1587728923816">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1608.07690: A Boundary Tilting Perspective" ID="ID_505713315" CREATED="1561470179802" MODIFIED="1587728923817">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial examples exist when the classification boundary lies close to the submanifold of sampled data" ID="ID_1481552910" CREATED="1561470188113" MODIFIED="1587728923818">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="present a methamatical analysis of this new perspective in the linear case" ID="ID_1964834611" CREATED="1561470216897" MODIFIED="1587728923819">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1704.03453: The Space of Transferable Adver" ID="ID_1245987641" CREATED="1561470882680" MODIFIED="1587728923820">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="the space of adversarial inputs" ID="ID_1468621692" CREATED="1561470888262" MODIFIED="1587728923821">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="adv examples span a contiguous subspace of large dimentionality" ID="ID_629015770" CREATED="1561470915253" MODIFIED="1587728923822">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="adv subspaces with higher dimentionality are more likely to intersect" ID="ID_1430026421" CREATED="1561470937285" MODIFIED="1587728923823">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1711.02846: INTRIGUING PROPERTIES" ID="ID_1981296730" CREATED="1561474047092" MODIFIED="1587728923823">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="origin of adv example is primarily due to an inherent uncertainty that neural networks have about their predictions" ID="ID_1386241790" CREATED="1561474062251" MODIFIED="1587728923824">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1907.07640: robustness properties of facebook&apos;s resnext WSL models" ID="ID_1226839210" CREATED="1588230026979" MODIFIED="1588230050841">
<node TEXT="ResNeXT WSL achieves good performance on these datasets" ID="ID_961670323" CREATED="1588230099619" MODIFIED="1588230114276">
<node TEXT="ImageNet-C" ID="ID_1680393358" CREATED="1588230115049" MODIFIED="1588230118767"/>
<node TEXT="ImageNet-P" ID="ID_1334396378" CREATED="1588230119069" MODIFIED="1588230121698"/>
<node TEXT="ImageNet-A" ID="ID_1078872378" CREATED="1588230121935" MODIFIED="1588230124470"/>
</node>
</node>
<node TEXT="2002.08347: on adaptive attacks to adversarial example defenses" FOLDED="true" ID="ID_1585039933" CREATED="1583144853417" MODIFIED="1587728923825">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: laying out the methodology and the approach necessary to perform an adaptive attack" ID="ID_1449805247" CREATED="1583144900530" MODIFIED="1587728923825">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="why: typical adaptive evaluations are incomplete" ID="ID_397227335" CREATED="1583145702734" MODIFIED="1587728923827">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: analysis and guidance on how to properly perform adaptive attacks against defenses" ID="ID_647620106" CREATED="1583145720528" MODIFIED="1587728923827">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Learned" ID="ID_1985758507" CREATED="1583146011292" MODIFIED="1587728923828">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Defense has been proven extraordinaryli difficult." ID="ID_1893503662" CREATED="1583146015119" MODIFIED="1587728923828">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="defenses are continuously cirvumvented by stronger attacks" ID="ID_1242171840" CREATED="1583146138193" MODIFIED="1587728923830">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="adaptive attack: attacks specifically designed to target a given defense" ID="ID_632072657" CREATED="1583146191521" MODIFIED="1587728923832">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="crucial fact: adaptive attacks cannot be automated and always require appropriate tuning to a given defense" ID="ID_826177723" CREATED="1583146771400" MODIFIED="1587728923834">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="attacks perform targeted attacks because if a targeted attack succeeds, then so will untargeted attacks" ID="ID_210403269" CREATED="1583146977659" MODIFIED="1587728923836">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="defenses perform untargeted attacks because a model robust to untargeted attacks is also robust to targeted attacks" ID="ID_1035489064" CREATED="1583146999049" MODIFIED="1587728923837">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="For any proposed attack, it is possible to build a non-robust defense that prevents the proposed attack" ID="ID_1686299340" CREATED="1583201190115" MODIFIED="1587728923838">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Attacks used" ID="ID_159558841" CREATED="1583201404419" MODIFIED="1587728923839">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="PGD: finding an adversarial example for an input that satisfies a given norm-bound" ID="ID_838429430" CREATED="1583201409053" MODIFIED="1587728923839">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="C&amp;W: find the smallest successful adversarial perturbation" ID="ID_775568800" CREATED="1583201441257" MODIFIED="1587728923840">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="BPDA: gradient apprxomation technique able to circumvent gradient masking" ID="ID_222850691" CREATED="1583201492155" MODIFIED="1587728923841">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="EOT: computing gradients of models with randomized components to bypass randomization" ID="ID_102667509" CREATED="1583201520470" MODIFIED="1587728923842">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Defenses analyzed" ID="ID_1658527791" CREATED="1583201211076" MODIFIED="1587728923849">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="k-winners take all" ID="ID_372667853" CREATED="1583201658264" MODIFIED="1587728923849">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="gradient masking" ID="ID_549479228" CREATED="1583201671196" MODIFIED="1587728923851">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="activation that intentionally designed to mask backpropagating gradients in order to defend against gradient-based attacks" ID="ID_62806177" CREATED="1583201676607" MODIFIED="1587728923852">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="the odds are odd" ID="ID_1556767837" CREATED="1583201737083" MODIFIED="1587728923853">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="statistical test / detection" ID="ID_549128541" CREATED="1583201742302" MODIFIED="1587728923855">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="based on the distribution of a classifier&apos;s logit values" ID="ID_484977827" CREATED="1583201757657" MODIFIED="1587728923857">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="generative classifiers" ID="ID_573862893" CREATED="1583201767974" MODIFIED="1587728923858">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="generative model" ID="ID_874066828" CREATED="1583202359690" MODIFIED="1587728923860">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="fourier transform" ID="ID_1547934532" CREATED="1583202365506" MODIFIED="1587728923860">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="for l_0 attacks" ID="ID_1443746560" CREATED="1583202381832" MODIFIED="1587728923862">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="rethinking softmax cross-entropy" ID="ID_322315497" CREATED="1583202396811" MODIFIED="1587728923862">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="new loss function" ID="ID_1529117361" CREATED="1583202438501" MODIFIED="1587728923864">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="error correcting codes" ID="ID_695028923" CREATED="1583202445454" MODIFIED="1587728923864">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="ensemble" ID="ID_1397561040" CREATED="1583202459661" MODIFIED="1587728923866">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="ensemble diversity" ID="ID_1024735583" CREATED="1583202473552" MODIFIED="1587728923866">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="ensemble" ID="ID_127893204" CREATED="1583202477614" MODIFIED="1587728923867">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="EMPIR" ID="ID_208323985" CREATED="1583202479809" MODIFIED="1587728923867">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="ensemble" ID="ID_224418570" CREATED="1583202495964" MODIFIED="1587728923868">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="mixed precision" ID="ID_1400202925" CREATED="1583202499854" MODIFIED="1587728923868">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="temporal depencency" ID="ID_455687680" CREATED="1583202504593" MODIFIED="1587728923868">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="for ASR attacks" ID="ID_179952702" CREATED="1583202516845" MODIFIED="1587728923869">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="mixup inference" ID="ID_355924421" CREATED="1583202528946" MODIFIED="1587728923869">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="stochastic interpolation" ID="ID_1560613170" CREATED="1583202535485" MODIFIED="1587728923871">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="ME-Net" ID="ID_82659652" CREATED="1583202552685" MODIFIED="1587728923871">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="pre-processing" ID="ID_1985361083" CREATED="1583202555620" MODIFIED="1587728923872">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="randomly discards a large fraction of pixels" ID="ID_271762182" CREATED="1583202562870" MODIFIED="1587728923873">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="asymmetrical adversarial training" ID="ID_1596651448" CREATED="1583202582960" MODIFIED="1587728923874">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial training" ID="ID_1418788051" CREATED="1583202600640" MODIFIED="1587728923875">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="extension of Madry defense" ID="ID_1484829437" CREATED="1583202604492" MODIFIED="1587728923876">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="truning a weakness into a strength" ID="ID_879056570" CREATED="1583202783862" MODIFIED="1587728923877">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="detection" ID="ID_705817471" CREATED="1583202793305" MODIFIED="1587728923878">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1. model prediction should not change substantially when the input is slightly perturbed by Gaussian noise." ID="ID_1869699855" CREATED="1583202796199" MODIFIED="1587728923879">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2. compute the number of steps necessary to turn x into an adversarial example" ID="ID_1083960255" CREATED="1583202912134" MODIFIED="1587728923880">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="NeuroSki" ID="ID_723369698" CREATED="1583202947833" MODIFIED="1587728923881">
<icon BUILTIN="clanbomber"/>
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
</node>
<node TEXT="2005.00060: Bridging Mode Connectivity in Loss Landscapes and Adversarial Robustness (ICLR20)" ID="ID_1824410767" CREATED="1590746249823" MODIFIED="1590746261350"/>
<node TEXT="Attribution" ID="ID_673824391" CREATED="1610423140937" MODIFIED="1610423144295">
<node TEXT="2101: Adversarial Attack Attribution:&#xa;Discovering Attributable Signals in Adversarial ML Attacks" ID="ID_1006153211" CREATED="1610423145771" MODIFIED="1610423156101"/>
</node>
<node TEXT="Analysis + Decomposition" ID="ID_1433768715" CREATED="1607498260972" MODIFIED="1607498281689">
<node TEXT="2012.03516: a singular value perspective on model robustness" ID="ID_749338171" CREATED="1607498444735" MODIFIED="1607498529923">
<icon BUILTIN="idea"/>
<node TEXT="images consist of robust and non-robust features" ID="ID_1732231365" CREATED="1607498468876" MODIFIED="1607498500921">
<node TEXT="robust features are largely visible" ID="ID_1357508759" CREATED="1607498501846" MODIFIED="1607498508370"/>
<node TEXT="non-robust features are not" ID="ID_1923890472" CREATED="1607498508553" MODIFIED="1607498512293"/>
</node>
<node TEXT="robustness leads to improved feature representations." ID="ID_1128278557" CREATED="1607498513354" MODIFIED="1607498524507"/>
</node>
</node>
<node TEXT="Analysis + Visualization" ID="ID_192399120" CREATED="1612164510615" MODIFIED="1612164515668">
<node TEXT="2101: SkeletonVis: Interactive Visualization for UnderstandingAdversarial Attacks on Human Action Recognition Models" ID="ID_1900941856" CREATED="1612164517394" MODIFIED="1612164527631"/>
</node>
</node>
<node TEXT="Defenses" FOLDED="true" ID="ID_1640464285" CREATED="1558005088518" MODIFIED="1607497439007">
<edge COLOR="#0000ff"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Overview" ID="ID_1874182065" CREATED="1590802845241" MODIFIED="1590802847746">
<node TEXT="2002.10703: G&#xf6;del&apos;s Sentence Is An Adversarial Example But Unsolvable" ID="ID_540984029" CREATED="1590802862032" MODIFIED="1590802872662"/>
</node>
<node TEXT="Gradient Masking" FOLDED="true" ID="ID_1081745433" CREATED="1559914030938" MODIFIED="1587728923882">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Known to be an incomplete defense to adv examples" ID="ID_876498295" CREATED="1561355640702" MODIFIED="1587728923884">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Obfuscated Gradients (a kind of gradient masking)" ID="ID_962194083" CREATED="1559916494586" MODIFIED="1587728923885">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1802.00420: Obfuscated Gradients Give a False Sense of Security: Circumventing Defenses to Adversarial Examples" ID="ID_117409156" CREATED="1559916513393" MODIFIED="1587728923887">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="BPDA" ID="ID_63489744" CREATED="1561356025369" MODIFIED="1587728923889">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="While defenses that cause obfuscated gradients appear to defeat iterative optimization-based attacks, we find defenses relying on this effect can be circumvented." ID="ID_106960775" CREATED="1561355426861" MODIFIED="1587728923890">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Shattered Gradients: when the defense is non-differentiable, introduces numeric instability, or otherwise causes a gradient to be nonexistent or incorrect." ID="ID_1412682879" CREATED="1561355725239" MODIFIED="1587728923891">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Stochastic Gradients: causes by randomized defenses." ID="ID_1364571566" CREATED="1561355793088" MODIFIED="1587728923899">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Exploding/Vanishing Gradients" ID="ID_494282107" CREATED="1561355860263" MODIFIED="1587728923901">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1803.07994: Adversarial Defense based" ID="ID_696817607" CREATED="1561525634955" MODIFIED="1587728923901">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="transformation on the gradients that strip them from any semantic information" ID="ID_76713832" CREATED="1561525646811" MODIFIED="1587728923902">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Defensive Distillation" FOLDED="true" ID="ID_697561845" CREATED="1559914035682" MODIFIED="1587728923903">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1607.05113: On the Effectiveness of Defensive Distillation" ID="ID_576059047" CREATED="1559915435622" MODIFIED="1587728923905">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Totally beaten by C&amp;W" ID="ID_1914819160" CREATED="1559915448269" MODIFIED="1587728923906">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="instead of transferring knowledge between different architectures, this paper proposed to use the knowledge extracted from a DNN to improve it&apos;s own resilience to adversarial examples" ID="ID_478391518" CREATED="1561468848504" MODIFIED="1587728923907">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="tested using untargetted attack" ID="ID_807772778" CREATED="1570536828402" MODIFIED="1587728923909">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Ensemble" FOLDED="true" ID="ID_889861776" CREATED="1561554882786" MODIFIED="1587728923909">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="2017-WOOT: Adversarial Example Defenses:" ID="ID_1009518823" CREATED="1561554890984" MODIFIED="1587728923911">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="ensemble of weak defenses is not sufficient to provide stron defense against adversarial examples" ID="ID_14968699" CREATED="1561554902635" MODIFIED="1587728923912">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1712.00673 Towards Robust Neural Networks via RandomSelf-ensemble" ID="ID_1023835294" CREATED="1573461969887" MODIFIED="1587728923913">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2003.05005: using an ensemble color space model to tackle adversarial examples" ID="ID_120560630" CREATED="1584591588103" MODIFIED="1587728923914">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Detection / Reconstruction" ID="ID_901472342" CREATED="1559914053914" MODIFIED="1587728923915">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1702.04267: ON DETECTING" ID="ID_633474487" CREATED="1561470490524" MODIFIED="1587728923917">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="augment deep neural networks with a small &quot;detector&quot; subnetwork which is trained on the binary classification task" ID="ID_1688313100" CREATED="1561470504859" MODIFIED="1587728923919">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1704.01155: Feature Squeezing:" ID="ID_342801703" CREATED="1561470728092" MODIFIED="1587728923920">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="comparing a DNN mode&apos;s prediction on the original input with that on squeezed inputs" ID="ID_121537895" CREATED="1561470748886" MODIFIED="1587728923923">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1705.09064: MagNet: a Two-Pronged Defense" ID="ID_1044359814" CREATED="1561471468424" MODIFIED="1587728923924">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="detector network learns to differentiate between normal and adversarial examples by approximating the manifold of normal examples" ID="ID_696389413" CREATED="1561471475783" MODIFIED="1587728923925">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="reformer network moves adversarial examples towards the manifold of normal examples" ID="ID_61851196" CREATED="1561471510078" MODIFIED="1587728923926">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1712.04006: Training Ensembles to Detect Ad" ID="ID_1094105990" CREATED="1561474657188" MODIFIED="1587728923927">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="detect adv examples" ID="ID_800532019" CREATED="1561474734148" MODIFIED="1587728923928">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="low classification error on random benign examples" ID="ID_830076442" CREATED="1561474660435" MODIFIED="1587728923929">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="simultaneously minimize agreement on examples outside the traning distribution" ID="ID_1478153361" CREATED="1561474693179" MODIFIED="1587728923929">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1806.00081: Resisting Adversarial Attacks" ID="ID_1988204496" CREATED="1561526176984" MODIFIED="1587728923930">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="variational autoencoder with a Gaussain mixture component corresponds to a single class" ID="ID_199732899" CREATED="1561526185751" MODIFIED="1587728923930">
<icon BUILTIN="help"/>
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="selective classification can be performaned using this model, thereby causing the adversarial objective to entail a conflict" ID="ID_1660416925" CREATED="1561526215390" MODIFIED="1587728923931">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1903.00788: AIRD: Adversarial Learning... Image Repurposing Detection" ID="ID_258989391" CREATED="1561554347669" MODIFIED="1587728923932">
<icon BUILTIN="closed"/>
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2004.11273: ensemble generative cleaning with feedbnack loops for defeinding adversarial attacks" ID="ID_1304236543" CREATED="1587729273598" MODIFIED="1587729296003">
<node TEXT="better performance than madry PGD" ID="ID_204953488" CREATED="1587729334477" MODIFIED="1587729350394"/>
</node>
<node TEXT="2003.06979: anomalous instance detection in deep learning: a survey" ID="ID_1994566903" CREATED="1588388373380" MODIFIED="1588388390369">
<node TEXT="anomaly detection &lt;- adversarial examples" ID="ID_1019569327" CREATED="1588388439675" MODIFIED="1588388449729"/>
</node>
<node TEXT="2003.05748: Explaining Away Attacks Against Neural Networks (MLSys2020 w)" ID="ID_1296470421" CREATED="1590798548838" MODIFIED="1590798573165"/>
<node TEXT="Detection and Continual Learning of Novel Face Presentation Attacks" ID="ID_1037802877" CREATED="1630530819757" MODIFIED="1630530829407">
<node TEXT="detection + continual learning" ID="ID_303726050" CREATED="1630530830542" MODIFIED="1630530839576"/>
</node>
</node>
<node TEXT="Adversarial Training" FOLDED="true" ID="ID_1446496307" CREATED="1559914025555" MODIFIED="1612163720883">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="first proposed by Ian, L-BFGS and FGSM" ID="ID_1078417139" CREATED="1561467866662" MODIFIED="1587728923933">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="\latex $\tilde{J}(x,y,\theta) = \alpha J(x,y,\theta) + (1-\alpha)J(\tilde{x},y,\theta)$" ID="ID_273885433" CREATED="1570539695829" MODIFIED="1587728923937">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="where adv x is untargetted" ID="ID_425125591" CREATED="1570539763309" MODIFIED="1587728923937">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1511.03034: LEARNING WITH A STRONG adversary" ID="ID_728275751" CREATED="1561468743033" MODIFIED="1587728923938">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="learning with a strong adversary, that learns robust classifiers from supervised data by generating adversarial examples as an intermediate step" ID="ID_212578931" CREATED="1561468799722" MODIFIED="1587728923941">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1511.05432: Understanding Adversarial Train" ID="ID_396035274" CREATED="1561469459638" MODIFIED="1587728923942">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="theoretical understanding of adversarial traning, as well as new optimization schemes, based on robust optimization" ID="ID_363766507" CREATED="1561469469845" MODIFIED="1587728923945">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="generating and using adversarial examples during training of anns can be derived from the powerful notion of robust optimization, which has many applications in machine learning and is closely related to regularization" ID="ID_1890484546" CREATED="1561469518373" MODIFIED="1587728923946">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1604.04326: Improving the Robustness of Deep" ID="ID_1504132408" CREATED="1561469734720" MODIFIED="1587728923947">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="NOT quite adversarial" ID="ID_1799121942" CREATED="1561469742647" MODIFIED="1587728923949">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="general stability traning method to stablize deep networks against small input distortions that result from various types of common image processing, such as compression, rescaling, and cropping" ID="ID_35716499" CREATED="1561469747901" MODIFIED="1587728923950">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="essentially data augmentation for better regularization." ID="ID_1534633828" CREATED="1561469789485" MODIFIED="1587728923951">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1611.01236: ADVERSARIAL MACHINE learning at scale" ID="ID_1646099767" CREATED="1561470317290" MODIFIED="1587728923952">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="apply adversarial training to ImageNet" ID="ID_603696831" CREATED="1561470329797" MODIFIED="1587728923954">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="adv training doesn&apos;t help much against iterative attacks" ID="ID_387408752" CREATED="1561470344217" MODIFIED="1587728923956">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="iteratively generated adv examples are less likely to be transferable" ID="ID_681581773" CREATED="1561470385699" MODIFIED="1587728923957">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="increasing the model capacity could help to increase robustness especially used in conjunction with adv training" ID="ID_545250309" CREATED="1561470408803" MODIFIED="1587728923959">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="\latex $Loss=L(X_i|y_i) + \lambda L(X^{adv}_i|y_i)$" ID="ID_647276622" CREATED="1570586407663" MODIFIED="1587728923960">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="label leaking" ID="ID_1381610745" CREATED="1573463425404" MODIFIED="1587728923960">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1705.07204: ENSEMBLE ADVERSARIAL TRAINING" ID="ID_864755566" CREATED="1561471071326" MODIFIED="1587728923960">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial training remains vulnerable to black box attacks" ID="ID_498811289" CREATED="1561471108495" MODIFIED="1587728923963">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Ensemble adversarial training, augments training data with perturbations transferred from other models" ID="ID_1703173924" CREATED="1561471131356" MODIFIED="1587728923964">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="adversarial training can be improved by decoupling the generationg of adv examples from the model being trained" ID="ID_1029880407" CREATED="1561471201325" MODIFIED="1587728923964">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1708.05493 towards interpretable deep neural networks by leveraging adversarial examples" ID="ID_1856027444" CREATED="1570535413571" MODIFIED="1587728923965">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="minimize the cross-entropy loss and internal representation distance during adversarial training, which can be seen as a defense version of feature adversary" ID="ID_1667205009" CREATED="1570535515441" MODIFIED="1587728923967">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1807.10454: Rob-GAN: Generator, Discrimi" ID="ID_960854771" CREATED="1561526299937" MODIFIED="1587728923971">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial training is acknowledged the most powerful defense algorithm" ID="ID_380037783" CREATED="1561526329474" MODIFIED="1587728923973">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="combine generator, discriminator and adversarial attacker" ID="ID_938773029" CREATED="1561526343487" MODIFIED="1587728923974">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="better generator and a more robust discriminator" ID="ID_20534074" CREATED="1561526358838" MODIFIED="1587728923975">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Madry Defense" ID="ID_235162" CREATED="1610428005928" MODIFIED="1610428009403">
<node TEXT="1706.06083: Towards Deep Learning Models (Madry, PGD)" ID="ID_78522731" CREATED="1561471713313" MODIFIED="1587728923975">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<node TEXT="robust optimization, a min-max problem" ID="ID_918215358" CREATED="1561471729153" MODIFIED="1587728923977">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="PGD adversary for attack and defense" ID="ID_1635588547" CREATED="1561471742126" MODIFIED="1587728923979">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="untargetted attack and untargetted adversary used for defense" ID="ID_363992037" CREATED="1570514568848" MODIFIED="1587728923980">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="does not mention targetted attack at all" ID="ID_1997079608" CREATED="1570514577999" MODIFIED="1587728923981">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1811.09600 says it is the only defense not broken under white-box attacks" ID="ID_1374629403" CREATED="1570538023207" MODIFIED="1587728923982">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2012: A Simple Fine-tuning Is All You Need:Towards Robust Deep Learning Via Adversarial Fine-tuning" ID="ID_1598177239" CREATED="1610428012740" MODIFIED="1610428173553">
<icon BUILTIN="bookmark"/>
<node TEXT="we hypothesize that effective learning rate scheduling during adversarial training can significantly reduce the overfitting issue, to a degree where one does not even need to adversarially train a model from scratch but can instead simply adversarially fine-tune a pre-trained model." ID="ID_105754139" CREATED="1610428031762" MODIFIED="1610428169408"/>
<node TEXT="will be included in realsafe" ID="ID_1426784486" CREATED="1610428177339" MODIFIED="1610428183643"/>
</node>
</node>
<node TEXT="1811.10716: Bilateral Adversarial Training:  (iccv19)" ID="ID_669041267" CREATED="1573462620652" MODIFIED="1587728923982">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: adversarial training" ID="ID_1279545229" CREATED="1573462659556" MODIFIED="1587728923984">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: also perturb the label" ID="ID_1943832053" CREATED="1573462669056" MODIFIED="1587728923985">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="note: use targeted adversarial example instead of targeted ones" ID="ID_789911203" CREATED="1573462682598" MODIFIED="1587728923986">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Madry defense achieves best robustness according to &quot;Obfuscased gradient ...&quot;" ID="ID_1456554546" CREATED="1573462767606" MODIFIED="1587728923987">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="improvment based on PGD, has very close final form compared to Madry defense." ID="ID_1977454596" CREATED="1573462696259" MODIFIED="1587728923988">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1812.03705: Defending Against Universal Perturbations With Shared Adversarial Training (iccv19)" ID="ID_711913451" CREATED="1573476597796" MODIFIED="1587728923993">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1901.04684: The limitationsap of adversarial" ID="ID_1025122759" CREATED="1561552625079" MODIFIED="1587728923968">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Matry is one of the most effective methods to defend against adversarial examples" ID="ID_1956858356" CREATED="1561552627639" MODIFIED="1587728923970">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1904.00887: Adversarial Defense by Restricting the Hidden Spaceof Deep Neural Networks (iccv19)" ID="ID_836224912" CREATED="1573480931788" MODIFIED="1587728923994">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="new adversarial tranining scheme" ID="ID_208567203" CREATED="1573480960486" MODIFIED="1587728923994">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1907.10737: joint adversarial training: incorporating both spatial and pixel attacks" ID="ID_1505509610" CREATED="1588300024361" MODIFIED="1588300060981"/>
<node TEXT="(Feature Scattering) 1907.10764: defense against adversarial attacks using feature scattering-based adversarial training" ID="ID_912422098" CREATED="1588300362729" MODIFIED="1588300387996"/>
<node TEXT="1909.00900: metric learning for adversarial robustness" ID="ID_1284852884" CREATED="1581130970743" MODIFIED="1587728923995">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: enhancing classifier robustness" ID="ID_914111910" CREATED="1581132182986" MODIFIED="1587728923995">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: adversarial training with auxiliarty triplet ranking loss and untargeted adversarial examples" ID="ID_540456040" CREATED="1581132193603" MODIFIED="1587728923996">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1909.09481: Adversarial Learning with Margin-based Triplet EmbeddingRegularization (iccv19)" ID="ID_345264768" CREATED="1573463948605" MODIFIED="1587728923989">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: defend classifier" ID="ID_1943917649" CREATED="1573463969701" MODIFIED="1587728923990">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: incorporate the triplet embedding loss and enlarge the margin between interested sample groups" ID="ID_1741916341" CREATED="1573463982337" MODIFIED="1587728923991">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="evaluated on face recognition" ID="ID_1803580955" CREATED="1573464007524" MODIFIED="1587728923992">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2003.04286: Manifold Regularization for Adversarial Robustness" ID="ID_1681692696" CREATED="1590802326488" MODIFIED="1590802337091"/>
<node TEXT="2004.00306 Towards Achieving Adversarial Robustness by Enforcing Feature Consistency Across Bit Planes (cvpr20)" ID="ID_1883210439" CREATED="1590806486562" MODIFIED="1590806500986">
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="2004.08628: single-step adversarial tranining with dropout scheduleing (cvpr20)" ID="ID_572409540" CREATED="1590746586456" MODIFIED="1590746614756">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2005.09147: increasing-margin adversarial (IMA) training to improve adversarial robustness of neural networks" ID="ID_1807795392" CREATED="1590026450095" MODIFIED="1590026470926"/>
<node TEXT="2005.11904: adaptive adversarial logits pairing" ID="ID_1100845919" CREATED="1590744604297" MODIFIED="1590744618968"/>
<node TEXT="2101: Understanding and Achieving Efficient Robustness withAdversarial Contrastive Learning" ID="ID_1832452173" CREATED="1612164112596" MODIFIED="1612164124277">
<node TEXT="adversarial training + metric-inspired constraint" ID="ID_535962861" CREATED="1612164126076" MODIFIED="1612164137063"/>
<node TEXT="not sota" ID="ID_767160625" CREATED="1612164182667" MODIFIED="1612164184201"/>
</node>
</node>
<node TEXT="Defense using Attack" ID="ID_989003422" CREATED="1610425103934" MODIFIED="1610425108893">
<node TEXT="2012: Beating Attackers At Their Own Games:Adversarial Example Detection Using Adversarial Gradient Directions (AAAI21)" ID="ID_558892897" CREATED="1610425109754" MODIFIED="1610425176707">
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="Denoising / EliminatingPerturbation" FOLDED="true" ID="ID_83889709" CREATED="1559914041385" MODIFIED="1587872294149">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1412.5608: DenoisingAE+ContractiveAE" ID="ID_1451530050" CREATED="1561467949055" MODIFIED="1587728923998">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="pre-processing with denoising auto encoders" ID="ID_1180279737" CREATED="1561467993790" MODIFIED="1587728923999">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="When DAE is stacked with the original DNN, the whole model can be attacked by new adversarial examples by even smaller distortion" ID="ID_1577257799" CREATED="1561468010836" MODIFIED="1587728924000">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Contractive Audoencoder to mitigate the problem" ID="ID_1847176525" CREATED="1561468097660" MODIFIED="1587728924001">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1707.05474: APE-GAN: Adversarial Pertur" ID="ID_314216753" CREATED="1561472764514" MODIFIED="1587728924002">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="use GAN to eliminate adv perturbation" ID="ID_1180972432" CREATED="1561472775707" MODIFIED="1587728924003">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1712.02976: Defense against Adversarial (HGD, NIPS2017)" ID="ID_341190230" CREATED="1561474370180" MODIFIED="1587728924003">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="high-level representation guided denoiser" ID="ID_75384337" CREATED="1561474409955" MODIFIED="1587728924004">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="loss function defined as the difference between the target model&apos;s outputs activated by the clean image and denoised image" ID="ID_1134433338" CREATED="1561474461715" MODIFIED="1587728924005">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Not effective to PGD (1804.03286)" ID="ID_1406719270" CREATED="1561359419980" MODIFIED="1587728924006">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1808.06645: Stochastic Combinatorial Ensem" ID="ID_1890852480" CREATED="1561526474355" MODIFIED="1587728924007">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="stochastically decides whether or not to insert noise removal operators such as VAEs between layers" ID="ID_597763063" CREATED="1561526476882" MODIFIED="1587728924007">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1805.06605: DEFENSE-GAN:" ID="ID_1572755171" CREATED="1561525803989" MODIFIED="1587728924008">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="models the distribution of unperturbed images." ID="ID_207174547" CREATED="1561525813004" MODIFIED="1587728924009">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="finds a close output to a given image which does not contain the adversarial changes." ID="ID_1004027010" CREATED="1561525842508" MODIFIED="1587728924010">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="this output is then fed to the classifier." ID="ID_81646525" CREATED="1561525866572" MODIFIED="1587728924010">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Transformations (input pre-processing, modifications)" FOLDED="true" ID="ID_499107414" CREATED="1559914045586" MODIFIED="1588216950881">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1511.06292: FOVEATION-BASED MECHA" ID="ID_1919727551" CREATED="1561469601248" MODIFIED="1587728924012">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial perturbations can be alleviated with a mechanism based on foveations -- applying the cnn in different image regions" ID="ID_373173108" CREATED="1561469624078" MODIFIED="1587728924013">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1711.00117: Countering Adversarial Images" ID="ID_1212514901" CREATED="1561473745579" MODIFIED="1587728924013">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="transforming the inputs before feeding them to the system" ID="ID_18188715" CREATED="1561473771816" MODIFIED="1587728924014">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="transformations such as bit-depth reduction, JPEG compression. total variance minimization, image quilting" ID="ID_1441627990" CREATED="1561473785761" MODIFIED="1587728924015">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="good at countering FGSM, DeepFool, C&amp;W" ID="ID_1288771989" CREATED="1561473814630" MODIFIED="1587728924016">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Pixel Deflextion" ID="ID_1138682487" CREATED="1561359361236" MODIFIED="1587728924016">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Pixel Deflection: locally corrupts the image by redistributing pixel values" ID="ID_57329609" CREATED="1561518620599" MODIFIED="1587728924017">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Wavelet-based denoising operation softens this corruption, as well as some of the adversarial changes" ID="ID_359554674" CREATED="1561518645711" MODIFIED="1587728924024">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Not effective to PGD (1804.03286)" ID="ID_1397094008" CREATED="1561359390227" MODIFIED="1587728924025">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Encoding" ID="ID_252143410" CREATED="1561470030024" MODIFIED="1587728924025">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1608.00853: A study of the effect of JPG" ID="ID_714007561" CREATED="1561470059057" MODIFIED="1587728924026">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="only effective to small epsilon" ID="ID_1096932904" CREATED="1561470078362" MODIFIED="1587728924026">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="this paper failed to give theoretical analysis" ID="ID_563941046" CREATED="1561470093616" MODIFIED="1587728924027">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="this paper is poor" ID="ID_1842056801" CREATED="1561470116272" MODIFIED="1587728924028">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="2003.07573: heat and blur: an effective and fast defense against adversarial examples" ID="ID_1104300687" CREATED="1585104935585" MODIFIED="1587728924028">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: defense based on input-modification" ID="ID_535784254" CREATED="1585104959664" MODIFIED="1587728924028">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="why: special architecture or training procedures are irrelevant to already trained networks" ID="ID_1808055465" CREATED="1585104975340" MODIFIED="1587728924029">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: combines feature visualization with input modification" ID="ID_1925815284" CREATED="1585105000339" MODIFIED="1587728924030">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Strength" ID="ID_1361361236" CREATED="1585105020883" MODIFIED="1587728924031">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="applicable to various pre-trained networks" ID="ID_348517997" CREATED="1585105039064" MODIFIED="1587728924031">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Speculation" ID="ID_889151810" CREATED="1585105058395" MODIFIED="1587728924031">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Madry&apos;s defense tutorial" ID="ID_1569970441" CREATED="1585105074840" MODIFIED="1587728924031">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="2004.13799: minority reports defense: defending against adversarial patches" ID="ID_1921363170" CREATED="1588216954282" MODIFIED="1588216972308">
<node TEXT="against adversarial patches" ID="ID_1078161610" CREATED="1588216980727" MODIFIED="1588216994425"/>
</node>
<node TEXT="2101: Error Diffusion Halftoning Against Adversarial Examples" ID="ID_10920568" CREATED="1612163569396" MODIFIED="1612163587115"/>
</node>
<node TEXT="Randomization" FOLDED="true" ID="ID_1058753721" CREATED="1561355819848" MODIFIED="1587728924032">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1705.07213: MTDeep: Boosting the Security" ID="ID_1515219519" CREATED="1561471412454" MODIFIED="1587728924032">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="To classify an input image, the MTDeep system uses a network selected randomly from an ensemble of networks" ID="ID_1954924104" CREATED="1561471416951" MODIFIED="1587728924034">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1711.01991: Mitigating adversarial effects" ID="ID_1861402099" CREATED="1561473946281" MODIFIED="1587728924034">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="random resizing and random padding to the input images" ID="ID_1426873031" CREATED="1561473956874" MODIFIED="1587728924035">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1712.00673: Towards Robust Neural Net via random self-ensemble (ECCV)" ID="ID_212634840" CREATED="1561474230588" MODIFIED="1587728924036">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Random self-ensemble" ID="ID_1270865781" CREATED="1561474242028" MODIFIED="1587728924037">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="add random noise layers to the neural network to prevent the strong gradient based attacks" ID="ID_1532024945" CREATED="1561474248658" MODIFIED="1587728924038">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="ensembles the prediction over random noises to stablize the performance" ID="ID_1440263815" CREATED="1561474278288" MODIFIED="1587728924039">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1810.01279: ADV-BNN: IMPROVED" ID="ID_1879573067" CREATED="1561526652949" MODIFIED="1587728924039">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="fusing randomness can improve the robustness of neural networks" ID="ID_1612375529" CREATED="1561526670875" MODIFIED="1587728924040">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="adding noise blindly to all the layers is not the optimal way to incorporate randomness" ID="ID_1639423576" CREATED="1561526685475" MODIFIED="1587728924041">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="model randomness under the framework of Bayesian neural network to formally learn the posterior distribution of models in a scalable way" ID="ID_873502683" CREATED="1561526703835" MODIFIED="1587728924042">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1902.06415: AuxBlocks: Defense Adversarial" ID="ID_1398384857" CREATED="1561554233300" MODIFIED="1587728924043">
<icon BUILTIN="closed"/>
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2004.14798: RAIN: robust and accurate classification networks with randomization and enhancement" ID="ID_1272938051" CREATED="1588299333667" MODIFIED="1588299352156"/>
</node>
<node TEXT="Robust Optimization" FOLDED="true" ID="ID_1726175904" CREATED="1559914057746" MODIFIED="1587728924043">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1710.10571: Certifying Some Distributional" ID="ID_1048349625" CREATED="1561473679537" MODIFIED="1587728924044">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="principled lens of distributionally robust optimization, which guarantees performance under adversarial input perturbations" ID="ID_774692838" CREATED="1561473682577" MODIFIED="1587728924044">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1711.00851: Provable Defenses against Adver" ID="ID_160869016" CREATED="1561473868499" MODIFIED="1587728924045">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="consider a convex outer approximation of the set of activations reachable through a norm-bounded perturbation" ID="ID_1460800898" CREATED="1561473876057" MODIFIED="1587728924046">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="robust optimzation procedure that minimizes the worst case loss other this outer region" ID="ID_199429529" CREATED="1561473911394" MODIFIED="1587728924047">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Model itself: Network Arch &amp; Loss Function &amp; Regularization" FOLDED="true" ID="ID_1719354455" CREATED="1561519011955" MODIFIED="1589849801488">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Saturated Network" ID="ID_615775135" CREATED="1561470606405" MODIFIED="1587728924048">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1703.09202: Biologically inspired protection" ID="ID_193769390" CREATED="1561470623565" MODIFIED="1587728924054">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="ensure that each element of the Jacobian of the model is saturated in terms of gradient" ID="ID_924188178" CREATED="1561470629237" MODIFIED="1587728924055">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="2018-ICLR: THERMOMETER ENCODING:" ID="ID_287022710" CREATED="1561474807308" MODIFIED="1587728924056">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="simple modification to standard neural network architectures, therometer encoding" ID="ID_1195825162" CREATED="1561474820964" MODIFIED="1587728924059">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1802.05666: Adversarial Risk and the Dan" ID="ID_1019966267" CREATED="1561519049944" MODIFIED="1587728924060">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial risk as an objective for achieving models robust to worse-case inputs" ID="ID_721236759" CREATED="1561519015544" MODIFIED="1587728924063">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1803.02988: Rethinking Feature Distribution" ID="ID_1456462757" CREATED="1561519717345" MODIFIED="1587728924064">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="large-margin Gaussian Mixture loss for deep neural networks in classification tasks. Classification margin and a likelihood regularization" ID="ID_263834131" CREATED="1561519724980" MODIFIED="1587728924066">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1803.06373: Adversarial Logit Pairing" ID="ID_91393218" CREATED="1561525451888" MODIFIED="1587728924067">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="encourages logits for pairs of examples to be similar" ID="ID_871516895" CREATED="1561525465081" MODIFIED="1587728924069">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="similar to defensive distillation" ID="ID_1999447279" CREATED="1561525487280" MODIFIED="1587728924070">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="close to defensive distillation when using high temperature" ID="ID_926560643" CREATED="1561525501441" MODIFIED="1587728924072">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1805.09190: TOWARDS THE FIRST AD" ID="ID_644002693" CREATED="1561526018039" MODIFIED="1587728924072">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="novel classification model that performs analysis by synthesis using learned class-conditional data distributions" ID="ID_1435285716" CREATED="1561526025566" MODIFIED="1587728924075">
<icon BUILTIN="help"/>
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1812.03411: Feature Denoising for Improving (cvpr19)" ID="ID_1651170309" CREATED="1561552437266" MODIFIED="1624370925801">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="develop new network architectures that increase adversarial robustness by performing feature denoising" ID="ID_1894697322" CREATED="1561552461320" MODIFIED="1587728924077">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1806.01768: Evidential Deep Learning to Quantify Classification Uncertainty" ID="ID_1810324786" CREATED="1561555499806" MODIFIED="1587728924078">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="We interpret softmax, the standard output of a classification network, as the parameter set of a categorical distribution." ID="ID_585426285" CREATED="1561555527062" MODIFIED="1587728924080">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1812.00037: Adversarial Defense by Stratified" ID="ID_1061340726" CREATED="1561541901210" MODIFIED="1587728924081">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="guided complement entropy (GCE)" ID="ID_1300058511" CREATED="1573478779546" MODIFIED="1587728924082">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Improving Adversarial Robustness via Guided Complement Entropy" ID="ID_630231682" CREATED="1573478826936" MODIFIED="1587728924082">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="In   this   paper,   we   propose   a   new   trainingparadigm calledGuidedComplementEntropy (GCE) thatis capable of achieving &#x201c;adversarial defense for free,&#x201d; whichinvolves  no  additional  procedures  in  the  process  of  im-proving adversarial robustness" ID="ID_1816415488" CREATED="1573478855965" MODIFIED="1587728924083">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="2003.03879: An empirical evaluation on robustness and uncertainty of regularization methods (ICML19)" ID="ID_362728857" CREATED="1589849810286" MODIFIED="1589849857757">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="idea"/>
</node>
<node TEXT="Neural ODE" ID="ID_1464668383" CREATED="1607504082910" MODIFIED="1607504086157">
<node TEXT="2012.02452: towards natureal robustness against adversarial examples" ID="ID_652408377" CREATED="1607504087232" MODIFIED="1607504107192">
<node TEXT="natural robustness of neural ODEs is better than adversarially trained methods." ID="ID_948249854" CREATED="1607504200971" MODIFIED="1607504224917"/>
</node>
</node>
</node>
<node TEXT="Manifold Assumption" FOLDED="true" ID="ID_799557779" CREATED="1559914055922" MODIFIED="1587728924084">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1804.02485: Fortified Networks: Improving" ID="ID_1117253970" CREATED="1561525707606" MODIFIED="1587728924085">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="fortities the hidden layers in a deep network by identifying when the hideen states are off of the data manifold, and maps these hidden states back to parts of the data manifold" ID="ID_221791580" CREATED="1561525710291" MODIFIED="1587728924086">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1812.00740: Disentangling Adversarial" ID="ID_1622336893" CREATED="1561542080364" MODIFIED="1587728924087">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Clarify the relationship between robustness and generalization, we assume an underlying, low-dimensional data manifold and ..." ID="ID_314141922" CREATED="1561542146101" MODIFIED="1587728924088">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1903.01015: A Kernelized Manifold Mapping" ID="ID_1890412199" CREATED="1561554381565" MODIFIED="1587728924088">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="non-linear radial basis convolutional feature maping by learning a Mahalanobis-like distance function" ID="ID_823263914" CREATED="1561554416565" MODIFIED="1587728924089">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="maps the convolutional features onto a linearly well-seperated manifold, which prevents small adversarial perturbations from forcing a sample to cross the decision boundary." ID="ID_278231742" CREATED="1561554453221" MODIFIED="1587728924090">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1903.01612: Defense Against Adversarial" ID="ID_1516218614" CREATED="1561554508254" MODIFIED="1587728924090">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="project the image back onto the image manifold" ID="ID_1530614646" CREATED="1561554511255" MODIFIED="1587728924091">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="approximate the projection onto the unkown image manifold by a nearest-neighbor search against a web-scale image database" ID="ID_118422729" CREATED="1561554539742" MODIFIED="1587728924092">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Certificate" FOLDED="true" ID="ID_1297922402" CREATED="1561518758899" MODIFIED="1587728924092">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1801.09344: CERTIFIED DEFENSES" ID="ID_138057642" CREATED="1561518766879" MODIFIED="1587728924092">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="method based on a semidefinite relaxation that outputs a certificate that for a given network and test input, no attack can force the error to exceed a certain value" ID="ID_1468243319" CREATED="1561518823312" MODIFIED="1587728924093">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="network verification" ID="ID_1509136560" CREATED="1570535688699" MODIFIED="1587728924093">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="verifying network properties is a promising solution to defend adversarial examples, because it may detect the new unseen attacks. Network Verification cheks the properties of a neural network: whether an input violates or satisfies the property." ID="ID_719492864" CREATED="1570535698946" MODIFIED="1587728924094">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="Numerical Precision" FOLDED="true" ID="ID_1367860673" CREATED="1587872302420" MODIFIED="1607504071703">
<node TEXT="2004.10162: EMPIR: ensembles of mixed precision deep networks for increased robustness against adversarial attacks (ICLR20)" ID="ID_309641165" CREATED="1587872305205" MODIFIED="1587872874712">
<icon BUILTIN="button_ok"/>
<node TEXT="defense too weak" ID="ID_38711650" CREATED="1587872943040" MODIFIED="1587872947338"/>
<node TEXT="precision method good against and FGSM, but not against BIM and PGD" ID="ID_384195614" CREATED="1587873033574" MODIFIED="1587873062673"/>
</node>
</node>
<node TEXT="Physical Device" FOLDED="true" ID="ID_1419373201" CREATED="1610426791196" MODIFIED="1610426795820">
<node TEXT="Optical co-processor" ID="ID_947069955" CREATED="1610426796827" MODIFIED="1610426800943">
<node TEXT="2101: Adversarial Robustness by Design through Analog Computing and Synthetic Gradients" ID="ID_1645275919" CREATED="1610426801742" MODIFIED="1610426811745"/>
</node>
</node>
<node TEXT="Specific Defense" FOLDED="true" ID="ID_970876899" CREATED="1590745845804" MODIFIED="1590745849690">
<node TEXT="patch attack" ID="ID_1982614400" CREATED="1590745850956" MODIFIED="1590745856254">
<node TEXT="2002.10733: (De)Randomized Smoothing for Certifiable Defense against Patch Attacks" ID="ID_844797603" CREATED="1590802678405" MODIFIED="1590802689252"/>
<node TEXT="2005.10884: PatchGuard: Provable Defense against Adversarial Patches Using Masks on Small Receptive Fields" ID="ID_1492755755" CREATED="1590745856851" MODIFIED="1590745872023"/>
</node>
</node>
<node TEXT="Detection" ID="ID_1297352698" CREATED="1630681676660" MODIFIED="1630681679091">
<node TEXT="ML-LOO: Detecting Adversarial Examples with Feature Attribution" ID="ID_171697470" CREATED="1630681680834" MODIFIED="1630681682939"/>
</node>
</node>
<node TEXT="Backdoor Attack, Or data poisoning" FOLDED="true" ID="ID_1177327652" CREATED="1576811431624" MODIFIED="1588230300151">
<edge COLOR="#7c7c00"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Attacking" ID="ID_554312414" CREATED="1589792894072" MODIFIED="1589792897173">
<node TEXT="1912.02771 (Madry) Label-consistent backdoor attacks" ID="ID_1550977405" CREATED="1576811606354" MODIFIED="1587728923793">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: inject modified samples into the training set, and the output of the trained network can be controlled by the backdoor trigger. On ordinary examples, the network behaves normally" ID="ID_1461495426" CREATED="1576811623122" MODIFIED="1587728923794">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: add easy backdoor pattern on the hard-to-classify examples. So the network will tend to make decision based on the backdoor triggers." ID="ID_1829316544" CREATED="1576811764402" MODIFIED="1587728923795">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="conclusion" ID="ID_1138259180" CREATED="1576811807387" MODIFIED="1587728923796">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1. more polluted training data lead to better result" ID="ID_207658334" CREATED="1576812404680" MODIFIED="1587728923796">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="insight" ID="ID_1862049419" CREATED="1576811815306" MODIFIED="1587728923797">
<icon BUILTIN="clanbomber"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1. how to defend such backdoor attack?" ID="ID_1903696856" CREATED="1576811822826" MODIFIED="1587728923797">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2. how to detect the existence of backdoor in a network?" ID="ID_526376182" CREATED="1576811831914" MODIFIED="1587728923798">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="deep ranking + backdoor attack: novel, but not promising enough" ID="ID_1798572584" CREATED="1576812433472" MODIFIED="1587728923799">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="1912.06895: Deep poisoning functions - towards robust privacy-safe image data sharing" ID="ID_1840192395" CREATED="1576830973644" MODIFIED="1587728923799">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: new framework for privacy-preserving data sharing (preventing data reconstruction)" ID="ID_833321901" CREATED="1576831000219" MODIFIED="1587728923800">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="robust to adv attack" ID="ID_131271819" CREATED="1576831044362" MODIFIED="1587728923801">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="over comes issues in previous methods" ID="ID_790273807" CREATED="1576831051402" MODIFIED="1587728923801">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="how: deep poisoning function" ID="ID_180223300" CREATED="1576831038850" MODIFIED="1587728923802">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="deliberately poison image data to prevent known adversarial attacks" ID="ID_1812080336" CREATED="1576831090091" MODIFIED="1587728923802">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="altered image data is functionally equivalent to original data" ID="ID_1178013226" CREATED="1576831124516" MODIFIED="1587728923803">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="2003.08633: backdooring and poisoning neural networks with image-scaling attacks" ID="ID_1316373270" CREATED="1588390585396" MODIFIED="1588390603589">
<node TEXT="hiding the artifacts used for backdoor attack" ID="ID_458431003" CREATED="1588390720676" MODIFIED="1588390736452"/>
</node>
<node TEXT="ICLR21 rej: DYNAMIC BACKDOOR ATTACKS AGAINST DEEP NEU-RAL NETWORKS" ID="ID_408536869" CREATED="1624454568802" MODIFIED="1625124838740"/>
<node TEXT="Disrupting Model Training with Adversarial Shortcuts" ID="ID_444963311" CREATED="1625124823846" MODIFIED="1625124863264">
<icon BUILTIN="button_cancel"/>
<node TEXT="arxiv" ID="ID_1763120599" CREATED="1625124848072" MODIFIED="1625124849061"/>
</node>
<node TEXT="Clean-label Backdoor Attack against Deep Hashing based Retrieval" ID="ID_198422640" CREATED="1634133057176" MODIFIED="1634133062887"/>
</node>
<node TEXT="Analysis &amp; Defense" ID="ID_610509930" CREATED="1590804820564" MODIFIED="1590804825720">
<node TEXT="2004.04692 Rethinking the Trigger of Backdoor Attack" ID="ID_1591744662" CREATED="1590804826432" MODIFIED="1590804836806"/>
</node>
<node TEXT="Detection" ID="ID_843929202" CREATED="1589792897456" MODIFIED="1589792901007">
<node TEXT="2005.06107: adversarial examples are useful too" ID="ID_243186329" CREATED="1589792909426" MODIFIED="1589792922589">
<node TEXT="telling whether a model is susceptible to backdoor attack" ID="ID_1350798644" CREATED="1589792949219" MODIFIED="1589792959867"/>
</node>
<node TEXT="Universal Litmus Patterns:Revealing Backdoor Attacks in CNNs (cvpr20)" ID="ID_512198742" CREATED="1592547154309" MODIFIED="1592547158290"/>
</node>
</node>
<node TEXT="Intersection and Applications" FOLDED="true" ID="ID_1007239063" CREATED="1558062484887" MODIFIED="1587728924094">
<edge COLOR="#007c00"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Auto Speech Recog (ASR)" ID="ID_1843540161" CREATED="1558073927026" MODIFIED="1587728924095">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1608: Hidden Voice Commands" ID="ID_80497338" CREATED="1561554749122" MODIFIED="1587728924097">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="attacks classical algorithms instead of neural networks" ID="ID_757884863" CREATED="1561554774105" MODIFIED="1587728924098">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1801.01944: Audio Adversarial Examples" ID="ID_701598480" CREATED="1561555368437" MODIFIED="1587728924098">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1808.05665: Adversarial Attacks Against Au" ID="ID_633367622" CREATED="1561556157418" MODIFIED="1587728924099">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1904: Imperceptible, Robust, and Tar" ID="ID_786128734" CREATED="1561556701934" MODIFIED="1587728924100">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Face Recognition" ID="ID_600631958" CREATED="1558062492783" MODIFIED="1587728924103">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1904.09290: FeatherNets: Convolutional Neu" ID="ID_1756636272" CREATED="1561554643754" MODIFIED="1587728924104">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="face anti-spoofing" ID="ID_773809109" CREATED="1561554650943" MODIFIED="1587728924106">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="improve the performance and reduce the complexity compared to the previous works" ID="ID_742451238" CREATED="1561554671119" MODIFIED="1587728924107">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="not talking about adversarial example" ID="ID_1464641931" CREATED="1561555913384" MODIFIED="1587728924107">
<icon BUILTIN="messagebox_warning"/>
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2018-WOOT: Fishy Faces: Crafting Adversar" ID="ID_207566206" CREATED="1561555330150" MODIFIED="1587728924108">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1904.04433: Efficient Decision-based Black" ID="ID_835042816" CREATED="1561556678870" MODIFIED="1587728924109">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="good, cvpr19" ID="ID_598731977" CREATED="1571893511235" MODIFIED="1587728924110">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1611.06179. LOTS about attacking deep features" ID="ID_1654877191" CREATED="1581132349988" MODIFIED="1587728924110">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: attacking face recognition" ID="ID_1792895408" CREATED="1581132670025" MODIFIED="1587728924111">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: feature adversary (see attack methods)" ID="ID_1650616880" CREATED="1581132678542" MODIFIED="1587728924112">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2002.02942: On the Robustness of Face Recognition Algorithms Against Attacks and Bias" ID="ID_1059859054" CREATED="1581828866912" MODIFIED="1587728924112">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: overview" ID="ID_954407663" CREATED="1581828875065" MODIFIED="1587728924113">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1908.08705 ADVHAT: REAL-WORLD ADVERSARIAL ATTACK ON ARCFACE FACE ID SYSTEM" ID="ID_1700401211" CREATED="1592537193710" MODIFIED="1592537210407">
<node TEXT="attack arcface (face ID)" ID="ID_1291238670" CREATED="1592537220600" MODIFIED="1592537226904"/>
</node>
</node>
<node TEXT="Object Detection" ID="ID_8351296" CREATED="1561555142022" MODIFIED="1587728924121">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1707.03501: NO Need to Worry about Ad" ID="ID_947096181" CREATED="1561555152819" MODIFIED="1587728924122">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="autonomous vehicles" ID="ID_870993364" CREATED="1561555170059" MODIFIED="1587728924123">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="physical adversarial examples do not distrupt object detection from a moving platform" ID="ID_1839352554" CREATED="1561555177644" MODIFIED="1587728924124">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1804.05810: ShapeShifter: Robust Physical" ID="ID_553479331" CREATED="1561555397695" MODIFIED="1587728924125">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Punches face of 1707.03501 authors." ID="ID_904262909" CREATED="1561555418174" MODIFIED="1587728924125">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1907.10310: Towards Adversarially Robust Object Detection (iccv19)" ID="ID_864251519" CREATED="1573481716794" MODIFIED="1625124415366">
<icon BUILTIN="button_ok"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="multi-task learning" ID="ID_552602406" CREATED="1588228764134" MODIFIED="1588228803688"/>
<node TEXT="adversarial training" ID="ID_906705180" CREATED="1588228803931" MODIFIED="1588228806861"/>
</node>
<node TEXT="2003.04367: category-wise attack: transferable adversarial examples for anchor free object detection" ID="ID_592670388" CREATED="1589849194530" MODIFIED="1589849225754"/>
<node TEXT="2004.04320 TOG: Targeted Adversarial Objectness Gradient Attacks on Real-time Object Detection Systems" ID="ID_1994944473" CREATED="1590805367333" MODIFIED="1590805376801">
<node TEXT="3 targeted attacks" ID="ID_453417687" CREATED="1590805392430" MODIFIED="1590805395914"/>
</node>
<node TEXT="2012.12528&#xa;The Translucent Patch: A Physical and Universal Attack on Object Detectors" ID="ID_782434187" CREATED="1608871356616" MODIFIED="1608871367850"/>
<node TEXT="2012: Sparse Adversarial Attack to Object Detection" ID="ID_793773663" CREATED="1610427446152" MODIFIED="1610427453195"/>
<node TEXT="CVPR21: Robust and Accurate Object Detection via Adversarial Learning" ID="ID_589344638" CREATED="1619588819531" MODIFIED="1619588832657">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="semantic segmentation" ID="ID_972289354" CREATED="1588228486478" MODIFIED="1624370768247">
<node TEXT="1711.09856: On the Robustness of Semantic" ID="ID_415259498" CREATED="1561555285099" MODIFIED="1587728924130">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="2003.06555: dynamic divide-and-conquer adversarial training for robust semantic segmentation" ID="ID_965494109" CREATED="1585274194741" MODIFIED="1587728924131">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="adversarial training + semantic segmentation" ID="ID_1268302019" CREATED="1585274245061" MODIFIED="1587728924132">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="dynamic divide-and-conquer adversarial training strategy to enhance the defense effect" ID="ID_1883779616" CREATED="1585274254719" MODIFIED="1587728924133">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2004.11072: improved noise and attack robustness for semantic segmentation by using multi-task training with self-supervised depth estimation" ID="ID_845593736" CREATED="1587730818428" MODIFIED="1587730887253"/>
</node>
<node TEXT="GAN / SR" ID="ID_1932454205" CREATED="1558074500348" MODIFIED="1624370787524">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1702.06832: ADVERSARIAL EXAMPLES" ID="ID_1290839932" CREATED="1561554966042" MODIFIED="1587728924119">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="attacking generative models" ID="ID_1395462943" CREATED="1561555060188" MODIFIED="1587728924120">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="previous works focus on classification" ID="ID_607966372" CREATED="1561554991827" MODIFIED="1587728924120">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2018-MM: When Deep Fool Meets Deep Prior" ID="ID_138659875" CREATED="1561555310964" MODIFIED="1587728924135">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1904.06097: Evaluating Robustness of Deep Image Super-ResolutionAgainst Adversarial Attacks (iccv19)" ID="ID_194104385" CREATED="1573482768250" MODIFIED="1587728924135">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Generalized Real-World Super-Resolution through Adversarial Robustness" ID="ID_437908117" CREATED="1630531977537" MODIFIED="1630531984160">
<node TEXT="adversarial training for better generalization" ID="ID_1815622001" CREATED="1630531985309" MODIFIED="1630531995037"/>
</node>
</node>
<node TEXT="Network/Graph Embedding" ID="ID_1296065792" CREATED="1561556369384" MODIFIED="1587728924141">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1809.01093: Adversarial Attacks on Node Em" ID="ID_1547628164" CREATED="1561556382398" MODIFIED="1587728924142">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1809.02797: Fast Gradient Attack on Network" ID="ID_1450424548" CREATED="1561556390381" MODIFIED="1587728924143">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="1901.01250: Learning Graph Embedding with" ID="ID_385516511" CREATED="1561556627239" MODIFIED="1587728924144">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="network compression" ID="ID_1518801491" CREATED="1573483453372" MODIFIED="1587728924151">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1903.12561: Adversarial Robustness vs. Model Compression, or Both?" ID="ID_1348498102" CREATED="1573483459783" MODIFIED="1587728924152">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="Network Architecture Search (NAS)" FOLDED="true" ID="ID_983748333" CREATED="1576809368742" MODIFIED="1587728924152">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1911.10695 when NAS meets robustness: in search of robust architectures against adversarial attacks (RobNets)" ID="ID_1911498835" CREATED="1576809516639" MODIFIED="1587728924153">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: architectural perspective, pattern of network architecture, resilient to adversarial attacks" ID="ID_1650609248" CREATED="1576809545752" MODIFIED="1587728924154">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="findings" ID="ID_1990849383" CREATED="1576809658824" MODIFIED="1587728924154">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="1. densely connected patterns result in improved robustness" ID="ID_1946478431" CREATED="1576809671681" MODIFIED="1587728924154">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="why?" ID="ID_1351520060" CREATED="1576809987428" MODIFIED="1587728924155">
<icon BUILTIN="clanbomber"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Ian:FGSM linearity theory" ID="ID_1851636640" CREATED="1576810025701" MODIFIED="1587728924156">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="(my theory) dense net connection means low momentum" ID="ID_172465612" CREATED="1576810156686" MODIFIED="1587728924156">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
<node TEXT="2. adding conv operations to direct connection edge is effective" ID="ID_1788857390" CREATED="1576809685360" MODIFIED="1587728924157">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="3. flow of solution procedure matrix indicates network robustness" ID="ID_1891321656" CREATED="1576809704744" MODIFIED="1587728924157">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="conclusion" ID="ID_541392877" CREATED="1576811071769" MODIFIED="1587728924158">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="architecture matters, but it doesn&apos;t look like the major factor" ID="ID_1228547698" CREATED="1576811078925" MODIFIED="1587728924158">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
</node>
<node TEXT="Optical Character Recognition (OCR)" FOLDED="true" ID="ID_125634099" CREATED="1581823295889" MODIFIED="1587728924159">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="2002.03095: attacking OCR systems with adversarial watermarks" ID="ID_1106657556" CREATED="1581823312362" MODIFIED="1587728924159">
<icon BUILTIN="idea"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: attack ocr systems" ID="ID_424111305" CREATED="1581823345127" MODIFIED="1587728924160">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="why: ordinary adversarial perturbations result in weird background" ID="ID_102688029" CREATED="1581823356307" MODIFIED="1587728924161">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: introduce adversarial watermark, within which the adversarial perturbation is hidden." ID="ID_1394252012" CREATED="1581823387861" MODIFIED="1587728924162">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="bad related works section" ID="ID_1768755619" CREATED="1581824225677" MODIFIED="1587728924163">
<icon BUILTIN="button_cancel"/>
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2101:&#xa;Robust Text CAPTCHAs Using Adversarial Examples" ID="ID_1589671838" CREATED="1610426519463" MODIFIED="1610426527474"/>
</node>
<node TEXT="High Frequency Trading" FOLDED="true" ID="ID_834918931" CREATED="1582943974356" MODIFIED="1587728924163">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="2002.09565: adversarial attacks on machine learing systems for high-frequency trading" ID="ID_1537471551" CREATED="1582943982450" MODIFIED="1587728924164">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: attacking high-frequency trading algorithm" ID="ID_221895827" CREATED="1582944101528" MODIFIED="1587728924164">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="Sequence modeling" ID="ID_912557210" CREATED="1582944216819" MODIFIED="1587728924165">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="how: new attacks specific o this domain. and related discussions" ID="ID_62829514" CREATED="1582944157484" MODIFIED="1587728924165">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="and analyze feasibility of realistic attack" ID="ID_809112659" CREATED="1582944284353" MODIFIED="1587728924166">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
</node>
</node>
<node TEXT="Video/Action Classification" ID="ID_97782697" CREATED="1585104469307" MODIFIED="1612165132052">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="2003.07637: motion-excited sampler: video adversarial attack with sparked prior" ID="ID_842233251" CREATED="1585104475922" MODIFIED="1587728924166">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="what: adversarial attack against video classification" ID="ID_1102149416" CREATED="1585104493022" MODIFIED="1587728924167">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="why: previous work on adversarial attack mainly focus on image models, while the vulnerability of video models is less explored" ID="ID_106678641" CREATED="1585104506783" MODIFIED="1587728924168">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="how: motion-excited sampler to obtain motion-aware noise prior" ID="ID_511768735" CREATED="1585104549511" MODIFIED="1587728924169">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2005.07151: towards understanding the adversarial vulnerability of skeleton-based action recognition" ID="ID_396371208" CREATED="1589767440273" MODIFIED="1589767459011"/>
<node TEXT="Understanding the Robustness of Skeleton-based Action Recognition under Adversarial Attack (cvpr21)" ID="ID_165928686" CREATED="1615350174679" MODIFIED="1615350187183"/>
</node>
<node TEXT="Pose" ID="ID_306388309" CREATED="1612165195048" MODIFIED="1612165201576">
<node TEXT="2101.10562 Investigating the significance of adversarial attacks and their relation to interpretability for radar-based human activity recognition systems" ID="ID_1677434751" CREATED="1612165135824" MODIFIED="1612165148757">
<node TEXT="simply study adversarial attack in radar-based CNN and human gesture recognition" ID="ID_455940776" CREATED="1612165150140" MODIFIED="1612165176354"/>
</node>
</node>
<node TEXT="3D / Physical" FOLDED="true" ID="ID_1905989221" CREATED="1559915805616" MODIFIED="1588896777261">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="2016: Accessorize to a Crime: Real and Stealthy Attacks on State-of-the-Art Face Recognition" ID="ID_1082815182" CREATED="1561351496615" MODIFIED="1587728923729">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="printing a pair of eyeglass frames" ID="ID_437301348" CREATED="1561351518553" MODIFIED="1587728923731">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1707.07397: Synthesizing Robust Adversarial Examples" ID="ID_1697169854" CREATED="1559915821025" MODIFIED="1587728923731">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="physical 3D asdversarial object" ID="ID_423473462" CREATED="1561354743862" MODIFIED="1587728923732">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="Valid from various viewpoints." ID="ID_294159648" CREATED="1559917032841" MODIFIED="1587728923733">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1707.08945: Robust Physical-World Attacks" ID="ID_1131827035" CREATED="1561354667389" MODIFIED="1587728923734">
<font NAME="Gentium" SIZE="10"/>
<node TEXT="robust adversarial perturbations under different physical conditions." ID="ID_1616205076" CREATED="1561354683460" MODIFIED="1587728923734">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="achieve high targeted misclassification rate against road sign classifiers." ID="ID_177892107" CREATED="1561354713685" MODIFIED="1587728923735">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="1904.00923: Robustness of 3D Deep Learning" ID="ID_1690150411" CREATED="1561556666277" MODIFIED="1587728924145">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="advPattern: Physical-World Attacks on Deep Person Re-Identification via (1908.09327)" ID="ID_1454125221" CREATED="1573027194415" MODIFIED="1587728923736">
<icon BUILTIN="bookmark"/>
<font NAME="Gentium" SIZE="10"/>
<node TEXT="WHAT: physical world attack against deep re-ID" ID="ID_29411881" CREATED="1573027227631" MODIFIED="1587728923736">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="HOW: new attack algorithm, named advPattern" ID="ID_1749037272" CREATED="1573027296844" MODIFIED="1587728923738">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="generate adv examples on cloth, using different camera angles" ID="ID_1137600919" CREATED="1573028428228" MODIFIED="1587728923739">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="proposed ranking objective, but impossible to solve due to black-box setting" ID="ID_1605585029" CREATED="1573028191634" MODIFIED="1587728923740">
<font NAME="Gentium" SIZE="10"/>
</node>
<node TEXT="instead, use distance objective" ID="ID_401885087" CREATED="1573028229904" MODIFIED="1587728923741">
<font NAME="Gentium" SIZE="10"/>
</node>
</node>
<node TEXT="2004.00543 Physically Realizable Adversarial Examples for LiDAR Object Detection (cvpr20)" ID="ID_957811602" CREATED="1590805960841" MODIFIED="1590805984672">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="2005.09161: adversarial attacks for embodied agents" ID="ID_852854691" CREATED="1590026357071" MODIFIED="1590026372413"/>
<node TEXT="2005.11626: shapeadv: generating shape-aware adversarial 3d point clouds" ID="ID_304457229" CREATED="1590744659684" MODIFIED="1590744683531"/>
</node>
</node>
</node>
</node>
</map>
