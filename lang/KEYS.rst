General Languages
=================

* Important Programming & DSL Languages to me::

  Python3         *****
  Tex             *****
  Markdown         ****
  Rust             ****
  C/C++            ****
  Awk               ***
  Make              ***
  ReST               **
  CMake              **
  Julia              **
  Raku                *
